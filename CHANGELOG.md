VERSION 3.1.x

- Nouvelle version de l'API en Lumen/Laravel 
    - Meilleure performance (~ 2x plus rapide), passage de 140ms à 70ms en moyenne par requête
    - Plus d'outils (ORM, Built-in classes pour gérer les longues tâches de façon asynchrone, etc...)

- Nouveau système d'authentification (OAuth2)
    - Pourquoi -> Architecture de plus en plus standard sur le web (Facebook, Github l'utilisent)
    - Intérêt -> Sécurité bien que l'implémentation actuelle reste fragile mais surtout véritable API pour développeurs (non-documentée encore)

- Nouveau système d'autorisation (lié à l'implémentation d'OAuth2)
    - S'accompagne d'une nouvelle interface d'attribution des rôles pour les membres du BDE
    - Un nouveau bouton est disponible sur la page des membres pour avoir un résumé des attributions de rôles

- Pour le BDE/Clubs : 
    - Module de news 100% COMPLET, plus besoin de passer par le webmail ou facebook. Les publications peuvent être différées et postées via les listes de diffusion ENT de l'ISIMA et les groupes Facebook de promos. Pour cela, il vous suffit de sélectionner les promotions auxquelles vous souhaitez envoyer une news, le système s'occupe du reste.  

    - Possibilité d'ajouter/modifier/supprimer des inscrits à un événement via le Manager d'Events pour les clubs

    - Amélioration de la recherche des articles dans le module d'encaissement, celle-ci ne prend plus en compte les accents (Ex: "cafe" trouvera Café)
    - Optimisations du module d'encaissement
    - Désoptimisation du module d'encaissement pour les listeux 

    - Les articles sont maintenant triés par ordre croissant des articles les plus achetés par l'utilisateur
    - Ajout d'une page de statistiques sur les ventes et l'état des soldes des membres 

- Evénements : 
    - L'interface d'inscription a été revue et améliorée afin d'être plus claire 
    - Vous pouvez maintenant voir qui participe aux événements dans l'onglet dédié à ceux-ci

- Général : 
    - Ajout de notifications PUSH comme sur une application mobile classique, pour recevoir des notifications, autorisez l'application à vous en envoyer lorsque le prompt apparaît. 
    - Ré-ajout du Leaderboard avec les statistiques de l'ancien site et celles récoltées depuis la nouvelle version
    - Améliorations visuelles mineures sur l'ensemble du site 
    - Et bien sûr plein de correction de bugs, et il y en aura encore alors faites-les remonter, merci !