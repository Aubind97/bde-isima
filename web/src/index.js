import React from 'react';
import ReactDOM from 'react-dom';
import App from './components';
import * as serviceWorker from './serviceWorker';
import './styles/index.scss';
import packageJson from '..//package.json';

global.appVersion = packageJson ? packageJson.version : 'inconnue';

const root = document.getElementById('root');

if(root) {
    //Allow synchronous mode
    ReactDOM.render(<App />, root);

    //Allow concurrent mode
    //ReactDOM.createRoot(root).render(<App />);
}

serviceWorker.register();
