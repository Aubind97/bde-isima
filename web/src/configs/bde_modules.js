import Finance from 'mdi-material-ui/Finance';
import Store from 'mdi-material-ui/Store';
import CalendarToday from 'mdi-material-ui/CalendarToday';
import AccountDetails from 'mdi-material-ui/AccountDetails';
import ClipboardText from 'mdi-material-ui/ClipboardText';
import BookOpenVariant from 'mdi-material-ui/BookOpenVariant';
import AccountGroupOutline from 'mdi-material-ui/AccountGroupOutline';
import Earth from 'mdi-material-ui/Earth';

export default {
    "Events": {
        "name": "Events",
        "path": "/hub/dashboard/events",
        "icon": ClipboardText,
        "scopes": [
            'can-create-events',
            'can-update-events',
            'can-validate-events',
            'can-delete-events',
        ],
    },
    "Analytics": {
        "name": "Analytics",
        "path": "/hub/dashboard/analytics",
        "icon": Finance,
        "scopes": [
            'can-read-analytics',
        ],
    },
    "Clubs": {
        "name": "Clubs",
        "path": "/hub/dashboard/clubs",
        "icon": AccountGroupOutline,
        "scopes": [
            'can-create-clubs',
            'can-update-clubs',
            'can-delete-clubs',
        ],
    },
    "Market": {
        "name": "Market",
        "path": "/hub/dashboard/market",
        "icon": Store,
        "scopes": [
            'can-create-articles',
            'can-update-articles',
            'can-delete-articles',
        ],
    },
    "Members": {
        "name": "Membres",
        "path": "/hub/dashboard/members",
        "icon": AccountDetails,
        "scopes": [
            'can-create-users',
            'can-read-users',
            'can-update-users',
            'can-delete-users',
        ],
        "children" : {
            "Permissions": {
                "name": "Permissions",
                "path": "/hub/dashboard/members/:id",
                "scopes": [
                    'can-update-users',
                ],
            },
        }
    },
    "Partners": {
        "name": "Partenaires",
        "path": "/hub/dashboard/partners",
        "icon": Earth,
        "scopes": [
            'can-create-partners',
            'can-update-partners',
            'can-delete-partners',
        ],
    },
    "Planning": {
        "name": "Planning",
        //"path": "/hub/dashboard/planning",
        "path": "https://bde-isima.herokuapp.com/",
        "icon": CalendarToday,
        "scopes": [
            '*'
        ],
    },
    "Promotions": {
        "name": "Promotions",
        "path": "/hub/dashboard/promotions",
        "icon": BookOpenVariant,
        "scopes": [
            'can-create-promotions',
            'can-update-promotions',
            'can-delete-promotions',
        ],
    }
}
