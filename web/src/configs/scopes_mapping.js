export default clubs => ({
    "/hub/dashboard/analytics": [
        'can-read-analytics',
    ],

    "/hub/dashboard/clubs": [
        'can-create-clubs',
        'can-update-clubs',
        'can-delete-clubs',
    ],

    "/hub/dashboard/events": [
        'can-update-events',
        'can-validate-events',
        'can-delete-events',
    ],

    "/hub/dashboard/market": [
        'can-create-articles',
        'can-update-articles',
        'can-delete-articles',
    ],

    "/hub/dashboard/members": [
        'can-create-users',
        'can-read-users',
        'can-update-users',
        'can-delete-users',
    ],

    "/hub/dashboard/members/:id": [
        'can-update-users',
    ],

    "/hub/dashboard/partners": [
        'can-create-partners',
        'can-update-partners',
        'can-delete-partners',
    ],

    "/hub/dashboard/promotions": [
        'can-create-promotions',
        'can-update-promotions',
        'can-delete-promotions',
    ],

    //"/hub/dashboard/planning": [
    "https://bde-isima.herokuapp.com/": [
        '*',
    ],

    ...{
        ...clubs.map(club => ({
            [`/hub/dashboard/${club.name}/edit`]: [
                `can-update-${club.name}`,
            ],

            [`/hub/dashboard/${club.name}/events`]: [
                `can-create-${club.name}-events`,
                `can-update-${club.name}-events`,
                `can-delete-${club.name}-events`
            ],

            [`/hub/dashboard/${club.name}/news`]: [
                `can-create-${club.name}-news`,
                `can-update-${club.name}-news`,
                `can-delete-${club.name}-news`
            ]
        })).reduce((obj, item) => Object.assign(obj, { ...item }), {})
    }
});
