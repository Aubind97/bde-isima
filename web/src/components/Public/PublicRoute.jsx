import React from 'react';
import { Route } from 'react-router-dom';

/**
 * This component is a wrapper for Route
 */
export default ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props => <Component {...props} />}
    />
);