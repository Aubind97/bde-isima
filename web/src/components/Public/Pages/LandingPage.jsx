import React from 'react';
import classnames from 'classnames';
import { withRouter, Link } from 'react-router-dom';
import { Link as ScrollLink } from 'react-scroll'
import { withStyles } from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grow from '@material-ui/core/Grow';
import ButtonBase from '@material-ui/core/ButtonBase';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import ArrowRight from 'mdi-material-ui/ArrowRight';
import ArrowDown from 'mdi-material-ui/ArrowDown';
import { Loads } from 'react-loads';
import TimeAgo from 'react-timeago';
import frenchStrings from 'react-timeago/lib/language-strings/fr';
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';
import { CRUD, DateUtils } from 'utils';

const formatter = buildFormatter(frenchStrings);

const styles = theme => ({
    root: {
        minHeight: '100vh',
        overflowX: 'hidden'
    },
    container: {
        width: 400,
        height: 300,
        alignContent: 'stretch'
    },
    bg: {
        backgroundImage: 'url(/images/illustrations/Background.svg)',
        [theme.breakpoints.down('md')]: {
            backgroundImage: 'url(/images/illustrations/MobileBackground.svg)',
        },
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
    },
    fullWidth: {
        wordBreak: 'break-word',
        margin: theme.spacing(2),
    },
    title: {
        textAlign: 'center',
        marginTop: theme.spacing(8),
        marginLeft: theme.spacing(8),
        [theme.breakpoints.down('md')]: {
            margin: theme.spacing(8, 2, 2, 2)
        },
    },
    header: {
        fontSize: theme.typography.pxToRem(70),
        marginTop: theme.spacing(2)
    },
    more: {
        borderRadius: '80px 0px 0px 80px',
        color: 'inherit !important',
    },
    learnMore: {
        borderRadius: '80px 80px 80px 80px',
        margin: theme.spacing(2)
    },
    actions: {
        marginRight: -theme.spacing(1)
    },
    bodyNews: {
        borderRadius: '0px 80px 0px 80px',
        margin: theme.spacing(2)
    },
    news: {
        width: 400,
        height: 100,
    },
    btnBase: {
        display: 'flex',
        alignItems: 'flex-start',
        width: '100%',
        height: '100%',
        maxHeight: 100,
        flexDirection: 'column',
        borderColor: 'rgb(235, 235, 235)',
        borderStyle: 'solid',
        borderWidth: '0px 1px 1px 1px',
        borderRadius: '0px 0px 0px 80px',
        padding: '16px 16px 16px 50px'
    },
    newsText: {
        textAlign: 'left',
        maxWidth: 400 - 66,
        [theme.breakpoints.down('md')]: {
            maxWidth: 350 - 66,
        },
    },
    arrow: {
        marginLeft: theme.spacing(1),
    },
    skeleton: {
        margin: theme.spacing(1),
    },
});

export default withRouter(withStyles(styles)(class LandingPage extends React.PureComponent {

    getNews = () => CRUD.get({
        url: '/news',
        params: {
            offset: 0,
            limit: 3,
            promotion: '*',
            orderBy: 'published_at',
            orderDirection: 'desc',
            noFuture: true,
            is_published: true,
        }
    });

    render() {
        const { classes, history } = this.props;
        return (
            <Grid className={classnames(classes.root, classes.bg)} container justify="flex-end" component={Paper}>

                <Grow in={true}>
                    <Grid className={classes.title} container item xs={12} justify="center" alignContent="flex-start" direction="column">
                        <Typography className={classes.header} color="textPrimary" variant="h1" paragraph><b>BDE ISIMA</b></Typography>
                        <Typography color="textPrimary" variant="h5" gutterBottom>L'Association des Étudiants Ingénieurs de l'ISIMA</Typography>
                        <Box display="flex" justifyContent="center">
                            <Fab
                                className={classes.learnMore}
                                variant="extended"
                                aria-label="En savoir plus"
                                color="primary"
                                component={ScrollLink}
                                to="school"
                                smooth={true}
                                duration={300}
                            >
                                <Typography variant="caption" color="secondary">En savoir plus</Typography>
                                <ArrowDown className={classes.arrow} color="secondary" />
                            </Fab>
                        </Box>
                    </Grid>
                </Grow>

                <Grow in={true}>
                    <Grid container item xs={12} md={5} justify="flex-end" alignContent="flex-start">
                        <Box display="flex" className={classes.bodyNews} flexDirection="column" component={Card}>
                            <Typography className={classes.fullWidth} variant="h6" color="textPrimary">
                                <b>ACTUALITÉS</b>
                            </Typography>

                            <Loads load={this.getNews}>
                                {({ response, isRejected, isPending, isResolved }) => (
                                    <Grid className={classes.container} container>
                                        {(isPending || isRejected) && [...Array(3).keys()].map(x => (
                                            <Grid key={x} style={{ paddingLeft: 50 }} item xs={12}>
                                                <Skeleton className={classes.skeleton} width="90%" height={24} />
                                                <Skeleton className={classes.skeleton} width="80%" height={12} />
                                                <Skeleton className={classes.skeleton} width="40%" height={8} />
                                            </Grid>
                                        ))}

                                        {(isResolved && response.data.length === 0) && [...Array(3).keys()].map(x => (
                                            <Grid key={x} className={classes.news} item xs={12}>
                                                <ButtonBase className={classes.btnBase} aria-label="Aucune news" />
                                            </Grid>
                                        ))}

                                        {(isResolved && Array.isArray(response.data)) && response.data.slice(0, 3).map(item => (
                                            <Grid key={item.id} className={classes.news} item xs={12}>
                                                <ButtonBase className={classes.btnBase} onClick={() => history.push(`/news/${item.id}`)} aria-label={item.title}>
                                                    <Typography className={classes.newsText} variant="h6" noWrap>
                                                        {item.title}
                                                    </Typography>
                                                    <Typography
                                                        className={classes.newsText}
                                                        variant="body2"
                                                        noWrap
                                                    >
                                                        {item.header}
                                                    </Typography>
                                                    <Typography className={classes.newsText} variant="caption" noWrap>
                                                        <TimeAgo
                                                            date={DateUtils.withOffsetDate(item.published_at)}
                                                            formatter={formatter}
                                                        />
                                                    </Typography>
                                                </ButtonBase>
                                            </Grid>
                                        ))}
                                    </Grid>
                                )}
                            </Loads>

                            <Box className={classes.actions} display="flex" justifyContent="flex-end" component={CardActions}>
                                <Fab className={classes.more} variant="extended" aria-label="Tout voir" color="primary" component={Link} to="/news">
                                    <Typography variant="caption" color="secondary">Tout voir</Typography>
                                    <ArrowRight className={classes.arrow} color="secondary" />
                                </Fab>
                            </Box>

                        </Box>
                    </Grid>
                </Grow>

            </Grid>
        );
    }
}));
