import React from 'react';
import { withStyles } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Carousel from '../Components/Carousel';
import { CRUD } from 'utils';

const styles = theme => ({
    root: {
        minHeight: '100vh',
        overflowX: 'hidden',
        backgroundColor: '#2A2E43',
        padding: theme.spacing(4),
        [theme.breakpoints.down('md')]: {
            padding: theme.spacing(4, 1),
        }
    },
    title: {
        fontSize: theme.typography.pxToRem(30),
    },
    line: {
        lineHeight: '35px',
    },
    illustration: {
        maxWidth: '100%',
        height: 'auto',
        borderRadius: 4
    }
});

export default withStyles(styles)(class Clubs extends React.Component {
    getClubs = () => CRUD.get({ url: '/clubs' });

    render() {
        const { classes } = this.props;
        return (
            <Box className={classes.root} display="flex" flexDirection="column" alignItems="center">
                
                <Container fixed>
                    <Typography className={classes.title} variant="h3" color="secondary" align="right" gutterBottom><b>LES CLUBS DU BDE</b></Typography>

                    <Grid container>
                        <Grid item xs={12} md={6}>
                            <img
                                className={classes.illustration}
                                src="/images/illustrations/Clubs.svg"
                                alt="illustration"
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <Typography
                                className={classes.line}
                                align="justify"
                                variant="subtitle2"
                                color="secondary"
                            >
                                Le BDE se décompose en une multitude de clubs avec chacun une activité propre. Il y en a pour tous les goûts 
                                (activités associatives scientifiques, techniques, technologiques, culturelles ou encore sportives) et c'est souvent Isibouffe
                                qui régale ! <br/>
                                La vie associative est un aspect très important du BDE, quelles que soient tes passions tu trouveras forcément un club qui te plaira !
                            </Typography>
                        </Grid>
                    </Grid>

                </Container>
                
                <Carousel items={() => this.getClubs()} spinColor="secondary" />
            </Box>
            
        );
    }
});
