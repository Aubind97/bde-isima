import React from 'react';
import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Fab from '@material-ui/core/Fab';
import Typography from '@material-ui/core/Typography';
import Link from 'mdi-material-ui/Link';

const styles = theme => ({
    root: {
        minHeight: '100vh',
        overflowX: 'hidden',
        padding: theme.spacing(4),
        [theme.breakpoints.down('md')]: {
            padding: theme.spacing(4, 1),
        }
    },
    title: {
        fontSize: theme.typography.pxToRem(30),
    },
    illustration: {
        maxWidth: '100%',
        height: 'auto',
        borderRadius: 4
    },
    isima: {
        marginTop: theme.spacing(2),
        maxWidth: '50%',
        height: 'auto',
    },
    line: {
        lineHeight: '35px',
    },
    arrow: {
        marginLeft: theme.spacing(1),
    }
});

export default withStyles(styles)(class School extends React.PureComponent {
    render() {
        const { classes } = this.props;
        return (
            <Box className={classes.root}>
                <Container fixed>
                    <Typography
                        className={classes.title}
                        variant="h3"
                        color="textPrimary"
                        gutterBottom
                    >
                        <b>L'ISIMA</b>
                    </Typography>

                    <Grid container>
                        <Grid item xs={12} md={6}>
                            <Typography
                                className={classes.line}
                                align="justify"
                                variant="subtitle2"
                                color="textPrimary"
                            >
                                Le bureau des étudiants à l'ISIMA (BDE), c'est une partie incontournable de la vie à l'école. 
                                D'abord parce qu'on organise pleins de soirées et d'événements au cours de l'année mais surtout
                                parce que le BDE est indispensable pour obtenir son diplôme à l'ISIMA. 
                                <a 
                                    href="https://www.cti-commission.fr/fonds-documentaire/document/7/chapitre/179?a=1" 
                                    target="_blank" 
                                    rel="noopener noreferrer" 
                                >
                                    <strong> La Commission des Titres d'Ingénieurs </strong>
                                </a>
                                oblige chaque école à disposer d'une vie étudiante.<br/>
                                L'équipe du BDE, les membres de clubs et les autres associations présentes à l'école sont donc 
                                responsables de l'accueil et de l'intégration des élèves ainsi du bon déroulement des événements.
                            </Typography>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <img
                                className={classes.illustration}
                                src="/images/illustrations/School.svg"
                                alt="illustration"
                            />
                        </Grid>
                    </Grid>

                    <div style={{ padding: 20 }}>
                        <Grid container spacing={5}>
                            <Grid
                                container
                                item
                                xs={12}
                                justify="center"
                            >
                                <Fab
                                    variant="extended"
                                    aria-label="Aller sur https://www.isima.fr"
                                    color="primary"
                                    href="https://www.isima.fr/"
                                >
                                    <Typography
                                        variant="caption"
                                        color="secondary"
                                    >
                                        www.isima.fr
                                    </Typography>
                                    <Link
                                        className={classes.arrow}
                                        color="secondary"
                                    />
                                </Fab>
                            </Grid>

                            <Grid
                                container
                                item
                                alignItems="center"
                                justify="center"
                            >
                                <img
                                    className={classes.isima}
                                    src="/images/logos/isima.png"
                                    alt="Logo de l'ISIMA"
                                />
                            </Grid>
                        </Grid>
                    </div>
                </Container>
            </Box>
        );
    }
});
