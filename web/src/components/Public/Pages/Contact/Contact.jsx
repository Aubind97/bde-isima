import React from 'react';
import { withSnackbar } from 'notistack';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import ContactForm from './ContactForm';
import { CRUD } from 'utils';

const styles = theme => ({
    root: {
        minHeight: '100vh',
        overflowX: 'hidden',
        backgroundColor: '#2A2E43',
        padding: theme.spacing(4),
        [theme.breakpoints.down('md')]: {
            padding: theme.spacing(4, 0),
        }
    },
    title: {
        fontSize: theme.typography.pxToRem(30),
    },
    marginBottom: {
        marginBottom: theme.spacing(4)
    },
    margin: {
        marginTop: theme.spacing(2)
    },
    padding: {
        padding: theme.spacing(2)
    },
});

export default withSnackbar(withStyles(styles)(class Contact extends React.Component {
    state = {
        submitting: false
    };

    handleInitialize = form => this.form = form;

    handleSubmit = newData => {
        this.setState({ submitting: true });

        CRUD.add({
            url: '/com/contact',
            newData,
            onSucceed: this.resetState,
            onRejection: this.resetState,
            props: this.props
        });
    };

    setProgress = progress => this.setState({ progress });

    resetState = () => this.setState({ submitting: false });

    render() {
        const { classes } = this.props;
        const { submitting } = this.state;
        return (
            <Box className={classes.root}>
                <Container fixed>
                    
                    <Typography 
                        className={classes.title} 
                        variant="h3" 
                        color="secondary" 
                        gutterBottom
                    >
                        <b>CONTACT</b>
                    </Typography>

                    <Typography 
                        className={classes.marginBottom} 
                        align="justify" 
                        variant="subtitle2" 
                        color="secondary" 
                        gutterBottom
                    >
                        Vous souhaitez prendre contact ?
                    </Typography>
                    
                    <ContactForm
                        submitting={submitting}
                        onInitialize={this.handleInitialize}
                        onValidSubmit={this.handleSubmit}
                        {...this.props}
                    />
                </Container>
            </Box>
        );
    };
}));