import React from 'react';
import { dripForm } from 'react-drip-form';
import Button from "@material-ui/core/Button";
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Email from 'mdi-material-ui/Email';
import { Field } from 'utils/forms';

export default dripForm({
    validations: {
        subject: {
            required: true,
            max: 255
        },
        email: {
            required: true,
            email: true,
            max: 255
        },
        message: {
            required: true,
            min: 200
        }
    },
    messages: {
        subject: {
            required: 'Ce champ est requis.',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        email: {
            required: 'Ce champ est requis.',
            email: 'Ce n\'est pas un email valide.',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        message: {
            required: 'Ce champ est requis.',
            min: 'Ce message doit contenir au moins 200 caractères.'
        }
    },
})(({
    handlers,
    meta: { invalid, pristine },
    submitting,
    ...props
}) => (
        <form onSubmit={handlers.onSubmit}>
            <Paper className={props.classes.padding}>
                <Box display="flex" flexDirection="column">

                    <Field
                        type="text"
                        id="subject"
                        name="subject"
                        label="Sujet"
                    />

                    <Field
                        type="email"
                        id="email"
                        name="email"
                        label="Adresse email"
                    />

                    <Field
                        type="text"
                        id="message"
                        name="message"
                        label="Message (min. 200 caractères)"
                        multiline
                        rows={15}
                    />

                    <Button
                        className={props.classes.margin}
                        startIcon={<Email />}
                        type="submit"
                        onClick={handlers.onSubmit}
                        disabled={invalid || pristine || submitting}
                        variant="contained"
                        color="primary"
                        size="large"
                        aria-label="Soumettre"
                    >
                        {submitting ? <CircularProgress size={25} color="secondary" /> : "Soumettre"}
                    </Button>
                </Box>
            </Paper>
        </form>
    )
);