import React from 'react';
import { Loads } from 'react-loads';
import TimeAgo from 'react-timeago';
import queryString from 'query-string';
import frenchStrings from 'react-timeago/lib/language-strings/fr';
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ChevronRight from 'mdi-material-ui/ChevronRight';
import ChevronLeft from 'mdi-material-ui/ChevronLeft';
import Pagination from 'material-ui-flat-pagination';
import { Share, CRUD, Image, DateUtils } from 'utils';
import Skeleton from '@material-ui/lab/Skeleton';

const formatter = buildFormatter(frenchStrings);

const styles = theme => ({
    root: {
        marginTop: 70 + theme.spacing(3),
        marginBottom: theme.spacing(3),
        minHeight: '100vh',
        overflowX: 'hidden'
    },
    mainFeaturedPost: {
        position: 'relative',
        color: theme.palette.common.white,
        marginBottom: theme.spacing(4),
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        height: 345
    },
    mainFeaturedPostContent: {
        position: 'relative',
        padding: theme.spacing(1, 3, 0, 3),
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(6),
            paddingRight: 0,
        },
    },
    overlay: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        backgroundColor: 'rgba(0,0,0,.2)',
    },
    mainGrid: {
        marginTop: theme.spacing(3),
    },
    margin: {
        [theme.breakpoints.up('md')]: {
            margin: theme.spacing(2),
        },
    },
    card: {
        display: 'flex',
    },
    cardDetails: {
        height: 345,
        padding: 20,
        width: '100%'
    },
    cardMedia: {
        width: 160,
    },
    cardArchives: {
        maxWidth: 345,
        margin: theme.spacing(2),
    },
    cardContent: {
        height: 125
    },
    skeleton: {
        margin: theme.spacing(1),
    },
});

export default withRouter(withStyles(styles)(class News extends React.Component {
    state = {
        offset: 0,
        limit: 12,
        count: 0,
        isReady: false
    };
    
    componentDidMount() {
        let { page } = queryString.parse(this.props.location.search);
        page = parseInt(page);
        this.setState({ offset: page ? (page - 1) * this.state.limit : 0, isReady: true })

        CRUD.get({ 
            url: '/news/count',
            params: {
                promotion: '*',
                orderBy: 'published_at',
                orderDirection: 'desc',
                noFuture: true,
                is_published: true,
            }
        }).then(result => this.setState({ count: result.data }));
    };

    getNews = () => CRUD.get({
        url: '/news',
        params: {
            offset: this.state.offset,
            limit: this.state.limit,
            promotion: '*',
            orderBy: 'published_at',
            orderDirection: 'desc',
            noFuture: true,
            is_published: true,
        }
    });

    redirect = id => this.props.history.push(`/news/${id}`);

    changePage = () => this.props.history.push(`/news?page=${this.state.offset/this.state.limit+1}`);

    handleClick = (offset, updateCallback) => this.setState({ offset }, () => {
        updateCallback(); 
        this.changePage();
    });
        
    render() {
        const { classes } = this.props;
        const { limit, offset, count, isReady } = this.state;

        return (
            <Container className={classes.root} fixed>
                { isReady && (
                    <Loads load={this.getNews}>
                        {({ load, response, isRejected, isPending, isResolved }) => (
                            <main>
                                {(isResolved && response.data.length > 0) && (
                                    <Paper
                                        className={classes.mainFeaturedPost}
                                        style={{ backgroundImage: `url(${Image.display('/images/illustrations/DefaultNews.svg')})` }}
                                    >
                                        {
                                            <img
                                                style={{ display: 'none' }}
                                                src={Image.display('/images/illustrations/DefaultNews.svg')}
                                                alt="background"
                                            />
                                        }
                                        <div className={classes.overlay} />
                                        <Grid container>
                                            <Grid item md={6}>
                                                <div className={classes.mainFeaturedPostContent}>
                                                    <Typography component="h1" variant="h4" color="inherit" gutterBottom>
                                                        <span title={response.data[0].title}>{`${response.data[0].title.substring(0, 40)}${response.data[0].title.length > 40 ? '...' : ''}`}</span>
                                                    </Typography>
                                                    <Typography variant="h6" color="inherit" paragraph>
                                                        <span title={response.data[0].header}>{`${response.data[0].header.substring(0, 40)}${response.data[0].header.length > 40 ? '...' : ''}`}</span>
                                                    </Typography>
                                                    <Button
                                                        variant="contained"
                                                        color="primary"
                                                        size="large"
                                                        onClick={() => this.redirect(response.data[0].id)}
                                                        aria-label="Continuer à lire"
                                                    >
                                                        Continuer à lire
                                            </Button>
                                                </div>
                                            </Grid>
                                            <Grid container item md={6} justify="flex-end" alignItems="flex-start">
                                                <Share className={classes.margin} link={`https://bde.isima.fr/news/${response.data[0].id}`} />
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                )}

                                {(isPending || isRejected) && (
                                    <>
                                        <Paper className={classes.mainFeaturedPost}>
                                            <Grid container>
                                                <Grid item md={6}>
                                                    <div className={classes.mainFeaturedPostContent}>
                                                        <Skeleton className={classes.skeleton} width="90%" height={36} />
                                                        <Skeleton className={classes.skeleton} width="50%" height={24} />
                                                        <Skeleton className={classes.skeleton} width="30%" height={50} />
                                                    </div>
                                                </Grid>
                                                <Grid container item md={6} justify="flex-end" alignItems="flex-start">
                                                    <Share className={classes.margin} />
                                                </Grid>
                                            </Grid>
                                        </Paper>

                                        <Grid container spacing={4}>
                                            {[...Array(2).keys()].map(idx => (
                                                <Grid key={idx} item xs={12} md={6}>
                                                    <Card className={classes.card}>
                                                        <div className={classes.cardDetails}>
                                                            <CardContent style={{ height: '85%', padding: 0 }}>
                                                                <Skeleton className={classes.skeleton} width="90%" height={24} />
                                                                <Skeleton className={classes.skeleton} width="80%" height={12} />
                                                                <Skeleton className={classes.skeleton} width="50%" height={12} />
                                                                <Skeleton className={classes.skeleton} width="30%" height={50} />
                                                            </CardContent>
                                                            <Box display="flex" justifyContent="flex-end">
                                                                <Share />
                                                            </Box>
                                                        </div>
                                                    </Card>
                                                </Grid>
                                            ))}
                                        </Grid>
                                    </>
                                )}

                                {isResolved && (
                                    <Grid container spacing={4} className={classes.cardGrid}>
                                        {response.data.slice(1, 3).map(post => (
                                            <Grid item key={post.id} xs={12} md={6}>
                                                <CardActionArea onClick={() => this.redirect(post.id)}>
                                                    <Card className={classes.card}>
                                                        <div className={classes.cardDetails}>
                                                            <CardContent style={{ height: '85%', padding: 0 }}>
                                                                <Typography component="h2" variant="h5" gutterBottom>
                                                                    <span title={post.title}>{`${post.title.substring(0, 40)}${post.title.length > 40 ? '...' : ''}`}</span>
                                                                </Typography>
                                                                <Typography variant="subtitle1" paragraph>
                                                                    <span title={post.header}>{`${post.header.substring(0, 40)}${post.header.length > 40 ? '...' : ''}`}</span>
                                                                </Typography>
                                                                <Typography variant="subtitle2" color="textSecondary" paragraph>
                                                                    <TimeAgo date={DateUtils.withOffsetDate(post.published_at)} formatter={formatter} />
                                                                </Typography>
                                                                <Button
                                                                    variant="contained"
                                                                    color="primary"
                                                                    size="large"
                                                                    to="#"
                                                                    component="div"
                                                                    aria-label="Continuer à lire"
                                                                >
                                                                    Continuer à lire
                                                                </Button>
                                                            </CardContent>
                                                            <Box display="flex" justifyContent="flex-end">
                                                                <Share link={`https://bde.isima.fr/news/${post.id}`} />
                                                            </Box>
                                                        </div>
                                                        <Hidden xsDown>
                                                            <CardMedia
                                                                className={classes.cardMedia}
                                                                image={Image.display('/images/illustrations/DefaultNews.svg')}
                                                                title={post.header}
                                                            />
                                                        </Hidden>
                                                    </Card>
                                                </CardActionArea>
                                            </Grid>
                                        ))}
                                    </Grid>
                                )}

                                <Grid container className={classes.mainGrid}>

                                    <Grid container item xs={12} direction="column">
                                        <Typography variant="h6" gutterBottom>
                                            Archives
                                        </Typography>
                                        <Divider />
                                    </Grid>

                                    <Grid container item xs={12} justify="center">

                                        {(isPending || isRejected) && [...Array(9).keys()].map(idx => (
                                            <Card key={idx} className={classes.cardArchives} component={Grid} item xs={12} md={6}>
                                                <CardActionArea className={classes.cardContent}>
                                                    <CardContent style={{ height: '100%' }}>
                                                        <Skeleton width="90%" height={12} />
                                                        <Skeleton width="80%" height={6} />
                                                        <Skeleton width="50%" height={6} />
                                                    </CardContent>
                                                </CardActionArea>
                                                <CardActions>
                                                    <Grid container item xs={12}>
                                                        <Grid container item xs={6} justify="flex-start">
                                                            <Share />
                                                        </Grid>
                                                        <Grid container item xs={6} justify="flex-end">
                                                            <Skeleton width="60%" height={6} />
                                                        </Grid>
                                                    </Grid>
                                                </CardActions>
                                            </Card>
                                        ))}

                                        {(isResolved && response.data.length === 0) && (
                                            <Box
                                                display="flex"
                                                justifyContent="center"
                                                alignItems="center"
                                                flexDirection="column"
                                            >
                                                <img
                                                    src="/images/illustrations/NoData.svg"
                                                    height="auto"
                                                    width="300"
                                                    alt="Aucune donnée"
                                                />
                                                <Typography
                                                    variant="h6"
                                                    gutterBottom
                                                >
                                                    Aucune
                                                    news publiée !
                                            </Typography>
                                            </Box>
                                        )}

                                        {(isResolved && Array.isArray(response.data)) && response.data.slice(3, response.data.length).map(item => (
                                            <Card key={item.id} className={classes.cardArchives} component={Grid} item xs={12} md={6}>
                                                <CardActionArea className={classes.cardContent} onClick={() => this.redirect(item.id)}>
                                                    <CardContent style={{ height: '100%' }}>
                                                        <Typography gutterBottom variant="h5" noWrap>
                                                            {item.title}
                                                        </Typography>
                                                        <Typography variant="body2" component="p" paragraph>
                                                            {item.header}
                                                        </Typography>
                                                    </CardContent>
                                                </CardActionArea>
                                                <CardActions>
                                                    <Share link={`https://bde.isima.fr/news/${item.id}`} />
                                                    <Typography style={{ width: "100%" }} align="right" variant="caption" component="p" noWrap>
                                                        <TimeAgo date={DateUtils.withOffsetDate(item.published_at)} formatter={formatter} />
                                                    </Typography>
                                                </CardActions>
                                            </Card>
                                        ))}

                                    </Grid>

                                </Grid>

                                <Pagination
                                    style={{ textAlign: 'center', margin: 8 }}
                                    classes={{
                                        rootCurrent: "current-pagination"
                                    }}
                                    limit={limit}
                                    offset={offset}
                                    total={count}
                                    onClick={(e, offset) => this.handleClick(offset, load)}
                                    nextPageLabel={<ChevronRight />}
                                    previousPageLabel={<ChevronLeft />}
                                    currentPageColor="inherit"
                                    otherPageColor="inherit"
                                />

                            </main>
                        )}
                    </Loads>
                )}
            </Container>
        );
    }
}));
