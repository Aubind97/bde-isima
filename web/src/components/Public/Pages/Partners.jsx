import React from 'react';
import { Link } from 'react-scroll';
import { withStyles } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Carousel from '../Components/Carousel';
import { CRUD } from 'utils';

const styles = theme => ({
    root: {
        minHeight: '100vh',
        overflowX: 'hidden',
        padding: theme.spacing(4),
        [theme.breakpoints.down('md')]: {
            padding: theme.spacing(4, 1),
        }
    },
    title: {
        fontSize: theme.typography.pxToRem(30),
        wordBreak: 'break-word'
    },
    line: {
        lineHeight: '35px',
    },
    illustration: {
        maxWidth: '100%',
        height: 'auto',
        borderRadius: 4
    }
});

export default withStyles(styles)(class Partners extends React.PureComponent {
    getPartners = () => CRUD.get({ url: '/partners' });

    render() {
        const { classes } = this.props;
        return (
            <Box className={classes.root} display="flex" flexDirection="column" alignItems="center">

                <Container fixed>
                    <Typography className={classes.title} variant="h3" color="textPrimary" gutterBottom><b>LES PARTENAIRES DU BDE</b></Typography>

                    <Grid container>
                        <Grid item xs={12} md={6}>
                            <Typography
                                className={classes.line}
                                align="justify"
                                variant="subtitle2"
                                color="textPrimary"
                            >
                                Le BDE dispose d'un membre responsable des partenariats, chargé de trouver et négocier des partenariats tout au long de l'année
                                afin de proposer aux élèves toutes sortes d'avantages (réductions, offres promotionnelles, etc ...). <br /><br />
                                Vous êtes intéressés pour entretenir un partenariat avec nous ? <br/>
                                Envoyez-nous un message via le <Button variant="text" component={Link} smooth={true} duration={300} to="contact"><strong>formulaire de contact</strong></Button>. 
                            </Typography>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <img
                                className={classes.illustration}
                                src="/images/illustrations/Partners.svg"
                                alt="illustration"
                            />
                        </Grid>
                    </Grid>

                </Container>

                <Carousel items={() => this.getPartners()} />

            </Box>

        );
    }
});
