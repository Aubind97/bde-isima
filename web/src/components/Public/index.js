import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Element } from 'react-scroll';
import LandingPage from './Pages/LandingPage';
import School from './Pages/School';
import Clubs from './Pages/Clubs';
import Partners from './Pages/Partners';
import News from './Pages/News';
import Contact from './Pages/Contact/Contact';
import Menu from './Components/Menu';
import Footer from './Components/Footer';
import NewsReader from '../Authenticated/Hub/Pages/NewsReader';

/**
 * This component is responsible for routing Public section components
 */
export default () => {
    return (
        <>
            <Menu />

            <Switch>
                <Route path="/news">
                    <Route exact path="/news" render={() => <News />} />
                    <Route exact path="/news/:id/:slug?" render={() => <NewsReader />} />
                </Route>

                <Route exact path="/" render={() => (
                    <>
                        <Element name="landing">
                            <LandingPage />
                        </Element>

                        <Element name="school">
                            <School />
                        </Element>

                        <Element name="clubs">
                            <Clubs />
                        </Element>

                        <Element name="partners">
                            <Partners />
                        </Element>

                        <Element name="contact">
                            <Contact />
                        </Element>
                    </>
                )} />
            </Switch>

            <Footer />
        </>
    )
};
