import React from 'react';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';
import UnfoldMoreHorizontal from 'mdi-material-ui/UnfoldMoreHorizontal';

const useStyles = makeStyles(theme => ({
    root: {
        margin: theme.spacing(0, 2)
    },
    rightIcon: {
        marginLeft: theme.spacing(1)
    },
}));

const languages = [
    'English',
    'Español',
    'Français',
    'العربية',
];

/**
 * This component is responsible for handling language switch
 */
export default function Language() {

    /** States initialisation */
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [selectedIndex, setSelectedIndex] = React.useState(2);

    function handleClickListItem(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleMenuItemClick(event, index) {
        setSelectedIndex(index);
        setAnchorEl(null);
    }

    function handleClose() {
        setAnchorEl(null);
    }

    return (
        <Box className={classes.root} component="span">
            <Typography 
                variant="body2" 
                color="textSecondary" 
                component="span"
                onClick={handleClickListItem}
            >
                Langue <Typography variant="overline" color="textSecondary" component="span">({languages[selectedIndex]})</Typography> 
                <IconButton className={classes.rightIcon} aria-label="Selection de la langue">
                    <UnfoldMoreHorizontal />
                </IconButton>
            </Typography>
            <Menu
                id="language-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {languages.map((option, index) => (
                    <MenuItem
                        key={option}
                        disabled={index !== 2}
                        selected={index === selectedIndex}
                        onClick={event => handleMenuItemClick(event, index)}
                    >
                        {option}
                    </MenuItem>
                ))}
            </Menu>
        </Box>
    );
};
