import React from 'react';
import "react-multi-carousel/lib/styles.css";
import { makeStyles, useTheme } from '@material-ui/core';
import { Loads } from 'react-loads';
import sanitizeHtml from 'sanitize-html';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Close from 'mdi-material-ui/Close';
import IconButton from '@material-ui/core/IconButton';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import CircularProgress from '@material-ui/core/CircularProgress';
import MultiCarousel from 'react-multi-carousel';
import Facebook from 'mdi-material-ui/Facebook';
import Twitter from 'mdi-material-ui/Twitter';
import Instagram from 'mdi-material-ui/Instagram';
import { Image } from 'utils';

const useStyles = makeStyles(theme => ({
    container: {
        width: "100%",
        flexGrow: 1,
        alignItems: 'flex-end !important',
        marginTop: theme.spacing(4)
    },
    card: {
        textAlign: 'center',
        maxWidth: 345,
        height: 345,
        margin: theme.spacing(0, 1)
    },
    content: {
        height: 100,
    },
    media: {
        objectFit: 'contain',
        maxHeight: 140,
        width: 'auto',
        maxWidth: '100% !important',
        padding: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            maxWidth: 345
        }
    },
    actionArea: {
        height: 140
    },
    dialogMedia: {
        objectFit: 'contain',
        height: 140,
        width: 'auto',
        maxWidth: 345,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: theme.spacing(2),
        padding: theme.spacing(2)

    },
    socialIcons: {
        color: theme.palette.text.primary,
        fontSize: theme.typography.pxToRem(20),
        margin: theme.spacing(1)
    },
    closeBtn: {
        margin: theme.spacing(2, 2, 0, 0),
    },
    progress: {
        margin: theme.spacing(2)
    }
}));

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3,
        paritialVisibilityGutter: 60
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        paritialVisibilityGutter: 50
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        paritialVisibilityGutter: 30
    }
};

/**
 * This component is responsible for displaying Card items in a MultiCarousel
 */
export default function Carousel(props) {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [open, setOpen] = React.useState(false);
    const [isMoving, setIsMoving] = React.useState(false);
    const [selected, setSelected] = React.useState(null);

    /**
    * Updater for selected value
    * @param {Object} selected the new selected value
    * @param {Object} open the new open value
    */
    const handleSelectionChange = (selected, open) => event => {
        if (!isMoving) {
            setSelected(selected);
            setOpen(open);
        }
    };

    return (
        <>
            <Loads load={props.items}>
                {({ response, isRejected, isPending, isResolved }) => (
                    <>
                        {(isPending || isRejected) && <CircularProgress className={classes.progress} size={25} color={props.spinColor} />}
                        {isResolved && (
                            <MultiCarousel
                                additionalTransfrom={0}
                                autoPlaySpeed={3000}
                                minimumTouchDrag={80}
                                centerMode
                                containerClass={classes.container}
                                dotListClass=""
                                focusOnSelect={false}
                                infinite
                                itemClass=""
                                keyBoardControl
                                responsive={responsive}
                                showDots={false}
                                sliderClass=""
                                slidesToSlide={fullScreen ? 2 : 3}
                                swipeable
                                beforeChange={() => setIsMoving(true)}
                                afterChange={() => setIsMoving(false)}
                            >
                                {Array.isArray(response.data) && response.data.map(item => (
                                    <Card key={item.id} className={classes.card}>
                                        <CardActionArea onClick={handleSelectionChange(item, true)}>
                                            <Box className={classes.actionArea} display="flex" alignItems="center" justifyContent="center">
                                                {item.logo && (
                                                    <img
                                                        className={classes.media}
                                                        src={Image.display(item.logo)}
                                                        alt={`Logo ${item.name}`}
                                                    />
                                                )}
                                            </Box>
                                            <CardContent className={classes.content}>
                                                <Typography gutterBottom variant="subtitle1" component="h2">
                                                    {item.name.toUpperCase()}
                                                </Typography>
                                            </CardContent>
                                        </CardActionArea>
                                        <CardActions style={{ justifyContent: "center" }}>
                                            {item.facebook && <Link href={item.facebook} target="_blank" rel="noopener noreferrer" aria-label="Lien Facebook BDE ISIMA"><Facebook className={classes.socialIcons} /></Link>}
                                            {item.twitter && <Link href={item.twitter} target="_blank" rel="noopener noreferrer" aria-label="Lien Twitter BDE ISIMA"><Twitter className={classes.socialIcons} /></Link>}
                                            {item.instagram && <Link href={item.instagram} target="_blank" rel="noopener noreferrer" aria-label="Lien Instagram BDE ISIMA"><Instagram className={classes.socialIcons} /></Link>}
                                        </CardActions>
                                    </Card>
                                ))}
                            </MultiCarousel>
                        )}
                    </>
                )}
            </Loads>

            {selected && (
                <Dialog
                    style={{ textAlign: 'center' }}
                    fullScreen={fullScreen}
                    open={open}
                    onClose={handleSelectionChange(null, false)}
                    aria-labelledby="responsive-dialog-title"
                >
                    <div style={{ textAlign: "right" }}>
                        <IconButton className={classes.closeBtn} onClick={handleSelectionChange(null, false)} aria-label="Fermer">
                            <Close />
                        </IconButton>
                    </div>
                    <img
                        className={classes.dialogMedia}
                        src={Image.display(selected.logo)}
                        alt={`Logo ${selected.name}`}
                    />
                    <DialogTitle id="responsive-dialog-title">{selected.name.toUpperCase()}</DialogTitle>
                    <DialogContent>
                        <DialogContentText component="div">
                            <Typography
                                variant="body2"
                                component="div"
                                align="justify"
                                paragraph
                                dangerouslySetInnerHTML={{
                                    __html: sanitizeHtml(selected.description, {
                                        allowedTags: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'p', 'a', 'ul', 'ol',
                                            'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div',
                                            'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'iframe', 'img'],
                                        allowedAttributes: {
                                            'a': ['href', 'target'],
                                            'img': ['src', 'alt', 'style'],
                                            'iframe': ['width', 'height', 'src', 'frameborder', 'allow', 'allowfullscreen', 'style']
                                        },
                                        allowedIframeHostnames: ['www.youtube.com']
                                    })
                                }}
                            />
                        </DialogContentText>
                        <Typography variant="caption" component="h2">
                            {selected.email}
                        </Typography>
                    </DialogContent>
                </Dialog>
            )}
        </>
    );
};
