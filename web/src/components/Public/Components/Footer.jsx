import React from 'react';
import { Link } from 'react-router-dom';
import { Loads } from 'react-loads';
import { makeStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import { Link as MUILink } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Facebook from 'mdi-material-ui/Facebook';
import Twitter from 'mdi-material-ui/Twitter';
import Instagram from 'mdi-material-ui/Instagram';
import { CRUD } from 'utils';
import Language from './Language';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2, 0),
    },
    uca: {
        maxWidth: '70%',
        height: 100,
        objectFit: 'contain',
        margin: theme.spacing(1, 0),
    },
    logo: {
        width: 100,
        height: 100,
    },
    socialIcons: {
        color: theme.palette.text.primary,
        fontSize: theme.typography.pxToRem(30),
        margin: theme.spacing(3),
    },
    link: {
        margin: theme.spacing(0, 2),
    }
}));

/**
 * This component is the Footer of Public section
 */
export default function Footer() {
    
    /** States initialisation */
    const classes = useStyles();
    const currentYear = new Date().getFullYear();

    /**
    * Request the BDE club from the API
    */
    const getClub = () => CRUD.get({ url: '/clubs?name=BDE' });

    return (
        <Loads load={getClub}>
            {({ response, isRejected, isPending, isResolved }) => (
                <Paper>
                    <Grid className={classes.root} container>
                        <Grid
                            container
                            item
                            xs={12}
                            md={4}
                            justify="center"
                            alignContent="center"
                        >
                            <Grid container item xs={12} md={6} justify="center">
                                <Avatar
                                    className={classes.logo}
                                    src="/images/logos/logo.svg"
                                    alt="Logo BDE ISIMA"
                                />
                            </Grid>
                            <Grid container item xs={12} md={6} justify="center" alignItems="center">
                                <Typography variant="caption">
                                    AEI ISIMA
                                    <br />
                                    1 Rue de la Chebarde
                                    <br />
                                    TSA 60125 - CS 60026
                                    <br />
                                    63170 AUBIERE
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            item
                            xs={12}
                            md={4}
                            justify="center"
                            alignContent="center"
                        >
                            <img
                                className={classes.uca}
                                src="/images/logos/uca.png"
                                alt="Logo de l'UCA"
                            />
                        </Grid>
                        <Grid
                            container
                            item
                            xs={12}
                            md={4}
                            justify="center"
                            alignItems="center"
                            direction="column"
                        >
                            {(isPending || isRejected) && (
                                <CircularProgress size={25} />
                            )}

                            {isResolved && (
                                <Grid container item justify="center" alignItems="center">
                                    <MUILink
                                        href={response.data.facebook}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        aria-label="Lien Facebook BDE ISIMA"
                                    >
                                        <Facebook className={classes.socialIcons} />
                                    </MUILink>
                                    <MUILink
                                        href={response.data.twitter}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        aria-label="Lien Twitter BDE ISIMA"
                                    >
                                        <Twitter className={classes.socialIcons} />
                                    </MUILink>
                                    <MUILink
                                        href={response.data.instagram}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        aria-label="Lien Instagram BDE ISIMA"
                                    >
                                        <Instagram className={classes.socialIcons} />
                                    </MUILink>
                                </Grid>
                            )}
                        </Grid>

                        <Grid container item xs={12} direction="column">

                            <Grid container item xs={12} justify="center" alignItems="center" component={Typography} color="textSecondary">
                            <Typography className={classes.link} variant="body2" color="textSecondary" component="span"><b>BDE ISIMA © {currentYear}</b></Typography>
                                -
                                <Link className={classes.link} to="/privacy">
                                    <Typography variant="body2" color="textSecondary" component="span">Politique de confidentialité</Typography>
                                </Link>
                                -
                                <Language /> 
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>
            )}
        </Loads>
    );
};
