import React from 'react';
import classnames from 'classnames';
import queryString from 'query-string';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import { Link, scrollSpy } from 'react-scroll';
import AppBar from '@material-ui/core/AppBar';
import Fab from '@material-ui/core/Fab';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withStyles, withMobileDialog, ListItemSecondaryAction } from '@material-ui/core';
import MenuIcon from 'mdi-material-ui/Menu';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import User from '../../Authenticated/Auth/Components/User';
import Login from '../../Authenticated/Auth/Components/Login';
import Hidden from '@material-ui/core/Hidden';
import Avatar from '@material-ui/core/Avatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import ArrowLeft from 'mdi-material-ui/ArrowLeft';
import AccountArrowRightOutline from 'mdi-material-ui/AccountArrowRightOutline';
import LoginVariant from 'mdi-material-ui/LoginVariant';
import AccountGroupOutline from 'mdi-material-ui/AccountGroupOutline';
import Newspaper from 'mdi-material-ui/Newspaper';
import Earth from 'mdi-material-ui/Earth';
import Fire from 'mdi-material-ui/Fire';
import School from 'mdi-material-ui/School';
import MessageTextOutline from 'mdi-material-ui/MessageTextOutline';
import BookSearch from 'mdi-material-ui/BookSearch';
import OpenInNew from 'mdi-material-ui/OpenInNew';
import Dialog from '@material-ui/core/Dialog';
import isInternalLink from "is-internal-link";

const menu = [
    { icon: <Fire />, text: 'LE BDE', to: 'landing', domain: '/' },
    { icon: <School />, text: 'L\'ÉCOLE', to: 'school', domain: '/'},
    { icon: <AccountGroupOutline />, text: 'CLUBS', to: 'clubs', domain: '/'},
    { icon: <Earth />, text: 'PARTENAIRES', to: 'partners', domain: '/'},
    { icon: <Newspaper />, text: 'NEWS', to: 'news', domain: '/news' },
    { icon: <BookSearch />, text: 'ANNALES', to: 'https://drive.google.com/drive/u/0/folders/1GCfFZ7y_rwpN3wy3UiI-ALXzgxmeoTVA' },
    { icon: <MessageTextOutline />, text: 'CONTACT', to: 'contact', domain: '/'}
];

function HideOnScroll(props) {
    return (
        <Slide appear={false} direction="down" in={!useScrollTrigger()}>
            {props.children}
        </Slide>
    );
}

const styles = theme => ({
    grow: {
        flexGrow: 1,
        height: '100%',
        display: 'flex',
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    fullList: {
        width: '70vw',
        textAlign: 'center'
    },  
    avatar: {
        margin: theme.spacing(0, 2, 0, 0),
    },
    mobileAvatar: {
        margin: theme.spacing(2)
    },
    navLink: {
        height: 64,
        borderRadius: 0,
    },
    link: {
        color: 'inherit !important',
    },
    navLinkActivePrimary: {
        borderBottom: '1.5px solid #2A2E43',
    },
    button: {
        margin: theme.spacing(1, 0, 0, 1),
    },
    margin: {
        margin: theme.spacing(1)
    },
    accountBtn: {
        marginLeft: theme.spacing(2),
        [theme.breakpoints.down('md')]: {
            height: 40,
            width: 40,
        }
    },
    leftIcon: {
        marginRight: theme.spacing(1)
    },
    selected: {
        color: '#fff'
    },
    menuBtn: {
        padding: theme.spacing(0, 2),
        width: '90%',
        borderRadius: '0px 80px 80px 0px',
        color: 'inherit !important',
    },
});

export default withMobileDialog()(withWidth()(withRouter(withStyles(styles)(class Menu extends React.PureComponent {
    state = {
        isMobileMenuOpen: false,
        isLoginDrawerOpen: false,
        activePath: '',
    };

    componentWillMount() {
        let { to } = queryString.parse(this.props.location.search);
        if (to) {
            this.setState({ isLoginDrawerOpen: true });
        }
        scrollSpy.update();
    };

    handleActiveChange = activePath => {
        this.setState({ activePath });
    }

    toggleDrawer = (name, value) => () => {
        this.setState({ [name]: value });
    };

    scrollTo = element => () => {
        if(element.domain)
            this.props.history.push(element.domain)
    };

    handleClickItem = path => () => {
        if (!isInternalLink(path))
            window.open(path, '_blank');
    };

    render() {
        const { classes, history, location, width, fullScreen } = this.props;
        const { isMobileMenuOpen, isLoginDrawerOpen, activePath } = this.state;
        const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
        const isAuth = User.hasActiveSession();
        const isUpLg = isWidthUp('lg', width);

        return (
            <>
                <SwipeableDrawer
                    anchor="left"
                    open={isMobileMenuOpen}
                    onClose={this.toggleDrawer('isMobileMenuOpen', false)}
                    onOpen={this.toggleDrawer('isMobileMenuOpen', true)}
                    disableBackdropTransition={!iOS}
                    disableDiscovery={iOS}
                >
                    <div tabIndex={0} role="button">
                        <div className={classes.fullList}>
                            <List>

                                <Box display="flex" alignItems="center">
                                    <Avatar
                                        alt="Logo BDE ISIMA"
                                        src="/images/logos/logo.svg"
                                        component={RouterLink}
                                        className={classes.mobileAvatar}
                                        to="/"
                                    />

                                    <Typography
                                        variant="h6"
                                        color="textPrimary"
                                    >
                                        BDE ISIMA
                                    </Typography>
                                </Box>

                                {menu.map(obj => (
                                    <ListItem
                                        key={obj.to}
                                        className={classes.link}
                                        disableGutters
                                    >
                                        <Button
                                            className={classes.menuBtn}
                                            component={!isInternalLink(obj.to) ? "div" : Link}
                                            onClick={this.handleClickItem(obj.to)}
                                            onMouseDown={this.scrollTo(obj)}
                                            onMouseUp={this.toggleDrawer('isMobileMenuOpen', false)}
                                            to={obj.to}
                                            aria-label={obj.text}
                                            onSetActive={this.handleActiveChange}
                                            spy={true}
                                            variant={activePath === obj.to || `${window.location.origin}${window.location.pathname}` === `${window.location.origin}/${obj.to}` ? "contained" : "text"}
                                            color="primary"
                                        >
                                            <Box
                                                display="flex"
                                                flexGrow={1}
                                                alignItems="center"
                                                justifyContent="flex-start"
                                            >
                                                <ListItemIcon>
                                                    {React.cloneElement(obj.icon, {
                                                        className: activePath === obj.to || `${window.location.origin}${window.location.pathname}` === `${window.location.origin}/${obj.to}` ? classes.selected : undefined,
                                                        color: 'inherit'
                                                    })}
                                                </ListItemIcon>
                                                <Box m={1} className={activePath === obj.to || `${window.location.origin}${window.location.pathname}` === `${window.location.origin}/${obj.to}` ? classes.selected : undefined}>
                                                    {obj.text.toUpperCase()}
                                                </Box>
                                                {!isInternalLink(obj.to) && (
                                                    <ListItemSecondaryAction>
                                                        <IconButton edge="end" aria-label="Open in new tab">
                                                            <OpenInNew />
                                                        </IconButton>
                                                    </ListItemSecondaryAction>
                                                )}
                                            </Box>
                                        </Button>
                                    </ListItem>
                                ))}

                                {isAuth ? (
                                    <Fab
                                        className={classes.margin}
                                        variant="extended"
                                        aria-label="Mon compte"
                                        color="primary"
                                        onClick={() => history.push('/hub')}
                                    >
                                        <AccountArrowRightOutline className={classes.leftIcon} />
                                        MON COMPTE
                                    </Fab>
                                ) : (
                                        <Fab
                                            className={classes.margin}
                                            variant="extended"
                                            aria-label="Mon compte"
                                            color="primary"
                                            onClick={this.toggleDrawer('isLoginDrawerOpen', true)}
                                        >
                                            <LoginVariant className={classes.leftIcon} />
                                            SE CONNECTER
                                    </Fab>
                                    )}
                            </List>

                            <ListItem alignItems="center" disableGutters>
                                <Typography variant="caption" align="center" color="textSecondary" component={Box} display="flex" flexGrow={1}>
                                    Version {global.appVersion}
                                </Typography>
                            </ListItem>

                        </div>
                    </div>
                </SwipeableDrawer>

                {!isAuth && (
                    <Dialog
                        open={isLoginDrawerOpen}
                        fullScreen={fullScreen}
                        onClose={this.toggleDrawer('isLoginDrawerOpen', false)}
                        aria-labelledby="view-notification-title"
                    >
                        <div style={{ textAlign: 'left' }}>
                            <IconButton
                                className={classes.button}
                                onClick={this.toggleDrawer('isLoginDrawerOpen', false)}
                                aria-label="Retour"
                            >
                                <ArrowLeft />
                            </IconButton>
                        </div>

                        <Login />
                    </Dialog>
                )}

                <HideOnScroll {...this.props}>
                    <AppBar position="fixed" color="inherit">
                        <Toolbar>
                            <Grid container>
                                <Hidden lgUp>
                                    <Grid container item xs={3} justify="flex-start" alignContent="center">
                                        <IconButton
                                            aria-label="Menu"
                                            color="inherit"
                                            onClick={this.toggleDrawer('isMobileMenuOpen', true)}
                                        >
                                            <MenuIcon />
                                        </IconButton>
                                    </Grid>
                                </Hidden>

                                <Grid container item xs lg={3} justify={isUpLg ? "flex-start" : "center"} alignItems="center">
                                    <Avatar
                                        className={classes.avatar}
                                        alt="Logo BDE ISIMA"
                                        src="/images/favicons/apple-touch-icon.png"
                                        component={RouterLink}
                                        to="/"
                                    />
                                    <Typography
                                        variant="subtitle2"
                                        color="textPrimary"
                                    >
                                        BDE ISIMA
                                </Typography>
                                </Grid>

                                <Grid container item xs={3} lg justify="flex-end" alignItems="center">
                                    <Hidden mdDown>
                                        {menu.map((obj, idx) => (
                                            <Button
                                                key={idx}
                                                className={classnames(classes.link, classes.navLink,
                                                    location.pathname === '/news' && obj.domain === '/news'
                                                        ? classes.navLinkActivePrimary
                                                        : undefined
                                                )}
                                                activeClass={
                                                    location.pathname === '/' && obj.domain === '/'
                                                        ? classes.navLinkActivePrimary
                                                        : undefined
                                                }
                                                component={!isInternalLink(obj.to) ? "div" : Link}
                                                onClick={this.handleClickItem(obj.to)}
                                                to={obj.to}
                                                duration={300}
                                                smooth={true}
                                                spy={true}
                                                aria-label={obj.text}
                                                onMouseDown={this.scrollTo(obj)}
                                            >
                                                <Typography
                                                    variant="subtitle1"
                                                    color="textPrimary"
                                                >
                                                    {obj.text}
                                                </Typography>
                                            </Button>
                                        ))}
                                    </Hidden>

                                    {isAuth ? (
                                        <Fab
                                            className={classes.accountBtn}
                                            variant={isUpLg ? "extended" : "round"}
                                            aria-label="Mon compte"
                                            color="primary"
                                            onClick={() => history.push('/hub')}
                                        >
                                            <AccountArrowRightOutline className={isUpLg ? classes.leftIcon : undefined} />
                                            {isUpLg && "Mon compte"}
                                        </Fab>
                                    ) : (
                                            <Fab
                                                className={classes.accountBtn}
                                                variant={isUpLg ? "extended" : "round"}
                                                aria-label="Se connecter"
                                                color="primary"
                                                onClick={this.toggleDrawer('isLoginDrawerOpen', true)}
                                            >
                                                <LoginVariant className={isUpLg ? classes.leftIcon : undefined} />
                                                {isUpLg && "Se connecter"}
                                            </Fab>
                                        )}
                                </Grid>
                            </Grid>
                        </Toolbar>
                    </AppBar>
                </HideOnScroll>
            </>
        );
    }
}))));
