import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import { LoadsContext, useLoads } from 'react-loads';
import CssBaseline from '@material-ui/core/CssBaseline';
import ModeProvider from './General/ModeProvider';
import Splash from './General/Splash';
import ConsentProvider from './General/ConsentProvider';
import Status from './General/Status';
import SessionChannel from './Authenticated/Auth/Components/SessionChannel';
import Installer from './General/Installer';
import ScrollToTop from './General/ScrollToTop';
import PushNotificationsManager from './General/PushNotificationsManager';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { ClubsProvider } from './General/ClubsContext';
import { MuiThemeProvider, createMuiTheme, responsiveFontSizes } from '@material-ui/core';
import { CRUD } from 'utils';
import NewVersion from './General/NewVersion';
import Messenger from './General/Messenger';
          
const Privacy = React.lazy(() => import('./General/Privacy'));
const ResetPassword = React.lazy(() => import('./Authenticated/Auth/Components/ResetPassword'));
const Authenticated = React.lazy(() => import('./Authenticated'));
const Public = React.lazy(() => import('./Public'));
const NotFound = React.lazy(() => import('./General/NotFound'));

const useStyles = makeStyles(theme => ({
    containerAnchorOriginBottomRight: {
        [theme.breakpoints.down('md')]: {
            bottom: '48px !important',
        },
    },
    containerAnchorOriginBottomLeft: {
        zIndex: 1199,
    },
    success: { color: '#fff' },
    error: { color: '#fff' },
    warning: { color: '#fff' },
    info: { color: '#fff' },
}));

export default () => {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const getClubs = React.useCallback(() => CRUD.get({ url: '/clubs' }), []);
    const { response: clubs, isResolved } = useLoads(getClubs);

    const getTheme = type => {
        return responsiveFontSizes(createMuiTheme({
            typography: {
                useNextVariants: true,
                fontFamily: ['Montserrat', 'Helvetica Neue', 'sans-serif'].join(',')
            },
            palette: {
                type,
                common: { black: '#222222' },
                primary: { main: '#2A2E43' },
                secondary: { main: '#fff' },
                error: { main: '#C91F37' },
            },
        }));
    }
    
    return (
        <SessionChannel>
            <ModeProvider>
                {({ type }) => (
                    <MuiThemeProvider theme={getTheme(type)}>
                        <CssBaseline />
                        <ConsentProvider>
                            <LoadsContext.Provider>
                                <SnackbarProvider
                                    maxSnack={fullScreen ? 2 : 3}
                                    classes={{
                                        containerAnchorOriginBottomRight: classes.containerAnchorOriginBottomRight,
                                        containerAnchorOriginBottomLeft: classes.containerAnchorOriginBottomLeft,
                                        variantSuccess: classes.success,
                                        variantError: classes.error,
                                        variantWarning: classes.warning,
                                        variantInfo: classes.info,
                                    }}
                                >
                                    <Installer>
                                        <PushNotificationsManager>
                                            <Status>
                                                <BrowserRouter>
                                                    <ScrollToTop>
                                                        <NewVersion>
                                                            <React.Suspense fallback={<Splash />}>
                                                                {isResolved && (
                                                                    <ClubsProvider value={clubs.data}>
                                                                        <Switch>
                                                                            <Route
                                                                                path="/privacy"
                                                                                component={Privacy}
                                                                            />
                                                                            <Route
                                                                                path="/reset_password"
                                                                                component={ResetPassword}
                                                                            />
                                                                            <Route
                                                                                path="/hub"
                                                                                component={Authenticated}
                                                                            />
                                                                            <Route
                                                                                exact 
                                                                                path={['/', '/news', '/news/:id/:slug?']}
                                                                                render={() => (
                                                                                    <Messenger>
                                                                                        <Public />
                                                                                    </Messenger>
                                                                                )}
                                                                            />
                                                                            <Route
                                                                                path="*"
                                                                                component={NotFound}
                                                                            />
                                                                        </Switch>
                                                                    </ClubsProvider>
                                                                )}
                                                            </React.Suspense>
                                                        </NewVersion>
                                                    </ScrollToTop>
                                                </BrowserRouter>
                                            </Status>
                                        </PushNotificationsManager>
                                    </Installer>
                                </SnackbarProvider>
                            </LoadsContext.Provider>
                        </ConsentProvider>
                    </MuiThemeProvider>
                )}
            </ModeProvider>
        </SessionChannel>
    );
};
