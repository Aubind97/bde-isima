import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from 'mdi-material-ui/Menu';
import Avatar from '@material-ui/core/Avatar';
import Hidden from '@material-ui/core/Hidden';
import Skeleton from '@material-ui/lab/Skeleton';
import Box from '@material-ui/core/Box';
import MenuBuilder from 'components/Authenticated/Dashboard/Components/Menu/MenuBuilder';
import MenuAuthorizer from 'components/Authenticated/Dashboard/Components/Menu/MenuAuthorizer';
import User from 'components/Authenticated/Auth/Components/User';
import ClubScope from 'components/Authenticated/Dashboard/Components/Menu/ClubScope';
import BDEScope from 'components/Authenticated/Dashboard/Components/Menu/BDEScope';
import ClubsContext from 'components/General/ClubsContext';
import MobileMenu from './MobileMenu';
import Menu from './Menu';
import config from './config';
import scopes_mapping from 'configs/scopes_mapping';

const Notifications = React.lazy(() => import('../../Dashboard/Components/Notifications/Notifications'));
const ModulesMenu = React.lazy(() => import('../../Dashboard/Components/ModulesMenu'));
const AvatarMenu = React.lazy(() => import('../../Dashboard/Components/AvatarMenu'));

const useStyles = makeStyles(theme => ({
    container: {
        width: '100%',
        height: '100%',
    },
    growIcon: {
        [theme.breakpoints.down('lg')]: {
            display: 'flex',
            flexGrow: 1,
            justifyContent: 'flex-start',
        },
    },
    appbar: {
        height: 64,
        justifyContent: 'center',
    },
    avatar: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.down('md')]: {
            margin: theme.spacing(2)
        }
    },
    modules: {
        margin: theme.spacing(0, 1),
    },
}));

export default function Nav() {

    /** States initialisation */
    const classes = useStyles();
    const [isOpen, setIsOpen] = React.useState(false);
    const clubs = React.useContext(ClubsContext);

    const menu = config(new MenuBuilder(
        new MenuAuthorizer(User.getData('scopes'), scopes_mapping(clubs)),
        new ClubScope('Club', clubs, null,
            new BDEScope('BDE', null, null)
        )));
    
    const toggleDrawer = open => () => setIsOpen(open);

    return (
        <AppBar
            className={classes.appbar}
            position="fixed"
            color="inherit"
        >
            <Toolbar variant="dense">

                <MobileMenu isOpen={isOpen} toggleDrawer={toggleDrawer} menu={menu} />

                <Hidden lgUp>
                    <div className={classes.growIcon}>
                        <IconButton
                            aria-label="Menu"
                            onClick={toggleDrawer(true)}
                            color="inherit"
                        >
                            <MenuIcon />
                        </IconButton>
                    </div>
                </Hidden>


                <Hidden mdDown>
                    <Box
                        display="flex"
                        className={classes.container}
                        alignItems="center"
                    >
                        <Avatar
                            className={classes.avatar}
                            alt="Logo BDE ISIMA"
                            src="/images/favicons/apple-touch-icon.png"
                            component={Link}
                            to="/"
                        />

                        <Menu menu={menu} />

                    </Box>
                </Hidden>

                <Box display="flex" alignItems="center">
                    <React.Suspense fallback={[...Array(3).keys()].map(x => <Skeleton variant="circle" width={40} height={40} className={classes.modules} key={x} />)}>
                        <ModulesMenu />
                        <Notifications />
                        <AvatarMenu />
                    </React.Suspense>
                </Box>
            </Toolbar>
        </AppBar>
    );
};
