import React from 'react';
import classnames from 'classnames';
import { makeStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
    flexGrow: {
        flexGrow: 1,
        height: 64,
        color: 'inherit !important',
        borderRadius: 0,
    },
    icon: {
        color: 'inherit !important',
    },
    menuActive: {
        borderBottom: `1.5px solid ${theme.palette.text.primary}`,
    },
}));

export default function Menu ({ menu }) {

    /** States initialisation */
    const classes = useStyles();

    return (
        (menu || []).filter(obj => obj.to).map(obj => (
            <Button
                key={obj.text}
                component={Link}
                to={obj.to}
                className={`${window.location.origin}${window.location.pathname}` === `${window.location.origin}${obj.to}` ? classnames(classes.flexGrow, classes.menuActive) : classes.flexGrow}
                aria-label={obj.text}
            >
                <obj.icon className={classes.icon} />
                <Box color="text.primary" m={1} fontWeight={`${window.location.origin}${window.location.pathname}` === `${window.location.origin}${obj.to}` ? "fontWeightBold" : "fontWeightRegular"}>
                    {obj.text}
                </Box>
            </Button>
        ))
    );
};
