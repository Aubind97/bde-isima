import Hubspot from 'mdi-material-ui/Hubspot';
import CalendarToday from 'mdi-material-ui/CalendarToday';
import Store from 'mdi-material-ui/Store';
import TrophyOutline from 'mdi-material-ui/TrophyOutline';
import ViewDashboardOutline from 'mdi-material-ui/ViewDashboardOutline';

export default builder => ([
    {
        text: 'HUB',
        icon: Hubspot,
        to: '/hub',
    },
    {
        text: 'EVENTS',
        icon: CalendarToday,
        to: '/hub/events',
    },
    {
        text: 'MARKET',
        icon: Store,
        to: '/hub/market',
    },
    {
        text: 'LEADERBOARD',
        icon: TrophyOutline,
        to: '/hub/leaderboard',
    },
    {
        text: 'DASHBOARD',
        icon: ViewDashboardOutline,
        to: builder.buildDefaultPath(),
    }
]);
