import React from 'react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import HomeOutline from 'mdi-material-ui/HomeOutline';
import User from '../../Auth/Components/User';
import Box from '@material-ui/core/Box';
import { UserAvatar } from 'utils';

const useStyles = makeStyles(theme => ({
    avatar: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.down('md')]: {
            margin: theme.spacing(2),
        },
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    icon: {
        color: 'inherit !important',
    },
    selected: {
        color: '#fff',
    },
    menuBtn: {
        minHeight: 40,
        padding: theme.spacing(0, 2),
        width: '90%',
        borderRadius: '0px 80px 80px 0px',
        color: 'inherit !important',
    },
}));

export default React.memo(function MobileMenu({ isOpen, toggleDrawer, menu }){

    /** States initialisation */
    const classes = useStyles();
    const [username] = React.useState(User.getUsername());
    const [iOS] = React.useState(process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent));

    const MenuList = React.memo(() => (
        (menu || []).filter(obj => obj.to).map(obj => (
            <ListItem
                key={obj.text}
                className={classes.icon}
                disableGutters
            >
                <Button
                    className={classes.menuBtn}
                    component={Link}
                    to={obj.to}
                    variant={`${window.location.origin}${window.location.pathname}` === `${window.location.origin}${obj.to}` ? "contained" : "text"}
                    color="primary"
                >
                    <Box
                        display="flex"
                        flexGrow={1}
                        alignItems="center"
                        justifyContent="flex-start"
                        color={`${window.location.origin}${window.location.pathname}` === `${window.location.origin}${obj.to}` ? "secondary" : "text.primary"}
                    >
                        <ListItemIcon>
                            <obj.icon className={`${window.location.origin}${window.location.pathname}` === `${window.location.origin}${obj.to}` ? classes.selected : undefined} />
                        </ListItemIcon>
                        <Box className={`${window.location.origin}${window.location.pathname}` === `${window.location.origin}${obj.to}` ? classes.selected : undefined} m={1}>
                            {obj.text}
                        </Box>
                    </Box>
                </Button>
            </ListItem>
        ))
    ))

    return (
        <SwipeableDrawer
            PaperProps={{
                style: {
                    width: '70vw',
                }
            }}
            open={isOpen}
            onClose={toggleDrawer(false)}
            onOpen={toggleDrawer(true)}
            disableBackdropTransition={!iOS}
            disableDiscovery={iOS}
        >
            <div
                tabIndex={0}
                role="button"
                onClick={toggleDrawer(false)}
                onKeyDown={toggleDrawer(false)}
            >
                <div>
                    <List>
                        <Box display="flex" alignItems="center">

                            <UserAvatar
                                AvatarProps={{
                                    'className': classes.avatar,
                                    'alt': 'Avatar utilisateur',
                                }}
                            />

                            <Typography variant="h6">
                                {username}
                            </Typography>

                        </Box>

                        <ListItem
                            button
                            component={Link}
                            to="/"
                            className={classnames(classes.icon, classes.menuBtn)}
                            disableGutters
                        >
                            <ListItemIcon>
                                <HomeOutline />
                            </ListItemIcon>
                            <Box color="text.primary" m={1}>
                                ACCUEIL
                            </Box>
                        </ListItem>

                        <Divider className={classes.divider} />

                        <MenuList />

                        <ListItem alignItems="center" disableGutters>
                            <Typography variant="caption" align="center" color="textSecondary" component={Box} display="flex" flexGrow={1}>
                                Version {global.appVersion}
                            </Typography>
                        </ListItem>
                    </List>
                </div>
            </div>
        </SwipeableDrawer>
    );
});
