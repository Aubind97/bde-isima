import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Nav from './Components/Nav';
import Home from './Pages/Home';
import Splash from 'components/General/Splash';

const Events = React.lazy(() => import('./Pages/Events/Events'));
const Articles = React.lazy(() => import('./Pages/Articles/Articles'));
const Average = React.lazy(() => import('./Pages/Average/Average'));
const Settings = React.lazy(() => import('./Pages/Settings/Settings'));
const Developers = React.lazy(() => import('./Pages/Developers/Developers'));
const Feedback = React.lazy(() => import('./Pages/Feedback/Feedback'));
const NewsReader = React.lazy(() => import('./Pages/NewsReader'));
const Subscribe = React.lazy(() => import('./Pages/Events/Subscribe'));
const Leaderboard = React.lazy(() => import('./Pages/Leaderboard/Leaderboard'));

export default () => (
    <Box style={{ minHeight: '100vh' }} display="flex" alignItems="center" flexDirection="column">
        <Nav />
        <React.Suspense fallback={<Splash />}>
            <Switch>
                <Route exact path="/hub/events" component={Events} />
                <Route exact path="/hub/events/:id" component={Subscribe} />
                <Route exact path="/hub/market" component={Articles} />
                <Route exact path="/hub/leaderboard" component={Leaderboard} />
                <Route exact path="/hub/average" component={Average} />
                <Route exact path="/hub/settings" component={Settings} />
                <Route exact path="/hub/developers" component={Developers} />
                <Route exact path="/hub/feedback" component={Feedback} />
                <Route exact path="/hub/news/:id/:slug?" component={NewsReader} />
                <Route path="/hub" component={Home} />
            </Switch>
        </React.Suspense>
    </Box>
);
