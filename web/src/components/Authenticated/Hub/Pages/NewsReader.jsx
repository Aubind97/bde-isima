import React from 'react';
import moment from 'moment-timezone';
import { Helmet } from "react-helmet";
import slugify from 'slugify';
import { Loads } from 'react-loads';
import sanitizeHtml from 'sanitize-html';
import { Redirect, withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Skeleton from '@material-ui/lab/Skeleton';
import { CRUD, Image, DateUtils } from 'utils';
import { format } from 'date-fns';

const styles = theme => ({
    root: {
        marginTop: 70 + theme.spacing(3),
        marginBottom: theme.spacing(3),
        minHeight: '100vh'
    },
    content: {
        padding: theme.spacing(2)
    },
    text: {
        wordWrap: 'break-word'
    },
    mainFeaturedPost: {
        position: 'relative',
        backgroundColor: theme.palette.grey[800],
        color: theme.palette.common.white,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
    },
    mainFeaturedPostContent: {
        position: 'relative',
        padding: theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(6),
            paddingRight: 0,
        },
    },
    overlay: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        backgroundColor: 'rgba(0,0,0,.2)',
    }
});

export default withRouter(withStyles(styles)(class NewsReader extends React.Component {

    getNews = () => {
        let { id } = this.props.match.params;
        return id = parseInt(id) ?
            CRUD.get({ url: `/news/${id}`})
                .then(response => {
                    if (!this.props.match.params.slug)
                        this.props.history.replace(`${this.props.location.pathname}/${slugify(response.data.title)}`);
                    return Promise.resolve(response);
                })
            :
            Promise.reject();
    }

    render() {
        const { classes } = this.props;
        return (
            <>
                <Helmet>
                    <meta property="og:locale" content="fr_FR" />
                    <meta property="og:type" content="website" />
                    <meta property="og:url" content={window.location.href} />
                    <meta property="og:site_name" content="BDE ISIMA" />
                    <meta property="og:image" content={Image.display('/images/illustrations/DefaultNews.png')} />
                    <meta property="og:image:secure_url" content={Image.display('/images/illustrations/DefaultNews.png')} />
                    <meta property="og:image:width" content="1280" />
                    <meta property="og:image:height" content="720" />
                    <meta name="twitter:card" content="summary_large_image" />
                    <meta name="twitter:image" content={Image.display('/images/illustrations/DefaultNews.png')} />
                </Helmet>
                
                <Loads load={this.getNews}>
                    {({ response, isRejected, isPending, isResolved }) => (
                        <Container className={classes.root} maxWidth="lg">
                            <Paper square>
                                {isRejected && <Redirect to="/hub" />}

                                {isPending && (
                                    <>
                                        <Skeleton height={260} width="100%" />

                                        <div className={classes.content}>
                                            {[...Array(10).keys()].map(x => <Skeleton key={x} height={12} width={`${85 + (x % 2 === 0 ? x : -x)}%`} />)}

                                            <div style={{ paddingTop: 16 }}>
                                                <Skeleton height={12} width="10%" />
                                            </div>
                                        </div>
                                    </>
                                )}

                                {isResolved && (
                                    <>
                                        <Helmet>
                                            <meta name="description" content={response.data.header} />
                                            <meta property="author" content={response.data.club.name} />
                                            <meta property="article:published_time" content={moment(response.data.published_at).toISOString()} />
                                            <meta property="og:title" content={response.data.title} />
                                            <meta property="og:description" content={response.data.header} />
                                            <meta name="twitter:title" content={response.data.title} />
                                            <meta name="twitter:description" content={response.data.header} />
                                        </Helmet>

                                        <div
                                            className={classes.mainFeaturedPost}
                                            style={{ backgroundImage: `url(${Image.display('/images/illustrations/DefaultNews.svg')})`}}
                                        >
                                            {
                                                <img
                                                    style={{ display: 'none' }}
                                                    src={Image.display('/images/illustrations/DefaultNews.svg')}
                                                    alt="background"
                                                />
                                            }
                                            <div className={classes.overlay} />

                                            <div className={classes.mainFeaturedPostContent}>
                                                <Typography className={classes.text} component="h1" variant="h4" color="inherit" gutterBottom>
                                                    {response.data.title}
                                                </Typography>
                                                <Typography className={classes.text} variant="h5" color="inherit" paragraph>
                                                    {response.data.header}
                                                </Typography>
                                            </div>
                                        </div>

                                        <div className={classes.content}>
                                            <Typography variant="caption" align="left" paragraph>
                                                Publié par : {response.data.club.name}
                                            </Typography>

                                            <Typography
                                                variant="body2"
                                                component="div"
                                                align="justify"
                                                paragraph
                                                dangerouslySetInnerHTML={{
                                                    __html: sanitizeHtml(response.data.content, {
                                                        allowedTags: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'p', 'a', 'ul', 'ol',
                                                            'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div',
                                                            'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'iframe', 'img'],
                                                        allowedAttributes: {
                                                            'a': ['href', 'target'],
                                                            'img': ['src', 'alt', 'margin', 'padding'],
                                                            'iframe': ['width', 'height', 'src', 'frameborder', 'allow', 'allowfullscreen', 'margin', 'padding']
                                                        },
                                                        allowedIframeHostnames: ['www.youtube.com']
                                                    })
                                                }}
                                            />

                                            <Typography variant="caption" align="left" paragraph>
                                                {format(DateUtils.withOffsetDate(response.data.published_at), 'dd/MM/yyyy à HH:mm')}
                                            </Typography>
                                        </div>
                                    </>
                                )}
                            </Paper>
                        </Container>
                    )}
                </Loads>
            </>
        );
    }
}));
