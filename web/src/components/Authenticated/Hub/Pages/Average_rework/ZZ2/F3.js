const ZZ2 = [
    {
        name: 'LANGUES (x8.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ2 F3 Anglais')) || 0,
                coeff: 4.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ2 F3 LV2')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x9.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F3 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F3 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Gestion',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F3 Gestion')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Conduite de projet',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F3 Conduite de projet')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'MÉTHODES ET OUTILS DE DÉVELOPPEMENT LOGICIEL (x7.0)',
        subjects: [
            {
                name: 'C++',
                mark: parseFloat(localStorage.getItem('ZZ2 F3 C++')) || 0,
                coeff: 3.0,
            },
            {
                name: 'UML TC',
                mark: parseFloat(localStorage.getItem('ZZ2 F3 UML TC')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Java',
                mark: parseFloat(localStorage.getItem('ZZ2 F3 Java')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'INFRASTRUCTURE (x4.0)',
        subjects: [
            {
                name: 'IoT (Internet des objets)',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F3 IoT (Internet des objets)'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Réseaux',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F3 Réseaux')
                    ) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'PROJET (x4.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F3 Projet par Binôme')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'CONNAISSANCES DE L\'ENTREPRISE (x7.5)',
        subjects: [
            {
                name: 'Sécurité et systèmes d\'information',
                mark: parseFloat(localStorage.getItem('ZZ2 F3 Sécurité et systèmes d\'information')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Développement mobile',
                mark: parseFloat(localStorage.getItem('ZZ2 F3 Développement mobile')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Architecture logicielles et qualités',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F3 Architecture logicielles et qualités')) ||
                    0,
                coeff: 2.0,
            },
            {
                name: 'Développement web',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F3 Développement web')) ||
                    0,
                coeff: 1.5,
            }
        ],
    },
    {
        name: 'SYSTÈMES D\'INFORMATION (x6.5)',
        subjects: [
            {
                name: 'Développement de bases de données',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F3 Développement de bases de données')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Fondements des bases de données',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F3 Fondements des bases de données')
                    ) || 0,
                coeff: 3.0,
            },
            {
                name: 'Introduction aux systèmes d\'information',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F3 Introduction aux systèmes d\'information')) || 0,
                coeff: 1.5,
            }
        ],
    },
    {
        name: 'AIDE À LA DÉCISION (x10.0)',
        subjects: [
            {
                name: 'Simulation à flux discrets',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F3 Simulation à flux discrets'
                        )
                    ) || 0,
                coeff: 4.0,
            },
            {
                name: 'Modélisation des processus aléatoires',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F3 Modélisation des processus aléatoires'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Analyse et fouille de données',
                mark: parseFloat(localStorage.getItem('ZZ2 F3 Analyse et fouille de données')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'RECHERCHE OPÉRATIONNELLE (x7.0)',
        subjects: [
            {
                name: 'Outils d\'aide à la décision',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F3 Outils d\'aide à la décision'
                        )
                    ) || 0,
                coeff: 3.0,
            },
            {
                name: 'Recherche opérationnelle',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F3 Recherche opérationnelle'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'R.O et I.A pour la productique',
                mark: parseFloat(localStorage.getItem('ZZ2 F3 R.O et I.A pour la productique')) || 0,
                coeff: 2.0,
            }
        ],
    }
];

export default ZZ2;