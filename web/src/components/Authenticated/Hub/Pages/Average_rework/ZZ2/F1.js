const ZZ2 = [
    {
        name: 'LANGUES (x8.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ2 F1 Anglais')) || 0,
                coeff: 4.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ2 F1 LV2')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x9.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F1 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F1 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Gestion',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F1 Gestion')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Conduite de projet',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F1 Conduite de projet')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'MÉTHODES ET OUTILS DE DÉVELOPPEMENT LOGICIEL (x7.0)',
        subjects: [
            {
                name: 'C++',
                mark: parseFloat(localStorage.getItem('ZZ2 F1 C++')) || 0,
                coeff: 3.0,
            },
            {
                name: 'UML TC',
                mark: parseFloat(localStorage.getItem('ZZ2 F1 UML TC')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Java',
                mark: parseFloat(localStorage.getItem('ZZ2 F1 Java')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'INFRASTRUCTURE (x4.0)',
        subjects: [
            {
                name: 'IoT (Internet des objets)',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F1 IoT (Internet des objets)'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Réseaux',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F1 Réseaux')
                    ) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'PROJET (x4.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F1 Projet par Binôme')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'SYSTÈMES EMBARQUÉS ET TEMPS RÉEL (x8.0)',
        subjects: [
            {
                name: 'Linux embarqué',
                mark: parseFloat(localStorage.getItem('ZZ2 F1 Linux embarqué')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Systèmes embarqués',
                mark: parseFloat(localStorage.getItem('ZZ2 F1 Systèmes embarqués')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Séminaire systèmes d\'exploitation embarqués',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F1 Séminaire systèmes d\'exploitation embarqués')) ||
                    0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'INGÉNIERIE DES SYSTÈMES NUMÉRIQUES (x7.0)',
        subjects: [
            {
                name: 'Architecture avancée',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F1 Architecture avancée')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Capteurs',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F1 Capteurs')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Design électronique',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F1 Design électronique')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'TRAITEMENT DU SIGNAL ET DES IMAGES (x6.0)',
        subjects: [
            {
                name: 'Traitement numérique du signal',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F1 Traitement numérique du signal'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Transmission des données',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F1 Transmission des données'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Imagerie vision',
                mark: parseFloat(localStorage.getItem('ZZ2 F1 Imagerie vision')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'SYSTÈMES INTELLIGENTS (x7.0)',
        subjects: [
            {
                name: 'Robotique',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F1 Robotique'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Réalité virtuelle',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F1 Réalité virtuelle'
                        )
                    ) || 0,
                coeff: 3.0,
            },
            {
                name: 'Cybernétique automatique',
                mark: parseFloat(localStorage.getItem('ZZ2 F1 Cybernétique automatique')) || 0,
                coeff: 2.0,
            }
        ],
    }
];

export default ZZ2;