const ZZ2 = [
    {
        name: 'LANGUES (x8.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ2 F2 Anglais')) || 0,
                coeff: 4.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ2 F2 LV2')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x9.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F2 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F2 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Gestion',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F2 Gestion')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Conduite de projet',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F2 Conduite de projet')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'MÉTHODES ET OUTILS DE DÉVELOPPEMENT LOGICIEL (x7.0)',
        subjects: [
            {
                name: 'C++',
                mark: parseFloat(localStorage.getItem('ZZ2 F2 C++')) || 0,
                coeff: 3.0,
            },
            {
                name: 'UML TC',
                mark: parseFloat(localStorage.getItem('ZZ2 F2 UML TC')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Java',
                mark: parseFloat(localStorage.getItem('ZZ2 F2 Java')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'INFRASTRUCTURE (x4.0)',
        subjects: [
            {
                name: 'IoT (Internet des objets)',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F2 IoT (Internet des objets)'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Réseaux',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F2 Réseaux')
                    ) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'PROJET (x4.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F2 Projet par Binôme')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'SYSTÈMES ET RÉSEAUX (x6.0)',
        subjects: [
            {
                name: 'Systèmes d\'exploitation : Programmation Système',
                mark: parseFloat(localStorage.getItem('ZZ2 F2 Systèmes d\'exploitation : Programmation Système')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Services Web',
                mark: parseFloat(localStorage.getItem('ZZ2 F2 Services Web')) || 0,
                coeff: 3.0,
            },
        ],
    },
    {
        name: 'GÉNIE LOGICIEL ET DÉVELOPPEMENT (x8.0)',
        subjects: [
            {
                name: 'Langages à objets / Développement .NET',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F2 Langages à objets / Développement .NET')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Outils de développement professionnels',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F2 Outils de développement professionnels')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Architecture logicielles et qualités',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F2 Architecture logicielles et qualités')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Ergonomie des IHM',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F2 Ergonomie des IHM')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'MODELISATION ET CALCUL (x5.0)',
        subjects: [
            {
                name: 'Simulation',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F2 Simulation'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Outils d\'aide à la décision',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F2 Outils d\'aide à la décision'
                        )
                    ) || 0,
                coeff: 3.0,
            },
        ],
    },
    {
        name: 'INFORMATIQUE POUR L\'ENTREPRISE (x8.0)',
        subjects: [
            {
                name: 'Développement de bases de données',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F2 Développement de bases de données'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Sécurité et systèmes d\'information',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F2 Sécurité et systèmes d\'information'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Linux embarqué',
                mark: parseFloat(localStorage.getItem('ZZ2 F2 Linux embarqué')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Conduite de projets web',
                mark: parseFloat(localStorage.getItem('ZZ2 F2 Conduite de projets web')) || 0,
                coeff: 2.0,
            }
        ],
    }
];

export default ZZ2;