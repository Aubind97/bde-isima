import React from 'react';
import { dripFormGroup } from 'react-drip-form';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

/**
 * This component is responsible for wrapping a switch in a dripFormGroup
 */
export default dripFormGroup()(({ value, meta, title, ...props }) => {
    console.log(title);

    return (
        <Grid item xs={3} direction="column">
            <Typography variant="body1" color='secondary' gutterBottom>
                {title}
            </Typography>


        </Grid>
    )
});
