const ZZ3 = [
    {
        name: 'LANGUES (x6.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ3 F1 Anglais')) || 0,
                coeff: 3.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ3 F1 LV2')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x7.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F1 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F1 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Intelligence économique',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F1 Intelligence économique')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN SCIENTIFIQUE (x4.0)',
        subjects: [
            {
                name: 'Méthodes et outils de développement logiciel',
                mark: parseFloat(localStorage.getItem('ZZ3 F1 Méthodes et outils de développement logiciel')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'PROJET (x5.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F1 Projet par Binôme')) || 0,
                coeff: 5.0,
            }
        ],
    },
    {
        name: 'PROGRAMMATION EMBARQUÉE (x6.0)',
        subjects: [
            {
                name: 'Programmation FPGA-VHDL',
                mark: parseFloat(localStorage.getItem('ZZ3 F1 Programmation FPGA-VHDL')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Programmation temps réel',
                mark: parseFloat(localStorage.getItem('ZZ3 F1 Programmation temps réel')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'OUTILS ET MÉTHODES DE RÉALITÉ VIRTUELLE (x10.0)',
        subjects: [
            {
                name: 'Interfaces naturelles',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F1 Interfaces naturelles')
                    ) || 0,
                coeff: 3.0,
            },
            {
                name: 'Réalité augmentée',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F1 Réalité augmentée')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Géométrie algorithmique',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F1 Géométrie algorithmique')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'SYSTÈMES INTERACTIFS (x8.0)',
        subjects: [
            {
                name: 'Intégration capteurs pour la robotique',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F1 Intégration capteurs pour la robotique'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Robotique',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F1 Robotique'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Robotique mobile',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F1 Robotique mobile'
                        )
                    ) || 0,
                coeff: 3.0,
            },
            {
                name: 'Capteurs communicants',
                mark: parseFloat(localStorage.getItem('ZZ3 F1 Capteurs communicants')) || 0,
                coeff: 1.0,
            }
        ],
    }
];

export default ZZ3;