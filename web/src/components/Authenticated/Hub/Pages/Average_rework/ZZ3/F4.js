const ZZ3 = [
    {
        name: 'LANGUES (x6.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Anglais')) || 0,
                coeff: 3.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 LV2')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x7.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F4 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F4 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Intelligence économique',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F4 Intelligence économique')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN SCIENTIFIQUE (x4.0)',
        subjects: [
            {
                name: 'Méthodes et outils de développement logiciel',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Méthodes et outils de développement logiciel')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'PROJET (x5.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F4 Projet par Binôme')) || 0,
                coeff: 5.0,
            }
        ],
    },
    {
        name: 'RECHERCHE OPÉRATIONNELLE (x10.0)',
        subjects: [
            {
                name: 'Étude de cas en R.O',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Étude de cas en R.O')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Algorithmique de l\'aide à la décision',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Algorithmique de l\'aide à la décision')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Optimisation des systèmes complexes',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Optimisation des systèmes complexes')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Programmation non-linéaire',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Programmation non-linéaire')) || 0,
                coeff: 1.5,
            },
            {
                name: 'Optimisation convexe',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Optimisation convexe')) || 0,
                coeff: 1.5,
            }
        ],
    },
    {
        name: 'MODÉLISATION SCIENTIFIQUE (x8.0)',
        subjects: [
            {
                name: 'Équations aux dérivées partielles',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F4 Équations aux dérivées partielles')
                    ) || 0,
                coeff: 4.0,
            },
            {
                name: 'Introduction aux EDS et aux maths financières',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F4 Introduction aux EDS et aux maths financières')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Mécanique du solide',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F4 Mécanique du solide')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'APPLICATIONS (x8.0)',
        subjects: [
            {
                name: 'Éléments de CAO',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F4 Éléments de CAO'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Algorithmique pour le calcul parallèle',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F4 Algorithmique pour le calcul parallèle'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Compléments de génie logiciel (Java)',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F4 Compléments de génie logiciel (Java)'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Apprentissage profond',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Apprentissage profond')) || 0,
                coeff: 1.0,
            },
            {
                name: 'Méthodes de décomposition de domaines',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Méthodes de décomposition de domaines')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Web data et Big data',
                mark: parseFloat(localStorage.getItem('ZZ3 F4 Web data et Big data')) || 0,
                coeff: 2.0,
            }
        ],
    }
];

export default ZZ3;