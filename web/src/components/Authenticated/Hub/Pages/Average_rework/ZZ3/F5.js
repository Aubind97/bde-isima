const ZZ3 = [
    {
        name: 'LANGUES (x6.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ3 F5 Anglais')) || 0,
                coeff: 3.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ3 F5 LV2')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x7.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F5 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F5 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Intelligence économique',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F5 Intelligence économique')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN SCIENTIFIQUE (x4.0)',
        subjects: [
            {
                name: 'Méthodes et outils de développement logiciel',
                mark: parseFloat(localStorage.getItem('ZZ3 F5 Méthodes et outils de développement logiciel')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'PROJET (x5.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F5 Projet par Binôme')) || 0,
                coeff: 5.0,
            }
        ],
    },
    {
        name: 'RÉSEAUX (x6.0)',
        subjects: [
            {
                name: 'Certification industrielle',
                mark: parseFloat(localStorage.getItem('ZZ3 F5 Certification industrielle')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Routage',
                mark: parseFloat(localStorage.getItem('ZZ3 F5 Routage')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'INFORMATIQUE DES RÉSEAUX (x12.0)',
        subjects: [
            {
                name: 'Administration des bases de données',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F5 Administration des bases de données')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Technologie web',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F5 technologie web')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Programmation objets avancée',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F5 Programmation objets avancée')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Virtualisation et cloud',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F5 Virtualisation et cloud')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Programmation d\'applications mobiles (PAM)',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F5 Programmation d\'applications mobiles (PAM)')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'SÉCURITÉ (x10.0)',
        subjects: [
            {
                name: 'Sécurité des réseaux',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F5 Sécurité des réseaux'
                        )
                    ) || 0,
                coeff: 4.0,
            },
            {
                name: 'Théorie des codes et cryptographie',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F5 Théorie des codes et cryptographie'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Politique de sécurité',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F5 Politique de sécurité'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Sécurité logicielle avancée',
                mark: parseFloat(localStorage.getItem('ZZ3 F5 Sécurité logicielle avancée')) || 0,
                coeff: 2.0,
            }
        ],
    }
];

export default ZZ3;