import React from 'react';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import ZZ1 from './ZZ1/ZZ1.js';
import { ZZ2F1, ZZ2F2, ZZ2F3, ZZ2F4, ZZ2F5 } from './ZZ2/index';
import { ZZ3F1, ZZ3F2, ZZ3F3, ZZ3F4, ZZ3F5 } from './ZZ3/index';
import { makeStyles } from '@material-ui/core';
import User from 'components/Authenticated/Auth/Components/User';
import ModelForm from './ModelForm.jsx';

const models = {
    ZZ1,
    ZZ2F1,
    ZZ2F2,
    ZZ2F3,
    ZZ2F4,
    ZZ2F5,
    ZZ3F1,
    ZZ3F2,
    ZZ3F3,
    ZZ3F4,
    ZZ3F5,
}

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginTop: 70 + theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    divider: {
        margin: theme.spacing(),
    },
    select: {
        width: 200,
        height: 30,
        margin: theme.spacing(2, 0),
    },
}));

const course = User.getPreferences('course');

export default function Average () {

    /** States initialisation */
    const classes = useStyles();
    const [choice, setChoice] = React.useState(`${User.getGrade()}${course ? course : ''}`);

    const handleChoice = event => setChoice(event.target.value); 

    return (
        <Container className={classes.root} fixed>
            <Typography variant="h3" align="center" gutterBottom>
                Calculateur de moyenne
            </Typography>

            <Divider className={classes.divider} />

            <Box display="flex" alignItems="flex-end" flexDirection="column">
                <InputLabel htmlFor="choice-helper">Choix</InputLabel>
                <Select
                    className={classes.select}
                    value={choice}
                    onChange={handleChoice}
                    input={<Input name="choice" id="choice-helper" />}
                >
                    <MenuItem value={'ZZ1'}>ZZ1</MenuItem>

                    <MenuItem value={'ZZ2F1'}>ZZ2 F1</MenuItem>
                    <MenuItem value={'ZZ2F2'}>ZZ2 F2</MenuItem>
                    <MenuItem value={'ZZ2F3'}>ZZ2 F3</MenuItem>
                    <MenuItem value={'ZZ2F4'}>ZZ2 F4</MenuItem>
                    <MenuItem value={'ZZ2F5'}>ZZ2 F5</MenuItem>

                    <MenuItem value={'ZZ3F1'}>ZZ3 F1</MenuItem>
                    <MenuItem value={'ZZ3F2'}>ZZ3 F2</MenuItem>
                    <MenuItem value={'ZZ3F3'}>ZZ3 F3</MenuItem>
                    <MenuItem value={'ZZ3F4'}>ZZ3 F4</MenuItem>
                    <MenuItem value={'ZZ3F5'}>ZZ3 F5</MenuItem>

                </Select>
            </Box>

            <ModelForm values={models[choice]} />
            
        </Container>
    );
};
