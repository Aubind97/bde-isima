import React from 'react';
import { dripForm } from 'react-drip-form';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import UEField from './UEField';

const useStyles = makeStyles(theme => ({
    root: {
        position: 'fixed',
        bottom: 0,
        left: 0,
        width: '100vw',
    }
}));

export default dripForm({
    validations: {
        
    },
    messages: {
        
    },
})(({
    handlers,
    fields,
    meta: { invalid, pristine },
    values,
}) => {
    
    /** States initialisation */
    const classes = useStyles();
    const [semesters] = React.useState(Object.getOwnPropertyNames(values));
    const [semester, setSemester] = React.useState(semesters[0]);

    const handleChangeSemester = (event, newValue) => setSemester(newValue);

    return (
        <Grid container>

            {semesters.map(sem => fields.map(`${sem}.*`, (ue, idx) => <UEField name={`${ue}.subjects`} title={values[sem][idx].name} fields={fields} /> ))}

            <Paper className={classes.root} elevation={12}>
                <BottomNavigation value={semester} onChange={handleChangeSemester} showLabels>
                    {semesters.map(sem => <BottomNavigationAction key={sem} label={sem} value={sem} />)}
                </BottomNavigation>
            </Paper>

        </Grid>
    )
});
