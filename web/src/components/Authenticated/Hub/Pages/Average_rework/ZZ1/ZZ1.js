export default ({
    "Semestre 1" : [
        {
            name: 'SCIENCES HUMAINES ET SOCIALES (x3.0)',
            subjects: [
                {
                    name: 'Anglais',
                    mark: parseFloat(localStorage.getItem('Anglais')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Langue vivante 2',
                    mark: parseFloat(localStorage.getItem('Langue vivante 2')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Management et organisation des entreprises',
                    mark: parseFloat(localStorage.getItem('Management et organisation des entreprises')) || 0,
                    coeff: 1.0,
                },
            ],
        },
        {
            name: 'INFORMATIQUE (x7.0)',
            subjects: [
                {
                    name: 'Langage C et Unix',
                    mark: parseFloat(localStorage.getItem('Langage C et Unix')) || 0,
                    coeff: 2.5,
                },
                {
                    name: 'Algorithmique et structures de données',
                    mark: parseFloat(localStorage.getItem('Algorithmique et structures de données')) || 0,
                    coeff: 2.5,
                },
                {
                    name: 'Programmation fonctionnelle',
                    mark: parseFloat(localStorage.getItem('Programmation fonctionnelle')) || 0,
                    coeff: 1,
                },
                {
                    name: 'Automates',
                    mark: parseFloat(localStorage.getItem('Automates')) || 0,
                    coeff: 1,
                },
            ],
        },
        {
            name: 'SCIENCES DE L\'INGÉNIEUR (x4.5)',
            subjects: [
                {
                    name: 'Physique',
                    mark: parseFloat(localStorage.getItem('Physique')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Transmission de données',
                    mark: parseFloat(localStorage.getItem('Transmission de données')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Traitement du signal',
                    mark: parseFloat(localStorage.getItem('Traitement du signal')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Conception de systèmes numériques',
                    mark: parseFloat(localStorage.getItem('Conception de systèmes numériques')) || 0,
                    coeff: 1.5,
                },
            ],
        },
        {
            name: 'AIDE À LA DÉCISION ET MATHÉMATIQUES APPLIQUÉES (x3.0)',
            subjects: [
                {
                    name: 'Théorie des graphes',
                    mark: parseFloat(localStorage.getItem('Théorie des graphes')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Probabilités',
                    mark: parseFloat(localStorage.getItem('Probabilités')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Analyse numérique',
                    mark: parseFloat(localStorage.getItem('Analyse numérique')) || 0,
                    coeff: 1.0,
                },
            ],
        },
    ],
    "Semestre 2" : [
        {
            name: 'SCIENCES HUMAINES ET SOCIALES (x3.0)',
            subjects: [
                {
                    name: 'Anglais',
                    mark: parseFloat(localStorage.getItem('Anglais')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Langue vivante 2',
                    mark: parseFloat(localStorage.getItem('Langue vivante 2')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Expression et communication',
                    mark: parseFloat(localStorage.getItem('Expression et communication')) || 0,
                    coeff: 1.0,
                },
            ],
        },
        {
            name: 'INFORMATIQUE (x4.5)',
            subjects: [
                {
                    name: 'Algorithmique et structures de données',
                    mark: parseFloat(localStorage.getItem('Algorithmique et structures de données')) || 0,
                    coeff: 1.5,
                },
                {
                    name: 'Bases de données',
                    mark: parseFloat(localStorage.getItem('Bases de données')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Sensibilisation à la cybersécurité',
                    mark: parseFloat(localStorage.getItem('Sensibilisation à la cybersécurité')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Systèmes d\'exploitation',
                    mark: parseFloat(localStorage.getItem('Systèmes d\'exploitation')) || 0,
                    coeff: 1.0,
                },
            ],
        },
        {
            name: 'SCIENCES DE L\'INGÉNIEUR (x10.0)',
            subjects: [
                {
                    name: 'TP Physique',
                    mark: parseFloat(localStorage.getItem('TP Physique')) || 0,
                    coeff: 3.0,
                },
                {
                    name: 'TP Transmission de données',
                    mark: parseFloat(localStorage.getItem('TP Transmission de données')) || 0,
                    coeff: 3.0,
                },
                {
                    name: 'Automatique',
                    mark: parseFloat(localStorage.getItem('Automatique')) || 0,
                    coeff: 4.0,
                },
            ],
        },
        {
            name: 'AIDE À LA DÉCISION ET MATHÉMATIQUES APPLIQUÉES (x4.5)',
            subjects: [
                {
                    name: 'Analyse numérique',
                    mark: parseFloat(localStorage.getItem('Analyse numérique')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Calcul différentiel',
                    mark: parseFloat(localStorage.getItem('Calcul différentiel')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Programmation linéaire',
                    mark: parseFloat(localStorage.getItem('Programmation linéaire')) || 0,
                    coeff: 1.0,
                },
                {
                    name: 'Analyse de données',
                    mark: parseFloat(localStorage.getItem('Analyse de données')) || 0,
                    coeff: 0.5,
                },
                {
                    name: 'Probabilités',
                    mark: parseFloat(localStorage.getItem('Probabilités')) || 0,
                    coeff: 1.0,
                },
            ],
        },
        {
            name: 'PROFESSIONNALISATION (x2.0)',
            subjects: [
                {
                    name: 'Projet (60h) ou Stage (6 semaines à l\'internationale)',
                    mark: parseFloat(localStorage.getItem('Projet (60h) ou Stage (6 semaines à l\'internationale)')) || 0,
                    coeff: 2.0,
                },
            ],
        },
    ]
});
