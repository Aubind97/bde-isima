import React from 'react';
import { withSnackbar } from 'notistack';
import { withStyles } from '@material-ui/core';
import classnames from 'classnames';
import Typography from '@material-ui/core/Typography';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Zoom from '@material-ui/core/Zoom';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import Fade from '@material-ui/core/Fade';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Poll from 'mdi-material-ui/Poll';
import { ZZ1 } from './ZZ1/index';
import { ZZ2F1, ZZ2F2, ZZ2F3, ZZ2F4, ZZ2F5 } from './ZZ2/index';
import { ZZ3F1, ZZ3F2, ZZ3F3, ZZ3F4, ZZ3F5 } from './ZZ3/index';
import User from '../../../Auth/Components/User';
import AverageSnack from './AverageSnack';

const models = {
    ZZ1,
    ZZ2F1,
    ZZ2F2,
    ZZ2F3,
    ZZ2F4,
    ZZ2F5,
    ZZ3F1,
    ZZ3F2,
    ZZ3F3,
    ZZ3F4,
    ZZ3F5
}

const styles = theme => ({
    root: {
        marginTop: 70 + theme.spacing(3),
        marginBottom: theme.spacing(3),
        [theme.breakpoints.down('md')]: {
            marginBottom: 80 +  theme.spacing(2)
        }
    },
    paper: {
        padding: theme.spacing(2),
    },
    formGroup: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
    },
    divider: {
        marginBottom: theme.spacing(2),
    },
    margin: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
        height: 83
    },
    alert: {
        backgroundColor: 'rgba(192, 57, 43, 0.5)',
        border: '.1em solid rgb(192, 57, 43, 0.75)'
    },
    bgcolor: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
        backgroundColor: '#2A2E43',
        height: 120
    },
    fixed: {
        padding: theme.spacing(2),
        [theme.breakpoints.down('md')]: {
            padding: theme.spacing(1),
            width: '100%'
        },
    },
    select: {
        width: 200,
        margin: theme.spacing(2, 0, 2, 0)
    },
    menuSelect: {
        height: 30,
    },
    rightIcon: {
        marginLeft: theme.spacing(1)
    },
    fab: {
        position: 'fixed',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    }
});

export default withSnackbar(withStyles(styles)(class Average extends React.Component {
    state = {
        visible: false,
        choice: `${User.getGrade()}${User.getData('course') ? User.getData('course') : ''}`,
        modules: models[`${User.getGrade()}${User.getData('course')}`] ? models[`${User.getGrade()}${User.getData('course')}`] : models['ZZ1']
    };

    componentDidMount() {
        this.getAverage();
    };

    componentWillUnmount() {
        this.props.closeSnackbar();
    }

    handleShowAverage = () => {
        this.props.enqueueSnackbar('', {
            persist: true,
            preventDuplicate: true,
            anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'right',
            },
            content: key => (
                <AverageSnack onRef={ref => this.snack = ref} key={key} average={this.state.average} handleVisibilityChange={this.handleVisibilityChange} />
            ),
        });
        this.setState({ visible: false });
    };

    handleVisibilityChange = key => {
        this.props.closeSnackbar(key);
        this.setState({ visible: true });
    }

    handleChange = (moduleIdx, subjectIdx) => event => {
        var value = parseFloat(event.target.value);

        if (value && value >= 0 && value <= 20) {
            var { modules } = this.state;
            var subject = modules[moduleIdx].subjects[subjectIdx];
            subject.mark = value;
            this.setState({ modules }, () =>
                localStorage.setItem(subject.name, subject.mark)
            );
        }

        this.getAverage();
    };

    inputContainer = (subject, modAverage, average, modIdx, subjectIdx,) => {
        const { classes } = this.props;
        return (
            <Box display="flex" key={subjectIdx} className={modAverage < 6 || (average < 10 && modAverage < 10) ? classnames(classes.margin, classes.alert) : classes.margin} flexDirection="column">
                <FormHelperText id={`${subject.name}-helper-text`}>
                    {`${subject.name} (x${subject.coeff})`}
                </FormHelperText>
                <Input
                    id={subject.name}
                    type="number"
                    step="0.01"
                    min={0}
                    max={20}
                    defaultValue={subject.mark}
                    onChange={this.handleChange(modIdx, subjectIdx)}
                    aria-describedby={`${subject.name}-helper-text`}
                    endAdornment={
                        <InputAdornment position="end">/20</InputAdornment>
                    }
                />
            </Box>
        );
    };

    getAverage = () => {
        var marks = 0, coeffs = 0;

        this.state.modules.forEach(mod =>
            mod.subjects.forEach(subject => {
                marks += subject.mark * subject.coeff;
                coeffs += subject.coeff;
            })
        );
        
        this.setState({ average: (marks / coeffs).toFixed(3) }, () => {
            if(!this.state.visible){
                this.handleShowAverage();
            }
            if(this.snack) {
                this.snack.setState({ average: this.state.average });
                this.snack.forceUpdate();
            }
        });
    };

    getModAverage = mod => {
        var marks = 0,
            coeffs = 0;
        mod.subjects.forEach(subject => {
            marks += subject.mark * subject.coeff;
            coeffs += subject.coeff;
        });
        return (marks / coeffs).toFixed(3);
    };

    handleChoiceChange = event => {
        this.setState({ 
            [event.target.name]: event.target.value,
            modules: models[event.target.value.replace(/\s/g,'')]
        });
    };

    render() {
        const { classes } = this.props;
        const { average, modules, choice, visible } = this.state;

        return (
            <Container className={classes.root} fixed>
                <Paper className={classes.paper}>
                    <Typography variant="h3" align="center" gutterBottom>
                        Calculateur de moyenne
                    </Typography>

                    <Typography variant="h6" align="center" color="textSecondary" gutterBottom>
                        (Attention, celui-ci n'est pas à jour pour les ZZ1)
                    </Typography>

                    <Divider className={classes.divider} />

                    <Box display="flex" alignItems="flex-end" flexDirection="column">
                        <InputLabel htmlFor="choice-helper">Choix</InputLabel>
                        <Select
                            className={classes.select}
                            value={choice}
                            onChange={this.handleChoiceChange}
                            input={<Input name="choice" id="choice-helper" />}
                            classes={{
                                select: classes.menuSelect
                            }}
                        >
                            <MenuItem value={'ZZ1'}>ZZ1</MenuItem>

                            <MenuItem value={'ZZ2F1'}>ZZ2 F1</MenuItem>
                            <MenuItem value={'ZZ2F2'}>ZZ2 F2</MenuItem>
                            <MenuItem value={'ZZ2F3'}>ZZ2 F3</MenuItem>
                            <MenuItem value={'ZZ2F4'}>ZZ2 F4</MenuItem>
                            <MenuItem value={'ZZ2F5'}>ZZ2 F5</MenuItem>

                            <MenuItem value={'ZZ3F1'}>ZZ3 F1</MenuItem>
                            <MenuItem value={'ZZ3F2'}>ZZ3 F2</MenuItem>
                            <MenuItem value={'ZZ3F3'}>ZZ3 F3</MenuItem>
                            <MenuItem value={'ZZ3F4'}>ZZ3 F4</MenuItem>
                            <MenuItem value={'ZZ3F5'}>ZZ3 F5</MenuItem>

                        </Select>
                    </Box>

                    <Grid container>
                        {modules.map((mod, modIdx) => {
                            const modAverage = this.getModAverage(mod);
                            return (
                                <Grid
                                    key={modIdx}
                                    item
                                    xs={12}
                                    md={3}
                                    className={classes.formGroup}
                                >
                                    <Box className={classes.bgcolor} display="flex" flexDirection="column">
                                        <Typography
                                            variant="body1"
                                            color='secondary'
                                            gutterBottom
                                        >
                                            {mod.name}
                                        </Typography>
                                        <Typography
                                            variant="caption"
                                            color='secondary'
                                            gutterBottom
                                        >
                                            Moyenne : {modAverage}
                                        </Typography>
                                    </Box>

                                    {mod.subjects.map((subject, subjectIdx) =>
                                        this.inputContainer(
                                            subject,
                                            modAverage,
                                            average,
                                            modIdx,
                                            subjectIdx
                                        )
                                    )}
                                </Grid>
                            );
                        })}
                    </Grid>

                    <Zoom in={visible}>
                        <Fab
                            aria-label="Moyenne générale"
                            className={classes.fab}
                            onClick={this.handleShowAverage}
                            color="primary"
                            size="large"
                        >
                            <Tooltip
                                title="Moyenne générale"
                                TransitionComponent={Fade}
                                placement="left"
                            >
                                <Poll />
                            </Tooltip>
                        </Fab>
                    </Zoom>
                </Paper>
            </Container>
        );
    }
}));
