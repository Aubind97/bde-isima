const ZZ2 = [
    {
        name: 'LANGUES (x8.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ2 F5 Anglais')) || 0,
                coeff: 4.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ2 F5 LV2')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x9.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Gestion',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Gestion')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Conduite de projet',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Conduite de projet')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'MÉTHODES ET OUTILS DE DÉVELOPPEMENT LOGICIEL (x7.0)',
        subjects: [
            {
                name: 'C++',
                mark: parseFloat(localStorage.getItem('ZZ2 F5 C++')) || 0,
                coeff: 3.0,
            },
            {
                name: 'UML TC',
                mark: parseFloat(localStorage.getItem('ZZ2 F5 UML TC')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Java',
                mark: parseFloat(localStorage.getItem('ZZ2 F5 Java')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'INFRASTRUCTURE (x4.0)',
        subjects: [
            {
                name: 'IoT (Internet des objets)',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F5 IoT (Internet des objets)'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Réseaux',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F5 Réseaux')
                    ) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'PROJET (x4.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Projet par Binôme')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'INFORMATIQUE DES RÉSEAUX (x13.0)',
        subjects: [
            {
                name: 'Réseaux avancés',
                mark: parseFloat(localStorage.getItem('ZZ2 F5 Réseaux avancés')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Systèmes d\'exploitation : Programmation systèmes',
                mark: parseFloat(localStorage.getItem('ZZ2 F5 Systèmes d\'exploitation : Programmation systèmes')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Services web',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Services web')) ||
                    0,
                coeff: 3.0,
            },
            {
                name: 'Intégration continue pour le web',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Intégration continue pour le web')) ||
                    0,
                coeff: 2.0,
            },
            {
                name: 'Certification industrielle',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Services web')) ||
                    0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'SÉCURITÉ (x10.0)',
        subjects: [
            {
                name: 'Sécurité des systèmes',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F5 Sécurité des systèmes')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Sécurité logicielle',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F5 Sécurité logicielle')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Sécurisation active des systèmes en réseau',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Sécurisation active des systèmes en réseau')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Sécurité des objets connectés',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 Sécurité des objets connectés')) || 0,
                coeff: 2.0,
            },
            {
                name: 'tests d\'intrusion (pentest)',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F5 tests d\'intrusion (pentest)')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'SIGNAL/COUCHE PHYSIQUE (x6.0)',
        subjects: [
            {
                name: 'Traitement numérique du signal',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F5 Traitement numérique du signal'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Transmission des données',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F5 Transmission des données'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Ondes et transmissions',
                mark: parseFloat(localStorage.getItem('ZZ2 F5 Ondes et transmissions')) || 0,
                coeff: 2.0,
            }
        ],
    }
];

export default ZZ2;