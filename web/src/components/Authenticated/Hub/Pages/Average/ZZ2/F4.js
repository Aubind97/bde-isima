const ZZ2 = [
    {
        name: 'LANGUES (x8.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 Anglais')) || 0,
                coeff: 4.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 LV2')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x9.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F4 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F4 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Gestion',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F4 Gestion')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Conduite de projet',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F4 Conduite de projet')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'MÉTHODES ET OUTILS DE DÉVELOPPEMENT LOGICIEL (x7.0)',
        subjects: [
            {
                name: 'C++',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 C++')) || 0,
                coeff: 3.0,
            },
            {
                name: 'UML TC',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 UML TC')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Java',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 Java')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'INFRASTRUCTURE (x4.0)',
        subjects: [
            {
                name: 'IoT (Internet des objets)',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F4 IoT (Internet des objets)'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Réseaux',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F4 Réseaux')
                    ) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'PROJET (x4.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F4 Projet par Binôme')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'MODÉLISATION ALÉATOIRE ET STATISTIQUE (x7.0)',
        subjects: [
            {
                name: 'Modélisation et optimisation de processus aléatoires',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 Modélisation et optimisation de processus aléatoires')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Mesures et probabilités',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 Mesures et probabilités')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Apprentissage statistique',
                mark:
                    parseFloat(localStorage.getItem('ZZ2 F4 Apprentissage statistique')) ||
                    0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'CALCUL SCIENTIFIQUE (x8.0)',
        subjects: [
            {
                name: 'Éléments finis',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F4 Éléments finis')
                    ) || 0,
                coeff: 4.0,
            },
            {
                name: 'Intégrations et distributions',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ2 F4 Intégrations et distributions')
                    ) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'RECHERCHE OPÉRATIONNELLE (x7.5)',
        subjects: [
            {
                name: 'Recherche opérationnelle',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F4 Recherche opérationnelle'
                        )
                    ) || 0,
                coeff: 4.0,
            },
            {
                name: 'Simulation/Productique',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F4 Simulation/Productique'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Optimisation',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 Optimisation')) || 0,
                coeff: 1.5,
            }
        ],
    },
    {
        name: 'APPLICATIONS (x11.0)',
        subjects: [
            {
                name: 'Matlab',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F4 Matlab'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Imagerie vision',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ2 F4 Imagerie vision'
                        )
                    ) || 0,
                coeff: 1.5,
            },
            {
                name: 'Mécanique du solide',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 Mécanique du solide')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Fortran et méthodes de différences finies',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 Fortran et méthodes de différences finies')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Bases de données et fouille de données',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 Bases de données et fouille de données')) || 0,
                coeff: 1.5,
            },
            {
                name: 'Mécanique des fluides',
                mark: parseFloat(localStorage.getItem('ZZ2 F4 Mécanique des fluides')) || 0,
                coeff: 2.0,
            }
        ],
    }
];

export default ZZ2;