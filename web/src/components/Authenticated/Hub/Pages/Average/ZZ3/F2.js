const ZZ3 = [
    {
        name: 'LANGUES (x6.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ3 F2 Anglais')) || 0,
                coeff: 3.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ3 F2 LV2')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x7.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F2 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F2 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Intelligence économique',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F2 Intelligence économique')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN SCIENTIFIQUE (x4.0)',
        subjects: [
            {
                name: 'Méthodes et outils de développement logiciel',
                mark: parseFloat(localStorage.getItem('ZZ3 F2 Méthodes et outils de développement logiciel')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'PROJET (x5.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F2 Projet par Binôme')) || 0,
                coeff: 5.0,
            }
        ],
    },
    {
        name: 'SYSTÈMES ET RÉSEAUX (x4.0)',
        subjects: [
            {
                name: 'Architecture, sécurité et programmation des réseaux',
                mark: parseFloat(localStorage.getItem('ZZ3 F2 Architecture, sécurité et programmation des réseaux')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Conception rapide d\'applications',
                mark: parseFloat(localStorage.getItem('ZZ3 F2 Conception rapide d\'applications')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'GÉNIE LOGICIEL ET DÉVELOPPEMENT (x10.0)',
        subjects: [
            {
                name: 'Programmation d\'applications mobiles',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F2 Programmation d\'applications mobiles')
                    ) || 0,
                coeff: 3.0,
            },
            {
                name: 'Ingénierie desm odèles et simulation',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F2 Ingénierie desm odèles et simulation')
                    ) || 0,
                coeff: 3.0,
            },
            {
                name: 'Algorithmique pour le calcul parallèle',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F2 Algorithmique pour le calcul parallèle')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Développement web',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F2 Développement web')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'MODÉLISATION ET CALCUL (x8.0)',
        subjects: [
            {
                name: 'Grille de calcul et cloud',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F2 Grille de calcul et cloud'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Infographie',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F2 Infographie'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Algorithmique de l\'aide à la décision',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F2 Algorithmique de l\'aide à la décision'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Apprentissage profond',
                mark: parseFloat(localStorage.getItem('ZZ3 F2 Apprentissage profond')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'INFORMATIQUE POUR L\'ENTREPRISE (x8.0)',
        subjects: [
            {
                name: 'Intégration d\'applications',
                mark: parseFloat(localStorage.getItem('ZZ3 F2 Intégration d\'applications')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Administration des bases de données',
                mark: parseFloat(localStorage.getItem('ZZ3 F2 Administration des bases de données')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Java professionnel',
                mark: parseFloat(localStorage.getItem('ZZ3 F2 Java professionnel')) || 0,
                coeff: 2.0,
            }
        ],
    }
];

export default ZZ3;