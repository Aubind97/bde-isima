const ZZ3 = [
    {
        name: 'LANGUES (x6.0)',
        subjects: [
            {
                name: 'LV1 Anglais',
                mark: parseFloat(localStorage.getItem('ZZ3 F3 Anglais')) || 0,
                coeff: 3.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('ZZ3 F3 LV2')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN TERTIAIRE (x7.0)',
        subjects: [
            {
                name: 'Droit',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F3 Droit')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Expression Communication',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F3 Expression Communication')) || 0,
                coeff: 3.0,
            },
            {
                name: 'Intelligence économique',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F3 Intelligence économique')) || 0,
                coeff: 2.0,
            }
        ],
    },
    {
        name: 'TRONC COMMUN SCIENTIFIQUE (x4.0)',
        subjects: [
            {
                name: 'Méthodes et outils de développement logiciel',
                mark: parseFloat(localStorage.getItem('ZZ3 F3 Méthodes et outils de développement logiciel')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'PROJET (x5.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F3 Projet par Binôme')) || 0,
                coeff: 5.0,
            }
        ],
    },
    {
        name: 'CONNAISSANCES ET MODÉLISATION DES ENTREPRISES (x7.0)',
        subjects: [
            {
                name: 'Modélisation et gestion intégrée de la chaîne logistique',
                mark: parseFloat(localStorage.getItem('ZZ3 F3 Modélisation et gestion intégrée de la chaîne logistique')) || 0,
                coeff: 4.0,
            },
            {
                name: 'Gestion et management',
                mark: parseFloat(localStorage.getItem('ZZ3 F3 Gestion et management')) || 0,
                coeff: 3.0,
            }
        ],
    },
    {
        name: 'INGÉNIERIE DES SYSTÈMES D\'INFORMATION (x15.0)',
        subjects: [
            {
                name: 'Intégration d\'applications',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F3 Intégration d\'applications')
                    ) || 0,
                coeff: 3.0,
            },
            {
                name: 'Web services',
                mark:
                    parseFloat(
                        localStorage.getItem('ZZ3 F3 Web services')
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Administration des bases de données',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F3 Administration des bases de données')) || 0,
                coeff: 4.0,
            },
            {
                name: 'Oracle applications',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F3 Oracle applications')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Business intelligence',
                mark:
                    parseFloat(localStorage.getItem('ZZ3 F3 Business intelligence')) || 0,
                coeff: 4.0,
            }
        ],
    },
    {
        name: 'MODÉLISATION POUR L\'AIDE À LA DÉCISION (x8.0)',
        subjects: [
            {
                name: 'Algorithmique de l\'aide à la décision',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F3 Algorithmique de l\'aide à la décision'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Big Data',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F3 Big Data'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Évaluation et optimisation des systèmes',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'ZZ3 F3 Évaluation et optimisation des systèmes'
                        )
                    ) || 0,
                coeff: 2.0,
            },
            {
                name: 'Langage Java',
                mark: parseFloat(localStorage.getItem('ZZ3 F3 Langage Java')) || 0,
                coeff: 2.0,
            }
        ],
    }
];

export default ZZ3;