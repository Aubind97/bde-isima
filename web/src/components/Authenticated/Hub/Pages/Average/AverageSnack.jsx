import React from 'react';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import ChevronUp from 'mdi-material-ui/ChevronUp';
import IconButton from '@material-ui/core/IconButton';

const styles = theme => ({
    divider: {
        marginBottom: theme.spacing(2),
    },
    fixed: {
        padding: theme.spacing(2),
        [theme.breakpoints.down('md')]: {
            padding: theme.spacing(1),
            width: '100%'
        },
    },
    rightIcon: {
        marginLeft: theme.spacing(1)
    },
});

export default withStyles(styles)(class AverageSnack extends React.Component {
    state = {
        average: this.props.average
    }

    componentDidMount() {
        this.props.onRef(this);
    };

    componentWillUnmount() {
        this.props.onRef(undefined);
    };

    render() {
        const { classes, key, handleVisibilityChange } = this.props;
        const { average } = this.state;

        return (
            <Paper id={key} className={classes.fixed}>
                <Typography variant="body1" align="center">
                    Moyenne générale
                    <IconButton className={classes.rightIcon} aria-label="Fermer" color="inherit" onClick={() => handleVisibilityChange(key)}>
                        <ChevronUp />
                    </IconButton>
                </Typography>

                <Divider className={classes.divider} />

                <Typography variant="body1" align="center">
                    {average}/20
                </Typography>
            </Paper>
        );
    }
});
