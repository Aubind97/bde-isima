const ZZ1 = [
    {
        name: 'MODULE GÉNÉRALE (x8.0)',
        subjects: [
            {
                name: 'Anglais',
                mark: parseFloat(localStorage.getItem('Anglais')) || 0,
                coeff: 2.0,
            },
            {
                name: 'LV2',
                mark: parseFloat(localStorage.getItem('LV2')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Sciences économiques',
                mark:
                    parseFloat(localStorage.getItem('Sciences économiques')) ||
                    0,
                coeff: 2.0,
            },
            {
                name: 'Expression communication',
                mark:
                    parseFloat(
                        localStorage.getItem('Expression communication')
                    ) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'PROJET (x2.0)',
        subjects: [
            {
                name: 'Projet par binôme',
                mark:
                    parseFloat(localStorage.getItem('Projet par binôme')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'LANGAGE C (x3.0)',
        subjects: [
            {
                name: 'Langage C',
                mark: parseFloat(localStorage.getItem('Langage C')) || 0,
                coeff: 3.0,
            },
        ],
    },
    {
        name: 'TECHNIQUES ALGORITHMIQUES (x6.0)',
        subjects: [
            {
                name: 'Algorithmique et structures de données',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'Algorithmique et structures de données'
                        )
                    ) || 0,
                coeff: 5.0,
            },
            {
                name: 'Programmation fonctionnelle',
                mark:
                    parseFloat(
                        localStorage.getItem('Programmation fonctionnelle')
                    ) || 0,
                coeff: 1.0,
            },
        ],
    },
    {
        name: 'CALCUL SCIENTIFIQUE (x3.5)',
        subjects: [
            {
                name: 'Analyse numérique',
                mark:
                    parseFloat(localStorage.getItem('Analyse numérique')) || 0,
                coeff: 2.0,
            },
            {
                name: 'Calcul différentiel (et soutien mathématiques)',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'Calcul différentiel (et soutien mathématiques)'
                        )
                    ) || 0,
                coeff: 1.5,
            },
        ],
    },
    {
        name: 'PHYSIQUE ET ÉLECTRONIQUE (x5.0)',
        subjects: [
            {
                name: 'Physique',
                mark: parseFloat(localStorage.getItem('Physique')) || 0,
                coeff: 1.5,
            },
            {
                name: 'Électronique',
                mark: parseFloat(localStorage.getItem('Électronique')) || 0,
                coeff: 1.5,
            },
            {
                name: 'Traitement du signal',
                mark:
                    parseFloat(localStorage.getItem('Traitement du signal')) ||
                    0,
                coeff: 1.0,
            },
            {
                name: 'Automatique',
                mark: parseFloat(localStorage.getItem('Automatique')) || 0,
                coeff: 1.0,
            },
        ],
    },
    {
        name: 'AIDE À LA DÉCISION (x5.0)',
        subjects: [
            {
                name: 'R.O (Théorie des graphes)',
                mark:
                    parseFloat(
                        localStorage.getItem('R.O (Théorie des graphes)')
                    ) || 0,
                coeff: 1.0,
            },
            {
                name: 'R.O (Programmation linéaire)',
                mark:
                    parseFloat(
                        localStorage.getItem('R.O (Programmation linéaire)')
                    ) || 0,
                coeff: 1.0,
            },
            {
                name: 'Analyse de données',
                mark:
                    parseFloat(localStorage.getItem('Analyse de données')) || 0,
                coeff: 1.0,
            },
            {
                name: 'Probabilités',
                mark: parseFloat(localStorage.getItem('Probabilités')) || 0,
                coeff: 2.0,
            },
        ],
    },
    {
        name: 'ARCHITECTURE DES ORDINATEURS ET SYSTÈMES (x5.0)',
        subjects: [
            {
                name: 'Sensibilisation à la cybersécurité',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'Sensibilisation à la cybersécurité'
                        )
                    ) || 0,
                coeff: 1.0,
            },
            {
                name: 'Conception de systèmes numériques',
                mark:
                    parseFloat(
                        localStorage.getItem(
                            'Conception de systèmes numériques'
                        )
                    ) || 0,
                coeff: 1.0,
            },
            {
                name: 'Automates',
                mark: parseFloat(localStorage.getItem('Automates')) || 0,
                coeff: 1.0,
            },
            {
                name: 'Systèmes',
                mark: parseFloat(localStorage.getItem('Systèmes')) || 0,
                coeff: 1.0,
            },
            {
                name: 'Base de données',
                mark: parseFloat(localStorage.getItem('Base de données')) || 0,
                coeff: 1.0,
            },
        ],
    },
];

export default ZZ1;