import React from 'react';
import { dripForm } from 'react-drip-form';
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import CircularProgress from '@material-ui/core/CircularProgress';
import MenuItem from '@material-ui/core/MenuItem';
import Email from 'mdi-material-ui/Email';
import { Field, SelectField } from 'utils/forms';
import { Accept } from 'utils';

export default dripForm({
    validations: {
        subject: {
            required: true,
            max: 255
        },
        message: {
            required: true,
            min: 30
        },
    },
    messages: {
        subject: {
            required: 'Ce champ est requis.',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        message: {
            required: 'Ce champ est requis.',
            min: 'Le message doit contenir au moins 30 caractères.'
        },
    },
})(({
    handlers,
    meta: { invalid, pristine },
    submitting,
    ...props
}) => (
    <form onSubmit={handlers.onSubmit}>
        <Box display="flex" flexDirection="column">

            <SelectField
                id="subject"
                name="subject"
                label="Sujet du message"
                className={props.selectStyle}
            >
                {[
                    'Suggestion(s)',
                    'Bug(s)',
                    "Retour(s) d'expériences",
                    'Autre',
                ].map((s, i) => (
                    <MenuItem key={i} value={s}>{s}</MenuItem>
                ))}
            </SelectField>

            <Field
                type="text"
                id="message"
                name="message"
                label="Message (min. 30 caractères)"
                multiline
                rows={10}
            />

            <Accept 
                multiple 
                onDrop={props.onDrop}
                onDelete={props.onDelete}
                files={props.files}
                progress={props.progress}
            />

            <Button
                type="submit"
                startIcon={submitting ? <CircularProgress size={25} color="secondary" /> : <Email />}
                onClick={handlers.onSubmit}
                disabled={invalid || pristine || submitting}
                variant="contained"
                color="primary"
                size="large"
                aria-label="Envoyer"
            >
                Envoyer
            </Button>
        </Box>
    </form>
));