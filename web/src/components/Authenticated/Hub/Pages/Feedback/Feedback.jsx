import React from 'react';
import { withSnackbar } from 'notistack';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import FeedbackForm from './FeedbackForm';
import { CRUD, CompressionHelper } from 'utils';
import { Link } from '@material-ui/core';

const styles = theme => ({
    root: {
        marginTop: 70 + theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    paper: {
        padding: theme.spacing(2),
    },
    select: {
        textAlign: 'left'
    },
});

export default withSnackbar(withStyles(styles)(class Feedback extends React.Component {
    state = {
        progress: '',
        files: [],
        submitting: false
    };
    
    handleInitialize = form => this.form = form;

    handleSubmit = newData => {
        this.setState({ submitting: true });

        CRUD.add({
            url: '/com/feedback',
            newData: Object.assign(newData, { images: this.state.files }),
            onUploadProgress: this.setProgress,
            onSucceed: this.resetState,
            onRejection: this.resetState,
            props: this.props
        });
    };

    onDelete = file => {
        const { files } = this.state;
        files.splice(files.indexOf(file), 1);
        this.setState({ files, progress: '' });
    };

    setProgress = progress => this.setState({ progress });

    resetState = () => this.setState({ progress: '', files: [], submitting: false });

    render() {
        const { classes } = this.props;
        const { submitting, progress, files } = this.state;
        return (
            <Container className={classes.root} fixed>
                <Paper className={classes.paper} >
                    <Typography variant="h4" paragraph>
                        Feedback
                    </Typography>
                    <Typography color="textSecondary" variant="caption" paragraph>
                        Vous pouvez aussi ouvrir une issue sur le <Link href="https://gitlab.com/Aubind97/bde-isima/issues" target="_blank" rel="noopener noreferrer" aria-label="Lien vers les issues Gitlab">Gitlab dédié</Link>
                    </Typography>
                    <FeedbackForm
                        submitting={submitting}
                        onInitialize={this.handleInitialize}
                        onValidSubmit={this.handleSubmit}
                        onDrop={CompressionHelper.onDrop({
                            handleOnStart: () => this.setState({ progress: `Compression en cours ...` }),
                            handleOnChange: files => this.setState({ progress: `Compression terminée !`, files })
                        })}
                        onDelete={this.onDelete}
                        files={files}
                        progress={progress}
                        selectStyle={classes.select}
                        classes={classes}
                    />
                </Paper>
            </Container>
        );
    }
}));
