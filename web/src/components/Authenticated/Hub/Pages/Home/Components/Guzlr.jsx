import React from 'react';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    container: {
        margin: theme.spacing(2),
    },
    divider: {
        width: '40%',
        margin: theme.spacing(2, 0, 2, 0)
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 300,
        backgroundColor: '#FBBC05CC',
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    imgRoot: {
        backgroundSize: 'contain',
        border: '16px solid transparent',
    },
    inherit: {
        color: 'inherit !important',
    },
}));

export default function Guzlr() {

    /** States initialisation */
    const classes = useStyles();

    return (
        <Box display="flex" className={classes.container} flexDirection="column">
            <Typography align="left" variant="h6">
                Nouveau !
            </Typography>

            <Divider className={classes.divider} />

            <div style={{ padding: '20px 0px' }}>
                <CardActionArea className={classes.inherit} href="https://guzlr.fr/" target="_blank" rel="noopener noreferrer">
                    <Card className={classes.root}>
                        <div className={classes.details}>
                            <CardContent className={classes.content}>
                                <Typography component="h5" variant="h5" gutterBottom>
                                    Guzlr
                                </Typography>
                                <Typography variant="caption" color="textSecondary" paragraph>
                                    Guzlr c’est l’application 100% Clermontoise qui te permet de profiter de verres offerts dans de nombreux bars de Clermont-Ferrand.
                                </Typography>

                                <div style={{ textAlign: 'right' }}>
                                    <Typography variant="subtitle1">
                                        Code promo <b>ISIMAGUZLR</b>
                                    </Typography>
                                    <Typography variant="caption" color="textSecondary">
                                        17 SEPT. AU 17 OCT. : 1 MOIS GRATUIT + 20% DE RÉDUCTION
                                    </Typography>
                                </div>
                            </CardContent>
                        </div>
                        <CardMedia
                            className={classes.cover}
                            classes={{ root: classes.imgRoot }}
                            image="/images/logos/guzlr.png"
                            title="Guzlr"
                        />
                    </Card>
                </CardActionArea>
            </div>
        </Box>
    );         
};
