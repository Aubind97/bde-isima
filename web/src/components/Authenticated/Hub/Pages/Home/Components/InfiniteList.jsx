import React from 'react';
import { makeStyles, List } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { useInfiniteScroll } from 'react-infinite-scroll-hook';

const useStyles = makeStyles(theme => ({
    container: {
        textAlign: 'center',
        width: '100%',
    },
    margin: {
        marginBottom: theme.spacing(3),
    },
}));

export default React.memo(function InfiniteList({ loadItems, renderer, comparator, count, visible = true }) {

    /** States initialisation */
    const classes = useStyles();
    const [loading, setLoading] = React.useState(false);
    const [items, setItems] = React.useState([]);

    const handleLoadMore = React.useCallback(() => {
        setLoading(true);
        loadItems(items, items.length).then(newArray => {
            setLoading(false);
            setItems(newArray);
        });
    }, [items, loadItems]);

    const infiniteRef = useInfiniteScroll({
        loading,
        hasNextPage: items.length < count && visible,
        onLoadMore: handleLoadMore,
        scrollContainer: 'window',
    });

    React.useEffect(() => {
        setItems([]);
    }, [count]);

    const filtered = items.filter(comparator);

    return (
        <div className={classes.container} maxWidth="sm">

            <List className={classes.margin} ref={infiniteRef}>

                {filtered.map(renderer)}

                {(!loading && (items.length === 0 || filtered.length === 0)) && (
                    <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        flexDirection="column"
                    >
                        <img
                            src="/images/illustrations/NoData.svg"
                            height="auto"
                            width="150"
                            alt="Aucune donnée"
                        />
                        <Typography
                            variant="caption"
                            gutterBottom
                        >
                            Aucune donnée !
                        </Typography>
                    </Box>
                )}

                {loading && <CircularProgress size={25} color="inherit" />}

            </List>

        </div>
    );
});
