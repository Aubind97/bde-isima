import React from 'react';
import { makeStyles, Tooltip } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TrendingDown from 'mdi-material-ui/TrendingDown';
import TrendingUp from 'mdi-material-ui/TrendingUp';
import LazyLoad from 'react-lazyload';
import User from 'components/Authenticated/Auth/Components/User';
import TransactionAvatar from './TransactionAvatar';
import Skeleton from '@material-ui/lab/Skeleton';
import { DateUtils } from 'utils';
import { format } from 'date-fns';

const useStyles = makeStyles(theme => ({
    container: { margin: theme.spacing(1, 0) },
    green: { color: '#27ae60' },
    red: { color: '#B71C1C' },
}));

export default function Transaction({ data, dense, userId }) {

    /** States initialisation */
    const classes = useStyles();
    const id = userId ? userId : User.getData('id');

    const getSignedValue = React.useCallback(val => {
        const amount = parseFloat(val);

        if (data.emitter_id) {
            //If the id is the emitter_id, display negative transaction
            if (data.emitter_id === id) {
                return `-${amount}`;
            }
            //If the id is the receiver_id, display positive transaction
            if (data.receiver_id === id) {
                return `+${amount}`;
            }
        }
        return amount > 0 ? `+${amount}` : amount;
    }, [data, id]);

    const amount = getSignedValue(data.amount);
    const isPositive = parseFloat(amount) > 0;

    return (
        <Grid className={dense ? undefined : classes.container} container>

            {/* Display avatar or Trend up/down icon */}
            <Grid className={isPositive ? classes.green : classes.red} container item xs={dense ? 3 : 2} justify="center" alignContent="center">
                {(!dense && data.emitter_id && data.emitter_id !== userId) ? (
                    <LazyLoad
                        height={48}
                        offset={48}
                        scroll
                        overflow
                        once
                        placeholder={<Skeleton variant="circle" width={48} height={48} />}
                    >
                        <TransactionAvatar data={data} userId={data.emitter_id} />
                    </LazyLoad>
                ) : isPositive ? <TrendingUp /> : <TrendingDown />}
            </Grid>
            {/* Display avatar or Trend up/down icon */}

            {/* Display description and date of transaction */}
            <Grid container item xs={6} justify="center" alignContent="center" direction="column">
                <Tooltip title={data.description}>
                    <Typography variant="caption">
                        {dense ? data.description.substring(0, 12) : data.description}
                    </Typography>
                </Tooltip>
                {!dense && (
                    <Typography variant="overline" color="textSecondary">
                        {format(DateUtils.withOffsetDate(data.created_at), 'dd/MM/yyyy à HH:mm')}
                    </Typography>
                )}
            </Grid>
            {/* Display description and date of transaction */}

            {/* Display the amount of the transaction */}
            <Grid container item xs={dense ? 3 : 2} justify="center" alignItems="center">
                <Typography className={isPositive ? classes.green : classes.red} variant="caption">
                    {amount} €
                    </Typography>
            </Grid>
            {/* Display the amount of the transaction */}

            {/* Display the old amount of the user */}
            {!dense && (
                <Grid container item xs={2} justify="center" alignItems="center">
                    <Typography variant="caption" color="textSecondary">
                        {data.receiver_id === id ? data.prev_receiver_balance : data.prev_emitter_balance} €
                    </Typography>
                </Grid>
            )}
            {/* Display the old amount of the user */}

        </Grid>
    );
};
