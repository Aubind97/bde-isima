import React from 'react';
import deburr from 'lodash/deburr';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core';
import Close from 'mdi-material-ui/Close';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';
import hideVirtualKeyboard from 'hide-virtual-keyboard';
import { CRUD } from 'utils';

const getSuggestionValue = suggestion => `${suggestion.card} - ${suggestion.firstname} ${suggestion.lastname} ${suggestion.nickname ? `(${suggestion.nickname})` : '' }`;

function smartSearch(a, b) {
    a = a.trim().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    b = b.trim().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    return a.includes(b);
}

const styles = theme => ({
    container: {
        position: 'relative',
        width: '100%',
        margin: theme.spacing(2, 0)
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing(1),
        left: 0,
        right: 0,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
});

export default withStyles(styles)(class SearchUser extends React.Component {
    state = {
        suggestions: [],
        loading: true,
    };

    componentDidMount(){
        CRUD.get({
            url: `/users/compact`,
            params: {
                is_enabled: '1',
                orderBy: 'card',
                orderDirection: 'asc',

            },
        })
        .then(results => {
            this.setState({ users: results.data, loading: false }, () => this.updateSelection());
        });
    };

    componentDidUpdate(prevProps) {
        if (this.props.to && prevProps.to !== this.props.to)
            this.updateSelection();
    }

    updateSelection = () => {
        if (this.props.to) {
            this.handleSuggestionsFetchRequested({ value: this.props.to.toString() });
        }
    };

    handleOnChange = searchInput => {
        if(this.props.onChange)
            this.props.onChange(searchInput);
    };
    
    handleOnSelection = selected => {
        const { users } = this.state;
        
        if (this.props.onSelection && selected){
            CRUD.get({ url: `/users/compact/${selected.id}` })
                .then(res => {
                    users.splice(users.findIndex(x => x.id === selected.id), 1, res.data);
                    this.props.onSelection(res.data);
                    this.setState({ users });
                });
            this.props.onSelection(selected);
            hideVirtualKeyboard();
        }
    };

    handleChange = (event, { newValue }) => {
        this.handleOnChange(newValue);
    };

    renderInputComponent = inputProps => {
        const { ref, component: Component, ...rest } = inputProps;
        const { loading } = this.state;
        const endAdornment = (
            <InputAdornment position="end">
                {loading && <CircularProgress size={20} color="inherit" />}
                <IconButton onClick={this.props.onClearRequested}>
                    <Close />
                </IconButton>
            </InputAdornment>
        );

        return (
            <Component
                type="text"
                id="search"
                name="search"
                fullWidth
                inputProps={rest}
                // eslint-disable-next-line react/jsx-no-duplicate-props
                InputProps={{
                    endAdornment: endAdornment
                }}
                inputRef={ref}
                {...rest}
                endAdornment={endAdornment}
            />
        );
    };

    getSuggestions = value => {
        const inputValue = deburr(value.trim()).toLowerCase();
        let count = 0;
        const results = inputValue.length === 0 || !this.state.users ? 
            []
            : 
            this.state.users.filter(suggestion => {
                const keep = (count < 5 || inputValue.length > 2) && smartSearch(getSuggestionValue(suggestion), inputValue);
                if (keep) count += 1;                    
                return keep;
            });
        if (results.length === 1 && (!this.props.selected || getSuggestionValue(this.props.selected).trim().toLowerCase() !== inputValue)) {
            this.handleOnChange(getSuggestionValue(results[0]));
            this.handleOnSelection(results[0]);
        }
        return results;
    };

    renderSuggestion = (suggestion, { query, isHighlighted }) => {
        const matches = match(getSuggestionValue(suggestion), query);
        const parts = parse(getSuggestionValue(suggestion), matches);

        return (
            <MenuItem selected={isHighlighted} component="div">
                <div>
                    {parts.map((part, index) =>
                        part.highlight ? (
                            <span key={String(index)} style={{ fontWeight: 600 }}>
                                {part.text}
                            </span>
                        ) : (
                            <strong key={String(index)} style={{ fontWeight: 300 }}>
                                {part.text}
                            </strong>
                        ),
                    )}
                </div>
            </MenuItem>
        );
    };

    handleSuggestionsFetchRequested = ({ value }) => {
        this.setState({ suggestions: this.getSuggestions(value) });
    };

    handleSuggestionsClearRequested = () => {
        this.setState({ suggestions: [] });
    };

    handleOnInputClearRequested = () => {
        this.handleOnChange('');
        this.handleOnSelection(null);
    };    

    handleOnSuggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
        hideVirtualKeyboard();
        this.handleOnChange(suggestionValue);
        this.handleOnSelection(suggestion);
    }

    render() {
        const { classes, id, label, placeholder, component, searchInput } = this.props;
        const { suggestions } = this.state;

        const autosuggestProps = {
            renderInputComponent: this.renderInputComponent,
            suggestions: suggestions,
            onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
            onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
            onSuggestionSelected: this.handleOnSuggestionSelected,
            getSuggestionValue,
            renderSuggestion: this.renderSuggestion
        };

        return (
            <Autosuggest
                id={id}
                {...autosuggestProps}
                inputProps={{
                    label,
                    placeholder,
                    value: searchInput,
                    onChange: this.handleChange,
                    component,
                    autoFocus: true,
                }}
                theme={{
                    container: classes.container,
                    suggestionsContainerOpen: classes.suggestionsContainerOpen,
                    suggestionsList: classes.suggestionsList,
                    suggestion: classes.suggestion,
                }}
                renderSuggestionsContainer={options => (
                    <Paper {...options.containerProps}>
                        {options.children}
                    </Paper>
                )}
            />
        )
    }
});
