import React from 'react';
import { withSnackbar } from 'notistack';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Close from 'mdi-material-ui/Close';
import Send from 'mdi-material-ui/Send';
import SwapHorizontal from 'mdi-material-ui/SwapHorizontal';
import SearchUser from './SearchUser';
import User from 'components/Authenticated/Auth/Components/User';
import { CRUD } from 'utils';

const styles = theme => ({
    margin: {
        margin: theme.spacing(2, 0),   
    },
});

export default withSnackbar(withStyles(styles)(class Transfer extends React.Component {
    state = {
        amount: '',
        description: this.props.noEmitter ? `Rechargement` : `Virement de ${User.getCompleteName()}`,
        searchInput: '',
        submitting: false,
        selected: null,
    };

    handleInitialize = form => this.form = form;

    handleChange = name => event => this.setState({ [name]: event.target.value });

    onClearRequested = () => this.setState({ open: false, selected: null, searchInput: '' });

    handleSubmit = event => {
        const { description, selected } = this.state;
        const amount = parseFloat(this.state.amount.toString().replace(',', '.'));
        this.setState({ submitting: true });

        event.preventDefault();

        if (selected) {
            CRUD.add({
                url: `/transactions`,
                newData: {
                    amount,
                    description,
                    receiver_id: selected.id,
                    receiver_name: `${selected.firstname} ${selected.lastname}`,
                    emitter_id: this.props.noEmitter ? null : User.getData('id'),
                    emitter_name: this.props.noEmitter ? null : `${User.getUsername()}`
                },
                onSucceed: result => {
                    if(!this.props.noEmitter)
                        User.setData(Object.assign(User.getAllData(), { 
                            balance: parseFloat(User.getData('balance')) + (this.props.noEmitter ? amount : -amount) 
                        }));
                    if (this.props.updateBalance)
                        this.props.updateBalance(amount);
                    this.resetState();
                    this.setState({ lastRequestId: result.data.inserted_id })
                },
                onRejection: this.resetState,
                props: this.props.allowCancel ? { ...this.props, ...{ handleCancel: this.handleCancel } } : this.props
            });
        }
        else {
            this.props.enqueueSnackbar("Veuillez sélectionner un utilisateur", {
                variant: 'error',
                anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'right'
                },
                action: key => (
                    <IconButton aria-label="Fermer" color="secondary" onClick={() => this.props.closeSnackbar(key)}>
                        <Close />
                    </IconButton>
                )
            });
            this.resetState();
        }
    };

    handleCancel = () => {
        const amount = parseFloat(this.state.amount.toString().replace(',', '.'));

        this.setState({ submitting: true });

        CRUD.delete({
            url: `/transactions/${this.state.lastRequestId}`,
            onSucceed: () => {
                if (this.props.updateBalance)
                    this.props.updateBalance(-amount);
                this.resetState();
            },
            onRejection: this.resetState,
            props: this.props
        })
    };

    resetState = () => this.setState({ submitting: false });

    render() {
        const { amount, description, searchInput, submitting, selected } = this.state;
        const { to, classes, allowCancel } = this.props;

        return (
            <form>
                <Typography align="center" variant="h6" gutterBottom>
                    Transférer de l'argent
                </Typography>

                <DialogContent>
                    <Box display="flex" flexDirection="column">

                        <Box className={classes.margin} display="flex" flexDirection="column" flexGrow={1}>
                            <TextField
                                type="number"
                                id="amount"
                                name="amount"
                                label="Montant"
                                value={amount}
                                onChange={this.handleChange('amount')}
                                className={classes.margin}
                                InputProps={{
                                    endAdornment: allowCancel && (
                                        <InputAdornment position="end">
                                            <IconButton aria-label="Changer le signe" onClick={() => this.setState({ amount: -1 * parseFloat(amount) })}>
                                                <SwapHorizontal />
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />

                            <TextField
                                type="text"
                                id="description"
                                name="description"
                                label="Description"
                                value={description}
                                onChange={this.handleChange('description')}
                                className={classes.margin}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <Typography color="textSecondary">{description && description.length}/100</Typography>
                                        </InputAdornment>
                                    )
                                }}
                            />

                            <SearchUser
                                id="search"
                                to={to}
                                searchInput={searchInput}
                                label="Destinataire"
                                selected={selected}
                                component={TextField}
                                onChange={searchInput => this.setState({ searchInput })}
                                onSelection={selected => this.setState({ selected })}
                                onClearRequested={this.onClearRequested}
                            />
                        </Box>

                        <Button
                            type="submit"
                            startIcon={<Send />}
                            onClick={this.handleSubmit}
                            disabled={submitting}
                            variant="contained"
                            color="primary"
                            size="large"
                            aria-label="Envoyer"
                        >
                            {submitting ? <CircularProgress size={25} color="secondary" /> : "Envoyer"}
                        </Button>
                    </Box>
                </DialogContent>
            </form>
        );
    };
}));