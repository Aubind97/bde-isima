import React from 'react';
import { withRouter } from 'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import Close from 'mdi-material-ui/Close';
import DialogContent from '@material-ui/core/DialogContent';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Magnify from 'mdi-material-ui/Magnify';
import InputBase from '@material-ui/core/InputBase';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme, makeStyles } from '@material-ui/core';
import { fade } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Fade from '@material-ui/core/Fade';
import CardActionArea from '@material-ui/core/CardActionArea';
import User from 'components/Authenticated/Auth/Components/User';
import { useLoads } from 'react-loads';
import InfiniteList from './InfiniteList';
import { CRUD } from 'utils';

const LIMIT = 10;

const useStyles = makeStyles(theme => ({
    grow: {
        flexGrow: 1,
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(9),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 10),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    dialogContent: {
        padding: 0
    }
}));

export default React.memo(withRouter(function AllNews({ isOpen, handleOpen, history }) {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [input, setInput] = React.useState('');

    /**
    * Retrieve the current page of items
    */
    const loadItems = React.useCallback((prevArray = [], startCursor = 0) => {
        const promotion = User.getData('promotion');
        return CRUD.get({
            url: `/news`,
            params: {
                offset: startCursor,
                limit: LIMIT,
                promotion: promotion ? `${promotion.year}` : null,
                orderBy: 'published_at',
                orderDirection: 'desc',
                noFuture: true,
                is_published: true,
            }
        }).then(res => prevArray.concat(res.data));
    }, []);

    /**
    * Retrieve the total number of transactions for a user
    */
    const getCount = React.useCallback(() => {
        const promotion = User.getData('promotion');
        return CRUD.get({
            url: `/news/count`,
            params: {
                promotion: promotion ? `${promotion.year}` : null,
                orderBy: 'published_at',
                orderDirection: 'desc',
                noFuture: true,
                is_published: true,
            }
        });
    }, []);
    const { response, isResolved } = useLoads(getCount);

    const handleSearchChange = event => setInput(event.target.value);

    return (
            <Dialog
                open={isOpen}
                TransitionComponent={Fade}
                fullScreen={fullScreen}
                onClose={handleOpen(false)}
                scroll="paper"
            >
                <AppBar position="static" color="inherit">
                    <Toolbar>
                        <Typography className={classes.title} variant="h6">
                            Toutes les news
                        </Typography>
                        <div className={classes.grow} />
                        <div className={classes.search}>
                            <div className={classes.searchIcon}>
                                <Magnify />
                            </div>
                            <InputBase
                                placeholder="Rechercher une news"
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput
                                }}
                                value={input}
                                onChange={handleSearchChange}
                            />
                        </div>
                        <div style={{ textAlign: "right" }}>
                        <IconButton onClick={handleOpen(false)} aria-label="Fermer" color="inherit">
                                <Close />
                            </IconButton>
                        </div> 
                    </Toolbar>
                </AppBar>   

                <DialogContent className={classes.dialogContent}>     
                    {isResolved && (
                        <InfiniteList
                            loadItems={loadItems}
                            renderer={item => (
                                <CardActionArea key={item.id} onClick={() => history.push(`/hub/news/${item.id}`)}>
                                    <ListItem>
                                        <ListItemText primary={item.title} secondary={item.header} />
                                    </ListItem>
                                    <Divider />
                                </CardActionArea>
                            )}
                        comparator={item => item.title.toLowerCase().match(input.toLowerCase())}
                            count={response.data.count}
                        />
                    )}
                </DialogContent>
            </Dialog>
        );
}));
