import React from 'react';
import { withRouter } from 'react-router-dom';
import { Loads } from 'react-loads';
import { parseISO, isSameDay } from 'date-fns';
import Skeleton from '@material-ui/lab/Skeleton';
import { withStyles, CircularProgress } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Check from 'mdi-material-ui/Check';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import DotsVertical from 'mdi-material-ui/DotsVertical';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import { CRUD, Image, DateUtils } from 'utils';

const styles = theme => ({
    eventsItems: {
        flexBasis: '25%',
        minHeight: 100,
        border: '1px solid rgb(235, 235, 235)',
        [theme.breakpoints.down('md')]: {
            flexBasis: '50%',
            maxWidth: '50%',
        },
    },
    divider: {
        width: '40%',
        margin: theme.spacing(2, 0),
    },
    subHeader: {
        width: '100%', 
        textAlign: 'left',
        margin: theme.spacing(2, 0),
    },
    container: {
        padding: theme.spacing(1),
        minHeight: 216,
    },
    margin: {
        margin: theme.spacing(2),
    },
    leftIcon: {
        marginRight: theme.spacing(1),
    },
    chips: {
        margin: theme.spacing(.5, 0),
    },
    chipsLabel: {
        maxWidth: 100,
        [theme.breakpoints.down('md')]: {
            maxWidth: 40,
        },
    },
    chipsAvatar: {
        [theme.breakpoints.down('md')]: {
            width: 30,
            height: 30,
        },
    },
});

export default withRouter(withStyles(styles)(class Upcoming extends React.Component {

    getEvents = () => CRUD.get({
        url: `/events`,
        params: {
            orderBy: 'subscriptions_end_at',
            orderDirection: 'asc',
            statusIn: [1]
        }
    });

    getAllSubscriptions = data => () => axios.all(data.map(event => CRUD.get({
        url: `/subscriptions/event/${event.id}`,
    })));

    getClub = id => () => CRUD.get({ url: `/clubs/${id}` });

    getNextSevenDays = startDate => [...Array(7).keys()].map(i => new Date(new Date().setDate(startDate.getDate() + i)));

    render() {
        const { classes, history } = this.props;
        return (
            <Box className={classes.margin} display="flex" flexDirection="column">
                <Typography align="left" variant="h6">
                    Évènements à venir
                </Typography>

                <Divider className={classes.divider}/>
                
                <Grid container> 

                    <Grid container className={classes.container} component={Paper} >

                        <Loads load={this.getEvents}>
                            {({ response, isResolved, isPending, isRejected }) => (
                                <>
                                    {(isPending || isRejected) && (
                                        <Grid container item xs={12} justify="center" alignItems="center">
                                            <CircularProgress size={25} color="inherit" />
                                        </Grid>
                                    )}

                                    {isResolved && (
                                        <Loads load={this.getAllSubscriptions(response.data)}>
                                            {({ response: request, isResolved, isPending, isRejected }) => (
                                                <>
                                                    {(isPending || isRejected) && (
                                                        <Grid container item xs={12} justify="center" alignItems="center">
                                                            <CircularProgress size={25} color="primary" />
                                                        </Grid>
                                                    )}

                                                    {isResolved && this.getNextSevenDays(new Date()).map((date, idx) => (
                                                        <React.Fragment key={idx}>
                                                            <Grid className={classes.eventsItems} item container alignContent="flex-start">

                                                                <Grid item container xs={12} justify="center">
                                                                    <Typography variant="subtitle1" align="center" gutterBottom>
                                                                        {`${date.toLocaleString('fr-FR', { weekday: 'short' }).toUpperCase()} ${date.getDate().toString().padStart(2, '0')}/${(date.getMonth() + 1).toString().padStart(2, '0')}`}
                                                                    </Typography>
                                                                </Grid>

                                                                {response.data.map((event, eventIdx) => (
                                                                    isSameDay(parseISO(date.toISOString()), DateUtils.withOffsetDate(event.takes_place_at)) && (
                                                                        <Grid key={eventIdx} item container xs={12} justify="center">
                                                                            <Chip
                                                                                className={classes.chips}
                                                                                label={<Tooltip title={event.name}><Typography className={classes.chipsLabel} noWrap>{event.name.substring(0, 10)}</Typography></Tooltip>}
                                                                                color="default"
                                                                                variant="outlined"
                                                                                deleteIcon={Object.getOwnPropertyNames(request[eventIdx].data).length > 0 ? <Tooltip title="Tu es bien inscrit"><Check /></Tooltip> : undefined}
                                                                                onDelete={Object.getOwnPropertyNames(request[eventIdx].data).length > 0 ? (() => undefined) : undefined}
                                                                                avatar={(
                                                                                    <Loads load={this.getClub(event.club_id)}>
                                                                                        {({ response, isRejected, isPending, isResolved }) => (
                                                                                            <>
                                                                                                {(isPending || isRejected) && <Skeleton variant="circle" width={40} height={40} />}
                                                                                                {isResolved && (
                                                                                                    <Avatar
                                                                                                        className={classes.chipsAvatar}
                                                                                                        src={Image.display(response.data.logo)}
                                                                                                        alt={`Logo ${response.data.name}`}
                                                                                                    />
                                                                                                )}
                                                                                            </>
                                                                                        )}
                                                                                    </Loads>
                                                                                )}
                                                                                onClick={() => this.props.history.push(`/hub/events/${event.id}`)}
                                                                            />
                                                                        </Grid>
                                                                    )
                                                                ))}

                                                            </Grid>
                                                        </React.Fragment>
                                                    ))}
                                                </>
                                            )}
                                        </Loads>
                                    )}
                                </>
                            )}
                        </Loads>

                    </Grid>
                    
                    <ListSubheader className={classes.subHeader}>
                        <Tooltip title="Plus">
                            <IconButton className={classes.leftIcon} aria-label="Plus" onClick={() => history.push(`/hub/events`)}>             
                                <DotsVertical />
                            </IconButton>   
                        </Tooltip>
                        Tous les évènements       
                    </ListSubheader>

                </Grid>
            </Box>
        );         
    };
}));
