import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';
import { useLoads } from 'react-loads';
import Skeleton from '@material-ui/lab/Skeleton';
import { CRUD, UserAvatar } from 'utils';

const useStyles = makeStyles(theme => ({
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: '#2A2E43',
    },
}));

/**
 * This component is responsible for displaying user's avatar in a Transaction component
 */
export default function TransactionAvatar({ data, userId }) {

    /** States initialisation */
    const classes = useStyles();
    
    /**
    * Retrieve user's information
    */
    const getUser = React.useCallback(() => CRUD.get({ 
        url: `/users/compact/${userId}` })
    , [userId]);
    const { response: user, isResolved, isPending, isRejected } = useLoads(getUser);

    return (
        <>
            {(isPending || isRejected) && <Skeleton variant="circle" width={40} height={40} />}

            {isResolved && (
                <UserAvatar
                    data={user.data}
                    AvatarProps={{
                        className: classes.avatar,
                        alt: "Avatar utilisateur de l'émetteur"
                    }}
                />
            )}
        </>
    );
};

TransactionAvatar.propTypes = {
    /** User data containing the user's profile picture */
    data: PropTypes.object,
    /** User ID from which fetch transactions */
    userId: PropTypes.number,
};
