import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core';
import User from 'components/Authenticated/Auth/Components/User';

const useStyles = makeStyles(() => ({
    green: { color: '#27ae60' },
    red: { color: '#C91F37' },
}));

/**
 * This component is responsible for displaying any user balance
 */
export default function Balance({ balance, variant }) {

    /** States initialisation */
    const classes = useStyles();
    const [amount, setAmount] = React.useState(balance || '?');

    const setBalance = () => setAmount(parseFloat(User.getData('balance')).toFixed(2));

    const positive = val => val >= 0;

    const signed = val => positive(val) ? `+${val}` : val;

    //By default, when component did mount, request current user balance 
    React.useEffect(() => {
        if (balance == null) {
            setBalance();
        }
        else {
            setAmount(parseFloat(balance).toFixed(2));
        }
        window.addEventListener('onUserChange', setBalance);

        //Finally, when component will unmount, cleanup listener
        return function cleanup() {
            window.removeEventListener('onUserChange', setBalance);
        }
    }, [balance]);

    return (
        <Typography
            className={positive(amount) ? classes.green : classes.red}
            variant={variant}
            align="center"
            gutterBottom
        >
            {signed(amount)} €
        </Typography>
    );
};
