import React from 'react';
import TimeAgo from 'react-timeago';
import frenchStrings from 'react-timeago/lib/language-strings/fr';
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';
import { withRouter } from 'react-router-dom';
import { Loads } from 'react-loads';
import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import DotsVertical from 'mdi-material-ui/DotsVertical';
import { Share, CRUD, DateUtils } from 'utils';
import AllNews from './AllNews';
import User from 'components/Authenticated/Auth/Components/User';
import Skeleton from '@material-ui/lab/Skeleton';

const formatter = buildFormatter(frenchStrings);

const styles = theme => ({
    container: {
        margin: theme.spacing(2),
        [theme.breakpoints.down('sm')]: {
            order: 4
        }
    },
    divider: {
        width: '40%',
        margin: theme.spacing(2, 0, 2, 0)
    },
    subHeader: {
        width: '100%',
        textAlign: 'left'
    },
    leftIcon: {
        marginRight: theme.spacing(1)
    },
    cardArchives: {
        maxWidth: 345,
        textAlign: 'left'
    },
    cardContent: {
        height: 125
    },
});

export default withRouter(withStyles(styles)(class RecentNews extends React.Component {

    state = {
        isRecentNewsOpen: false,
    };

    handleRecentNewsOpen = isRecentNewsOpen => () => {
        this.setState({ isRecentNewsOpen });
    };

    redirect = id => this.props.history.push(`/hub/news/${id}`);

    getNews = () => {
        const promotion = User.getData('promotion');
        return CRUD.get({
            url: `/news`,
            params: {
                offset: 0,
                limit: 4,
                promotion: promotion ? `${promotion.year}` : null,
                orderBy: 'published_at',
                orderDirection: 'desc',
                noFuture: true,
                is_published: true,
            }
        })
    }; 

    render() {
        const { classes } = this.props;
        const { isRecentNewsOpen } = this.state;
        
        return (
            <Box display="flex" className={classes.container} flexDirection="column">
                <Typography align="left" variant="h6">
                    News récentes
                </Typography>

                <Divider className={classes.divider}/>

                <div style={{ padding: '20px 0px' }}>
                    <Grid container spacing={4} justify="flex-start">

                        <Loads load={this.getNews}>
                            {({ response, isRejected, isPending, isResolved }) => (
                                <>
                                    { (isPending || isRejected) && (
                                        [...Array(4).keys()].map(idx =>
                                            <Grid key={idx} item xs={12} md={6}>
                                                <Card className={classes.cardArchives} >
                                                    <CardActionArea className={classes.cardContent}>
                                                        <CardContent style={{ height: '100%' }}>
                                                            <Skeleton width="90%" height={24} />
                                                            <Skeleton width="80%" height={12} />
                                                            <Skeleton width="50%" height={12} />
                                                        </CardContent>
                                                    </CardActionArea>
                                                    <CardActions>
                                                        <Grid container item xs={12}>
                                                            <Grid container item xs={6} justify="flex-start">
                                                                <Share />
                                                            </Grid>
                                                            <Grid container item xs={6} justify="flex-end">
                                                                <Skeleton width="60%" height={12} />
                                                            </Grid>
                                                        </Grid>
                                                    </CardActions>
                                                </Card>
                                            </Grid>
                                        ))}

                                    {(isResolved && response.data.length === 0) && (
                                        <Box
                                            display="flex"
                                            justifyContent="center"
                                            alignItems="center"
                                            flexDirection="column"
                                            flexGrow={1}
                                        >
                                            <img
                                                src="/images/illustrations/NoData.svg"
                                                height="auto"
                                                width={300}
                                                alt="Aucune donnée"
                                            />
                                            <Typography
                                                variant="subtitle1"
                                                gutterBottom
                                            >
                                                Aucune news publiée !
                                            </Typography>
                                        </Box>
                                    )}

                                    {(isResolved && Array.isArray(response.data)) && response.data.slice(0, response.data.length).map(post =>
                                        <Grid key={post.id} item xs={12} md={6}>
                                            <Card className={classes.cardArchives} >
                                                <CardActionArea className={classes.cardContent} onClick={() => this.redirect(post.id)}>
                                                    <CardContent style={{ height: '100%' }}>
                                                        <Typography gutterBottom variant="h6" noWrap>
                                                            {post.title}
                                                        </Typography>
                                                        <Typography variant="body2" component="p" color="textSecondary" paragraph>
                                                            {post.header}
                                                        </Typography>
                                                    </CardContent>
                                                </CardActionArea>
                                                <CardActions>
                                                    <Share link={`https://bde.isima.fr/hub/news/${post.id}`} />
                                                    <Typography style={{ width: "100%" }} align="right" variant="caption" component="p" noWrap>
                                                        <TimeAgo date={DateUtils.withOffsetDate(post.published_at)} formatter={formatter} />
                                                    </Typography>
                                                </CardActions>
                                            </Card>
                                        </Grid>
                                    )}
                                </>               
                            )}
                        </Loads> 

                        <ListSubheader className={classes.subHeader}>
                            <Tooltip title="Plus">
                                <IconButton className={classes.leftIcon} aria-label="Plus" onClick={this.handleRecentNewsOpen(true)}>
                                    <DotsVertical />
                                </IconButton>
                            </Tooltip>
                            Toutes les news
                            <AllNews isOpen={isRecentNewsOpen} handleOpen={this.handleRecentNewsOpen} />
                        </ListSubheader>
                    </Grid>
                </div>
            </Box>
        );         
    };
}));
