import React from 'react';
import PropTypes from 'prop-types';
import { useLoads } from 'react-loads';
import { makeStyles, Grid } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import User from 'components/Authenticated/Auth/Components/User';
import InfiniteList from './InfiniteList';
import Transaction from './Transaction';
import { Picker, CRUD, DateUtils } from 'utils';

const LIMIT = 10;

const useStyles = makeStyles(theme => ({
    margin: {
        margin: theme.spacing(1),
    },
}));

/**
 * This component is responsible for displaying user's history of transactions
 */
export default React.memo(function History({ userId, dense, load = true, visible }) {

    /** States initialisation */
    const classes = useStyles();
    const [startDate, setStartDate] = React.useState(new Date('1970/01/01').setHours(0, 0, 0, 0));
    const [endDate, setEndDate] = React.useState(new Date().setHours(0, 0, 0, 0));

    /**
    * Handle date change
    */
    const handleDateChange = React.useCallback(setter => date => setter(date), []);

    /**
    * Reset date
    */
    const resetDate = React.useCallback((setter, date) => () => setter(date), []);

    /**
    * Retrieve the total number of transactions for a user
    */
    const getCount = React.useCallback(() => CRUD.get({ url: `/transactions/${userId}/count` }), [userId]);
    const { response, update, isResolved } = useLoads(getCount, {
        update: getCount,
    });

    const loadItems = React.useCallback((prevArray = [], startCursor = 0) => {
        return CRUD.get({
            url: `/transactions/${userId}`,
            params: {
                offset: startCursor,
                limit: LIMIT
            },
        }).then(res => prevArray.concat(res.data));
    }, [userId]);

    const onUpdateRequested = React.useCallback(() => {
        if (load) update();
    }, [update, load]);

    React.useEffect(() => {
        update();
        
        /**
        * Subscribe to transaction made event to update whenever an transaction is executed
        */
        window.addEventListener('onTransactionMade', onUpdateRequested);

        /**
        * Unsubscribe from transaction made event
        */
        return function cleanup() {
            window.removeEventListener('onTransactionMade', onUpdateRequested);
        };
    }, [userId, update, onUpdateRequested]);

    return (
        <Box display="flex" flexDirection="column" alignItems="center">

            <Typography variant="h6" align="center" gutterBottom>
                Historique des transactions
            </Typography>

            <Box display="flex" justifyContent="center">
                <Picker
                    className={classes.margin}
                    label="De"
                    handleChange={handleDateChange(setStartDate)}
                    date={startDate}
                    onClear={resetDate(setStartDate, new Date('1970/01/01').setHours(0, 0, 0, 0))}
                />

                <Picker
                    className={classes.margin}
                    label="À"
                    handleChange={handleDateChange(setEndDate)}
                    date={endDate}
                    onClear={resetDate(setEndDate, new Date().setHours(0, 0, 0, 0))}
                    disableFuture
                />
            </Box>

            <Grid className={dense ? undefined : classes.container} container>

                <Grid container item xs={2} justify="center" alignContent="center">
                    <Typography variant="caption" align="center" gutterBottom color="textSecondary">
                        Débit/Crédit
                    </Typography>
                </Grid>

                <Grid container item xs={6} justify="center" alignContent="center">
                    <Typography variant="caption" align="center" gutterBottom color="textSecondary">
                        Description et date
                    </Typography>
                </Grid>

                <Grid container item xs={2} justify="center" alignItems="center">
                    <Typography variant="caption" align="center" gutterBottom color="textSecondary">
                        Montant
                    </Typography>
                </Grid>

                <Grid container item xs={2} justify="center" alignItems="center">
                    <Typography variant="caption" align="center" gutterBottom color="textSecondary">
                        Ancien solde
                    </Typography>
                </Grid>

            </Grid>

            {(load && isResolved) && (
                <InfiniteList
                    loadItems={loadItems}
                    renderer={item => <Transaction key={item.id} data={item} userId={userId} dense={dense} />}
                    comparator={item => startDate <= DateUtils.withOffsetDate(item.created_at).setHours(0, 0, 0, 0) && DateUtils.withOffsetDate(item.created_at).setHours(0, 0, 0, 0) <= endDate}
                    count={response.data.count}
                    visible={visible}
                />
            )}
            
        </Box>
    );
});

History.propTypes = {
    /** User ID from which fetch transactions */
    userId: PropTypes.number,
    /** Rather or not to reduce size of UI */
    dense: PropTypes.bool,
};

History.defaultProps = {
    /** Default props for the user id if not specified fall back to current user */
    userId: User.getData('id'),
};
