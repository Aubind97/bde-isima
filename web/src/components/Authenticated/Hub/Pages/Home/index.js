import React from 'react';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import CubeSend from 'mdi-material-ui/CubeSend';
import { Loads } from 'react-loads';
import { withStyles, Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Card from '@material-ui/core/Card';
import IconButton from '@material-ui/core/IconButton';
import DialogActions from '@material-ui/core/DialogActions';
import Close from 'mdi-material-ui/Close';
import History from 'mdi-material-ui/History';
import Dialog from '@material-ui/core/Dialog';
import Fade from '@material-ui/core/Fade';
import Skeleton from '@material-ui/lab/Skeleton';
import DialogContent from '@material-ui/core/DialogContent';
import Container from '@material-ui/core/Container';
import RecentNews from './Components/RecentNews';
import Transaction from './Components/Transaction';
import MyHistory from './Components/History';
import Transfer from './Components/Transfer';
import User from '../../../Auth/Components/User';
import Balance from './Components/Balance';
import Upcoming from './Components/Upcoming';
import Guzlr from './Components/Guzlr';
import { CRUD } from 'utils';

const styles = theme => ({
    root: {
        marginTop: 70 + theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    group: {
        margin: theme.spacing(2)
    },
    recentNews: {
        maxWidth: '100%',
        margin: theme.spacing(2),
        [theme.breakpoints.down('md')]: {
            order: 2
        }
    },
    aside: {
        margin: theme.spacing(2)
    },
    content: {
        height: '100%'
    },
    show: {
        marginLeft: theme.spacing(2),
    },
    card: {
        padding: theme.spacing(3, 2),        
    },
    skeleton: {
        margin: theme.spacing(.5)
    }
});

export default withMobileDialog()(withStyles(styles)(class extends React.Component {
    state = {
        isTransferDialogOpen: false,
        isHistoryDialogOpen: false,
    };

    handleDialogOpen = (name, open) => () => this.setState({ [name]: open });

    getTransactions = () => CRUD.get({ 
        url: `/transactions/${User.getData('id')}`,
        params: {
            offset: 0,
            limit: 10
        }
    });

    render() {
        const { classes, fullScreen } = this.props;
        const { isTransferDialogOpen, isHistoryDialogOpen } = this.state;
        return (
            <Container className={classes.root} fixed>
                <Grid container justify="center">

                    <Grid className={classes.recentNews} item lg={8} md={12}>
                        <Guzlr />
                        <Upcoming />
                        <RecentNews />
                    </Grid>

                    <Grid className={classes.aside} container item lg={3} md={12} justify="center" alignItems="flex-start">
                        <Card className={classes.card}>

                            <Box display="flex" className={classes.content} flexDirection="column" justifyContent="center" alignItems="center">

                                <Balance variant="h3" />

                                <Typography variant="subtitle2" color="textSecondary" gutterBottom>
                                    Solde carte n°{User.getData('card')}
                                </Typography>
                                
                                <Loads load={this.getTransactions}>
                                    {({ response, isRejected, isPending, isResolved }) => (
                                        <>
                                            {(isPending || isRejected) && [...Array(10).keys()].map(x => <Skeleton className={classes.skeleton} key={x} height={12} width="80%" />)}
                                            {isResolved && response.data.map((x, idx) => <Transaction key={x.id} data={x} idx={idx} dense /> )}
                                        </>
                                    )}
                                </Loads>

                                <ButtonGroup className={classes.group} aria-label="Groupe de bouton pour le transfert d'argent et consulter son historique">
                                    <Button
                                        variant="outlined"
                                        startIcon={<CubeSend />}
                                        aria-label="Transférer"
                                        onClick={this.handleDialogOpen('isTransferDialogOpen', true)}
                                    >
                                        Envoyer
                                </Button>
                                    <Button
                                        variant="outlined"
                                        startIcon={<History />}
                                        className={classes.show}
                                        aria-label="Voir l'historique"
                                        onClick={this.handleDialogOpen('isHistoryDialogOpen', true)}
                                    >
                                        Historique
                                </Button>
                                </ButtonGroup>

                            </Box>

                        </Card>

                        <Dialog
                            TransitionComponent={Fade}
                            fullScreen={fullScreen}
                            open={isTransferDialogOpen}
                            onClose={this.handleDialogOpen('isTransferDialogOpen', false)}
                        >
                            <DialogActions align="right">
                                <IconButton onClick={this.handleDialogOpen('isTransferDialogOpen', false)} aria-label="Fermer">
                                    <Close />
                                </IconButton>
                            </DialogActions>
                            <DialogContent>
                                <Transfer />
                            </DialogContent>
                        </Dialog>

                        <Dialog
                            TransitionComponent={Fade}
                            fullScreen={fullScreen}
                            open={isHistoryDialogOpen}
                            onClose={this.handleDialogOpen('isHistoryDialogOpen', false)}
                        >
                            <DialogActions align="right">
                                <IconButton onClick={this.handleDialogOpen('isHistoryDialogOpen', false)} aria-label="Fermer">
                                    <Close />
                                </IconButton>
                            </DialogActions>
                            <DialogContent>
                                <MyHistory userId={User.getData('id')} />
                            </DialogContent>
                        </Dialog>

                    </Grid>
                </Grid>
            </Container>          
        );
    }
}));
