import React from 'react';
import { withStyles } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Input from '@material-ui/core/Input';

const styles = theme => ({
    formControl: {
        margin: theme.spacing(3)
    },
    input: {
        margin: theme.spacing(.5, 0)
    }
});

export default withStyles(styles)(class CommentSection extends React.Component {

    render() {
        const { classes, product, handleCommentChange } = this.props;
        return (
            <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">Commentaires</FormLabel>

                <Input
                    className={classes.input}
                    placeholder="(Allergies, demandes particulières)"
                    multiline
                    rows={5}
                    defaultValue={product.comment}
                    onChange={handleCommentChange}
                />

            </FormControl>
        );
    };
});
