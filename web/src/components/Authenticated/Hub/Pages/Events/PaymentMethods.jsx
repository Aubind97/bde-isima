import React from 'react';
import Button from '@material-ui/core/Button';
import CashMultiple from 'mdi-material-ui/CashMultiple';
import Cellphone from 'mdi-material-ui/Cellphone';
import CardBulleted from 'mdi-material-ui/CardBulleted';
import Typography from '@material-ui/core/Typography';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    group: {
        width: '100%',
    },
}));    

/**
 * This component is responsible for displaying different payment methods available
 */
export default function PaymentMethods({ subscription, handlePaymentMethodChange }) {

    /** States initialisation */
    const classes = useStyles();

    return (
        <ButtonGroup className={classes.group} aria-label="Méthodes de paiement">
            <Tooltip title="Payer par Lydia">
                <Button
                    className={classes.group}
                    startIcon={<Cellphone />}
                    aria-label="Payer par Lydia"
                    variant={subscription.payment_method === 'lydia' ? "contained" : "outlined"}
                    onClick={handlePaymentMethodChange('lydia')}
                    color={subscription.payment_method === 'lydia' ? "primary" : "inherit"}
                >
                    <Typography variant="caption">Lydia</Typography>
                </Button>
            </Tooltip>

            <Tooltip title="Payer par carte BDE">
                <Button
                    className={classes.group}
                    startIcon={<CardBulleted />}
                    aria-label="Payer par carte BDE"
                    variant={subscription.payment_method === 'bde' ? "contained" : "outlined"}
                    onClick={handlePaymentMethodChange('bde')}
                    color={subscription.payment_method === 'bde' ? "primary" : "inherit"}
                >
                    <Typography variant="caption">BDE</Typography>
                </Button>
            </Tooltip>

            <Tooltip title="Payer par espèces">
                <Button
                    className={classes.group}
                    startIcon={<CashMultiple />}
                    aria-label="Payer par espèces"
                    variant={subscription.payment_method === 'cash' ? "contained" : "outlined"}
                    onClick={handlePaymentMethodChange('cash')}
                    color={subscription.payment_method === 'cash' ? "primary" : "inherit"}
                >
                    <Typography variant="caption">Espèces</Typography>
                </Button>
            </Tooltip>
        </ButtonGroup>
    );
};
