import React, { useState } from 'react';
import { useTheme, useMediaQuery, makeStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import CartPlus from 'mdi-material-ui/CartPlus';
import PlusCircleOutline from 'mdi-material-ui/PlusCircleOutline';
import MinusCircleOutline from 'mdi-material-ui/MinusCircleOutline';
import CommentSection from './CommentSection';
import Options from './Options';

const useStyles = makeStyles(() => ({
    dialog: {
        zIndex: '3100 !important',
    },
    dialogContent: {
        display: 'flex',
        flexDirection: 'column',
    },
}));

/**
 * This component is responsible for selecting options in a dialog and displaying relative information
 */
export default function SelectOptions({ product, options, handleAddToCart, handleClose }) {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [selected, setSelected] = useState([]);
    const [quantity, setQuantity] = useState(1);

    const handleOptionsChange = (option, idx) => {
        selected[idx] = option;
        setSelected(selected => [...selected]);
    };

    const total = (
        quantity * 
        (product ? parseFloat(product.price) : 0) +
        (selected.length ? selected.reduce((acc, val) => acc + (val ? val.reduce((accu, value) => accu + parseFloat(value.price), 0) : 0), 0) : 0)
    ).toFixed(2);

    const handleCommentChange = event => Object.assign(product, { comment: event.target.value });

    const handleQuantityChange = value => () => {
        if (quantity + value > 0) setQuantity(quantity => quantity + value);
    };

    const handleAddOptions = () => {
        handleAddToCart({
            name: product.name,
            options: selected.flat(2).filter(x => Boolean(x)).map(x => x.name).join(', '),
            price: total,
            quantity,
            comment: product.comment
        });
        setSelected([]);
        handleClose();
    };

    return (
        product && (
            <Dialog
                className={classes.dialog}
                open={Boolean(product)}
                fullScreen={fullScreen}
                onClose={handleClose}
                aria-labelledby="alert-dialog-select-options-title"
                aria-describedby="alert-dialog-select-options-description"
            >
                <DialogTitle id="alert-dialog-select-options-title">{product.name}</DialogTitle>

                <DialogContent className={classes.dialogContent}>

                    {(options && options.options) && (
                        <>
                            {options.options.map((option, optionIdx) => (
                                <Options
                                    key={optionIdx}
                                    idx={optionIdx}
                                    option={option}
                                    handleOptionChange={handleOptionsChange}
                                />
                            ))}

                            <CommentSection
                                product={product}
                                handleCommentChange={handleCommentChange}
                            />
                        </>
                    )}

                </DialogContent>

                <DialogActions>
                    <Box display="flex" justifyContent="center" alignItems="center" flexGrow={1}>
                        <IconButton onClick={handleQuantityChange(-1)} aria-label="Retirer 1">
                            <MinusCircleOutline />
                        </IconButton>
                        <Typography variant="subtitle1" color="textSecondary">
                            {quantity}
                        </Typography>
                        <IconButton onClick={handleQuantityChange(1)} aria-label="Ajouter 1">
                            <PlusCircleOutline />
                        </IconButton>
                    </Box>
                </DialogActions>

                <DialogActions>
                    <Button aria-label="Annuler" onClick={handleClose} color="inherit">
                        Annuler
                     </Button>
                    <Button
                        onClick={handleAddOptions}
                        startIcon={<CartPlus />}
                        variant="contained"
                        color="primary"
                        aria-label={`Ajouter pour un total de ${total}`}
                        autoFocus
                    >
                        Total {total} €
                    </Button>
                </DialogActions>
            </Dialog>
        )
    );
};
