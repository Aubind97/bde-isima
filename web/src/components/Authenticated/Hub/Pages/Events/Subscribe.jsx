import React from 'react';
import axios from 'axios';
import { Loads } from 'react-loads';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/core';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import CardHeader from '@material-ui/core/CardHeader';
import Skeleton from '@material-ui/lab/Skeleton';
import IconButton from '@material-ui/core/IconButton';
import Close from 'mdi-material-ui/Close';
import CloseOutline from 'mdi-material-ui/CloseOutline';
import User from 'components/Authenticated/Auth/Components/User';
import Product from './Product';
import Cart from './Cart';
import MobileCart from './MobileCart';
import SelectOptions from './SelectOptions';
import { withSnackbar } from 'notistack';
import { withRouter } from 'react-router-dom';
import { CRUD, Image, DateUtils } from 'utils';
import { format } from 'date-fns';

const styles = theme => ({
    root: {
        textAlign: 'left',
        padding: theme.spacing(2),
        marginTop: 70 + theme.spacing(3),
        marginBottom: 64 + theme.spacing(3),
    },
    divider: {
        margin: theme.spacing(1, 0, 1, 0)
    },
    avatar: {
        width: 100,
        height: 100,
    },
    list: {
        marginBottom: theme.spacing(5)
    },
    leftIcon: {
        marginRight: theme.spacing(1)
    },
});

export default withSnackbar(withRouter(withMobileDialog()(withStyles(styles)(class Subscribe extends React.Component {
    state = {
        selected: null,
        event: null,
        options: null,
        subscribing: false,
        unsubscribing: false,
        club: null,
    };

    getClub = id => CRUD.get({ url: `/clubs/${id}` }).then(result => this.setState({ club: result.data }));

    getEvent = () => CRUD.get({ url: `/events/${this.props.match.params.id}` }).then(result => {
        this.getClub(result.data.club_id);
        this.setState({ event: result.data });
        return Promise.resolve(result);
    });

    getSubscriptions = () => CRUD.get({ url: `/subscriptions/event/${this.props.match.params.id}` })
        .then(result => Promise.resolve(Array.isArray(result.data.cart) ? result : { ...result, data: { ...result.data, ...{ payment_method: 'bde', cart: [] }} }));

    handleSelectedChange = (selected, options) => () => this.setState({ selected, options });

    handleSubmit = newData => e => {
        const { event } = this.state;
        this.setState({ subscribing: true });

        if (event.products.length === 0 || (event.products.length > 0 && newData.cart.length > 0)) {
            if (newData.id && newData.event_id && newData.user_id) {
                CRUD.update({
                    url: `/subscriptions/${newData.id}`,
                    newData,
                    onSucceed: () => {
                        this.resetState();
                        this.props.history.push('/hub/events');
                    },
                    onRejection: this.resetState,
                    props: this.props
                });
            }
            else {
                CRUD.add({
                    url: `/subscriptions`,
                    newData: {
                        event_id: this.props.match.params.id,
                        user_id: User.getData('id'),
                        payment_method: newData.payment_method,
                        cart: newData.cart
                    },
                    onSucceed: () => {
                        this.resetState();
                        this.props.history.push('/hub/events');
                    },
                    onRejection: this.resetState,
                    props: this.props
                });
            }
        }
        else {
            this.props.enqueueSnackbar('Votre panier est vide', {
                preventDuplicate: true,
                variant: 'error',
                anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'right',
                },
                action: key => (
                    <IconButton aria-label="Fermer" color="secondary" onClick={() => this.props.closeSnackbar(key)}>
                        <Close />
                    </IconButton>
                )
            });
            this.setState({ subscribing: false });
        }
    };

    handleUnsubscribe = data => () => {
        this.setState({ unsubscribing: true });

        if (data.id) {
            CRUD.delete({
                url: `/subscriptions/${data.id}`,
                onSucceed: () => {
                    this.resetState();
                    this.props.history.push('/hub/events');
                },
                onRejection: this.resetState,
                props: this.props
            });
        }
    };

    handleQuantityChange = cart => (item, quantity) => () => {
        const newQt = parseInt(item.quantity) + parseInt(quantity); 
        if (newQt > 0) {
            item.price = (newQt * (parseFloat(item.price) / parseFloat(item.quantity))).toFixed(2);
            item.quantity = newQt;
        }
        else
            cart.splice(cart.findIndex(x => x === item), 1);
        this.forceUpdate();
    };

    handleAddToCart = cart => item => {
        const exist = cart.find(x => `${x.name} ${x.options}` === `${item.name} ${item.options}`);
        if (exist) {
            const total_price = parseFloat(exist.price);
            const quantity = parseInt(exist.quantity);
            const newQuantity = quantity + parseInt(item.quantity);

            const unit_price = total_price / quantity;
            
            exist.price = (unit_price * newQuantity).toFixed(2);
            exist.quantity = newQuantity;

            if (exist.comment !== item.comment) {
                exist.comment = item.comment;
            }
        }
        else {
            cart.push(item);
        }
        this.forceUpdate();
    };

    resetState = () => {
        this.setState({ subscribing: false, unsubscribing: false });
    };

    redirectTo = url => () => this.props.history.push(url);

    render() {
        const { classes, fullScreen } = this.props;
        const { selected, options, subscribing, unsubscribing, club } = this.state;

        return (
            <Container className={classes.root} fixed>

                <Loads load={() => axios.all([this.getEvent(), this.getSubscriptions()])}>
                    {({ response, isResolved, isPending }) => (
                        <>
                            {isResolved && (
                                <Breadcrumbs aria-label="Breadcrumb">
                                    <Link color="textPrimary" onClick={this.redirectTo('/hub/events')}>
                                        Events
                                    </Link>
                                    <Typography color="inherit">{response[0].data.name}</Typography>
                                </Breadcrumbs>
                            )}

                            {!club && (
                                <CardHeader
                                    avatar={<Skeleton variant="circle" className={classes.avatar} />}
                                    title={(
                                        <>
                                            <Skeleton height={12} width="40%" />
                                            <Skeleton height={12} width="30%" />
                                        </>
                                    )}
                                    subheader={(
                                        <>
                                            <Skeleton height={12} width="20%" />
                                            <Skeleton height={12} width="20%" />
                                        </>
                                    )}
                                />
                            )}

                            {(club && isResolved) && (
                                <CardHeader
                                    avatar={(
                                        <Avatar
                                            className={classes.avatar}
                                            src={Image.display(club.logo)}
                                            alt={`Logo ${club.name}`}
                                        />
                                    )}
                                    title={(
                                        <>
                                            <Typography variant="h6">
                                                {response[0].data.name}
                                            </Typography>
                                            <Typography gutterBottom>
                                                Organisé par : {club.name.toUpperCase()}
                                            </Typography>
                                            <Typography gutterBottom>
                                                {response[0].data.description && `${response[0].data.description}`}
                                            </Typography>
                                        </>
                                    )}
                                    subheader={(
                                        <>
                                            <Typography variant="subtitle2">
                                                Date: {format(DateUtils.withOffsetDate(response[0].data.takes_place_at), 'dd/MM/yyyy à HH:mm')}
                                            </Typography>
                                            <Typography variant="subtitle2">
                                                Date limite : {format(DateUtils.withOffsetDate(response[0].data.subscriptions_end_at), 'dd/MM/yyyy à HH:mm')}
                                            </Typography>
                                        </>
                                    )}
                                />
                            )}
                            
                            <Typography color="textSecondary" variant="caption">Vous pouvez modifier/vous désinscrire à tout moment jusqu'à la date limite.</Typography>

                            <Divider className={classes.divider} />

                            <Grid container>

                                <List className={classes.list} component={Grid} container item xs={12} md={8} alignContent="flex-start">
                                    {(isPending || (isResolved && new Date() < DateUtils.withOffsetDate(response[0].data.subscriptions_end_at))) && (
                                        <>
                                            {(isResolved ? response[0].data.products : [...Array(6).keys()]).map((product, productIdx) => (
                                                <Product
                                                    key={productIdx}
                                                    isLoading={!isResolved}
                                                    product={product}
                                                    handleClick={isResolved ? this.handleSelectedChange(product, response[0].data) : null}
                                                />
                                            ))}
    
                                            {(isResolved ? response[0].data.standalone : [...Array(4).keys()]).map((standalone, standaloneIdx) => (
                                                <Product
                                                    key={standaloneIdx}
                                                    isLoading={!isResolved}
                                                    product={standalone}
                                                    handleClick={isResolved ? this.handleSelectedChange(standalone, standalone) : null}
                                                />
                                            ))}
                                        </>
                                    )}
                                </List>

                                {(isResolved && new Date() < DateUtils.withOffsetDate(response[0].data.subscriptions_end_at)) && (
                                    <>
                                        <SelectOptions
                                            product={selected}
                                            options={options}
                                            handleClose={this.handleSelectedChange(null, null)}
                                            handleAddToCart={this.handleAddToCart(response[1].data.cart)}
                                        />

                                        {fullScreen ?
                                            <MobileCart
                                                showPaymentMethods={response[0].data.products.length > 0 || response[0].data.standalone.length > 0}
                                                subscribing={subscribing}
                                                unsubscribing={unsubscribing}
                                                subscription={response[1].data}
                                                handleUnsubscribe={this.handleUnsubscribe(response[1].data)}
                                                handleSubmit={this.handleSubmit(response[1].data)}
                                                handleQuantityChange={this.handleQuantityChange(response[1].data.cart)}
                                            />
                                            :
                                            <Cart
                                                showPaymentMethods={response[0].data.products.length > 0 || response[0].data.standalone.length > 0}
                                                subscribing={subscribing}
                                                unsubscribing={unsubscribing}
                                                subscription={response[1].data}
                                                handleUnsubscribe={this.handleUnsubscribe(response[1].data)}
                                                handleSubmit={this.handleSubmit(response[1].data)}
                                                handleQuantityChange={this.handleQuantityChange(response[1].data.cart)}
                                            />}
                                    </>
                                )}

                                {(isResolved && new Date() > DateUtils.withOffsetDate(response[0].data.subscriptions_end_at)) && (
                                    <Grid container item xs={12} justify="center" alignItems="center" component={Typography} color="textSecondary">
                                        <CloseOutline className={classes.leftIcon} />
                                        <Typography color="textSecondary" variant="h6" component="span">
                                            Inscriptions fermées
                                        </Typography>
                                    </Grid>
                                )}
                            </Grid>
                        </>
                    )}
                </Loads>
            </Container>
        );
    }
}))));
