import React from 'react';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from 'mdi-material-ui/Close';
import CartOff from 'mdi-material-ui/CartOff';
import Check from 'mdi-material-ui/Check';
import CartOutline from 'mdi-material-ui/CartOutline';
import Close from 'mdi-material-ui/Close';
import Slide from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { withStyles } from '@material-ui/core';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import PlusCircleOutline from 'mdi-material-ui/PlusCircleOutline';
import MinusCircleOutline from 'mdi-material-ui/MinusCircleOutline';
import PaymentMethods from './PaymentMethods';

const styles = theme => ({
    appBar: {
        top: 'auto',
        bottom: 0,
        zIndex: 1400,
    },
    button: {
        width: '100%',
    },
    icon: {
        margin: theme.spacing(1),
    },
    itemText: {
        textAlign: 'center',
    },
    btn: {
        width: '100%',
        height: '100%',
    },
    btnGroup: {
        margin: theme.spacing(1),
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    alert: {
        color: '#C72C41',
    },
    box: {
        padding: theme.spacing(3),
        marginBottom: 60,
    }
});

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default withStyles(styles)(class MobileCart extends React.Component {
    state = {
        open: false,
    };

    handleOpen = open => () => {
        this.setState({ open });
    };

    getTotal = () => {
        const { subscription } = this.props;
        return subscription && subscription.hasOwnProperty('cart') ? 
            subscription.cart.reduce((acc, val) => acc + parseFloat(val.price), 0).toFixed(2) 
            : 0;
    };

    handlePaymentMethodChange = value => () => {
        const { subscription } = this.props;
        subscription.payment_method = value;
        this.forceUpdate();
    };

    render() {
        const { classes, showPaymentMethods, subscription, subscribing, unsubscribing, handleSubmit, handleUnsubscribe, handleQuantityChange } = this.props;
        const { open } = this.state;
        const total = this.getTotal();

        return (
            <AppBar position="fixed" color="inherit" className={classes.appBar}>
                <Toolbar>
                    <Grid className={classes.btnGroup} container spacing={1}>

                        <Grid item xs={9}>
                            {open ? (
                                <Button
                                    className={classes.btn}
                                    startIcon={<Check />}
                                    variant="contained"
                                    aria-label="Valider"
                                    color="primary"
                                    onClick={handleSubmit}
                                    disabled={subscribing}
                                >
                                    {subscribing ? <CircularProgress size={25} color="secondary" /> : subscription.id ? "Modifier" : "S'inscrire"}
                                </Button>
                            ) : (
                                <Button
                                    className={classes.btn}
                                    startIcon={<CartOutline />}
                                    variant="contained"
                                    aria-label="Voir mon panier"
                                    color="primary"
                                    onClick={this.handleOpen(true)}
                                >
                                    Voir mon panier
                                </Button>        
                            )}
                        </Grid>
                        <Grid container item xs={3} justify="center" alignContent="center">
                            <Typography className={classes.itemText} variant="caption" color="textSecondary">
                                {`${total} €`}
                            </Typography>
                        </Grid>
                    </Grid>
                </Toolbar>

                <Dialog fullScreen open={open} onClose={this.handleOpen(false)} TransitionComponent={Transition}>
                    <DialogActions>
                        <IconButton className={classes.icon} edge="end" onClick={this.handleOpen(false)} aria-label="Réduire">
                            <CloseIcon />
                        </IconButton>
                    </DialogActions>

                    <DialogContent>
                        <Typography variant="h6" paragraph>
                            Récapitulatif d'inscription
                        </Typography>

                        <Divider className={classes.divider} />

                        <List>
                            {subscription && subscription.hasOwnProperty('cart') && subscription.cart.map((x, idx) => (
                                <ListItem key={idx} dense>
                                    <ListItemIcon>
                                        <Box display="flex" alignItems="center">
                                            <IconButton onClick={handleQuantityChange(x, -1)} aria-label={`Supprimer 1 ${x.name}`}>
                                                <MinusCircleOutline />
                                            </IconButton>
                                            <Typography variant="overline">
                                                {x.quantity}
                                            </Typography>
                                            <IconButton onClick={handleQuantityChange(x, 1)} aria-label={`Ajouter 1 ${x.name}`}>
                                                <PlusCircleOutline />
                                            </IconButton>
                                        </Box>
                                    </ListItemIcon>

                                    <ListItemText className={classes.itemText} id={idx} primary={x.name} secondary={x.options} />

                                    <ListItemText className={classes.itemText} primary={x.price} />
                                </ListItem>
                            ))}

                            {showPaymentMethods && subscription && subscription.hasOwnProperty('cart') && subscription.cart.length === 0 && (
                                <ListItem>
                                    <ListItemIcon>
                                        <CartOff />
                                    </ListItemIcon>
                                    <ListItemText primary="Votre panier est vide !" />
                                </ListItem>
                            )}

                            <Divider className={classes.divider} />

                            <ListItem>
                                <ListItemText primary="Total" />
                                <ListItemText className={classes.itemText} primary={`${total} €`} />
                            </ListItem>

                            <Divider className={classes.divider} />

                            <ListItem>
                                <ListItemText primary="Moyen de paiement" secondary="(Le prélèvement n'est pas immédiat)" />
                            </ListItem>

                            <ListItem>
                                {showPaymentMethods && <PaymentMethods subscription={subscription} handlePaymentMethodChange={this.handlePaymentMethodChange} />}
                            </ListItem>

                            {subscription.id && (
                                <Box className={classes.box} display="flex" flexDirection="column" justifyContent="center" alignContent="center">        
                                    <Typography variant="body2" align="center" color="textSecondary" paragraph>
                                        T'étais pas là pour être ici ?
                                    </Typography>

                                    <Button
                                        className={classes.alert}
                                        startIcon={<Close />}
                                        variant="outlined"
                                        aria-label="Se désinscrire"
                                        color="inherit"
                                        onClick={handleUnsubscribe}
                                        disabled={unsubscribing}
                                    >
                                        {unsubscribing ? <CircularProgress size={25} color="primary" /> : "Se désinscrire"}
                                    </Button>
                                </Box>
                            )}
                        </List>
                    </DialogContent>
                </Dialog>

            </AppBar>
        );
    }
});
