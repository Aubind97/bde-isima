import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Checkbox from '@material-ui/core/Checkbox';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(3),
    },
    group: {
        margin: theme.spacing(1, 0),
    },
}));

/**
 * This component is responsible for handling event options display depending on the type of options (exclusives, combinables, ...)
 */
export default function Options({ option, handleOptionChange, idx }) {

    /** States initialisation */
    const classes = useStyles();
    const [index, setIndex] = useState(0);
    const [selected] = useState([]);

    const handleCombinableChange = index => event => {
        selected[index] = event.target.checked;
        handleOptionChange(option.items.filter((item, pos) => selected[pos]), idx)
    };

    const handleExclusiveChange = event => {
        const newIndex = parseInt(event.target.value);
        setIndex(newIndex);
        handleOptionChange([option.items[newIndex]], idx);
    };

    useEffect(() => {
        if (option.items && option.type === 'exclusive') {
            const newIndex = option.items.findIndex(x => parseFloat(x.price) === 0);            
            if (newIndex > -1) {
                setIndex(newIndex);
                handleOptionChange([option.items[newIndex]], idx);
            }
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <FormControl className={classes.formControl} component="fieldset">
            <FormLabel component="legend">{option.name}</FormLabel>

            {option.type === 'combinable' && (
                <FormGroup
                    className={classes.group}
                    aria-label={`Options de ${option.name}`}
                >
                    {option.items && option.items.map((item, itemIdx) => (
                        <FormControlLabel
                            key={itemIdx}
                            control={(
                                <Checkbox
                                    value={item.name}
                                    onChange={handleCombinableChange(itemIdx)}
                                    color="inherit"
                                />
                            )}
                            label={`${item.name} ${item.price > 0 ? `(+${item.price} €)` : ''}`}
                        />
                    ))}
                </FormGroup>
            )}

            {option.type === 'exclusive' && (
                <RadioGroup
                    className={classes.group}
                    aria-label={`Options de ${option.name}`}
                    value={`${index}`}
                    onChange={handleExclusiveChange}
                >
                    {option.items && option.items.map((item, itemIdx) => (
                        <FormControlLabel
                            key={itemIdx}
                            control={(
                                <Radio
                                    value={`${itemIdx}`}
                                    color="inherit"
                                />
                            )}
                            label={`${item.name} ${item.price > 0 ? `(+${item.price} €)` : ''}`}
                        />
                    ))}
                </RadioGroup>
            )}

        </FormControl>
    );
};
