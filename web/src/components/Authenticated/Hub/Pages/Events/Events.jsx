import React from 'react';
import { Loads } from 'react-loads';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import Eye from 'mdi-material-ui/Eye';
import PencilOutline from 'mdi-material-ui/PencilOutline';
import { withStyles } from '@material-ui/core';
import { CRUD, Image, DateUtils } from 'utils';
import Skeleton from '@material-ui/lab/Skeleton';
import axios from 'axios';
import { format } from 'date-fns';
import AvatarGroup from 'utils/misc/AvatarGroup';

const styles = theme => ({
    root: {
        padding: theme.spacing(2),
        marginTop: 70 + 12 + theme.spacing(3), //+12 for the spacing in Articles
        marginBottom: theme.spacing(3),
    },
    card: {
        textAlign: 'left',
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
    },
    cardActions: {
        maxHeight: 100,
        flex: '1 1 auto',
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    margin: {
        margin: theme.spacing(2),
    },
    avatar: {
        width: 40,
        height: 40,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(2)
    },
});

export default withStyles(styles)(class Events extends React.Component {

    getEvents = () => CRUD.get({
        url: '/events',
        params: {
            orderBy: 'subscriptions_end_at',
            orderDirection: 'asc',
            statusIn: [1]
        }
    }).then(result => {
        return axios.all(result.data.map(event => {
            return CRUD.get({
                url: `/subscriptions/event/${event.id}`,
            })
                .then(result => ({ ...event, isSubscribed: result.data.event_id === event.id }))
                .catch(() => ({ ...event, isSubscribed: false }))
        }))
    });

    getClub = id => () => CRUD.get({ url: `/clubs/${id}` });

    render() {
        const { classes } = this.props;
        return (
            <Container className={classes.root} fixed>
                <Loads load={this.getEvents}>
                    {({ response, isRejected, isResolved }) => (
                        <>
                            <Typography variant="h3" paragraph>
                                Évènements à venir
                                </Typography>

                            <Divider className={classes.margin} />

                            {(isResolved && response.filter(x => new Date() < DateUtils.withOffsetDate(x.subscriptions_end_at)).length === 0) && (
                                <Box
                                    display="flex"
                                    justifyContent="center"
                                    alignItems="center"
                                    flexDirection="column"
                                >
                                    <img
                                        src="/images/illustrations/NoData.svg"
                                        height="auto"
                                        width="300"
                                        alt="Aucune donnée"
                                    />
                                    <Typography variant="h6">
                                        Aucun événement à venir !
                                    </Typography>
                                </Box>
                            )}

                            <div style={{ padding: 20 }}>
                                <Grid container spacing={5} justify="flex-start">

                                    {(isResolved ? response.filter(x => new Date() < DateUtils.withOffsetDate(x.subscriptions_end_at)) : [...Array(3).keys()]).map((event, idx) => (
                                        <Grid key={idx} item xs={12} md={6} lg={4}>
                                            <Card className={classes.card}>
                                                <CardHeader
                                                    avatar={(
                                                        isResolved ? (
                                                            <Loads load={this.getClub(event.club_id)}>
                                                                {({ response, isResolved }) => (
                                                                    <Avatar
                                                                        className={classes.avatar}
                                                                        src={isResolved ? Image.display(response.data.logo) : Image.display(null)}
                                                                        alt={`Logo ${isResolved ? response.data.name : ''}`}
                                                                    />
                                                                )}
                                                            </Loads>
                                                        ) : (
                                                                <Skeleton variant="circle" width={40} height={40} />
                                                            )
                                                    )}
                                                    title={(!isResolved || isRejected) ? <Skeleton height={12} width="80%" /> : event.name}
                                                    subheader={(!isResolved || isRejected) ? <Skeleton height={12} width="80%" /> : `Date limite : ${format(DateUtils.withOffsetDate(event.subscriptions_end_at), 'dd/MM/yyyy à HH:mm')}`}
                                                    action={(!isResolved || isRejected) ? <Skeleton variant="circle" width={40} height={40} /> : <AvatarGroup event_id={event.id} />}
                                                />

                                                <CardActions className={classes.cardActions}>
                                                    {isResolved ? (
                                                        <Button
                                                            variant="contained"
                                                            startIcon={event.isSubscribed ? <PencilOutline /> : <Eye />}
                                                            color="primary"
                                                            onClick={() => this.props.history.push(`/hub/events/${event.id}`)}
                                                            aria-label="S'inscrire"
                                                        >
                                                            {event.isSubscribed ? "Modifier" : "Consulter"}
                                                        </Button>
                                                    ) : (
                                                        <Skeleton height={48} width={140} />
                                                    )}
                                                </CardActions>

                                            </Card>
                                        </Grid>
                                    ))}
                                </Grid>
                            </div>

                        </>
                    )}
                </Loads>
            </Container>
        );
    }
});
