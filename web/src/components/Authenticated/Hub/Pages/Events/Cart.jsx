import React from 'react';
import { withStyles } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import PlusCircleOutline from 'mdi-material-ui/PlusCircleOutline';
import MinusCircleOutline from 'mdi-material-ui/MinusCircleOutline';
import Check from 'mdi-material-ui/Check';
import Close from 'mdi-material-ui/Close';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Grow from '@material-ui/core/Grow';
import CircularProgress from '@material-ui/core/CircularProgress';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import PaymentMethods from './PaymentMethods';

const styles = theme => ({
    card: {
        margin: theme.spacing(2, 0, 0, 0),
        width: '90%',
    },
    itemText: {
        textAlign: 'right',
        margin: theme.spacing(2),
    },
    noPadding: {
        padding : 0,
    },
    group: {
        width: '100%',
    },
    btn: {
        width: '100%',
        height: 60,
    },
});

export default withStyles(styles)(class Cart extends React.Component {

    getTotal = () => {
        const { subscription } = this.props;
        return subscription && subscription.hasOwnProperty('cart') ? 
            subscription.cart.reduce((acc, val) => acc + parseFloat(val.price), 0).toFixed(2) 
            : 0;
    };

    handlePaymentMethodChange = value => () => {
        const { subscription } = this.props;        
        subscription.payment_method = value;
        this.forceUpdate();
    };

    render() {
        const { classes, showPaymentMethods, subscribing, unsubscribing, subscription, handleSubmit, handleUnsubscribe, handleQuantityChange } = this.props;
        return (
            <Grid container item xs={4} justify="flex-end" alignItems="flex-start">
                <Card className={classes.card}>
                    <CardActions className={classes.noPadding}>
                        <ButtonGroup className={classes.group} color="primary" aria-label="Inscription/Désinscription">
                            {subscription.id && (
                                <Button
                                    className={classes.btn}
                                    startIcon={<Close />}
                                    aria-label="Se désinscrire"
                                    color="inherit"
                                    onClick={handleUnsubscribe}
                                    disabled={unsubscribing || subscribing}
                                >
                                    {unsubscribing ? <CircularProgress size={25} color="primary" /> : "Se désinscrire"}
                                </Button>                     
                            )}
                            <Button
                                className={classes.btn}
                                startIcon={<Check />}
                                variant="contained"
                                aria-label="Confirmer l'inscription"
                                color="primary"
                                onClick={handleSubmit}
                                disabled={subscribing || unsubscribing}
                            >
                                {subscribing ? <CircularProgress size={25} color="secondary" /> : subscription.id ? "Modifier" : "S'inscrire" }
                            </Button>
                        </ButtonGroup>
                    </CardActions>
                    <CardContent>
                        <List>
                            {subscription && subscription.hasOwnProperty('cart') && subscription.cart.map((x, idx) => (
                                <Grow key={idx} in={true}>
                                    <ListItem dense disableGutters>
                                        <ListItemIcon>
                                            <Box display="flex" alignItems="center">
                                                <IconButton onClick={handleQuantityChange(x, -1)} aria-label={`Supprimer 1 ${x.name}`}>
                                                    <MinusCircleOutline />
                                                </IconButton>
                                                <Typography variant="overline">
                                                    {x.quantity}
                                                </Typography>
                                                <IconButton onClick={handleQuantityChange(x, 1)} aria-label={`Ajouter 1 ${x.name}`}>
                                                    <PlusCircleOutline />
                                                </IconButton>
                                            </Box>
                                        </ListItemIcon>

                                        <ListItemText className={classes.itemText} primary={x.name} secondary={x.options} />

                                        <ListItemText className={classes.itemText} primary={`${x.price} €`} />
                                    </ListItem>
                                </Grow>
                            ))}

                            <Divider />                            

                            <ListItem>
                                <ListItemText primary="Total" />
                                <ListItemText className={classes.itemText} primary={`${this.getTotal()} €`} />
                            </ListItem>

                            <ListItem dense>
                                <ListItemText secondary="Le prélèvement n'est pas immédiat." />
                            </ListItem>

                        </List>
                    </CardContent>

                    <CardActions>
                        {showPaymentMethods && <PaymentMethods subscription={subscription} handlePaymentMethodChange={this.handlePaymentMethodChange} />}
                    </CardActions>
                </Card>
            </Grid>
        );
    }
});
