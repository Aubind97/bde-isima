import React from 'react';
import { makeStyles } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import CardActionArea from '@material-ui/core/CardActionArea';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles(theme => ({
    card: {
        width: '100%',
        height: '100%',
    },
    itemText: {
        margin: theme.spacing(2),
    },
}));    


/**
 * This component is responsible for displaying a single Product Card
 */
export default React.memo(function Product({ isLoading, product, handleClick }) {

    /** States initialisation */
    const classes = useStyles();

    return (
        <ListItem component={Grid} item xs={6} onClick={handleClick}>
            <Card className={classes.card}>
                <CardActionArea>
                    <ListItemText
                        className={classes.itemText}
                        primary={isLoading ? <Skeleton height={18} width="30%" /> : product.name}
                        secondary={isLoading ? <Skeleton height={12} width="10%" /> : `${parseFloat(product.price).toFixed(2)} €`}
                        primaryTypographyProps={{ component: "div", variant: "body1" }}
                        secondaryTypographyProps={{ component: "div", variant: "body2" }}
                    />
                </CardActionArea>
            </Card>
        </ListItem>
    );
});
