import React from 'react';
import TimeAgo from 'react-timeago';
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';
import frenchStrings from 'react-timeago/lib/language-strings/fr';
import { makeStyles } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Records from './Records';
import { CRUD, DateUtils } from 'utils';
import { useLoads } from 'react-loads';
import Skeleton from '@material-ui/lab/Skeleton';

const formatter = buildFormatter(frenchStrings);

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginTop: 70 + 12 + theme.spacing(3), //+12 for the spacing in Articles
        marginBottom: theme.spacing(3),
    },
    margin: {
        margin: theme.spacing(2),
    },
    crown: {
        color: '#f1c40f',
        transform: 'rotate(30deg) translate(0px, -4px)',
    }
}));

export default React.memo(function Leaderboard() {

    /** States initialisation */
    const classes = useStyles();

    const getAnalytics = React.useCallback(() => CRUD.get({ url: '/analytics/leaderboard' }), []);
    const { response: analytics, isResolved } = useLoads(getAnalytics);

    return (
        <Container className={classes.root} fixed>

            <Typography variant="h3" gutterBottom>Leaderboard</Typography>
            
            {isResolved ? (
                <Typography variant="caption" color="textSecondary">
                    Dernière mise à jour {<TimeAgo
                        date={DateUtils.withOffsetDate(analytics.data.updated_at)}
                        formatter={formatter}
                    />}
                </Typography>
            ) : (
                <Skeleton height={12} width="20%" />
            )}

            <Divider className={classes.margin} />

            <Records analytics={analytics} isLoading={!isResolved} classes={classes} />

        </Container>
    );
});
