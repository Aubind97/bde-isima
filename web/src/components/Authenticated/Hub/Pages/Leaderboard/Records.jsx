import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CardHeader from "@material-ui/core/CardHeader";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Avatar from "@material-ui/core/Avatar";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import TrophyOutline from 'mdi-material-ui/TrophyOutline';
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { useTheme } from '@material-ui/core';
import CrownOutline from 'mdi-material-ui/CrownOutline';
import Badge from '@material-ui/core/Badge';
import User from "components/Authenticated/Auth/Components/User";
import { UserAvatar, Image } from "utils";

let id = 0;
function createData({ article_id, article_photo, article_name, units, picture, username }) {
    id += 1;
    return { id, article_id, article_photo, article_name, units, picture, username };
}

function Records({ analytics, isLoading, classes }) {

    /** States initialisation */
    const theme = useTheme();
    const user_stats = User.getData('stats');

    const getUserStats = article_id => {
        const entry = user_stats.articles.find(x => x.id === article_id);
        return entry !== undefined ? entry.units : 0;
    }

    const displayUserScore = row => {
        const score = getUserStats(row.article_id);
        return (
            <TableCell className="MuiCardContent-cell" align="right">
                {(score >= row.units) ? (
                    <Badge badgeContent={<CrownOutline className={classes.crown} />} component="span">
                        {score}
                    </Badge>
                ) : score }
            </TableCell>
        );
    }

    return (
        <MuiThemeProvider
            theme={createMuiTheme({
                typography: {
                    useNextVariants: true,
                    fontFamily: ['Montserrat', 'Helvetica Neue', 'sans-serif'].join(','),
                },
                overrides: Records.getTheme(theme)
            })}
        >
            <Card className="MuiElevatedCard--01">

                {isLoading && <CircularProgress size={25} color="inherit" />}

                <CardHeader
                    className="MuiCardHeader-root"
                    title="Records d'achats"
                    avatar={<TrophyOutline className={"MuiCardHeader-avatar"} />}
                    subheader="Achetezzz-les tous !"
                    classes={{
                        title: "MuiCardHeader-title",
                        subheader: "MuiCardHeader-subheader"
                    }}
                />

                {!isLoading && (
                    <CardContent className={"MuiCardContent-root"}>
                        <div className={"MuiCardContent-inner"}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell className="MuiCardContent-cell">Photo</TableCell>
                                        <TableCell className="MuiCardContent-cell">Article</TableCell>
                                        <TableCell className="MuiCardContent-cell" align="right">Nombre d'unités</TableCell>
                                        <TableCell className="MuiCardContent-cell" align="right">Nom d'utilisateur</TableCell>
                                        <TableCell className="MuiCardContent-cell" align="right">Votre score</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {analytics.data.ladder.map(x => createData(x)).map(row => (
                                        <TableRow key={row.id}>
                                            <TableCell className="MuiCardContent-cell" align="left">
                                                <Avatar
                                                    src={Image.display(row.article_photo)}
                                                    alt={`Photo de ${row.article_name}`}
                                                />
                                            </TableCell>
                                            <TableCell className="MuiCardContent-cell" component="th" scope="row">{row.article_name}</TableCell>
                                            <TableCell className="MuiCardContent-cell" align="right">{row.units}</TableCell>
                                            <TableCell className="MuiCardContent-cell" align="right">
                                                <Grid container spacing={2} justify="flex-end" alignItems="center">
                                                    <Grid item>
                                                        {row.username}
                                                    </Grid>
                                                    <Grid item>
                                                        <UserAvatar
                                                            data={row}
                                                            username={row.username}
                                                            AvatarProps={{
                                                                alt: "Avatar utilisateur du détenteur du record"
                                                            }}
                                                        />
                                                    </Grid>
                                                </Grid>
                                            </TableCell>
                                            {(user_stats && user_stats.articles) && displayUserScore(row)}
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </div>
                    </CardContent>
                )}

            </Card>
        </MuiThemeProvider>
    )
};

Records.getTheme = muiBaseTheme => {
    const offset = 40;
    const headerShadow = "4px 4px 20px 1px rgba(0, 0, 0, 0.2)";
    return {
        MuiCard: {
            root: {
                "&.MuiElevatedCard--01": {
                    fontFamily: 'Montserrat',
                    marginTop: offset + 24,
                    borderRadius: muiBaseTheme.spacing() / 2,
                    textAlign: 'center',
                    transition: "0.3s",
                    position: "relative",
                    overflow: "initial",
                    background: muiBaseTheme.palette.background.paper,
                    padding: `${muiBaseTheme.spacing() * 5}px 0`,
                    "& .MuiCardHeader-root": {
                        flexShrink: 0,
                        position: "absolute",
                        top: -offset,
                        right: 20,
                        left: 20,
                        borderRadius: muiBaseTheme.spacing() / 2,
                        backgroundColor: '#222222',
                        overflow: "hidden",
                        boxShadow: headerShadow,
                        textAlign: "left",
                        "& .MuiCardHeader-title": {
                            color: muiBaseTheme.palette.secondary.main,
                            fontWeight: 900,
                            letterSpacing: 1
                        },
                        "& .MuiCardHeader-avatar": {
                            color: "#fff",
                        },
                        "& .MuiCardHeader-subheader": {
                            color: muiBaseTheme.palette.secondary.main,
                            opacity: 0.87,
                            fontWeight: 200,
                            letterSpacing: 0.4
                        }
                    },
                    "& .MuiCardContent-root": {
                        textAlign: "left",
                        "& .MuiCardContent-inner": {
                            padding: "16px",
                            overflowX: "auto"
                        },
                        "& .MuiCardContent-cell": {
                            color: muiBaseTheme.palette.text.primary,
                        }
                    }
                }
            }
        }
    };
};

Records.metadata = {
    name: "Elevated Card Header I",
    description: "Wonderful elevated card header"
};

export default Records;
