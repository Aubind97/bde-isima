import React from 'react';
import { dripForm } from 'react-drip-form';
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import MenuItem from '@material-ui/core/MenuItem';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';
import Update from 'mdi-material-ui/Update';
import { Field, HiddenField, SelectField, SwitchField } from 'utils/forms';
import { Accept, DateUtils } from 'utils';
import AvatarManager from './AvatarManager';

export default dripForm({
    validations: {
        nickname: {
            max: 255
        },
        email: {
            required: true,
            email: true,
            max: 255            
        },
        password: {
            min: 3
        },
        confirm: {
            min: 3,
            same: 'password'
        },
    },
    messages: {
        nickname: {
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        email: {
            required: 'Ce champ est requis.',
            email: 'Ce n\'est pas un email valide.',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        password: {
            min: 'Votre mot de passe doit contenir au moins 3 caractères.',
        },
        confirm: {
            min: 'Votre mot de passe doit contenir au moins 3 caractères.',
            same: 'Les mots de passe ne correspondent pas.'
        },
    },
})(({
    handlers,
    meta: { invalid, pristine },
    submitting,
    values,
    userData,
    files,
    ...props
}) => (
        <form className={props.classes.form} onSubmit={handlers.onSubmit}>
            <Box display="flex" flexDirection="column">

                <FormLabel style={{ fontWeight: 800, marginTop: 32 }}>INFORMATIONS PERSONNELLES</FormLabel>
                <Divider className={props.classes.divider} />

                <HiddenField
                    id="id"
                    name="id"
                />

                <Field
                    type="text"
                    id="firstname"
                    name="firstname"
                    label="Prénom"
                    disabled
                />

                <Field
                    type="text"
                    id="lastname"
                    name="lastname"
                    label="Nom"
                    disabled
                />

                <Field
                    type="text"
                    id="nickname"
                    name="nickname"
                    label="Surnom"
                />

                <FormLabel style={{ fontWeight: 800, marginTop: 32 }}>COORDONNÉES</FormLabel>
                <Divider className={props.classes.divider} />

                <Field
                    type="email"
                    id="email"
                    name="email"
                    label="Adresse email"
                    autoComplete="email"
                />

                <Field
                    type="password"
                    id="password"
                    name="password"
                    label="Nouveau mot de passe"
                    autoComplete="new-password"
                />

                <Field
                    type="password"
                    id="confirm"
                    name="confirm"
                    label="Confirmer le nouveau mot de passe"
                    autoComplete="new-password"
                />

                <FormLabel style={{ fontWeight: 800, marginTop: 32 }}>ISIMA</FormLabel>
                <Divider className={props.classes.divider} />

                <SelectField
                    id="preferences.course"
                    name="preferences.course"
                    label="Filière"
                    style={{ textAlign: 'left' }}
                    disabled={values.promotion && new Date() < DateUtils.withOffsetDate(`${parseInt(values.promotion.year) - 2}-08-31`)}
                >
                    {[
                        'F1',
                        'F2',
                        'F3',
                        'F4',
                        'F5'
                    ].map((s, i) => (
                        <MenuItem key={i} value={s}>{s}</MenuItem>
                    ))}
                </SelectField>

                <FormLabel style={{ fontWeight: 800, marginTop: 32 }}>BDE</FormLabel>
                <Divider className={props.classes.divider} />

                <Field
                    type="text"
                    id="card"
                    name="card"
                    label="N° de carte"
                    disabled
                />

                {values.promotion && (
                    <Field
                        type="text"
                        id="promotion.year"
                        name="promotion.year"
                        label="Promotion"
                        disabled
                    />
                )}

                <Typography color="textSecondary">Cotisation {new Date().getFullYear()}-{new Date().getFullYear()+1} payée : {!!+values.is_member ? "Oui" : "Non"}</Typography>

                <FormLabel style={{ fontWeight: 800, marginTop: 32 }}>PRÉFÉRENCES</FormLabel>
                <Divider className={props.classes.divider} />

                <AvatarManager 
                    userData={userData}
                    handleUserDataChange={props.handleUserDataChange} 
                />

                <Accept onDrop={props.onDrop} onDelete={props.onDelete} files={files} progress={props.progress} />

                <SwitchField
                    id="preferences.dark_mode"
                    name="preferences.dark_mode"
                    value={values.preferences && +Boolean(values.preferences.dark_mode)}
                    label="Activer le mode sombre"
                    checked={values.preferences && !!+values.preferences.dark_mode}
                />

                <Button
                    type="submit"
                    startIcon={<Update />}
                    onClick={handlers.onSubmit}
                    disabled={invalid || pristine || submitting}
                    className={props.classes.submitBtn}
                    variant="contained"
                    color="primary"
                    size="large"
                    aria-label="Mettre à jour"
                >
                    {submitting ? <CircularProgress size={25} color="secondary" /> : "Mettre à jour"}
                </Button>
            </Box>
        </form>
));