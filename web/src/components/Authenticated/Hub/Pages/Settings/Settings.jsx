import React from 'react';
import { Loads } from 'react-loads';
import { withSnackbar } from 'notistack';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import SettingsForm from './SettingsForm';
import User from '../../../Auth/Components/User';
import { CRUD, CompressionHelper } from 'utils';

const styles = theme => ({
    root: {
        marginTop: 70 + theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    paper: {
        padding: theme.spacing(2),
    },
    center: {
        textAlign: 'center',
    },
    form: {
        textAlign: 'left',
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    submitBtn: {
        margin: theme.spacing(2, 0),
    },
});

export default withSnackbar(withStyles(styles)(class Settings extends React.Component {
    state = {
        progress: '',
        files: [],
        submitting: false,
        userData: User.getAllData(),
    };

    handleInitialize = (form) => {
        this.form = form;
    };

    updateData = newData => {
        //A second request is then made to update user data with notif feedback this time and without formData
        CRUD.update({
            url: `/users/${newData.id}`,
            newData,
            onSucceed: () => {
                User.setData({
                    ...User.getAllData(),
                    ...newData,
                });
                this.resetState();
                this.handleUserDataChange();
            },
            onRejection: this.resetState,
            props: this.props
        });
    };

    handleSubmit = newData => {
        const { files } = this.state;

        delete newData.picture; 

        this.setState({ submitting: true });

        //A silent request is first made with formData so the file upload can be done
        if (files[0]) {
            CRUD.update({
                url: `/users/${newData.id}`,
                newData: { picture: files[0] },
                withFormData: true,
                quietSucceed: true,
                onSucceed: () => {
                    this.updateData(newData);
                    this.setState({ picture: files[0].preview });
                    User.setData({
                        ...User.getAllData(),
                        ...(files[0] ? { picture: files[0].preview } : {}),
                    });
                    this.handleUserDataChange();
                },
                onRejection: this.resetState,
                onUploadProgress: this.setProgress,
                props: this.props
            });
        }
        else {
            this.updateData(newData);
        }
    };

    handleUserDataChange = () => {
        this.setState({ userData: User.getAllData() });
    };

    onDelete = file => {
        const { files } = this.state;
        files.splice(files.indexOf(file), 1);
        this.setState({ files, progress: '' });
    };

    setProgress = progress => this.setState({ progress });

    resetState = () => this.setState({ progress: '', files: [], submitting: false });

    render() {
        const { classes } = this.props;
        const { submitting, files, progress, userData } = this.state;
        return (
            <Container className={classes.root} fixed>
                <Paper className={classes.paper}>
                    <Typography variant="h4" paragraph>
                        Paramètres
                    </Typography>

                    <Loads load={User.fetchData}>
                        {({ response, isRejected, isPending, isResolved }) => (
                            <div className={classes.center}>
                                {(isPending || isRejected) && <CircularProgress size={25} color="inherit" />}

                                {isResolved && (
                                    <SettingsForm
                                        onInitialize={this.handleInitialize}
                                        values={response.data}
                                        submitting={submitting}
                                        onValidSubmit={this.handleSubmit}
                                        onDrop={CompressionHelper.onDrop({
                                            handleOnStart: () => this.setState({ progress: `Compression en cours ...` }),
                                            handleOnChange: files => {
                                                this.setState({ progress: `Compression terminée !`, files });
                                                this.form.updateDirty("lastname", true); //Choosing random field
                                            }
                                        })}
                                        files={files}
                                        onDelete={this.onDelete}
                                        handleUserDataChange={this.handleUserDataChange}
                                        progress={progress}
                                        userData={userData}
                                        classes={classes}
                                    />
                                )}
                            </div>
                        )}
                    </Loads>
                </Paper>
            </Container>
        );
    }
}));
