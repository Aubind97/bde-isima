import React from 'react';
import { makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import Badge from '@material-ui/core/Badge';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import IconButton from '@material-ui/core/IconButton';
import Close from 'mdi-material-ui/Close';
import { useTheme } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import { UserAvatar, CRUD } from 'utils';
import User from 'components/Authenticated/Auth/Components/User';

const useStyles = makeStyles(theme => ({
    avatar: {
        width: 100,
        height: 100,
        marginRight: 'auto',
        marginLeft: 'auto',
    },
    margin: {
        margin: theme.spacing(2),
    }
}));

/**
 * This component is responsible for managing User Avatar deletion process
 */
export default function AvatarManager({ userData, handleUserDataChange }) {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [isOpen, setIsOpen] = React.useState(false);
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const handleDialogOpen = isOpen => () => {
        setIsOpen(isOpen);
    };

    const handleAvatarDeletion = () => {
        CRUD.delete({
            url: `/users/picture/${userData.id}`,
            onSucceed: () => {
                setIsOpen(false);
                User.setData({ ...User.getAllData(), ...{ picture: null } });
                handleUserDataChange();
            },
            onRejection: setIsOpen(false),
            props: { enqueueSnackbar, closeSnackbar },
        });
    }

    return (
        <>
            <Box display="flex" justifyContent="center">
                <Badge
                    badgeContent={(
                        userData.picture && (
                            <IconButton
                                aria-label="Supprimer l'image de profil"
                                onClick={handleDialogOpen(true)}
                            >
                                <Close />
                            </IconButton>
                    ))}
                    className={classes.margin}
                >
                    <UserAvatar
                        data={userData}
                        AvatarProps={{
                            className: classes.avatar,
                            alt: "Avatar utilisateur",
                        }}
                    />
                </Badge>
            </Box>

            <Dialog
                open={isOpen}
                fullScreen={fullScreen}
                onClose={handleDialogOpen(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Voulez-vous vraiment supprimer votre photo de profil ?
                </DialogTitle>

                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Vous ne pourrez pas revenir en arrière. Toute suppression est définitive.
                    </DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleDialogOpen(false)} variant="contained" color="primary">
                        Annuler
                    </Button>
                    <Button onClick={handleAvatarDeletion} color="inherit" autoFocus>
                        Supprimer
                    </Button>
                </DialogActions>
            </Dialog>           
        </>
    );
};

AvatarManager.propTypes = {
    /** User data object */
    userData: PropTypes.object,
    /** Callback to update user data */
    handleUserDataChange: PropTypes.func,
};
