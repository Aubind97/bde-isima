import React from 'react';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

const styles = theme => ({
    root: {
        marginTop: 70 + theme.spacing(3),
        marginBottom: theme.spacing(3),
        textAlign: 'center',
    },
    margin: {
        fontSize: theme.typography.pxToRem(100),
        margin: theme.spacing(2),
    },
});

export default withStyles(styles)(class Developers extends React.Component {
    render() {
        const { classes } = this.props;
        return (
            <Container className={classes.root} fixed>
                <img
                    className={classes.margin}
                    src="/images/illustrations/WIP.svg"
                    height="auto"
                    width="60%"
                    alt="Work in progress"
                />
                <Typography align="center" variant="h5">
                    Interfaces en cours de développement
                </Typography>
            </Container>
        );
    }
});
