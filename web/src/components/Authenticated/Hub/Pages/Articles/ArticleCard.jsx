import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Skeleton from '@material-ui/lab/Skeleton';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import LazyLoad from 'react-lazyload';
import { Image } from 'utils';

const useStyles = makeStyles(theme => ({
    red: {
        color: '#d63031',
        borderColor: '#d63031',
    },
    green: {
        color: '#00b894',
        borderColor: '#00b894',
    },
    card: {
        width: 345,
        height: 250
    },
    media: {
        height: 100
    },
    title: {
        height: 100,
        padding: theme.spacing(1),
        display: 'flex',
        alignContent: 'center',
        justifyContent: 'center',
    },
    skeleton: {
        margin: 0,
    },
}));

/**
 * This component is responsible for displaying a single card of an Article on the Market page 
 */
export default React.memo(function ArticleCard({ item, sizes, isLoading }) {

    /** States initialisation */
    const classes = useStyles();

    return (
        <Grid xs={sizes[0]} md={sizes[1]} xl={sizes[2]} container item justify="center">
            <Card className={classes.card}>
                <CardActionArea>
                    <LazyLoad
                        height={100}
                        offset={100}
                        scroll
                        once
                        placeholder={<Skeleton width="100%" height={100} className={classes.skeleton} />}
                    >
                        <CardMedia
                            className={classes.media}
                            image={Image.display(item.photo)}
                            title={item.name}
                            alt={item.name}
                        />
                    </LazyLoad>
                    <CardContent className={classes.title}>
                        <Box display="flex" flexDirection="column" flexGrow={1} alignItems="center">
                            {isLoading ? (
                                <>
                                    <Skeleton width="70%" height={12} />
                                    <Skeleton width="65%" height={12} />
                                </>
                            ) : (
                                    <>
                                        <Typography variant="body2" align="center">
                                            {item.name} • {item.member_price}€
                                        </Typography>
                                        <Typography variant="caption" align="center" color="textSecondary">
                                            Non-cotisant • {item.price}€
                                        </Typography>
                                    </>
                                )}
                        </Box>
                    </CardContent>
                </CardActionArea>
                <CardActions style={{ alignItems: 'flex-start', justifyContent: 'center' }}>
                    {isLoading ? (
                        <Skeleton width="50%" height={36} />
                    ) : (
                            <Chip
                                label={`Stock : ${item.stock}`}
                                className={item.stock < 10 ? classes.red : classes.green}
                                variant="outlined"
                            />
                        )}
                </CardActions>
            </Card>
        </Grid>
    );
});
