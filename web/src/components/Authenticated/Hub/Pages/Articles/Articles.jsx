import React from 'react';
import { useLoads } from 'react-loads';
import { makeStyles } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import ArticleCard from './ArticleCard';
import { CRUD } from 'utils';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginTop: 70 + theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    margin: {
        margin: theme.spacing(2),
    },
    grid: {
        margin: theme.spacing(0, 2),
    },
}));

/**
 * This component is responsible for displaying the different Article Cards on the Market page
 */
export default React.memo(function Articles() {

    /** States initialisation */
    const classes = useStyles();

    const getArticles = React.useCallback(() => CRUD.get({
        url: `/articles`,
        params: {
            orderBy: 'created_at',
            orderDirection: 'DESC'
        }
    }), []);
    const { response: articles, isResolved } = useLoads(getArticles);

    const renderArticles = React.useCallback((isLoading, array, sizes) => array.map((item, idx) => <ArticleCard key={idx} item={item} sizes={sizes} isLoading={isLoading} />), []);

    return (
        <Container className={classes.root} fixed>
            <Grid container justify="center">

                <Grid
                    className={classes.grid}
                    container
                    item
                    spacing={3}
                    lg={3}
                    sm={12}
                    alignContent="flex-start"
                >
                    <Grid xs={12} item>
                        <Typography variant="h3" paragraph>
                            Nouveautés
                         </Typography>

                        <Divider className={classes.margin} />
                    </Grid>

                    {(isResolved && articles.data.length === 0) && (
                        <Box
                            display="flex"
                            justifyContent="center"
                            alignItems="center"
                            flexDirection="column"
                        >
                            <img
                                src="/images/illustrations/NoData.svg"
                                height="auto"
                                width="300"
                                alt="Aucune donnée"
                            />

                            <Typography variant="subtitle2" gutterBottom>
                                Aucune nouveauté !
                            </Typography>
                        </Box>
                    )}

                    {renderArticles(!isResolved, isResolved ? articles.data.slice(0, 4) : [...Array(4).keys()], [6, 6, 6])}
                </Grid>

                <Grid
                    className={classes.grid}
                    container
                    item
                    spacing={3}
                    lg={7}
                    sm={12}
                    justify="flex-start"
                >
                    <Grid xs={12} item>
                        <Typography variant="h3" paragraph align="right">
                            Articles disponibles au BDE
                        </Typography>
                        <Divider className={classes.margin} />
                    </Grid>

                    {(isResolved && articles.data.length === 0) && (
                        <Box
                            display="flex"
                            justifyContent="center"
                            alignItems="center"
                            flexDirection="column"
                        >
                            <img
                                src="/images/illustrations/NoData.svg"
                                height="auto"
                                width="300"
                                alt="Aucune donnée"
                            />

                            <Typography variant="subtitle2" gutterBottom>
                                Aucun article en vente !
                            </Typography>
                        </Box>
                    )}

                    {renderArticles(!isResolved, isResolved ? articles.data : [...Array(16).keys()], [4, 3, 3])}
                </Grid>

            </Grid>
        </Container>
    );
});
