import React from 'react';
import { Switch } from 'react-router-dom';
import AuthenticatedRoute from './AuthenticatedRoute';
import MenuAuthorizer from './Dashboard/Components/Menu/MenuAuthorizer';
import User from './Auth/Components/User';
import scopes_mapping from 'configs/scopes_mapping';
import { ListeuxProvider } from './Dashboard/ListeuxContext';
import ClubsContext from 'components/General/ClubsContext';
import Snow from 'components/General/Snow';

const Dashboard = React.lazy(() => import('./Dashboard'));
const Hub = React.lazy(() => import('./Hub'));

export default () => {
    
    /** States initialisation */
    const clubs = React.useContext(ClubsContext);
    const [isListeux, setIsListeux] = React.useState(new MenuAuthorizer(User.getData('scopes'), scopes_mapping(clubs)).hasScope('listeux'));
    
    const changeIsListeux = () => setIsListeux(new MenuAuthorizer(User.getData('scopes'), scopes_mapping(clubs)).hasScope('listeux'));

    React.useEffect(() => {
        window.addEventListener('onUserChange', changeIsListeux);

        return function cleanup() {
            window.removeEventListener('onUserChange', changeIsListeux);
        }
    });

    return (
        <ListeuxProvider value={isListeux}>
            <Snow>
                <Switch>
                    <AuthenticatedRoute path="/hub/dashboard" component={Dashboard} />
                    <AuthenticatedRoute path="/hub" component={Hub} />
                </Switch>
            </Snow>
        </ListeuxProvider>
    )
};

