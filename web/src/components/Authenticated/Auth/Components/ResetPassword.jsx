import React from 'react';
import queryString from 'query-string'
import { withSnackbar } from 'notistack';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Card from '@material-ui/core/Card';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ArrowLeft from 'mdi-material-ui/ArrowLeft';
import ResetPasswordForm from './ResetPasswordForm';
import UpdatePasswordForm from './UpdatePasswordForm';
import { CRUD } from 'utils';

const styles = theme => ({
    logo: {
        marginLeft: 'auto',
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        marginRight: 'auto',
        width: 100,
        height: 100
    },
    card: {
        [theme.breakpoints.down('md')]: {
            minHeight: '100vh',
            margin: theme.spacing(0)
        },
        margin: theme.spacing(3),
        padding: theme.spacing(2),
    },
    container: {
        textAlign: 'center',
        [theme.breakpoints.down('md')]: {
            padding: 0,
            width: '100%'
        },
    },
    padding: {
        padding: theme.spacing(3)
    },
    backBtn: {
        margin: theme.spacing(2, 0, 0, 2),
    }
});

export default withSnackbar(withRouter(withStyles(styles)(class ResetPassword extends React.Component {
    state = {
        submitting: false, 
        token: queryString.parse(this.props.location.search).token
    };

    handleInitialize = form => this.form = form;

    handleResetPassword = newData => {
        this.setState({ submitting: true });
        CRUD.add({
            url: '/reset_init',
            newData,
            onSucceed: this.resetState,
            onRejection: this.resetState,
            props: this.props
        });
    };
    
    handleUpdatePassword = newData => {
        this.setState({ submitting: true });
        CRUD.add({
            url: '/reset',
            newData: { token: this.state.token, ...newData },
            onSucceed: this.resetState,
            onRejection: this.resetState,
            props: this.props
        });
    };

    resetState = () => {
        this.setState({ submitting: false });
        this.props.history.push('/');
    };

    render() {
        const { classes } = this.props;
        const { submitting, token } = this.state;
        return (
            <Container className={classes.container} fixed>
                <Card className={classes.card}>

                    <div style={{ textAlign: 'left' }} >
                        <IconButton
                            className={classes.backBtn}
                            onClick={() => this.props.history.push('/')}
                            aria-label="Retour à la page d'accueil"
                        >
                            <ArrowLeft />
                        </IconButton>
                    </div>

                    <Avatar
                        className={classes.logo}
                        src="/images/logos/logo.svg"
                        alt="Logo BDE ISIMA"
                    />

                    {token ? (
                        <>
                            <Typography variant="h4" gutterBottom>
                                Changer mon mot de passe
                            </Typography>
                            <UpdatePasswordForm
                                submitting={submitting}
                                onInitialize={this.handleInitialize}
                                onValidSubmit={this.handleUpdatePassword}
                                {...this.props}
                            />
                        </>
                    ) : (
                       <>
                            <Typography variant="h4" gutterBottom>
                                Mot de passe oublié
                            </Typography>
                            <ResetPasswordForm
                                submitting={submitting}
                                onInitialize={this.handleInitialize}
                                onValidSubmit={this.handleResetPassword}
                                {...this.props}
                            /> 
                       </>
                    )}

                </Card>
            </Container>
        );
    }
})));