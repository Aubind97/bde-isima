import React from 'react';
import { dripForm } from 'react-drip-form';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Eye from 'mdi-material-ui/Eye';
import EyeOff from 'mdi-material-ui/EyeOff';
import { Field, SwitchField } from 'utils/forms';

export default dripForm({
    validations: {
        username: {
            required: true
        },
        password: {
            required: true,
        }
    },
    messages: {
        username: {
            required: 'Ce champ est requis.',
        },
        password: {
            required: 'Ce champ est requis.',
        }
    }
})(() => {
    const [showPassword, setShowPassword] = React.useState(false);

    return (
        <Container>
            <Box display="flex" flexDirection="column" alignItems="center">
                <Field
                    id="username"
                    name="username"
                    label="Adresse email (ou n° de carte)"
                    autoComplete="username"
                    style={{ width: 250 }}
                    autoFocus
                />

                <Field
                    type={showPassword ? "text" : "password"}
                    id="password"
                    name="password"
                    label="Mot de passe"
                    autoComplete="current-password"
                    style={{ width: 250 }}
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                                aria-label={showPassword ? "Cacher le mot de passe" : "Montrer le mot de passe"}
                                onClick={() => setShowPassword(!showPassword)}
                            >
                                {showPassword ? <Eye /> : <EyeOff />}
                            </IconButton>
                        </InputAdornment>
                    }
                />

                <SwitchField
                    id="rememberMe"
                    name="rememberMe"
                    value="rememberMe"
                    label="Se souvenir de moi"
                />
            </Box>
        </Container>
    )
});
