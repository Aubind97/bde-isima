import React from 'react';
import queryString from 'query-string';
import { withSnackbar } from 'notistack';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import User from './User';
import LoginForm from './LoginForm';
import { CRUD } from 'utils';

const styles = theme => ({
    root: {
        width: 460,
        [theme.breakpoints.down('md')]: {
            width: '100%',
        },
    },
    logo: {
        margin: theme.spacing(0, 0, 2, 0),
        width: 80,
        height: 80,
        [theme.breakpoints.down('md')]: {
            width: 40,
            height: 40,
        }
    },
    margin: {
        margin: theme.spacing(2),
    },
    fullWidth: {
        width: '100%',
    },
});

export default withSnackbar(withRouter(withStyles(styles)(class Login extends React.Component {
    state = {
        submitting: false,
    };

    handleInitialize = form => this.form = form;

    handleSubmit = newData => {
        this.setState({ submitting: true });

        CRUD.update({
            url: '/oauth/token',
            quietSucceed: true,
            newData: {
                ...newData,
                grant_type: 'password',
            },
            onSucceed: result => {
                const { messages, expires_in, ...rest } = result.data;
                let { to } = queryString.parse(this.props.location.search);

                User.setStorage(newData.rememberMe ? 'local' : 'session');
                User.setData({ ...User.getAllData(), ...{ 
                    oauth2: {
                        ...rest, 
                        ...{ access_expires_at: Date.now() + expires_in * 1000 }
                    }
                }});

                this.props.history.push(to ? to : '/hub');
            },
            onRejection: this.resetState,
            props: this.props
        });
    };

    handleLogin = e => {
        e.preventDefault();
        this.form.submit();
    };

    handleKeyDown = e => {
        if (e.key === 'Enter')
            this.handleLogin(e);
    }

    resetState = () => this.setState({ submitting: false });

    render() {
        const { classes } = this.props;
        const { submitting } = this.state;

        return (
            <>
                <DialogContent onKeyDown={this.handleKeyDown}>
                    <Box
                        className={classes.root}
                        display="flex"
                        alignItems="center"
                        flexDirection="column"
                    >
                        <Avatar
                            className={classes.logo}
                            src="/images/logos/logo.svg"
                            alt="Logo BDE ISIMA"
                        />

                        <Typography variant="h6" gutterBottom>
                            BDE ISIMA
                        </Typography>

                        <Typography variant="caption" align="center" paragraph>
                            De retour parmi nous ! Veuillez vous connecter.
                        </Typography>

                        <LoginForm
                            onInitialize={this.handleInitialize}
                            onValidSubmit={this.handleSubmit}
                            {...this.props}
                        />

                        <Typography
                            className={classes.margin}
                            component="a"
                            variant="caption"
                            color="textSecondary"
                            onClick={() => this.props.history.push('/reset_password')}
                        >
                            Première connexion ? Mot de passe oublié ?
                        </Typography>

                    </Box>
                </DialogContent>

                <DialogActions disableSpacing>
                    <Button
                        className={classes.fullWidth}
                        type="submit"
                        onClick={this.handleLogin}
                        disabled={submitting}
                        variant="contained"
                        color="primary"
                        size="large"
                        aria-label="Connexion"
                    >
                        {submitting ? <CircularProgress size={25} color="secondary" /> : 'Connexion'}
                    </Button>
                </DialogActions>
            </>
        );
    }
})));
