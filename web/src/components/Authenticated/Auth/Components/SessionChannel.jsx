import React from 'react';
import User from './User';
import BroadcastChannel from 'broadcast-channel';

export default function SessionChannel(props) {

    const [channel] = React.useState(new BroadcastChannel('bde-isima-channel'));

    function hasClosed() {
        return channel.closed;
    }

    function receiveSession() {
        //If newly tab is created
        if (!hasClosed() && localStorage.getItem('storage') === 'session' && !User.getToken()) {
            //Request other tabs to send data, if none is opened, will not receive any response
            channel.postMessage("onRequestData");
        }
    }

    function sendSession() {
        //If the current tab has data to share, send them
        if (!hasClosed() && localStorage.getItem('storage') === 'session' && User.getToken()) {
            channel.postMessage(sessionStorage.getItem('data'));
        }
    }

    channel.onmessage = msg => {
        //If the current tab receives a request for data, sends them
        if (msg === 'onRequestData') {
            sendSession();            
        }
        //If the current tab receives a request to flush the data, removes them
        if (msg === 'onRequestFlush') {
            User.clear();
            window.location.replace('/');
        }
        //Else the current tab is receiving data from another tab
        else if (!['onRequestData', 'onRequestFlush'].includes(msg)) {
            sessionStorage.setItem('data', msg);
            window.location.reload();
        }
    }

    //By default, when component did mount, send request for data  
    React.useEffect(() => {
        receiveSession();
        //Finally, when component will unmount, close the connection 
        return function cleanup() {
            channel.close();
        }
    });

    return props.children;
};
