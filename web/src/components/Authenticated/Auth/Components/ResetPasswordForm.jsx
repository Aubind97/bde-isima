import React from 'react';
import { dripForm } from 'react-drip-form';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Field } from 'utils/forms';

export default dripForm({
    validations: {
        email: {
            required: true,
            email: true
        }
    },
    messages: {
        email: {
            required: 'Ce champ est requis.',
            email: 'Ce n\'est pas un email valide.'
        }
    }
})(({ handlers, meta: { invalid, pristine }, submitting, ...props }) => (
    <form onSubmit={handlers.onSubmit} >
        <Container className={props.classes.padding} maxWidth="xs" component={Box} display="flex" flexDirection="column">
            <Field
                type="email"
                id="email"
                name="email"
                label="Adresse email"
                autoComplete="email"
            />

            <Button
                type="submit"
                onClick={handlers.onSubmit}
                disabled={invalid || pristine || submitting}
                variant="contained"
                color="primary"
                size="large"
                aria-label="Réinitialiser mon mot de passe"
            >
                {submitting ? <CircularProgress size={25} color="secondary" /> : 'Réinitialiser mon mot de passe'}
            </Button>
        </Container>
    </form>
));
