import React from 'react';
import { dripForm } from 'react-drip-form';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Field } from 'utils/forms';

export default dripForm({
    validations: {
        password: {
            required: true,
            min: 3
        },
        confirm: {
            required: true,
            min: 3,
            same: 'password'
        }
    },
    messages: {
        password: {
            required: 'Ce champ est requis.',
            min: 'Votre mot de passe doit contenir au moins 3 caractères.'
        },
        confirm: {
            required: 'Ce champ est requis.',
            min: 'Votre mot de passe doit contenir au moins 3 caractères.',
            same: 'Les mots de passe ne correspondent pas.'
        },
    }
})(({ handlers, meta: { invalid, pristine }, submitting, ...props }) => (
    <form onSubmit={handlers.onSubmit} >
        <Container className={props.classes.padding} maxWidth="xs" component={Box} display="flex" flexDirection="column">
            
            <Field
                type="password"
                id="password"
                name="password"
                label="Mot de passe"
            />

            <Field
                type="password"
                id="confirm"
                name="confirm"
                label="Confirmer le mot de passe"
            />

            <Button
                type="submit"
                onClick={handlers.onSubmit}
                disabled={invalid || pristine || submitting}
                variant="contained"
                color="primary"
                size="large"
                aria-label="Changer mon mot de passe"
            >
                {submitting ? <CircularProgress size={25} color="secondary" /> : 'Changer mon mot de passe'}
            </Button>
        </Container>
    </form>
));
