import { CRUD, DateUtils } from 'utils';

class User {
    /* Construction from data stored */
    constructor() {
        this._data = JSON.parse(this.getStorage().getItem('data'));
        this._event = document.createEvent('MutationEvents');
    }

    /* Data getter */
    getData = key => this._data ? this._data[key] : null;

    /* All Data getter */
    getAllData = () => this._data;

    /* Data setter */
    setData = values => {

        //Special event for theme changes to avoid infinite loop with onUserChange 
        if (values.preferences && !!+values.preferences.dark_mode !== !!+(this.getPreferences('dark_mode'))) {
            this._event.initEvent('onThemeChange', true, true);
            this._event.isDark = !!+values.preferences.dark_mode;
            window.dispatchEvent(this._event);
        }

        this.getStorage().setItem('data', JSON.stringify(values));
        this._data = values;
        
        this._event.initEvent('onUserChange', true, true);
        window.dispatchEvent(this._event);
    }

    /* Returns the storage handler to manage short/long sessions */
    getStorage = () => localStorage.getItem('storage') === 'local' ? localStorage : sessionStorage;

    /* Set the storage to local */
    setStorage = storage => localStorage.setItem('storage', storage);

    /* API call */
    fetchData = () => CRUD.get({ url: `/users/${this.getData('id')}` });

    /* Clear all session related cookies from user browser */
    clear = () => {
        this.getStorage().removeItem('data');
        this._data = {};
    };

    getPreferences = attribute => {
        const preferences = this.getData('preferences');
        if (preferences && preferences.hasOwnProperty(attribute)) {
            return preferences[attribute];
        }
        return null;
    };

    /* Custom get methods go here */
    getCompleteName = () => `${this.getData('firstname')} ${this.getData('lastname')}`;

    getUsername = () => { 
        const nickname =  this.getData('nickname');
        return nickname ? nickname : this.getData('firstname');
    };

    toString = user => user.nickname ? user.nickname : `${user.firstname} ${user.lastname}`;

    getGrade = () => {
        const currentDate = new Date();
        const fix = Date.now() > new Date(`${new Date().getFullYear()}-08-31`) && new Date(`${new Date().getFullYear()}-12-31`) ? 1 : 0;
        const base = new Date(new Date().setFullYear(currentDate.getFullYear() + 2 + fix)).getFullYear();
        switch (this.getData('promotion')) {
            case `${base - 2}`  : return 'ZZ3';
            case `${base - 1}`  : return 'ZZ2';
            default             : return 'ZZ1';
        }
    };

    getToken = () => {
        const oauth2 = this.getData('oauth2');
        return oauth2 ? oauth2.access_token : null;
    }

    getScopes = () => {
        const scopes = this.getData('scopes');
        return scopes ? scopes.map(x => x.id) : [];
    }
    
    /* Check whether the user has a token in storage or not */
    hasActiveSession = () => {
        const oauth2 = this.getData('oauth2');
        return oauth2 && Boolean(oauth2.access_token);
    };

    /* Check whether the user didn't go over the token expiration date */
    hasValidSession = () => {
        const oauth2 = this.getData('oauth2');
        return oauth2 && DateUtils.withOffsetDate(oauth2.access_expires_at) > new Date();
    };
}

export default new User();
