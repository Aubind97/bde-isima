import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { Loads } from 'react-loads';
import User from './Auth/Components/User';
import { CRUD } from 'utils';

const requestIsAuthenticated = () => {
    if (User.hasActiveSession()) {
        if (User.hasValidSession()) {
            return CRUD.update({
                url: '/retrieve',
                quietSucceed: true,
                quietRejection: true,
                onSucceed: response => {
                    const { messages, ...data } = response.data;
                    User.setData(Object.assign(User.getAllData(), data));
                },
                onRejection: () => User.clear()
            });
        }
    }
    User.clear();
    return Promise.reject();
}

export default ({ component: Component, ...rest }) => (
    <Loads load={requestIsAuthenticated}>
        {({ isRejected, isResolved }) => (
            <>
                { isRejected && <Redirect to={`/?to=${window.location.pathname}`} /> }
                { isResolved && <Route {...rest} render={props => <Component {...props} />} /> }
            </>
        )}
    </Loads>
);
    