import React from 'react';
import Papa from 'papaparse';
import { withSnackbar } from 'notistack';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DatabaseImport from 'mdi-material-ui/DatabaseImport';
import { Accept, CRUD } from 'utils';

export default withSnackbar(class CSVImporter extends React.Component {
    state = {
        open: false,
        progress: '',
        files: [],
        entries: [],
        submitting: false
    };

    handleConfirmDialog = open => this.setState({ open });

    setProgress = progress => this.setState({ progress });

    resetState = () => {
        this.setState({ open: false, files: [], entries: [], submitting: false });

        if (this.props.tableRef.current)
            this.props.tableRef.current.onQueryChange();
    };
    
    onCSVDrop = files => this.setState({ files });

    onCSVDropAccepted = files => {
        Papa.parse(files[0], { 
            header: true,
            skipEmptyLines: true,
            complete: results => {
                this.handleConfirmDialog(true);
                this.setState({ entries: results.data });
            }
        });
        //More config options if needed at https://www.papaparse.com/docs
    };

    update = () => {
        this.setState({ submitting: true });

        CRUD.update({
            url: '/articles/batch',
            newData: Object.assign({}, { articles: this.state.entries }),
            onUploadProgress: this.setProgress,
            onSucceed: this.resetState,
            onRejection: this.resetState,
            props: this.props
        });
    };

    render() {
        const { open, progress, files, entries, submitting } = this.state;
        return (
            <>
                <Accept
                    noLabel
                    noPreview
                    noButton
                    accept="text/csv"
                    onDrop={this.onCSVDrop}
                    onDropAccepted={this.onCSVDropAccepted}
                    files={files}
                    icon={<DatabaseImport />}
                />

                <Dialog
                    open={open}
                    onClose={() => this.handleConfirmDialog(false)}
                    aria-labelledby="alert-validation-title"
                    aria-describedby="alert-validation-description"
                >
                    <DialogTitle id="alert-validation-title">Voulez-vous vraiment mettre à jour {entries.length} lignes ?</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-validation-description">
                            Vous ne pourrez pas revenir en arrière.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button aria-label="Annuler" variant="outlined" onClick={() => this.handleConfirmDialog(false)} color="primary">
                            Annuler
                        </Button>
                        <Button aria-label="Importer" variant="contained" disabled={submitting} onClick={this.update} color="primary" autoFocus>
                            {submitting && progress ? progress : `Importer`}
                        </Button>
                    </DialogActions>
                </Dialog>
            </>
        );
    }
});