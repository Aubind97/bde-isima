import React from 'react';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import Avatar from '@material-ui/core/Avatar';
import ButtonBase from '@material-ui/core/ButtonBase';
import Typography from '@material-ui/core/Typography';
import LazyLoad from 'react-lazyload';
import CashingSnack from './CashingSnack';
import Skeleton from '@material-ui/lab/Skeleton';
import useInterval from 'utils/misc/useInterval';
import { Image } from 'utils';
import ListeuxContext from '../../ListeuxContext';
import Shake from './Shake';

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

/**
 * This component is responsible for displaying an article in the Catalog component
 */
export default function Article({ classes, selected, article, updateBalance, shake, style }) {

    /** States initialisation */
    const [disabled, setDisabled] = React.useState(false);
    const isListeux = React.useContext(ListeuxContext);
    const { enqueueSnackbar } = useSnackbar();

    useInterval(() => {
        setDisabled(disabled => !disabled);
    }, isListeux ? getRandomArbitrary(2500, 5000) : null);

    /**
     * Triggers the dynamic snackbar
     */
    const triggerTransaction = () => {
        enqueueSnackbar('', {
            anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'right'
            },
            content: key => (
                <CashingSnack id={key} selected={selected} article={article} updateBalance={updateBalance} />
            ),
        });
    };

    return (
        <div className={disabled ? classes.disabled : undefined} style={style}>
            <Shake shake={shake}>
                <ButtonBase className={classes.block} onClick={triggerTransaction} disabled={disabled}>
                    <LazyLoad
                        height={48}
                        offset={48}
                        scroll
                        overflow
                        once
                        placeholder={<Skeleton className={classes.marginAuto} variant="circle" width={48} height={48} />}
                    >
                        <Avatar className={classes.marginAuto} src={Image.display(article.photo)} alt={article.name} />
                    </LazyLoad>
                    <Typography component="div" variant="caption" color="inherit">
                        {article.name}
                    </Typography>
                    <Typography component="div" variant="caption" color="inherit">
                        {!!+selected.is_member ? article.member_price : article.price} €
                    </Typography>
                </ButtonBase>
            </Shake>
        </div>
    );
};

Article.propTypes = {
    /** CSS classes for inline-style */
    classes: PropTypes.object,
    /** The selected user */
    selected: PropTypes.object,
    /** The current article */
    article: PropTypes.object,
    /** Callback to update user's balance */
    updateBalance: PropTypes.func,
};