import React from 'react';
import classnames from 'classnames';
import { useLoads } from 'react-loads';
import { useSnackbar } from 'notistack';
import { makeStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Close from 'mdi-material-ui/Close';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import AlertCircleOutline from 'mdi-material-ui/AlertCircleOutline';
import Undo from 'mdi-material-ui/Undo';
import CloseCircle from 'mdi-material-ui/CloseCircle';
import TimerSand from 'mdi-material-ui/TimerSand';
import CheckboxMarkedCircleOutline from 'mdi-material-ui/CheckboxMarkedCircleOutline';
import MessageAlertOutline from 'mdi-material-ui/MessageAlertOutline';
import Toolbar from '@material-ui/core/Toolbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { CRUD } from 'utils';

const useStyles = makeStyles(theme => ({
    appBar: {
        borderRadius: 5,
    },
    icon: {
        margin: theme.spacing(.5),
    },
    success: { color: '#27ae60' },
    error: { color: '#C91F37' },
    info: { color: '#222222' },
}));

export default React.forwardRef(({ id, selected, article, updateBalance }, ref) => {

    /** States initialisation */
    const classes = useStyles();
    const [insertedId, setInsertedId] = React.useState(null);
    const [gotCancelled, setGotCancelled] = React.useState(false);
    const { closeSnackbar } = useSnackbar();

    const resetState = React.useCallback(() => setInsertedId(null), []);

    const onCancel = React.useCallback(() => {
        if (insertedId) {
            setGotCancelled(true);

            updateBalance(+(!!+selected.is_member ? article.member_price : article.price));
            
            return CRUD.delete({
                url: `/transactions/${insertedId}`,
                onSucceed: resetState,
                onRejection: resetState,
                quietSucceed: true,
                quietRejection: true,
            });

        }
    }, [selected, article, insertedId, resetState, updateBalance]);

    const onTransaction = React.useCallback(() => {
        const amount = !!+selected.is_member ? article.member_price : article.price;

        return CRUD.add({
            url: '/transactions',
            newData: {
                amount: -amount,
                description: article.name,
                receiver_id: selected.id,
                receiver_name: `${selected.lastname} ${selected.firstname}`,
                article_id: article.id,
            },
            quietSucceed: true,
            quietRejection: true,
            onSucceed: response => {
                setInsertedId(response.data.inserted_id);
                updateBalance(-amount);

                // Update history
                const event = document.createEvent('MutationEvents');
                event.initEvent('onTransactionMade', true, true);
                window.dispatchEvent(event);
            },
            onRejection: resetState,
        });
    }, [article, selected, updateBalance, resetState]); 
    const { update: undo, isResolved, isPending, isRejected } = useLoads(onTransaction, { update: onCancel });

    const onClose = React.useCallback(() => closeSnackbar(id), [id, closeSnackbar]);

    const getColor = () => {
        if (isRejected || gotCancelled) return classes.error;
        if (isResolved) return classes.success;
        if (isPending) return classes.info;
    };

    return (
        <AppBar innerRef={ref} className={classes.appBar} position="static" color="inherit">
            <Toolbar>
                <Grid container>
                    <Grid container item xs={2} justify="center" alignItems="center">
                        <span className={classnames(classes.icon, getColor())}>
                            {isPending && <TimerSand />}
                            {isRejected && <MessageAlertOutline />}
                            {(isResolved && !gotCancelled) && <CheckboxMarkedCircleOutline />}
                            {(isResolved && gotCancelled) && <CloseCircle /> }
                        </span>
                    </Grid>
                    <Grid container item xs={4} justify="center" alignItems="center">
                        <Typography variant="caption" align="center">
                            {isPending && "Transaction en cours ..."}
                            {isRejected && "Transaction échouée"}
                            {(isResolved && !gotCancelled) && `Encaissé : ${article.name}`}
                            {(isResolved && gotCancelled) && "Transaction annulée"}
                        </Typography>
                    </Grid>
                    <Grid container item xs={6} justify="flex-end" alignItems="center">

                        <span className={classes.icon}>
                            {isRejected && <AlertCircleOutline />}
                            {isPending && <CircularProgress size={24} color="inherit" />}
                            {(isResolved && !gotCancelled) && (
                                <IconButton
                                    onClick={undo}
                                    aria-label="Annuler"
                                    color="inherit"
                                >
                                    <Undo />
                                </IconButton>
                            )}
                        </span>

                        {isResolved && (
                            <IconButton
                                onClick={onClose}
                                aria-label="Fermer la notification"
                                color="inherit"
                            >
                                <Close />
                            </IconButton>
                        )}

                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    )
});
