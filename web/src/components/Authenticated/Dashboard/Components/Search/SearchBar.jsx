import React from 'react';
import { makeStyles } from '@material-ui/core';
import Draggable from 'react-draggable';
import Fab from '@material-ui/core/Fab';
import Hidden from '@material-ui/core/Hidden';
import Magnify from 'mdi-material-ui/Magnify';
import Input from '@material-ui/core/Input';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from 'mdi-material-ui/Menu';
import Fade from '@material-ui/core/Fade';
import Box from '@material-ui/core/Box';
import MobileMenu from '../Menu/MobileMenu';
import Skeleton from '@material-ui/lab/Skeleton';
import MenuAuthorizer from '../Menu/MenuAuthorizer';
import User from 'components/Authenticated/Auth/Components/User';
import ClubsContext from 'components/General/ClubsContext';
import scopes_mapping from 'configs/scopes_mapping';

const SearchUser = React.lazy(() => import('../../../Hub/Pages/Home/Components/SearchUser'));
const Notifications = React.lazy(() => import('../Notifications/Notifications'));
const AvatarMenu = React.lazy(() => import('../AvatarMenu'));
const ModulesMenu = React.lazy(() => import('../ModulesMenu'));
const Cashing = React.lazy(() => import('./Cashing'));

const useStyles = makeStyles(theme => ({
    container: {
        margin: theme.spacing(0, 1, 0, 0),
    },
    placeholder: {
        margin: theme.spacing(0, 1),
        color: 'rgba(0, 0, 0, 0.5)',
    },
    magnify: {
        margin: theme.spacing(0, 2, 0, 1),
    },
    fab: {
        zIndex: 1200,
        position: 'fixed',
        bottom: theme.spacing(6),
        right: theme.spacing(2),
    },
    grow: {
        flexGrow: 1,
    },
    toolbar: {
        height: 64,
    },
}));

export default function SearchBar() {

    /** States initialisation */
    const classes = useStyles();
    const [isOpen, setIsOpen] = React.useState(false);
    const [isMenuOpen, setIsMenuOpen] = React.useState(false);
    const [input, setInput] = React.useState('');
    const [selected, setSelected] = React.useState(null);
    const clubs = React.useContext(ClubsContext);
    const [isAuthorized] = React.useState(new MenuAuthorizer(User.getData('scopes'), scopes_mapping(clubs)).hasScope('can-create-transactions'));

    console.log(isOpen)

    const handleOpen = () => {
        console.log('hey')
        setIsOpen(true);
    };

    const toggleDrawer = isOpen => () => setIsMenuOpen(isOpen);

    const onSelection = selected => {
        setIsOpen(true);
        setSelected(selected);
    };

    const onClose = () => {
        setInput('');
        setIsOpen(false);
    };

    const onClearRequested = () => {
        setSelected(null);
        setInput('');
    };

    const updateBalance = amount => {
        setSelected(selected => ({
            ...selected,
            balance: (parseFloat(selected.balance) + parseFloat(amount)).toFixed(2),
        }));
    };

    return (
        <>
            <React.Suspense>
                {isAuthorized && (
                    <Hidden lgUp>
                        <Draggable axis="both" bounds="body" onMouseDown={handleOpen}>
                            <Fab className={classes.fab} aria-label="Rechercher un membre" color="primary">
                                <Magnify />
                            </Fab>
                        </Draggable>
                    </Hidden>
                )}

                {isAuthorized && (
                    <Cashing
                        open={isOpen}
                        selected={selected}
                        onClose={onClose}
                        updateBalance={updateBalance}
                        onSelection={onSelection}
                        onClearRequested={onClearRequested}
                    />
                )}
            </React.Suspense>

            <MobileMenu isOpen={isMenuOpen} toggleDrawer={toggleDrawer} />

            <AppBar position="static" color="inherit">
                <Toolbar className={classes.toolbar} variant="dense">

                    <Hidden lgUp>
                        <IconButton
                            color="inherit"
                            aria-label="Ouvrir le menu"
                            onClick={toggleDrawer(true)}
                        >
                            <MenuIcon />
                        </IconButton>
                    </Hidden>

                    <Hidden lgUp>
                        <div className={classes.grow} />
                    </Hidden>

                    <Hidden mdDown>
                        <div className={classes.grow}>
                            <Fade in={isAuthorized}>
                                <Box className={classes.container} display="flex" flexGrow={1} alignItems="center">
                                    <Magnify className={classes.magnify} />

                                    <SearchUser
                                        searchInput={input}
                                        placeholder="Rechercher un membre"
                                        label="Rechercher un membre"
                                        component={Input}
                                        selected={selected}
                                        onChange={setInput}
                                        onSelection={onSelection}
                                        onClearRequested={onClearRequested}
                                    />

                                </Box>
                            </Fade>
                        </div>
                    </Hidden>

                    <React.Suspense fallback={[...Array(3).keys()].map(x => <Skeleton variant="circle" width={40} height={40} className={classes.placeholder} key={x} />)}>
                        <ModulesMenu />
                        <Notifications />
                        <AvatarMenu />
                    </React.Suspense>

                </Toolbar>
            </AppBar>
        </>
    );
};
