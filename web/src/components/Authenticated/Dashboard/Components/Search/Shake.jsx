import React from 'react';
import { ShakeCrazy } from 'reshake';

/**
 * This component is responsible for wrapping articles in Shake animation
 */
export default function Shake ({ shake, children}) {

    return shake ? (
        <ShakeCrazy
            h={100}
            v={100}
            r={360}
            dur={1000}
            int={38.7}
            max={100}
            fixed={true}
            fixedStop={false}
            freez={false}
            active={shake}
        >
            {children}
        </ShakeCrazy>
    ) : children;
};