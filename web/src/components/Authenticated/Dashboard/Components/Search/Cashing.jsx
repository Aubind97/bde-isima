import React from 'react';
import classnames from 'classnames';
import { useLoads } from 'react-loads';
import { useSwipeable } from 'react-swipeable'
import { makeStyles, useTheme, Paper, Input, Fab } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import CurrencyEur from 'mdi-material-ui/CurrencyEur';
import Close from 'mdi-material-ui/Close';
import CartOutline from 'mdi-material-ui/CartOutline';
import History from 'mdi-material-ui/History';
import Magnify from 'mdi-material-ui/Magnify';
import IconButton from '@material-ui/core/IconButton';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import LinearProgress from '@material-ui/core/LinearProgress';
import { CRUD, TrollDialog } from 'utils';
import SearchUser from 'components/Authenticated/Hub/Pages/Home/Components/SearchUser';
import User from 'components/Authenticated/Auth/Components/User';

const Transfer = React.lazy(() => import('../../../Hub/Pages/Home/Components/Transfer'));
const Catalog = React.lazy(() => import('./Catalog'));
const MyHistory = React.lazy(() => import('../../../Hub/Pages/Home/Components/History'));

const useStyles = makeStyles(theme => ({
    minHeight: {
        height: '100%',
        [theme.breakpoints.up('md')]: {
            height: 700,
        },
    },
    noOverflow: {
        overflow: 'hidden',
    },
    paper: {
        width: '100%',
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1.5),
        top: theme.spacing(1.5),
        color: theme.palette.text.secondary,
    },
    button: {
        margin: theme.spacing(2),
        width: 200,
    }, 
    fab: {
        margin: theme.spacing(2),
    }, 
    wrapper: {
        color: theme.palette.text.primary,
    },
}));

const DialogTitle = props => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <Close />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
};

const DialogContent = withStyles(theme => ({
    root: {
        padding: theme.spacing(3, 3, 0, 3),
        textAlign: 'center',
    },
}))(MuiDialogContent);

const DialogActions = withStyles(() => ({
    root: {
        margin: 0,
        padding: 0,
    },
}))(MuiDialogActions);

function TabPanel(props) {
    const { children, value, index, className, ...other } = props;

    return (
        <DialogContent
            className={className}
            role="tabpanel"
            hidden={value !== index}
            id={`cashing-tabpanel-${index}`}
            aria-labelledby={`cashing-tab-${index}`}
            {...other}
        >
            {children}
        </DialogContent>
    );
}

function a11yProps(index) {
    return {
        id: `cashing-tab-${index}`,
        'aria-controls': `cashing-tabpanel-${index}`,
    };
}

export default function Cashing({ open, selected, updateBalance, onClose, onSelection, onClearRequested }) {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [index, setIndex] = React.useState(fullScreen ? 0 : 1);
    const [input, setInput] = React.useState('');
    const [loadHistory, setLoadHistory] = React.useState(false);

    const getArticles = React.useCallback(() => CRUD.get({ 
        url: '/articles',
        params: { is_enabled: '1' },
    }), []);
    const { response: articles, isResolved } = useLoads(getArticles);

    const handleChangeIndex = index => () => setIndex(index);

    const handleChangeValue = (event, value) => setIndex(value);
    
    const onHistoryLoad = () => setLoadHistory(true);

    const handlers = useSwipeable({
        onSwipedLeft: () => setIndex(index > 2 ? index : index + 1),
        onSwipedRight: () => setIndex(index < 1 ? index : index - 1),
    });

    const onClear = () => {
        setInput('');
        onClearRequested();
    }

    const NoSearchMade = React.memo(() => (
        [...Array(3).keys()].map(x => (
            <TabPanel key={x} className={classes.minHeight} value={index} index={x + 1}>
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    flexDirection="column"
                >
                    <img
                        src="/images/illustrations/NoData.svg"
                        height="auto"
                        width="300"
                        alt="Aucune donnée"
                    />
                    <Typography variant="h6" gutterBottom>
                        Aucun résultat
                    </Typography>
                    <Button
                        className={classes.button}
                        startIcon={<Magnify />}
                        variant="contained"
                        size="large"
                        aria-label="Rechercher"
                        onClick={handleChangeIndex(0)}
                        color="primary"
                    >
                        Rechercher
                    </Button>
                </Box>
            </TabPanel>
        ))
    ));

    React.useEffect(() => {
        if (selected) {
            setIndex(1);
            setLoadHistory(false);
        };
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selected ? selected.id : selected]);

    return (
        <TrollDialog isCashingOpen={open}>
            <Dialog
                open={open}
                scroll="paper"
                fullScreen={fullScreen}
                keepMounted
                fullWidth
                maxWidth="md"
                onClose={onClose}
                aria-labelledby="cashing-dialog-title"
                aria-describedby="cashing-dialog-description"
                PaperProps={{ className: classes.minHeight }}
                {...handlers}
            >
                <DialogTitle id="cashing-dialog-title" onClose={onClose} classes={classes} />

                <React.Suspense fallback={(<TabPanel className={classes.minHeight}><LinearProgress color={!!+User.getPreferences('dark_mode') ? "secondary" : "primary"} /></TabPanel>)}>

                    <TabPanel className={classes.minHeight} value={index} index={0}>
                        <SearchUser
                            searchInput={input}
                            placeholder="Rechercher un membre"
                            label="Rechercher un membre"
                            component={Input}
                            selected={selected}
                            onChange={setInput}
                            onSelection={onSelection}
                            onClearRequested={onClear}
                        />
                    </TabPanel>

                    {selected ? [
                        <TabPanel className={classnames(classes.minHeight, classes.noOverflow)} key={0} value={index} index={1}>
                            <Catalog
                                selected={selected}
                                isResolved={isResolved}
                                articles={isResolved ? articles.data : null}
                                updateBalance={updateBalance}
                            />
                        </TabPanel>,

                        <TabPanel className={classes.minHeight} key={1} value={index} index={2}>
                            <MyHistory 
                                className={index !== 2 ? classes.collapsed : undefined} 
                                userId={selected.id} 
                                load={loadHistory} 
                                visible={index === 2}
                            />
                            {!loadHistory && (
                                <Fab className={classes.fab} variant="extended" aria-label="Charger" onClick={onHistoryLoad} color="primary">
                                    Charger l'historique
                                </Fab>
                            )}
                        </TabPanel>,

                        <TabPanel className={classes.minHeight} key={2} value={index} index={3}>
                            <Transfer
                                to={selected.card}
                                updateBalance={updateBalance}
                                noEmitter
                                allowCancel
                            />
                        </TabPanel>,
                    ] : (
                        <NoSearchMade />
                    )}

                </React.Suspense>

                <DialogActions>
                    <Paper className={classes.paper} elevation={12}>
                        <BottomNavigation value={index} onChange={handleChangeValue}>
                            <BottomNavigationAction
                                classes={{ wrapper: classes.wrapper }}
                                value={0}
                                label="Recherche"
                                icon={<Magnify />}
                                {...a11yProps(0)}
                            />
                            <BottomNavigationAction
                                classes={{ wrapper: classes.wrapper }}
                                value={1}
                                label="Encaisser"
                                icon={<CartOutline />}
                                {...a11yProps(1)}
                            />
                            <BottomNavigationAction
                                classes={{ wrapper: classes.wrapper }}
                                value={2}
                                label="Historique"
                                icon={<History />}
                                {...a11yProps(2)}
                            />
                            <BottomNavigationAction
                                classes={{ wrapper: classes.wrapper }}
                                value={3}
                                label="Envoyer"
                                icon={<CurrencyEur />}
                                {...a11yProps(3)}
                            />
                        </BottomNavigation>
                    </Paper>
                </DialogActions>
            </Dialog>
        </TrollDialog>
    );
};
