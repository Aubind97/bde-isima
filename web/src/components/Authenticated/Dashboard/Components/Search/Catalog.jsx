import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Magnify from 'mdi-material-ui/Magnify';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import Balance from 'components/Authenticated/Hub/Pages/Home/Components/Balance';
import useInterval from 'utils/misc/useInterval';
import ListeuxContext from '../../ListeuxContext';
import { useTheme, useMediaQuery } from '@material-ui/core';
import { VariableSizeGrid as Grid } from 'react-window';
import AutoSizer from "react-virtualized-auto-sizer";
import Article from './Article';

const useStyles = makeStyles(theme => ({
    container: {
        [theme.breakpoints.up('md')]: {
            height: 400,
        },
        [theme.breakpoints.down('md')]: {
            minHeight: '80%',
        },
    },
    header: {
        minHeight: '30%',
        [theme.breakpoints.down('md')]: {
            minHeight: '20%',
        },
    },
    block: {
        display: 'inline-block',
        width: '100%',
    },
    marginAuto: {
        margin: 'auto',
    },
    margin: {
        margin: theme.spacing(1, 0),
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        [theme.breakpoints.up('sm')]: {
            width: 'auto',
            margin: theme.spacing(1, 0),
        },
        margin: theme.spacing(.5, 0, 1, 0),
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    disabled: {
        background: 'rgba(247,241,227, 0.7)',
        '&:after': {
            background: 'rgba(247,241,227, 0.7)',
        },
    },
}));

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}

function smartSearch(a, b) {
    a = a.trim().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    b = b.trim().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    return a.includes(b);
}

export default function Catalog({ selected, isResolved, articles, updateBalance }) {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [input, setInput] = React.useState('');
    const [shake, setShake] = React.useState(false);
    const isListeux = React.useContext(ListeuxContext);
    const itemsPerRow = fullScreen ? 4 : 5;
    const filtered = articles.filter(article => smartSearch(article.name, input));

    const handleInputChange = event => setInput(event.target.value);

    const Cell = ({ columnIndex, rowIndex, style }) => {
        const item = filtered[rowIndex * itemsPerRow + columnIndex];

        if (!item) {
            return null;
        }

        return (
            <Article
                style={style}
                article={item}
                selected={selected}
                updateBalance={updateBalance}
                classes={classes}
                shake={shake}
            />
        );
    };

    // Shuffle array and activate shake animation
    useInterval(() => {
        if (isListeux) {
            if (getRandomInt(4) === 1) {
                setShake(true);
            }
            else {
                setShake(false);
            }
        }
    }, isListeux ? 5000 : null);

    // Shuffle array and activate shake animation
    useInterval(() => {
        if (isListeux) {
            if (getRandomInt(4) === 1) {
                shuffle(articles);
            }
        }
    }, isListeux ? 2500 : null);

    React.useEffect(() => {
        if(isListeux) {
            // Replace image by a troll image
            articles.forEach(x => {
                x.photo = '/images/easter_eggs/listeux/pradier.png'
            });
        }
        else if (selected && selected.stats && selected.stats.articles) {

            // Sort articles by stats units
            const stats = selected.stats.articles;
            articles.sort((a, b) => {
                const a_stats = stats.find(x => x.id === a.id);
                const b_stats = stats.find(x => x.id === b.id);

                if (!a_stats || !b_stats)
                    return a_stats ? -1 : 1;

                if (a_stats.units < b_stats.units)
                    return 1;

                return a_stats.units > b_stats.units ? -1 : 0;
            });
        }
    }, [selected, articles, isListeux]);

    return (
        <Box display="flex" flexDirection="column" flexGrow={1} style={{ height: '100%' }}>
            <Typography variant={fullScreen ? "subtitle2" : "h6"} align="center">
                {selected.firstname} {selected.lastname} {selected.nickname && `(${selected.nickname})`} - Carte N° {selected.card}
            </Typography>

            <Balance balance={selected.balance} variant={fullScreen ? "subtitle2" : "h4"} />

            <div className={classes.search}>
                {!isListeux && (
                    <InputBase
                        placeholder="Rechercher un article"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput
                        }}
                        startAdornment={<Magnify className={classes.rightIcon} />}
                        value={input}
                        onChange={handleInputChange}
                    />
                )}
            </div>

            {(isResolved && articles.length === 0) && (
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    flexDirection="column"
                >
                    <img
                        src="/images/illustrations/NoData.svg"
                        height="auto"
                        width="300"
                        alt="Aucune donnée"
                    />
                    <Typography
                        variant="subtitle2"
                        gutterBottom
                    >
                        Aucun article !
                    </Typography>
                </Box>
            )}

            <AutoSizer>
                {({ height, width }) => (
                    isResolved && (
                        <Grid
                            columnCount={itemsPerRow}
                            columnWidth={index => Math.floor(width / itemsPerRow) - 5}
                            height={height - 110}
                            rowCount={Math.ceil(filtered.length / itemsPerRow)}
                            rowHeight={index => 100}
                            width={width}
                        >
                            {Cell}
                        </Grid>
                    )
                )}
            </AutoSizer>
        </Box>
    );
};
