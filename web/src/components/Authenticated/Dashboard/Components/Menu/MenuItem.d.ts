export default abstract class MenuItem {
    text: string;
    path: string;
    isActive: boolean;
    icon?: React.ReactNode;
    [key: string]: any;

    constructor(text: string, path: string, icon: React.ReactNode) {
        this.text = text;
        this.path = path;
        this.isActive = false;
        this.isVisible = false;
        this.icon = icon;
    }
}