import MenuItem from './MenuItem';

export default abstract class Scope {

    name: string;
    fallback?: string;
    isActive: boolean;
    items?: Array<MenuItem>;
    handler?: Scope;
    base: Array<any>;
    [key: string]: any;

    constructor(name: string, base: Array<any>, fallback?: string, handler?: Scope) {
        this.name = name;
        this.base = base;
        this.fallback = fallback;
        this.isActive = false;
        this.handler = handler;
    }

    buildMenuPart(): Array<Scope> {
        return this.handler ? this.handler.buildMenuPart().concat(this) : new Array(this);
    }
}
