import React from 'react';
import classnames from 'classnames';
import { animateScroll } from 'react-scroll'
import { Link, withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core';
import isInternalLink from "is-internal-link";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import Avatar from '@material-ui/core/Avatar';
import Zoom from '@material-ui/core/Zoom';
import IconButton from '@material-ui/core/IconButton';
import ChevronUp from 'mdi-material-ui/ChevronUp';
import ChevronDown from 'mdi-material-ui/ChevronDown';
import DashNav from './DashNav';

const AdapterLink = React.forwardRef((props, ref) => <Link innerRef={ref} {...props} />);

const styles = theme => ({
    root: {
        width: 100,
        backgroundColor: theme.palette.common.black,
        position: 'fixed',
    },
    fullHeight: {
        height: '100vh',
    },  
    grow: {
        flexGrow: 1,
    },
    button: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: 50,
        width: 100,
        color: 'inherit !important',
    },
    active: {
        width: 100,
        backgroundColor: 'rgba(0,0,0,0.5)',
        clipPath: 'polygon(100% 0, 100% 50%, 100% 100%, 0% 100%, 20% 50%, 0% 0%)',
        '&:hover': {
            backgroundColor: 'rgba(0,0,0,0.3)',
        }
    },
    margin: {
        margin: theme.spacing(2),
    },
    scroller: {
        margin: theme.spacing(0.5),
    },
    itemsMenu: {
        maxHeight: 500,
        width: 100,
        overflow: 'auto',
    },
    icon: {
        width: 30,
        height: 30,
        margin: '0 auto',
    },
    inherit: {
        color: 'inherit !important',
    },
});

export default withRouter(withStyles(styles)(class Menu extends React.Component {

    handleClickItem = path => () => {
        const { history } = this.props;

        if (isInternalLink(path))
            history.push(path);
        else
            window.location.replace(path);
    };

    scroll = offset => () => {
        animateScroll.scrollMore(offset, {
            containerId: "clubs_list"
        });
    };

    render() {
        const { classes, location } = this.props;

        return (
            <Paper className={classes.root} square>
                <Box
                    className={classes.fullHeight}
                    display="flex"
                    alignItems="center"
                    flexDirection="column"
                >
                    <Avatar
                        className={classes.margin}
                        src="/images/favicons/android-chrome-192x192.png"
                        component={Link}
                        to="/hub"
                        alt="Logo BDE ISIMA"
                    />

                    <DashNav {...this.props}>
                        {({ menu }) => (
                            <>
                                {/* Build Scope Buttons */}
                                {menu.map(scope =>
                                    <Zoom
                                        key={scope.name}
                                        in={Boolean(location.pathname)}
                                    >
                                        <ListItem
                                            key={scope.name}
                                            className={classnames(scope.isActive ? classes.active : undefined, classes.button)}
                                            component={AdapterLink}
                                            to={scope.fallback}
                                            aria-label={scope.name}
                                            button
                                            disableGutters
                                        >
                                            <Typography variant="subtitle2" color="secondary">
                                                {scope.name}
                                            </Typography>
                                        </ListItem>
                                    </Zoom>
                                )}
                                {/* Build Scope Buttons */}

                                <div className={classes.grow} />

                                <span className={classes.button}>
                                    <IconButton
                                        className={classes.scroller}
                                        onClick={this.scroll(-300)}
                                        aria-label="Scroller vers le haut"
                                        color="secondary"
                                    >
                                        <ChevronUp />
                                    </IconButton>
                                </span>

                                {/* Build Items Buttons */}
                                <Box
                                    id="clubs_list"
                                    display="flex"
                                    flexDirection="column"
                                    className={classes.itemsMenu}
                                    component={List}
                                    disablePadding
                                >                                    
                                    {menu.filter(scope => scope.items && scope.isActive).map(scope =>
                                        scope.items.map(item => item.icon && item.isVisible && (
                                            <Tooltip
                                                key={item.path}
                                                title={item.text.toUpperCase()}
                                                placement="right"
                                                enterDelay={500}
                                            >
                                                <Zoom in={Boolean(location.pathname)}>
                                                    <ListItem
                                                        style={{ padding: 0 }}
                                                        component={!isInternalLink(item.path) ? "div" : AdapterLink}
                                                        onClick={this.handleClickItem(item.path)}
                                                        to={item.path}
                                                        aria-label={item.text}
                                                        disableGutters
                                                        button
                                                        classes={{
                                                            root: classes.inherit,
                                                        }}
                                                    >
                                                        <span className={classnames(item.isActive ? classes.active : undefined, classes.button)}>
                                                            <item.icon.type
                                                                {...item.icon.props}
                                                                className={classes.icon}
                                                            />
                                                        </span>
                                                    </ListItem>
                                                </Zoom> 
                                            </Tooltip>
                                        )
                                    ))}

                                </Box>

                                <span className={classes.button}>
                                    <IconButton
                                        className={classes.scroller}
                                        onClick={this.scroll(300)}
                                        aria-label="Scroller vers le bas"
                                        color="secondary"
                                    >
                                        <ChevronDown />
                                    </IconButton>
                                </span>

                                {/* Build Items Buttons */}
                            </>
                        )}
                    </DashNav>
                </Box>
            </Paper>
        );
    }
}));
