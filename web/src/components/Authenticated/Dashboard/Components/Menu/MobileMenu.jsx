import React from 'react';
import isInternalLink from "is-internal-link";
import { Link, withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import HomeOutline from 'mdi-material-ui/HomeOutline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import LazyLoad from 'react-lazyload';
import Skeleton from '@material-ui/lab/Skeleton';
import User from '../../../Auth/Components/User';
import { UserAvatar } from 'utils';
import DashNav from './DashNav';

const AdapterLink = React.forwardRef((props, ref) => <Link innerRef={ref} {...props} />);

const useStyles = makeStyles(theme => ({
    title: {
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(2),
    },
    avatar: {
        margin: theme.spacing(2),
        backgroundColor: '#2A2E43',
    },
    link: {
        color: 'inherit !important',
    },
    selected: {
        color: '#fff',
    },
    menuBtn: {
        padding: theme.spacing(0, 2),
        width: '90%',
        borderRadius: '0px 80px 80px 0px',
        color: 'inherit !important',
    },
}));

export default React.memo(withRouter(function MobileMenu({ isOpen, toggleDrawer, history, ...props }) {

    /** States initialisation */
    const classes = useStyles();
    const [username] = React.useState(User.getUsername());
    const [iOS] = React.useState(process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent));

    const handleClickItem = React.useCallback(path => () => {
        if (isInternalLink(path))
            history.push(path);
        else
            window.location.replace(path);
    }, [history]);

    const ScopeList = React.memo(({ scope }) => (
        scope.items.map(item => item.icon && item.isVisible && (
            <ListItem
                key={item.path}
                className={classes.link}
                disableGutters
            >
                <Button
                    className={classes.menuBtn}
                    component={!isInternalLink(item.path) ? "div" : AdapterLink}
                    onClick={handleClickItem(item.path)}
                    to={item.path}
                    variant={`${window.location.origin}${window.location.pathname}` === `${window.location.origin}${item.path}` ? "contained" : "text"}
                    color="primary"
                >
                    <Box
                        display="flex"
                        flexGrow={1}
                        alignItems="center"
                        justifyContent="flex-start"
                    >
                        <ListItemIcon>
                            <LazyLoad
                                height={40}
                                offset={40}
                                scroll
                                overflow
                                once
                                placeholder={<Skeleton variant="circle" width={40} height={40} />}
                            >
                                {React.cloneElement(item.icon, {
                                    className: `${window.location.origin}${window.location.pathname}` === `${window.location.origin}${item.path}` ? classes.selected : undefined,
                                    color: 'inherit',
                                })}
                            </LazyLoad>
                        </ListItemIcon>
                        <Box m={1} className={`${window.location.origin}${window.location.pathname}` === `${window.location.origin}${item.path}` ? classes.selected : undefined}>
                            {item.text.toUpperCase()}
                        </Box>
                    </Box>
                </Button>
            </ListItem>
        ))
    ));

    const MenuList = React.memo(({ menu }) => (
        menu.map(scope => (
            <React.Fragment key={scope.name}>
                <Typography
                    className={classes.title}
                    variant="subtitle1"
                    gutterBottom
                >
                    {scope.name}
                </Typography>

                <Divider />

                <ScopeList scope={scope} />

            </React.Fragment>
        ))
    ));

    return (
        <SwipeableDrawer
            PaperProps={{
                style: {
                    width: '70vw',
                }
            }}
            open={isOpen}
            onClose={toggleDrawer(false)}
            onOpen={toggleDrawer(true)}
            disableBackdropTransition={!iOS}
            disableDiscovery={iOS}
        >
            <div
                tabIndex={0}
                role="button"
                onClick={toggleDrawer(false)}
                onKeyDown={toggleDrawer(false)}
            >
                <div>
                    <List>
                        <Box display="flex" alignItems="center">

                            <UserAvatar
                                AvatarProps={{
                                    'className': classes.avatar,
                                    'alt': 'Avatar utilisateur',
                                }}
                            />

                            <Typography variant="h6">
                                {username}
                            </Typography>

                        </Box>

                        <Typography
                            className={classes.title}
                            variant="subtitle1"
                            gutterBottom
                        >
                            HUB
                            </Typography>

                        <Divider />

                        <ListItem
                            button
                            component={Link}
                            to="/hub"
                            className={classes.link}
                        >
                            <ListItemIcon>
                                <HomeOutline color="inherit" />
                            </ListItemIcon>
                            <Box color="text.primary" m={1}>
                                HUB
                                    </Box>
                        </ListItem>

                        {/* Build Items Buttons */}
                        <DashNav {...props}>
                            {({ menu }) => <MenuList menu={menu} />}
                        </DashNav>
                        {/* Build Items Buttons */}

                        <ListItem alignItems="center" disableGutters>
                            <Typography variant="caption" align="center" color="textSecondary" component={Box} display="flex" flexGrow={1}>
                                Version {global.appVersion}
                            </Typography>
                        </ListItem>

                    </List>
                </div>
            </div>
        </SwipeableDrawer>
    );
}));
