import React from 'react';
import 'array-flat-polyfill';
import Avatar from '@material-ui/core/Avatar';
import Scope from './Scope';
import MenuItem from './MenuItem.d.ts';
import { Image } from 'utils';

export default class ClubScope extends Scope {

    labels = [
        'edit',
        'events',
        'news'
    ];

    constructor(name, base, fallback, handler) {
        super(name, base, fallback, handler);

        this.items = base.map(club => this.labels.map(path =>
            new MenuItem(
                club.name,
                `/hub/dashboard/${club.name.toLowerCase()}/${path}`,
                <Avatar
                    src={Image.display(club.logo)}
                    alt={`Logo ${club.name}`}
                />
            )
        )).flat(2);
    }
}
