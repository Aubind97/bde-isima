import React from 'react';
import Scope from './Scope';
import MenuItem from './MenuItem.d.ts';
import modules from 'configs/bde_modules';

export default class BDEScope extends Scope {
    constructor(name, fallback, handler) {
        super(name, [], fallback, handler);
        
        this.items = Object.getOwnPropertyNames(modules).map(propertyName => {
            const item = modules[propertyName];
            return new MenuItem(item.name, item.path, <item.icon color="secondary" />);
        });
    }
}
