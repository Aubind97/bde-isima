import isInternalLink from "is-internal-link";
import Authorizer from './Authorizer';
import Scope from './Scope';

type OAuthScope = {
    id: string;
    description: string;
}

export default class MenuAuthorizer implements Authorizer {

    scopes: Array<OAuthScope>;
    config: { [key: string]: any; };

    constructor(scopes: Array<OAuthScope>, config: Array<any>) {
        this.scopes = scopes;
        this.config = config;
    }

    hasScope(scope: string) {
        return this.scopes && this.scopes.findIndex(x => x.id === scope || (x.id === '*' && scope !== 'listeux')) !== -1;
    }

    isInScope(currentPath: string, currentScope: string) {
        const path = Object.getOwnPropertyNames(this.config).find(x => x === currentPath);
        if (path) {
            return this.config[path].includes(currentScope);
        }
        return false;
    }

    isAuthorized(currentPath: string): boolean {
        if (!isInternalLink(currentPath))
            return true;

        //Returns true if at least 1 authorization includes the given path
        const res = this.scopes ? 
            this.scopes.reduce((isAuthorized, currentScope) => {
                return isAuthorized 
                    || this.isInScope(currentPath, currentScope.id)
                || currentScope.id === '*'
            }, false as boolean)
            : false;

        return res;
    }

    filter(scopes: Array<Scope>): Array<Scope> {   
        // eslint-disable-next-line array-callback-return
        return scopes.filter(scope => {
            //Filter authorized MenuItem
            if (scope.items) {
                scope.items = scope.items.filter(item => this.isAuthorized(item.path));

                //Rebuild fallback for each scopes
                if (scope.items.length > 0 && scope.items[0]) {

                    //Exclude scopes composed only of external link
                    if (scope.items.filter(x => !isInternalLink(x.path)).length === scope.items.length) {
                        return false;
                    }
                    else {
                        scope.fallback = scope.items[0].path;

                        //Returns only scopes that have items
                        return scope.items.length > 0;
                    }
                }
            }
        });    
    }

    defaultPath(scopes: Array<Scope>): string {
        //Returns the first authorized path for Dashboard button redirection 
        for(const scope of scopes)
            for (const item of scope.items)
                if (this.isAuthorized(item.path) && isInternalLink(item.path))
                    return item.path;
        return null;
    }
}
