export default interface Authorizer {
    isAuthorized: Function,
    filter: Function,
    defaultPath: Function
}  