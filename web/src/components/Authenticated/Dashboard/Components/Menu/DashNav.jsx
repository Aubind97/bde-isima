import React from 'react';
import ClubScope from './ClubScope';
import BDEScope from './BDEScope';
import MenuBuilder from './MenuBuilder.ts';
import MenuAuthorizer from './MenuAuthorizer.ts';
import User from 'components/Authenticated/Auth/Components/User';
import scopes_mapping from 'configs/scopes_mapping';
import ClubsContext from 'components/General/ClubsContext';

/**
 * This component is responsible for fetching and building menu then providing menu to children
 */
export default function DashNav(props) {

    /** States initialisation */
    const [location, setLocation] = React.useState(props.location.pathname);
    const clubs = React.useContext(ClubsContext);
    const builder = new MenuBuilder(
        new MenuAuthorizer(User.getData('scopes'), scopes_mapping(clubs)),
        new ClubScope('CLUB', clubs, null, new BDEScope('BDE', null, null))
    );
    const [menu, setMenu] = React.useState(builder.buildMenu(props.location.pathname));
    
    /** States initialisation */
    React.useEffect(() => {

        function rebuild() {
            setMenu(builder.buildMenu(props.location.pathname));
        }

        if (location !== props.location.pathname){
            setLocation(props.location.pathname);
            rebuild();
        }

        /**
        * Subscribe to clubs loaded event so it rebuilds the menu
        */
        window.addEventListener('onClubsLoaded', rebuild);

        /**
        * Unsubscribe from clubs loaded event
        */
        return function cleanup() {
            window.removeEventListener('onClubsLoaded', rebuild);
        };
    }, [builder, location, props.location.pathname]);
        
    return props.children({ menu });
}
