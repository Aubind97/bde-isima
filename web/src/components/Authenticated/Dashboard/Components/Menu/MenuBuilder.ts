import { matchPath } from "react-router";
import Scope from './Scope';
import Authorizer from './Authorizer';
import MenuItem from './MenuItem';

export default class MenuBuilder {

    handler: Scope;
    visitor: Authorizer;

    constructor(visitor: Authorizer, handler: Scope) {
        this.visitor = visitor;
        this.handler = handler;
    }

    getHandler(): Scope {
        return this.handler;
    }

    getVisitor() : Authorizer {
        return this.visitor;
    }

    buildMenu(location: string) {
        return this.removeDuplicate(this.setActive(location, this.visitor.filter(this.getHandler().buildMenuPart())));
    }

    buildDefaultPath() {
        return this.visitor.defaultPath(this.getHandler().buildMenuPart());
    }

    getUnique(arr: Array<MenuItem>, comp: string): Array<MenuItem> {
        return arr
            .map((e: MenuItem) => e[comp])
            .map((e, i, final) => final.indexOf(e) === i && i)
            .filter(e => arr[e])
            .map(e => arr[e]);
    }

    removeDuplicate(scopes: Array<Scope>) : Array<Scope> {        
        scopes.forEach((scope: Scope) => {
            const active = scope.items.find(x => x.isActive);
            
            if(active) {
                const uniqueArr = this.getUnique(scope.items, 'text');
                scope.items.forEach(x => {
                    x.isVisible = x.path === active.path ||
                    uniqueArr.some(y => y.path === x.path && y.text !== active.text);
                });
            }
            else {
                const uniqueArr = this.getUnique(scope.items, 'text');
                scope.items.forEach(x => {
                    x.isVisible = uniqueArr.some(y => y.path === x.path);
                });
            }

        });
        return scopes;
    }

    setActive(location: string, scopes : Array<Scope>) : Array<Scope> {
        scopes.forEach(scope => {
            
            scope.isActive = scope.items.some(item => Boolean(matchPath(location, {
                path: item.path,
                exact: true
            })));

            if(scope.isActive) {
                scope.items.forEach(item => {
                    item.isActive = Boolean(matchPath(location, {
                        path: item.path,
                        exact: true
                    }));
                });
            }

        });

        return scopes; 
    }
}
