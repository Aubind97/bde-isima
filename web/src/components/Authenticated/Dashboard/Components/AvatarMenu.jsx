import React from 'react';
import { Link } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import Typography from '@material-ui/core/Typography';
import User from '../../Auth/Components/User';
import CogOutline from 'mdi-material-ui/CogOutline';
import Logout from 'mdi-material-ui/Logout';
import MessageAlertOutline from 'mdi-material-ui/MessageAlertOutline';
import { CRUD, UserAvatar } from 'utils';
import { withSnackbar } from 'notistack';
import BroadcastChannel from 'broadcast-channel';

const styles = theme => ({
    avatar: {
        margin: theme.spacing(0, 1.5, 0, 2.5),
        cursor: 'pointer'
    },
    btn: {
        color: theme.palette.text.primary,
        margin: theme.spacing(0, 2, 0, 0)
    },
    link: {
        color: 'inherit !important'
    },
});

export default withSnackbar(withStyles(styles)(class AvatarMenu extends React.Component {
    state = {
        anchorEl: null,
    };

    requestLogout = () => {
        new BroadcastChannel('bde-isima-channel').postMessage("onRequestFlush");
    
        CRUD.update({
            url: '/logout',
            quietSucceed: true,
            quietRejection: true,
            onSucceed: () => {
                User.clear();
                window.location.replace('/');
            },
            props: this.props
        });
    };

    handleMenuOpen = event => this.setState({ anchorEl: event.currentTarget });

    handleMenuClose = () => this.setState({ anchorEl: null });

    render() {
        const { classes } = this.props;
        const { anchorEl } = this.state;

        const open = Boolean(anchorEl);

        return (
            <>
                <UserAvatar 
                    AvatarProps={{
                        'className': classes.avatar,
                        'onClick': this.handleMenuOpen,
                        'aria-label': "Ouvrir le menu utilisateur",
                        'aria-owns': open ? "avatar-menu" : undefined,
                        'aria-haspopup': "true",
                        'alt': 'Avatar utilisateur' 
                    }}      
                />

                <Menu
                    id="avatar-menu"
                    anchorEl={this.state.anchorEl}
                    getContentAnchorEl={null}
                    anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "right",
                    }}
                    transformOrigin={{
                        vertical: "top",
                        horizontal: "right",
                    }}
                    open={open}
                    onClose={this.handleMenuClose}
                >
                    <MenuItem
                        className={classes.link}
                        component={Link}
                        to="/hub/settings"
                        onClick={this.handleMenuClose}
                    >
                        <CogOutline className={classes.btn} />
                        <Typography color="textPrimary">Paramètres</Typography>
                    </MenuItem>

                    {/*<MenuItem
                        className={classes.link}
                        component={Link}
                        to="/hub/developers"
                        onClick={this.handleMenuClose}
                    >
                        <DeveloperBoard className={classes.btn} />
                        <Typography color="textPrimary">Développeurs</Typography>
                    </MenuItem>*/}

                    <MenuItem
                        className={classes.link}
                        component={Link}
                        to="/hub/feedback"
                        onClick={this.handleMenuClose}
                    >
                        <MessageAlertOutline className={classes.btn} />
                        <Typography color="textPrimary">Feedback</Typography>
                    </MenuItem>

                    <MenuItem 
                        onClick={this.requestLogout}
                    >
                        <Logout className={classes.btn} />
                        Déconnexion
                    </MenuItem>
                </Menu>
            </>
        );
    }
}));
