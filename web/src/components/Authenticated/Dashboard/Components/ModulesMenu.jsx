import React from 'react';
import { Link } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Apps from 'mdi-material-ui/Apps';
import CalculatorVariant from 'mdi-material-ui/CalculatorVariant';
import PuzzleOutline from 'mdi-material-ui/PuzzleOutline';

const useStyles = makeStyles(theme => ({
    btn: {
        color: theme.palette.text.primary,
        margin: theme.spacing(0, 1)
    },
    listItem: {
        padding: theme.spacing(1),
        height: 48,
        color: 'inherit !important'
    },
    header: {
        padding: theme.spacing(1),
        margin: theme.spacing(0, 2),
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    title: {
        display: 'flex',
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
}));

/**
 * This component is responsible for displaying all modules item in a dropdown menu
 */
export default React.memo(function ModulesMenu() {

    /** States initialisation */
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const isModuleMenuOpen = Boolean(anchorEl);

    const handleModuleMenuOpen = event => setAnchorEl(event.currentTarget);

    const handleModuleMenuClose = () => setAnchorEl(null);

    return (
        <>
            <IconButton
                className={classes.btn}
                aria-label="Voir les modules"
                aria-owns={isModuleMenuOpen ? 'module-menu' : undefined}
                aria-haspopup="true"
                onClick={handleModuleMenuOpen}
                color="inherit"
            >
                <Apps />
            </IconButton>

            <Menu
                id="module-menu"
                anchorEl={anchorEl}
                open={isModuleMenuOpen}
                onClose={handleModuleMenuClose}
                getContentAnchorEl={null}
                anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                MenuListProps={{
                    disablePadding: true
                }}
                PaperProps={{
                    style: {
                        maxHeight: 72 * 10,
                        width: 300,
                    },
                }}
                disableAutoFocusItem
            >
                <Typography
                    className={classes.header}
                    variant="subtitle1"
                    gutterBottom
                >
                    Modules
                </Typography>

                <MenuItem
                    className={classes.listItem}
                    onClick={handleModuleMenuClose}
                    component={Link}
                    to="/hub/average"
                >
                    <Grid container justify="center">
                        <CalculatorVariant className={classes.btn} />
                        <Typography className={classes.title} variant="subtitle2" align="center" color="textPrimary">Calculateur de moyenne</Typography>
                    </Grid>
                </MenuItem>

                <MenuItem
                    className={classes.listItem}
                    onClick={handleModuleMenuClose}
                    component={Link}
                    to="/hub/feedback"
                >
                    <Grid container justify="center">
                        <PuzzleOutline className={classes.btn} />
                        <Typography className={classes.title} variant="subtitle2" align="center" color="textPrimary">Proposez vos extensions !</Typography>
                    </Grid>
                </MenuItem>
            </Menu>
        </>
    );
});
