import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Send from 'mdi-material-ui/Send';
import { useTheme } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import { CRUD } from 'utils';

/**
 * This component is responsible for sending notifications to all of the website users
 */
export default function SendNotif({ isSendNotifOpen, handleDialogOpen }) {

    /** States initialisation */
    const theme = useTheme();
    const [message, setMessage] = React.useState('');
    const [submitting, setSubmitting] = React.useState(false);
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    /**
    * HTTP request that posts written message to the API 
    */
    function sendNotification() {
        setSubmitting(true);
        CRUD.add({
            url: `/notifications`,
            newData: { message },
            onSucceed: () => {
                handleDialogOpen(false)();
                setSubmitting(false);
            },
            onRejection: () => setSubmitting(false),
            props: { enqueueSnackbar, closeSnackbar }
        });
    };

    return (
        <Dialog open={isSendNotifOpen} fullScreen={fullScreen} onClose={handleDialogOpen(false)} aria-labelledby="send-notification-title">
            <DialogTitle id="send-notification-title">Envoyer une notification</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Cette notification sera envoyée à tous les membres
                </DialogContentText>
                <TextField 
                    autoFocus 
                    margin="dense" 
                    id="message" 
                    label="Message" 
                    type="text" 
                    value={message} 
                    onChange={e => setMessage(e.target.value)} 
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <Typography color="textSecondary">{message && message.length}/255</Typography>
                            </InputAdornment>
                        )
                    }}
                    fullWidth 
                    multiline 
                    rows={5} 
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDialogOpen(false)} color="inherit">
                    Annuler
                </Button>
                <Button startIcon={<Send />} disabled={submitting} onClick={sendNotification} color="primary" variant="contained">
                    {submitting ? <CircularProgress size={25} color="secondary" /> : "Envoyer"}
                </Button>
            </DialogActions>
        </Dialog>
    );
}

SendNotif.propTypes = {
    /** Boolean that orders whether or not the dialog should be open */
    isSendNotifOpen: PropTypes.bool,
    /** Callback to be called on dialog actions */
    handleDialogOpen: PropTypes.func,
    /** CSS classes for inline-style */
    classes: PropTypes.object
};
