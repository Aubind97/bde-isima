import React from 'react';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Typography from '@material-ui/core/Typography';
import { useTheme } from '@material-ui/core';
import { DateUtils } from 'utils';

/**
 * This component is responsible for displaying notification messages
 */
export default function ViewNotif({ notification, handleOnSelectedNotificationChange, formatter }) {

    /** States initialisation */
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        notification && (
            <Dialog open={Boolean(notification)} fullScreen={fullScreen} onClose={handleOnSelectedNotificationChange(null)} aria-labelledby="view-notification-title">
                <DialogTitle id="view-notification-title">
                    Nouvelle notification ! 
                    <DialogContentText variant="caption" color="textSecondary">
                        <TimeAgo
                            date={DateUtils.withOffsetDate(notification.date)}
                            formatter={formatter}
                        />
                    </DialogContentText>
                </DialogTitle>
                <DialogContent>
                    <Typography>
                        {notification.message}
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleOnSelectedNotificationChange(null)} color="inherit">
                        Fermer
                </Button>
                </DialogActions>
            </Dialog>
        )
    );
}

ViewNotif.propTypes = {
    /** Boolean that orders whether or not the dialog should be open */
    isViewNotifOpen: PropTypes.bool,
    /** The notification object queryied from the API to be displayed */
    notification: PropTypes.object,
    /** Callback to be called on dialog actions */
    handleDialogOpen: PropTypes.func,
    /** Localization for Time-ago */
    formatter: PropTypes.func
};
