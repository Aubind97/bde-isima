import React from 'react';
import queryString from 'query-string';
import Badge from '@material-ui/core/Badge';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import axios from 'axios';
import BellOutline from 'mdi-material-ui/BellOutline';
import Send from 'mdi-material-ui/Send';
import { useLoads } from 'react-loads';
import TimeAgo from 'react-timeago';
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';
import frenchStrings from 'react-timeago/lib/language-strings/fr';
import { CRUD, DateUtils } from 'utils';
import User from '../../../Auth/Components/User';
import MenuAuthorizer from '../Menu/MenuAuthorizer';
import SendNotif from './SendNotif';
import ViewNotif from './ViewNotif';
import ClubsContext from 'components/General/ClubsContext';
import scopes_mapping from 'configs/scopes_mapping';
import { withRouter } from 'react-router';

const formatter = buildFormatter(frenchStrings);

const useStyles = makeStyles(theme => ({
    btn: {
        color: theme.palette.text.primary,
        margin: theme.spacing(0, 1),
    },
    listItem: {
        padding: theme.spacing(1),
    },
    header: {
        padding: theme.spacing(1),
        margin: theme.spacing(0, 2),
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    margin: {
        marginTop: theme.spacing(1),
    },
    notifs: {
        width: 275,
    },
    send: {
        marginRight: theme.spacing(2),
    }
}));


/**
 * This component is responsible for displaying notifications menu
 */
export default withRouter(function Notifications({ location }) {

    /** States initialisation */
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);
    const [isSendNotifOpen, setIsSendNotifOpen] = React.useState(false);
    const [selected, setSelected] = React.useState(null); 
    const clubs = React.useContext(ClubsContext);
    const [isAuthorized] = React.useState(new MenuAuthorizer(User.getData('scopes'), scopes_mapping(clubs)).hasScope('can-create-notifications'));

    const open = Boolean(anchorEl);

    const getNotifications = React.useCallback(() => CRUD.get({ url: `/notifications/${User.getData('id')}` }), []);
    const { response: notifications, isResolved, isPending, isRejected } = useLoads(getNotifications);  

    const updateNotification = React.useCallback(id => CRUD.update({
        url: `/notifications/${id}`,
        quietSucceed: true,
        quietRejection: true,
    }), []);

    const getUnreadCount = React.useCallback(() => notifications ? notifications.data.reduce((n, val) => n + ((!!+val.is_read) ? 0 : 1), 0) : 0, [notifications]);

    const handleNotificationMenuOpen = React.useCallback(event => {
        if (event) setAnchorEl(event.currentTarget);

        if (notifications) axios.all(notifications.data.filter(x => !(!!+x.is_read)).map(x => updateNotification(x.id)))
            .then(() => notifications.data.forEach(x => x.is_read = true));
           
    }, [notifications, updateNotification]);

    const handleLongMenuClose = React.useCallback(() => setAnchorEl(null), []);

    const handleDialogOpen = React.useCallback(isSendNotifOpen => () => setIsSendNotifOpen(isSendNotifOpen), []); 

    const handleOnSelectedNotificationChange = React.useCallback(selected => () => setSelected(selected), []); 

    React.useEffect(() => {
        if (notifications && open) handleNotificationMenuOpen();
    }, [notifications, handleNotificationMenuOpen, open]);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    React.useEffect(() => {
        let { n_id } = queryString.parse(location.search);
        if (notifications && n_id) {
            n_id = parseInt(n_id);
            const notif = notifications.data.find(x => x.id === n_id);
            if (notif) setSelected(notif);
        }
    }, [location, notifications]);

    return (
        <>
            <IconButton
                className={classes.btn}
                aria-label="Voir les notifications"
                aria-owns={open ? 'long-menu' : undefined}
                aria-haspopup="true"
                onClick={handleNotificationMenuOpen}
            >
                <Badge badgeContent={getUnreadCount()} max={99} color="error">
                    <BellOutline />
                </Badge>
            </IconButton>

            <ViewNotif
                notification={selected}
                handleOnSelectedNotificationChange={handleOnSelectedNotificationChange}
                formatter={formatter}
            />

            <SendNotif
                isSendNotifOpen={isSendNotifOpen}
                handleDialogOpen={handleDialogOpen}
            />

            <Menu
                id="long-menu"
                anchorEl={anchorEl}
                open={open && !isSendNotifOpen}
                onClose={handleLongMenuClose}
                getContentAnchorEl={null}
                anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                MenuListProps={{
                    disablePadding: true,
                }}
                PaperProps={{
                    style: {
                        maxHeight: 72 * 5,
                        width: 300,
                    },
                }}
                disableAutoFocusItem
            >
                <Typography
                    className={classes.header}
                    variant="subtitle1"
                    gutterBottom
                >              
                    {isAuthorized && (
                        <IconButton
                            className={classes.send}
                            aria-label="Envoyer une notification"
                            onClick={handleDialogOpen(true)}
                            size="small"
                            color="inherit"
                        >
                            <Send />
                        </IconButton>
                    )}
                    Notifications
                </Typography>

                {/* Loading placeholders */}
                {(isPending || isRejected) && (
                    <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        flexDirection="column"
                    >
                        {[...Array(5).keys()].map(x => <Skeleton height={24} width="80%" key={x} />)}
                    </Box>
                )}

                {/* Actual layout */}
                {isResolved && notifications.data.map(notification =>
                    <MenuItem
                        key={notification.id}
                        className={classes.listItem}
                        onClick={handleOnSelectedNotificationChange(notification)}
                        selected={!(!!+notification.is_read)}
                    >
                        <Box className={classes.notifs} display="flex" flexDirection="column">
                            <Typography variant="subtitle2" gutterBottom noWrap>
                                {notification.message}
                            </Typography>
                            <Typography variant="caption" gutterBottom>
                                <TimeAgo
                                    date={DateUtils.withOffsetDate(notification.created_at)}
                                    formatter={formatter}
                                />
                            </Typography>
                        </Box>
                    </MenuItem>
                )}

                {/* No notifications placeholder */}
                {(isResolved && notifications.data.length === 0) && (
                    <Box
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        flexDirection="column"
                    >
                        <img
                            src="/images/illustrations/NoData.svg"
                            height="auto"
                            width="150"
                            alt="Aucune donnée"
                        />
                        <Typography variant="caption" gutterBottom>
                            Aucune notification !
                            </Typography>
                    </Box>
                )}

            </Menu>
        </>
    );
});
