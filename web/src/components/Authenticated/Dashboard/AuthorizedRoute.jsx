import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useLoads } from 'react-loads';
import User from '../Auth/Components/User';
import scopes_mapping from 'configs/scopes_mapping';
import { CRUD } from 'utils';
import ClubsContext from 'components/General/ClubsContext';

export default ({ component: Component, ...rest }) => {

    const clubs = React.useContext(ClubsContext);

    const requestIsAuthorized = ({ path, location }) => () => {
        if (User.hasActiveSession() && User.hasValidSession()) {
            const mapping = scopes_mapping(clubs);
            return CRUD.get({
                url: '/can',
                params: { scopes: mapping[path] ? mapping[path] : mapping[location.pathname] },
                quietSucceed: true,
                quietRejection: true,
            });
        }
        return Promise.reject();
    }

    const { isResolved, isRejected } = useLoads(requestIsAuthorized(rest));    

    return (
        <React.Suspense>
            {isResolved && <Route {...rest} render={props => <Component {...props} />} />}
            {isRejected && <Redirect to="/hub" />}
        </React.Suspense>
    )
};
