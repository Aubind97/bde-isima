import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import ArrowLeft from 'mdi-material-ui/ArrowLeft';
import Divider from '@material-ui/core/Divider';
import Magnify from 'mdi-material-ui/Magnify';
import Check from 'mdi-material-ui/Check';
import ChevronLeft from 'mdi-material-ui/ChevronLeft';
import ChevronRight from 'mdi-material-ui/ChevronRight';
import InputBase from '@material-ui/core/InputBase';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import { useLoads } from 'react-loads';
import { CRUD } from 'utils';
import { withRouter } from 'react-router';
import { useSnackbar } from 'notistack';
import axios from 'axios';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    cardHeader: {
        padding: theme.spacing(1, 2),
    },
    margin: {
        margin: theme.spacing(2),
    },
    list: {
        height: 500,
        minWidth: 200,
        backgroundColor: theme.palette.background.paper,
        overflow: 'auto',
    },
    button: {
        margin: theme.spacing(0.5, 0),
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        [theme.breakpoints.up('sm')]: {
            margin: theme.spacing(2, 3),
            width: 'auto',
        },
        margin: theme.spacing(1, 0)
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
}));

function not(a, b) {
    return a.filter(aItem => b.findIndex(bItem => bItem.id === aItem.id) === -1);
}

function intersection(a, b) {
    return a.filter(aItem => b.findIndex(bItem => bItem.id === aItem.id) !== -1);
}

function union(a, b) {
    return [...a, ...not(b, a)];
}

export default withRouter(function Permissions(props) {

    /** States initialisation */
    const classes = useStyles();
    const [checked, setChecked] = React.useState([]);
    const [left, setLeft] = React.useState([]);
    const [right, setRight] = React.useState([]);
    const [inputLeft, setInputLeft] = React.useState('');
    const [inputRight, setInputRight] = React.useState('');
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    // Query scopes and user info
    const getScopesAndUser = React.useCallback(() => axios.all([
        CRUD.get({ url: '/oauth/scopes' }),
        CRUD.get({ url: `/users/${props.match.params.id}` }),
    ]).then(res => {
        const [scopes, user] = res;
        const user_scopes = user.data.scopes ? user.data.scopes : [];

        // Set scopes list items
        const diff = not(scopes.data, user_scopes);
        setLeft(diff);
        
        // Set user scopes list items
        setRight(user_scopes);

        return Promise.resolve([diff, user.data]);
    })
    , [props.match.params.id]);
    const { response, isPending, isResolved, isRejected } = useLoads(getScopesAndUser);

    const leftChecked = intersection(left, checked);
    const rightChecked = intersection(right, checked);

    const redirection = React.useCallback(() => props.history.push('/hub/dashboard/members'), [props.history]);

    const handleToggle = React.useCallback(value => () => {
        const currentIndex = checked.findIndex(x => x.id === value.id);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    }, [checked]);

    const numberOfChecked = React.useCallback(items => intersection(checked, items).length, [checked]);

    const handleToggleAll = React.useCallback(items => () => {
        if (numberOfChecked(items) === items.length) {
            setChecked(not(checked, items));
        } else {
            setChecked(union(checked, items));
        }
    }, [checked, numberOfChecked]);

    const handleCheckedRight = React.useCallback(() => {
        setRight([...not(leftChecked, right), ...right]);
        setLeft(not(left, leftChecked));
        setChecked(not(checked, leftChecked));
    }, [checked, left, right, leftChecked]);

    const handleCheckedLeft = React.useCallback(() => {
        setLeft([...not(rightChecked, left), ...left]);
        setRight(not(right, rightChecked));
        setChecked(not(checked, rightChecked));
    }, [checked, left, right, rightChecked]);

    const handleSubmit = React.useCallback(() => {
        CRUD.update({
            url: `/users/${response[1].id}`,
            newData: { scopes: right },
            onSucceed: redirection,
            props: { enqueueSnackbar, closeSnackbar }
        })
        .then(() => CRUD.update({
            url: `/users/revoke/${response[1].id}`,
            quietSucceed: true,
        }))
    }, [redirection, response, right, enqueueSnackbar, closeSnackbar]);

    const customList = React.useCallback((title, items, input, setInput, isResolved) => (
        <Card>
            <CardHeader
                className={classes.cardHeader}
                avatar={
                    <Checkbox
                        onClick={handleToggleAll(items)}
                        checked={numberOfChecked(items) === items.length && items.length !== 0}
                        indeterminate={numberOfChecked(items) !== items.length && numberOfChecked(items) !== 0}
                        disabled={items.length === 0}
                        inputProps={{ 'aria-label': 'Eléments sélectionné(s)' }}
                        color="primary"
                    />
                }
                title={title}
                subheader={`${numberOfChecked(items)}/${items.length} sélectionné(s)`}
            />

            <div className={classes.search}>
                <InputBase
                    placeholder="Rechercher"
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput
                    }}
                    startAdornment={<Magnify className={classes.rightIcon} />}
                    value={input}
                    onChange={e => setInput(e.target.value)}
                />
            </div>

            <Divider />

            {(!isResolved) && <CircularProgress className={classes.margin} size={25} color="primary" />}

            <List className={classes.list} dense component="div" role="list">
                {isResolved && items.filter(x => x.description.toLowerCase().includes(input.toLowerCase())).map(item => {
                    const labelId = `transfer-list-all-item-${item.id}-label`;

                    return (
                        <ListItem key={item.id} role="listitem" button onClick={handleToggle(item)}>
                            <ListItemIcon>
                                <Checkbox
                                    checked={checked.findIndex(x => x.id === item.id) !== -1}
                                    tabIndex={-1}
                                    disableRipple
                                    inputProps={{ 'aria-labelledby': labelId }}
                                    color="default"
                                />
                            </ListItemIcon>
                            <ListItemText id={labelId} primary={item.description} />
                        </ListItem>
                    );
                })}
                <ListItem />
            </List>
        </Card>
    ), [checked, classes, handleToggle, handleToggleAll, numberOfChecked]);

    return (
        <Grid className={classes.root} container component={Container} fixed>

            <Grid container item xs={6} justify="flex-start">
                <Button
                    className={classes.margin}
                    startIcon={<ArrowLeft />}
                    variant="outlined"
                    aria-label="Retour"
                    onClick={redirection}
                    color="inherit"
                >
                    Retour
                </Button>
            </Grid>

            <Grid container item xs={6} justify="flex-end">
                <Button
                    className={classes.margin}
                    startIcon={<Check />}
                    variant="contained"
                    aria-label="Retour"
                    onClick={handleSubmit}
                    color="primary"
                >
                    Modifier
                </Button>
            </Grid>

            <Grid container item xs={12} justify="center" alignContent="center">
                {isResolved && (
                    <Typography variant="h5" color="inherit">
                        Permissions de {response[1].firstname} {response[1].lastname} {response[1].nickname && `(${response[1].nickname})`}
                    </Typography>
                )}

                {(isPending || isRejected) && <Skeleton height={12} width="50%" />}
            </Grid>

            <Grid item xs={12}>
                <Divider className={classes.margin} />
            </Grid>

            <Grid container item spacing={2} justify="center" alignItems="center">

                <Grid item>
                    {customList('Disponibles', left, inputLeft, setInputLeft, isResolved)}
                </Grid>

                <Grid item>
                    <Grid container direction="column" alignItems="center">
                        <Button
                            variant="contained"
                            size="small"
                            className={classes.button}
                            onClick={handleCheckedRight}
                            disabled={leftChecked.length === 0}
                            aria-label="Transférer à droite"
                            color="primary"
                        >
                            <ChevronRight />
                    </Button>
                        <Button
                            variant="contained"
                            size="small"
                            className={classes.button}
                            onClick={handleCheckedLeft}
                            disabled={rightChecked.length === 0}
                            aria-label="Transférer à gauche"
                            color="primary"
                        >
                            <ChevronLeft />
                    </Button>
                    </Grid>
                </Grid>

                <Grid item>
                    {customList('Permissions', right, inputRight, setInputRight, isResolved)}
                </Grid>
            </Grid>
        </Grid>
    );
});  
