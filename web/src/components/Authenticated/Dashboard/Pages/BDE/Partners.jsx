import React from 'react';
import { withSnackbar } from 'notistack';
import Container from '@material-ui/core/Container';
import MaterialTable from 'material-table';
import Avatar from '@material-ui/core/Avatar';
import { Accept, tableProps, CRUD, Image, CompressionHelper } from 'utils';
import { withMobileDialog, MuiThemeProvider, withTheme } from '@material-ui/core';

export default withTheme(withMobileDialog()(withSnackbar(class Partners extends React.Component {
    state = {
        progress: '',
        files: []
    };

    setProgress = progress => this.setState({ progress });

    resetState = () => this.setState({ progress: '', files: [] });

    render() {
        const { fullScreen, theme } = this.props;
        const { files, progress } = this.state;
        return (
            <Container maxWidth="xl">
                <MuiThemeProvider theme={tableProps.tableTheme(theme)}>
                    <MaterialTable
                        onChangeColumnHidden={tableProps.onChangeColumnHidden('partners')}
                        onChangeRowsPerPage={tableProps.onChangeRowsPerPage('partners')}
                        columns={[
                            {
                                title: 'id',
                                field: 'id',
                                hidden: tableProps.isHidden(`partners_id`, true)
                            },
                            {
                                title: 'Logo',
                                field: 'logo',
                                export: false,
                                render: rowData => (
                                    <Avatar
                                        src={Image.display(rowData.logo)}
                                        alt={`Logo ${rowData.name}`}
                                    />
                                ),
                                editComponent: () => (
                                    <Accept
                                        noLabel
                                        noPreview
                                        onDrop={CompressionHelper.onDrop({
                                            handleOnStart: () => this.setState({ progress: `Compression en cours ...` }),
                                            handleOnChange: files => this.setState({ progress: `Compression terminée !`, files })
                                        })}
                                        files={files}
                                        progress={progress}
                                    />
                                ),
                                hidden: tableProps.isHidden(`partners_logo`)
                            },
                            {
                                title: 'Nom',
                                field: 'name',
                                hidden: tableProps.isHidden(`partners_name`)
                            },
                            {
                                title: 'Description',
                                field: 'description',
                                render: rowData => <div title={rowData.description}>{`${rowData.description.substring(0, 80)}${rowData.description.length > 80 ? '...' : ''}`}</div>,
                                hidden: tableProps.isHidden(`partners_description`)
                            }
                        ]}
                        data={CRUD.remote('/partners')}
                        title="Partenaires"
                        editable={{
                            onRowAdd: newData => CRUD.add({
                                url: '/partners',
                                newData: Object.assign(newData, { logo: files[0] }),
                                withFormData: true,
                                onUploadProgress: this.setProgress,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            }),
                            onRowUpdate: (newData, oldData) => CRUD.update({
                                url: `/partners/${newData.id}`,
                                newData: Object.assign(newData, { logo: files[0] }),
                                withFormData: true,
                                onUploadProgress: this.setProgress,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            }),
                            onRowDelete: oldData => CRUD.delete({
                                url: `/partners/${oldData.id}`,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            })
                        }}
                        icons={tableProps.tableIcons}
                        options={{
                            ...tableProps.tableOptions, ...{
                                exportCsv: (columns, data) => tableProps.tableExporter('/partners', 'Partners', columns, data),
                                showTitle: !fullScreen,
                                searchFieldAlignment: fullScreen ? "left" : "right",
                                pageSize: tableProps.getPageSize('partners')
                            }
                        }}
                        localization={tableProps.tableLocalization}
                    />
                </MuiThemeProvider>
            </Container>
        );
    }
})));
