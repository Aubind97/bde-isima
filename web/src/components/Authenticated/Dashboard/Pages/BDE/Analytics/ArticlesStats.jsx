import React from 'react';
import { makeStyles } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { CRUD } from 'utils';
import { useLoads } from 'react-loads';
import { ValueScale, Animation, EventTracker } from '@devexpress/dx-react-chart';
import {
    Chart,
    ArgumentAxis,
    ValueAxis,
    BarSeries,
    Legend,
    Tooltip,
    Title
} from '@devexpress/dx-react-chart-material-ui';

const useStyles = makeStyles(theme => ({
    chart: {
        minHeight: 500,
    },
    select: {
        margin: theme.spacing(2),
        width: 200,
    },
    margin: {
        margin: theme.spacing(2),
    },
}));

const labelHalfWidth = 25;
let lastLabelCoordinate;

const UnitsLabel = props => {
    const { x } = props;
    if (lastLabelCoordinate && lastLabelCoordinate < x && x - lastLabelCoordinate <= labelHalfWidth)
        return null;

    lastLabelCoordinate = x;
    return <ValueAxis.Label {...props} />;
};

export default React.memo(function ArticlesStats() {

    /** States initialisation */
    const classes = useStyles();
    const [period, setPeriod] = React.useState('month');
    const [stats, setStats] = React.useState([]);

    const getArticlesStats = React.useCallback(() => CRUD.get({ 
        url: '/analytics/articles',
        params: {
            period,
        },
    }).then(res => setStats(res.data)), [period]);
    const { update, isResolved, isPending, isRejected } = useLoads(getArticlesStats, {
        update: getArticlesStats,
    });

    const handleChange = event => setPeriod(event.target.value);

    React.useEffect(() => {
        update();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [period]);

    return(
        <Container maxWidth = "xl">
            <Paper>

                {(isPending || isRejected) && <CircularProgress className={classes.margin} size={25} color="inherit" />}

                <Box display="flex" flexDirection="column">

                    {isResolved && (
                        <>
                            <Select
                                className={classes.select}
                                labelId="select-period-label"
                                id="select-period"
                                value={period}
                                onChange={handleChange}
                            >
                                <MenuItem value="week">Cette semaine</MenuItem>
                                <MenuItem value="month">Ce mois</MenuItem>
                                <MenuItem value="year">Cette année</MenuItem>
                            </Select>

                            <Chart 
                                className={classes.chart}
                                data={stats} 
                                height={stats.length * 20} 
                                rotated
                            >

                                <ValueScale name="units" />

                                <ArgumentAxis />

                                <ValueAxis scaleName="units" showGrid={false} showLine showTicks labelComponent={UnitsLabel} />

                                <BarSeries
                                    name="Nombre d'unités"
                                    valueField="units"
                                    argumentField="name"
                                    scaleName="units"
                                    color="#2c3e50"
                                />

                                <Title text="Ventes d'articles" />
                                <Animation />
                                <EventTracker />
                                <Legend position="bottom" />
                                <Tooltip />

                            </Chart>
                        </>
                    )}
                </Box>
            </Paper>
        </Container>
    );
});
