import React from 'react';
import { format } from 'date-fns';
import { makeStyles } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import { ValueScale, Animation, Stack, EventTracker } from '@devexpress/dx-react-chart';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { CRUD, DateUtils } from 'utils';
import { useLoads } from 'react-loads';
import {
    Chart,
    ArgumentAxis,
    ValueAxis,
    BarSeries,
    SplineSeries,
    Legend,
    Tooltip,
    Title
} from '@devexpress/dx-react-chart-material-ui';

const useStyles = makeStyles(theme => ({
    select: {
        margin: theme.spacing(2),
        width: 200,
    },
    margin: {
        margin: theme.spacing(2),
    },
}));

const Point = (props) => {
    const { style, ...restProps } = props;
    return (
        <BarSeries.Point
            style={{ ...style, animationDuration: `${(restProps.index + 1) * 0.3}s` }}
            {...restProps}
        />
    );
};

const Label = symbol => (props) => {
    const { text } = props;
    return (
        <ValueAxis.Label
            {...props}
            text={text + symbol}
        />
    );
};

const PriceLabel = Label(' €');

const labelHalfWidth = 50;
let lastLabelCoordinate;

const CreatedAtLabel = props => {
    const { x, text, ...rest } = props;
    if (lastLabelCoordinate && lastLabelCoordinate < x && x - lastLabelCoordinate <= labelHalfWidth)
        return null;

    lastLabelCoordinate = x;
    return <ArgumentAxis.Label text={format(DateUtils.withOffsetDate(text), 'dd-MM-yy')} {...{ x, ...rest }} />;
};

export default React.memo(function CurrentBalance() {

    /** States initialisation */
    const classes = useStyles();
    const [period, setPeriod] = React.useState('month');

    const getAnalytics = React.useCallback(() => CRUD.get({ 
        url: '/analytics',
        params: {
            orderDirection: 'asc', 
            period,
        }
    }), [period]);
    const { response: analytics, update, isResolved, isPending, isRejected } = useLoads(getAnalytics, {
        update: getAnalytics,
    });

    const getMaxCount = () => analytics && analytics.data.length > 0 ? analytics.data[0].total_positive_users + analytics.data[0].total_negative_users + 500 : 0;

    const modifyUsersDomain = domain => [domain[0], getMaxCount() ];

    const handleChange = event => setPeriod(event.target.value);

    React.useEffect(() => {
        update();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [period]);
    
    return(
        <Container maxWidth = "xl">
            <Paper>

                {(isPending || isRejected) && <CircularProgress className={classes.margin} size={25} color="inherit" />}

                <Box display="flex" flexDirection="column">

                    {isResolved && (
                        <>
                            <Select
                                className={classes.select}
                                labelId="select-period-label"
                                id="select-period"
                                value={period}
                                onChange={handleChange}
                            >
                                <MenuItem value="week">Cette semaine</MenuItem>
                                <MenuItem value="month">Ce mois</MenuItem>
                                <MenuItem value="year">Cette année</MenuItem>
                            </Select>

                            <Chart data={analytics.data}>

                                <ValueScale name="total_positive_users" modifyDomain={modifyUsersDomain} />
                                <ValueScale name="total_negative_users" modifyDomain={modifyUsersDomain} />
                                <ValueScale name="current_balance" />

                                <ArgumentAxis showGrid labelComponent={CreatedAtLabel} />
                                
                                <ValueAxis scaleName="total_positive_users" showGrid={false} showLine showTicks />
                                <ValueAxis scaleName="current_balance" position="right" showGrid={false} showLine showTicks labelComponent={PriceLabel} />

                                <BarSeries
                                    name="Nombre utilisateurs positifs"
                                    valueField="total_positive_users"
                                    argumentField="created_at"
                                    scaleName="total_positive_users"
                                    pointComponent={Point}
                                    color="#27ae60"
                                />

                                <BarSeries
                                    name="Nombre utilisateurs négatifs"
                                    valueField="total_negative_users"
                                    argumentField="created_at"
                                    scaleName="total_negative_users"
                                    pointComponent={Point}
                                    color="#C91F37"
                                />

                                <SplineSeries
                                    name="Bilan des soldes"
                                    valueField="current_balance"
                                    argumentField="created_at"
                                    scaleName="current_balance"
                                    color="#2980b9"
                                />

                                <Stack
                                    stacks={[
                                        { series: ['Nombre utilisateurs positifs', 'Nombre utilisateurs négatifs'] },
                                    ]}
                                />

                                <EventTracker />
                                <Animation />
                                <Title text="Soldes des membres" />
                                <Legend position="bottom" />
                                <Tooltip />

                            </Chart>
                        </>
                    )}
                </Box>
            </Paper>
        </Container>
    );
});
