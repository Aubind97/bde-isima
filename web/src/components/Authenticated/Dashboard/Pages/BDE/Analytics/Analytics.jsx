import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import CurrentBalance from './CurrentBalance';
import { makeStyles } from '@material-ui/core';
import ArticlesStats from './ArticlesStats';

const useStyles = makeStyles(theme => ({
    margin: {
        margin: theme.spacing(2),
    },
}));

export default React.memo(function Analytics() {

    /** States initialisation */
    const classes = useStyles();

    return(
        <Container maxWidth="xl">

            <Typography variant="h3" paragraph>
                Statistiques
            </Typography>

            <Divider className={classes.margin} />

            <Grid container spacing={5}>
                <Grid item xs={12} md={6}>
                    <CurrentBalance />
                </Grid>

                <Grid item xs={12} md={6}>
                    <ArticlesStats />
                </Grid>
            </Grid>
        </Container>
    );
});
