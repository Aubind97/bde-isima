import React from 'react';
import { withSnackbar } from 'notistack';
import Chip from '@material-ui/core/Chip';
import Container from '@material-ui/core/Container';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MaterialTable from 'material-table';
import Check from 'mdi-material-ui/Check';
import CheckAll from 'mdi-material-ui/CheckAll';
import History from 'mdi-material-ui/History';
import TimerSand from 'mdi-material-ui/TimerSand';
import CurrencyUsd from 'mdi-material-ui/CurrencyUsd';
import ClubsContext from 'components/General/ClubsContext';
import { tableProps, CRUD, DateUtils } from 'utils';
import { MuiThemeProvider, withTheme } from '@material-ui/core';
import { format } from 'date-fns';

export default withTheme(withMobileDialog()(withSnackbar(class Events extends React.Component {
    static contextType = ClubsContext;

    state = {
        isConfirmDialogOpen: false,
        selected: null,
    };

    tableRef = React.createRef();

    validateEvent = rowData => CRUD.update({
        url: `/events/${rowData.id}`,
        newData: { status: 1 },
        onSucceed: () => {
            if (this.tableRef.current)
                this.tableRef.current.onQueryChange();
        },
        props: this.props
    });

    handleValidation = () => {
        this.checkoutEvent(this.state.selected);
        this.handleConfirmDialog(false, {});
    };

    checkoutEvent = newData => CRUD.update({
        url: `/events/validate/${newData.id}`,
        onSucceed: this.resetState,
        onRejection: this.resetState,
        props: this.props
    });

    handleConfirmDialog = (isConfirmDialogOpen, selected) => this.setState({ isConfirmDialogOpen, selected });

    resetState = () => {
        this.setState({ selected: null });

        if (this.tableRef.current)
            this.tableRef.current.onQueryChange();
    };

    render() {
        const { fullScreen, theme } = this.props;
        const { isConfirmDialogOpen } = this.state;
        
        return (
            <Container maxWidth="xl">
                <MuiThemeProvider theme={tableProps.tableTheme(theme)}>
                    <MaterialTable
                        tableRef={this.tableRef}
                        onChangeColumnHidden={tableProps.onChangeColumnHidden('events_club')}
                        onChangeRowsPerPage={tableProps.onChangeRowsPerPage('events_club')}
                        columns={[
                            {
                                title: 'Nom',
                                field: 'name',
                                hidden: tableProps.isHidden(`events_club_name`)
                            },
                            {
                                title: 'Club',
                                field: 'club',
                                render: rowData => {
                                    const club = this.context.find(x => x.id === rowData.club_id);
                                    return club ? club.name.toUpperCase() : '?';
                                },
                                hidden: tableProps.isHidden(`events_club_club`)
                            },
                            {
                                title: 'Statut',
                                field: 'status',
                                render: rowData => {
                                    switch (rowData.status) {
                                        //StandBy events chip
                                        case 0: return (
                                            fullScreen ?
                                                <TimerSand style={{ color: '#e67e22' }} />
                                                :
                                                <Chip
                                                    style={{ color: '#e67e22', borderColor: '#e67e22' }}
                                                    label="En attente de validation par le BDE"
                                                    icon={<TimerSand style={{ color: '#e67e22' }} />}
                                                    variant="outlined"
                                                />
                                        );
                                        //Ongoing or past events
                                        case 1: return (
                                            new Date() < DateUtils.withOffsetDate(rowData.subscriptions_end_at) ? (
                                                //Subscriptions are opened
                                                fullScreen ?
                                                    <History style={{ color: '#e67e22' }} />
                                                    :
                                                    <Chip
                                                        style={{ color: '#e67e22', borderColor: '#e67e22' }}
                                                        label="Inscriptions ouvertes"
                                                        icon={<History style={{ color: '#e67e22' }} />}
                                                        variant="outlined"
                                                    />
                                            ) : (
                                                    //Event is past
                                                    fullScreen ?
                                                        <Check style={{ color: '#27ae60' }} />
                                                        :
                                                        <Chip style={{ color: '#27ae60', borderColor: '#27ae60' }}
                                                            label="Clotûré et encaissable"
                                                            icon={<Check style={{ color: '#27ae60' }} />}
                                                            variant="outlined"
                                                        />
                                                )
                                        );
                                        //Event is past and checked out 
                                        default: return (
                                            fullScreen ?
                                                <CheckAll style={{ color: '#27ae60' }} />
                                                :
                                                <Chip
                                                    style={{ color: '#27ae60', borderColor: '#27ae60' }}
                                                    label="Terminé"
                                                    icon={<CheckAll style={{ color: '#27ae60' }} />}
                                                    variant="outlined"
                                                />
                                        );
                                    }
                                },
                                hidden: tableProps.isHidden(`events_club_status`)
                            },
                            {
                                title: 'Date de l\'événement',
                                field: 'takes_place_at',
                                render: rowData => format(DateUtils.withOffsetDate(rowData.takes_place_at), 'dd/MM/yyyy à HH:mm'),
                                hidden: tableProps.isHidden(`events_club_status`)
                            },
                            {
                                title: 'Date de fin des inscriptions',
                                field: 'subscriptions_end_at',
                                render: rowData => format(DateUtils.withOffsetDate(rowData.subscriptions_end_at), 'dd/MM/yyyy à HH:mm'),
                                hidden: tableProps.isHidden(`events_club_date`)
                            }
                        ]}
                        data={CRUD.remote('/events', {
                            orderBy: 'subscriptions_end_at',
                            orderDirection: 'desc',
                        })}
                        title="Événements"
                        editable={{
                            onRowDelete: oldData => CRUD.delete({
                                url: `/events/${oldData.id}`,
                                props: this.props
                            })
                        }}
                        actions={[
                            rowData => ({
                                icon: React.forwardRef((props, ref) => <Check {...props} ref={ref} />),
                                tooltip: 'Valider',
                                onClick: (event, rowData) => this.validateEvent(rowData),
                                disabled: rowData.status !== 0
                            }),
                            rowData => ({
                                icon: React.forwardRef((props, ref) => <CurrencyUsd {...props} ref={ref} />),
                                tooltip: 'Encaisser',
                                onClick: (event, rowData) => this.handleConfirmDialog(true, rowData),
                                disabled: rowData.status !== 1 || new Date() < DateUtils.withOffsetDate(rowData.subscriptions_end_at)
                            }),
                        ]}
                        icons={tableProps.tableIcons}
                        options={{
                            ...tableProps.tableOptions, ...{
                                exportButton: false,
                                showTitle: !fullScreen,
                                searchFieldAlignment: fullScreen ? "left" : "right",
                                pageSize: tableProps.getPageSize('events_club')
                            }
                        }}
                        localization={tableProps.tableLocalization}
                    />
                </MuiThemeProvider>

                <Dialog
                    open={isConfirmDialogOpen}
                    onClose={() => this.handleConfirmDialog(false, {})}
                    aria-labelledby="alert-validation-title"
                    aria-describedby="alert-validation-description"
                >
                    <DialogTitle id="alert-validation-title">Voulez-vous vraiment valider et encaisser l'événement ?</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-validation-description">
                            Cette action est irréversible.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button aria-label="Annuler" variant="outlined" onClick={() => this.handleConfirmDialog(false, {})} color="inherit">
                            Annuler
                        </Button>
                        <Button aria-label="Confirmer" variant="contained" onClick={this.handleValidation} color="primary" autoFocus>
                            Confirmer
                    </Button>
                    </DialogActions>
                </Dialog>
            </Container>
        );
    };
})));
