import React from 'react';
import { withSnackbar } from 'notistack';
import Container from '@material-ui/core/Container';
import MaterialTable from 'material-table';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { withTheme, withMobileDialog, MuiThemeProvider } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import { Accept, CRUD, tableProps, Image, CompressionHelper } from 'utils';

export default withTheme(withMobileDialog()(withSnackbar(class Clubs extends React.Component {
    state = {
        progress: '',
        files: []
    };

    setProgress = progress => this.setState({ progress });

    resetState = () => this.setState({ progress: '', files: [] });

    render() {
        const { fullScreen, theme } = this.props;
        const { files, progress } = this.state;
        return (
            <Container maxWidth="xl">
                <MuiThemeProvider theme={tableProps.tableTheme(theme)}>
                    <MaterialTable
                        onChangeColumnHidden={tableProps.onChangeColumnHidden('clubs')}
                        onChangeRowsPerPage={tableProps.onChangeRowsPerPage('clubs')}
                        columns={[
                            {
                                title: 'id',
                                field: 'id',
                                hidden: tableProps.isHidden(`clubs_id`, true)
                            },
                            {
                                title: 'Logo',
                                field: 'logo',
                                export: false,
                                render: rowData => (
                                    <Avatar
                                        src={Image.display(rowData.logo)}
                                        alt={`Logo du ${rowData.name}`}
                                    />
                                ),
                                editComponent: () => (
                                    <Accept
                                        noLabel
                                        noPreview
                                        onDrop={CompressionHelper.onDrop({
                                            handleOnStart: () => this.setState({ progress: `Compression en cours ...` }),
                                            handleOnChange: files => this.setState({ progress: `Compression terminée !`, files })
                                        })}
                                        files={files}
                                        progress={progress}
                                    />
                                ),
                                hidden: tableProps.isHidden(`clubs_logo`)
                            },
                            {
                                title: 'Nom',
                                field: 'name',
                                editable: 'onAdd',
                                hidden: tableProps.isHidden(`clubs_name`)
                            },
                            {
                                title: 'Email',
                                field: 'email',
                                hidden: tableProps.isHidden(`clubs_email`)
                            },
                            {
                                title: 'Description',
                                field: 'description',
                                render: rowData => <div title={rowData.description}>{`${rowData.description.substring(0, 80)}${rowData.description.length > 80 ? '...' : ''}`}</div>,
                                editComponent: props => (
                                    <TextareaAutosize
                                        aria-label="Description textarea"
                                        value={props.value ? props.value : ""}
                                        onChange={e => props.onChange(e.target.value)}
                                    />
                                ),
                                hidden: tableProps.isHidden(`clubs_description`)
                            },
                            {
                                title: 'Facebook',
                                field: 'facebook',
                                hidden: tableProps.isHidden(`clubs_facebook`)
                            },
                            {
                                title: 'Twitter',
                                field: 'twitter',
                                hidden: tableProps.isHidden(`clubs_twitter`)
                            },
                            {
                                title: 'Instagram',
                                field: 'instagram',
                                hidden: tableProps.isHidden(`clubs_instagram`)
                            }
                        ]}
                        data={CRUD.remote('/clubs')}
                        title="Clubs"
                        editable={{
                            onRowAdd: newData => CRUD.add({
                                url: '/clubs',
                                newData: Object.assign(newData, { logo: files[0] }),
                                withFormData: true,
                                onUploadProgress: this.setProgress,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            }),
                            onRowUpdate: (newData, oldData) => CRUD.update({
                                url: `/clubs/${newData.id}`,
                                newData: Object.assign(newData, { logo: files[0] }),
                                withFormData: true,
                                onUploadProgress: this.setProgress,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            }),
                            onRowDelete: oldData => CRUD.delete({
                                url: `/clubs/${oldData.id}`,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            })
                        }}
                        icons={tableProps.tableIcons}
                        options={{
                            ...tableProps.tableOptions, ...{
                                exportCsv: (columns, data) => tableProps.tableExporter('/clubs', 'Clubs', columns, data),
                                showTitle: !fullScreen,
                                searchFieldAlignment: fullScreen ? "left" : "right",
                                pageSize: tableProps.getPageSize('clubs')
                            }
                        }}
                        localization={tableProps.tableLocalization}
                    />
                </MuiThemeProvider>
            </Container>
        );
    }
})));
