import React from 'react';
import { withSnackbar } from 'notistack';
import Checkbox from '@material-ui/core/Checkbox';
import Check from 'mdi-material-ui/Check';
import Cancel from 'mdi-material-ui/Cancel';
import MaterialTable from 'material-table';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import Tooltip from '@material-ui/core/Tooltip';
import Icon from '@material-ui/core/Icon';
import CSVImporter from '../../Components/CSVImporter';
import { Accept, CRUD, tableProps, Image, CompressionHelper } from 'utils';
import { withMobileDialog, MuiThemeProvider, withTheme } from '@material-ui/core';

export default withTheme(withMobileDialog()(withSnackbar(class Market extends React.Component {
    state = {
        progress: '',
        files: []
    };

    tableRef = React.createRef();

    setProgress = progress => this.setState({ progress });

    resetState = () => {
        this.setState({ progress: '', files: [] });

        if (this.tableRef.current)
            this.tableRef.current.onQueryChange();
    };

    //Extracted from material-table icon renderer, added icon size 48x48 as a temporary layout fix 
    iconButtonRenderer = props => {
        let action = props.action;
        if (typeof action === 'function') {
            action = action(props.data);
            if (!action) {
                return null;
            }
        }

        if (action.hidden) {
            return null;
        }

        const handleOnClick = event => {
            if (action.onClick) {
                action.onClick(event, props.data);
                event.stopPropagation();
            }
        };

        const button = (
            <span>
                <IconButton
                    style={{ width: 48, height: 48 }}
                    size={props.size}
                    color="inherit"
                    disabled={action.disabled}
                    onClick={(event) => handleOnClick(event)}
                >
                    {typeof action.icon === "string" ? (
                        <Icon {...action.iconProps}>{action.icon}</Icon>
                    ) : (
                            <action.icon
                                {...action.iconProps}
                                disabled={action.disabled}
                            />
                        )
                    }
                </IconButton>
            </span>
        );

        return action.tooltip ? <Tooltip title={action.tooltip}>{button}</Tooltip> : button;
    };

    render() {
        const { fullScreen, theme } = this.props;
        const { files, progress } = this.state;
        return (
            <Container maxWidth="xl">
                <MuiThemeProvider theme={tableProps.tableTheme(theme)}>
                    <MaterialTable
                        tableRef={this.tableRef}
                        onChangeColumnHidden={tableProps.onChangeColumnHidden('market')}
                        onChangeRowsPerPage={tableProps.onChangeRowsPerPage('market')}
                        columns={[
                            {
                                title: 'id',
                                field: 'id',
                                hidden: true,
                            },
                            {
                                title: 'Photo',
                                field: 'photo',
                                export: false,
                                render: rowData => (
                                    <Avatar
                                        src={Image.display(rowData.photo)}
                                        alt={`Logo ${rowData.name}`}
                                    />
                                ),
                                editComponent: () => (
                                    <Accept
                                        noLabel
                                        noPreview
                                        onDrop={CompressionHelper.onDrop({
                                            handleOnStart: () => this.setState({ progress: `Compression en cours ...` }),
                                            handleOnChange: files => this.setState({ progress: `Compression terminée !`, files })
                                        })}
                                        files={files}
                                        progress={progress}
                                    />
                                ),
                                hidden: tableProps.isHidden(`market_photo`)
                            },
                            {
                                title: 'Nom',
                                field: 'name',
                                hidden: tableProps.isHidden(`market_name`)
                            },
                            {
                                title: 'Prix',
                                field: 'price',
                                type: 'numeric',
                                render: rowData => `${parseFloat(rowData.price).toFixed(2)} €`,
                                hidden: tableProps.isHidden(`market_price`)
                            },
                            {
                                title: 'Prix adhérent',
                                field: 'member_price',
                                type: 'numeric',
                                render: rowData => `${parseFloat(rowData.member_price).toFixed(2)} €`,
                                hidden: tableProps.isHidden(`market_member_price`)
                            },
                            {
                                title: 'Stock',
                                field: 'stock',
                                type: 'numeric',
                                hidden: tableProps.isHidden(`market_stock`)
                            },
                            {
                                title: 'Activation',
                                field: 'is_enabled',
                                type: 'boolean',
                                cellStyle: {
                                    textAlign: 'center',
                                },
                                editComponent: props => (
                                    <Checkbox
                                        checked={!!+props.rowData.is_enabled}
                                        onChange={e => props.onChange(e.target.checked ? '1' : '0')}
                                        color="primary"
                                    />
                                ),
                                render: rowData => !!+rowData.is_enabled ? <Check /> : <Cancel />,
                                hidden: tableProps.isHidden(`market_is_enabled`)
                            },
                        ]}
                        data={CRUD.remote('/articles')}
                        title="Market"
                        editable={{
                            onRowAdd: newData => CRUD.add({
                                url: '/articles',
                                newData: Object.assign(newData, { photo: files[0] }),
                                withFormData: true,
                                onUploadProgress: this.setProgress,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            }),
                            onRowUpdate: (newData, oldData) => CRUD.update({
                                url: `/articles/${newData.id}`,
                                newData: Object.assign(newData, { photo: files[0] }),
                                withFormData: true,
                                onUploadProgress: this.setProgress,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            }),
                            onRowDelete: oldData => CRUD.delete({
                                url: `/articles/${oldData.id}`,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            })
                        }}
                        actions={[
                            {
                                icon: React.forwardRef((props, ref) => <CSVImporter {...props} ref={ref} tableRef={this.tableRef} />),
                                tooltip: 'Importer',
                                isFreeAction: true
                            }
                        ]}
                        components={{
                            Action: props => this.iconButtonRenderer(props)
                        }}
                        icons={tableProps.tableIcons}
                        options={{
                            ...tableProps.tableOptions, ...{
                                exportCsv: (columns, data) => tableProps.tableExporter('/articles', 'Market', columns, data),
                                showTitle: !fullScreen,
                                searchFieldAlignment: fullScreen ? "left" : "right",
                                pageSize: tableProps.getPageSize('market')
                            }
                        }}
                        localization={tableProps.tableLocalization}
                    />
                </MuiThemeProvider>
            </Container>
        );
    }
})));
