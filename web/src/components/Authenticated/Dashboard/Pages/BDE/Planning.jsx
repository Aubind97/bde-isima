import React, { useState, useMemo, useCallback } from 'react';
import classnames from 'classnames';
import moment from 'moment-timezone';
import { useLoads } from 'react-loads';
import { makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ChevronLeft from 'mdi-material-ui/ChevronLeft';
import ChevronRight from 'mdi-material-ui/ChevronRight';
import { CRUD, Image } from 'utils';

moment.locale('fr');

const useStyles = makeStyles(theme => ({
    cell: {
        minHeight: 100,
    },
    innerCell: {
        border: '1px solid rgb(235, 235, 235)',
    },
    margin: {
        margin: theme.spacing(.5),
    },
}));

const startOfThisWeek = moment().startOf('isoWeek');
const startOfNextWeek = moment().add(1, 'weeks').startOf('isoWeek');

export default function Planning() {

    /** States initialisation */
    const classes = useStyles();
    const { response, isResolved } = useLoads(() => CRUD.get({ url: '/users/compact', params: { scope_id: '*' } }));    
    const [page, setPage] = useState(0);
    const [schedule, setSchedule] = useState({});

    const days = [
        //Start of this week
        [null, ...[...Array(5).keys()].map(x => startOfThisWeek.clone().add(x, 'days'))],
        //Start of next week
        [null, ...[...Array(5).keys()].map(x => startOfNextWeek.clone().add(x, 'days'))],
    ];

    const times = [
        { from: null, to: null, },
        { from: '09:50', to: '10:10', },
        { from: '12:00', to: '13:30', },
        { from: '15:20', to: '15:40', },
        { from: '17:30', to: '19:30', },
        { from: '19:30', to: '21:00', },
        { from: '21:00', to: '23:59', },
    ];

    const changePage = value => () => {
        setPage(page => page + value);
    };

    const onDragStart = user => e => {
        e.dataTransfer.setData("user", JSON.stringify(user));
    };

    const onDragOver = e => {
        e.preventDefault();
    };

    const onDrop = label => e => {
        const selected = JSON.parse(e.dataTransfer.getData("user"));
        setSchedule(schedule => ({
            ...schedule,
            [label]: [...(schedule[label] || []).filter(x => x.id !== selected.id), selected]
        }));
    };

    const onDelete = (id, label) => () => {
        setSchedule(schedule => ({
            ...schedule,
            [label]: (schedule[label] || []).filter(x => x.id !== id)
        }));
    };

    const veryFirstCell = key => (
        <Grid key={key} className={classes.cell} container item xs={2} alignContent="center" justify="center">
            <Tooltip title="Semaine précédente">
                <span>
                    <IconButton aria-label="Semaine précédente" onClick={changePage(-1)} disabled={page === 0}>
                        <ChevronLeft />
                    </IconButton>
                </span>
            </Tooltip>
            <Tooltip title="Semaine prochaine">
                <span>
                    <IconButton aria-label="Semaine prochaine" onClick={changePage(1)} disabled={page === 1}>
                        <ChevronRight />
                    </IconButton>
                </span>
            </Tooltip>
        </Grid>
    );

    const UserChip = useCallback(({ user, draggable, onDelete }) => {
        const label = `${user.firstname} ${user.lastname.slice(0, 2)}.`;

        return (
            <Chip
                className={classes.margin}
                avatar={<Avatar src={Image.display(user.picture)} alt={label} />}
                label={label}
                onDelete={onDelete}
                variant="outlined"
                onDragStart={draggable ? onDragStart(user) : undefined}
                draggable={draggable}
            />
        );
    }, [classes.margin]);

    const DraggableUsers = useMemo(() => (
        <div style={{ padding: 20, width: '100%', textAlign: 'left' }}>
            <Typography variant="caption" color="textSecondary" paragraph>Drag and drop les éléments ci-dessous pour répartir :</Typography>
            <Grid container item xs={12}>
                {isResolved && response.data.map(user => (
                    <UserChip key={user.id} user={user} draggable />
                ))}
            </Grid>
        </div>
    ), [isResolved, response.data]);

    return (
        <Container maxWidth="xl">
            <Paper>
                <Box display="flex" p={2}>
                    <Grid container>
                        {times.map((time, timeIdx) => days[page].map((day, dayIdx) => {

                            //First row
                            if (timeIdx === 0) {
                                //Very first cell
                                if (dayIdx === 0) {
                                    return veryFirstCell(dayIdx);
                                }
                                //Days of the week labels
                                else {
                                    return (
                                        <Grid key={dayIdx} className={classes.cell} container item xs={2} alignContent="center" justify="center">
                                            <Typography variant="subtitle2" color="textSecondary">
                                                {day.format('dddd DD/MM')}
                                            </Typography>
                                        </Grid>
                                    );
                                }
                            }
                            //First column
                            else if (dayIdx === 0) {
                                return (
                                    <Grid key={dayIdx} className={classes.cell} container item xs={2} alignContent="center" justify="center">
                                        <Typography variant="subtitle2" color="textSecondary">
                                            {time.from} - {time.to}
                                        </Typography>
                                    </Grid>
                                );
                            }

                            const [fromHour, fromMin] = time.from.split(':');
                            const [toHour, toMin] = time.to.split(':');

                            const fromDate = day.clone();
                            const toDate = day.clone();

                            fromDate.hour(fromHour);
                            fromDate.minute(fromMin);
                            toDate.hour(toHour);
                            toDate.minute(toMin);

                            const label = fromDate.toISOString();

                            return (
                                <Grid
                                    key={dayIdx}
                                    className={classnames(classes.cell, classes.innerCell)}
                                    item
                                    xs={2}
                                    onDragOver={onDragOver}
                                    onDrop={onDrop(label)}
                                >
                                    {schedule[label] && schedule[label].map(user => (
                                        <UserChip key={user.id} user={user} onDelete={onDelete(user.id, label)} />
                                    ))}
                                </Grid>
                            );
                        }))}

                        {DraggableUsers}
                     </Grid>
                </Box>
            </Paper>
        </Container>
    );
}