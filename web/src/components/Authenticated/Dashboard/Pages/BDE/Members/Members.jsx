import React from 'react';
import { withRouter } from 'react-router-dom';
import { withSnackbar } from 'notistack';
import MaterialTable from 'material-table';
import Checkbox from '@material-ui/core/Checkbox';
import Container from '@material-ui/core/Container';
import Tune from 'mdi-material-ui/Tune'; 
import CardAccountDetailsOutline from 'mdi-material-ui/CardAccountDetailsOutline'; 
import Check from 'mdi-material-ui/Check';
import Cancel from 'mdi-material-ui/Cancel';
import User from 'components/Authenticated/Auth/Components/User';
import MenuAuthorizer from '../../../Components/Menu/MenuAuthorizer';
import { Accept, tableProps, CRUD, CompressionHelper, UserAvatar } from 'utils';
import { withTheme, withMobileDialog, MuiThemeProvider } from '@material-ui/core';
import SelectPromotionField from 'utils/forms/SelectPromotionField';
import ClubsContext from 'components/General/ClubsContext';
import scopes_mapping from 'configs/scopes_mapping';
import Summary from './Summary';

export default withTheme(withMobileDialog()(withSnackbar(withRouter(class Members extends React.Component {
    static contextType = ClubsContext;
    
    state = {
        isSummaryOpen: false,
        progress: '',
        files: [],
        authorizer: new MenuAuthorizer(User.getData('scopes'), scopes_mapping(this.context))
    };

    setProgress = progress => this.setState({ progress });

    resetState = () => this.setState({ progress: '', files: [] });

    handleIsSummaryOpen = isSummaryOpen => () => this.setState({ isSummaryOpen });

    handleSubmit = newData => {
        const { files } = this.state;

        delete newData.picture;

        if (files[0]) {
            return CRUD.update({
                url: `/users/${newData.id ? newData.id : ''}`,
                newData: { picture: files[0] },
                withFormData: true,
                quietSucceed: true,
                onSucceed: () => this.updateData(newData),
                onRejection: this.resetState,
                onUploadProgress: this.setProgress,
                props: this.props
            });
        }

        return this.updateData(newData);
    }

    updateData = newData => {
        return CRUD.update({
            url: `/users/${newData.id ? newData.id : ''}`,
            newData,
            onSucceed: this.resetState,
            onRejection: this.resetState,
            props: this.props
        });
    };

    render() {
        const { fullScreen, theme } = this.props;
        const { isSummaryOpen, files, progress } = this.state;

        return (
            <Container maxWidth="xl">
                <MuiThemeProvider theme={tableProps.tableTheme(theme)}>
                    <MaterialTable
                        onChangeColumnHidden={tableProps.onChangeColumnHidden('members')}
                        onChangeRowsPerPage={tableProps.onChangeRowsPerPage('members')}
                        columns={[
                            {
                                title: 'id',
                                field: 'id',
                                hidden: tableProps.isHidden(`members_id`, true)
                            },
                            {
                                title: 'Photo de profil',
                                field: 'picture',
                                export: false,
                                render: rowData => (
                                    <UserAvatar
                                        data={rowData}
                                        username={User.toString(rowData)}
                                    />
                                ),
                                editComponent: () => (
                                    <Accept
                                        noLabel
                                        noPreview
                                        onDrop={CompressionHelper.onDrop({
                                            handleOnStart: () => this.setState({ progress: `Compression en cours ...` }),
                                            handleOnChange: files => this.setState({ progress: `Compression terminée !`, files })
                                        })}
                                        files={files}
                                        progress={progress}
                                    />
                                ),
                                hidden: tableProps.isHidden(`members_picture`)
                            },
                            {
                                title: 'Nom',
                                field: 'lastname',
                                hidden: tableProps.isHidden(`members_lastname`)
                            },
                            {
                                title: 'Prénom',
                                field: 'firstname',
                                hidden: tableProps.isHidden(`members_firstname`)
                            },
                            {
                                title: 'Surnom',
                                field: 'nickname',
                                hidden: tableProps.isHidden(`members_nickname`)
                            },
                            {
                                title: 'Email',
                                field: 'email',
                                hidden: tableProps.isHidden(`members_email`)
                            },
                            {
                                title: 'N° carte',
                                field: 'card',
                                type: 'numeric',
                                hidden: tableProps.isHidden(`members_card`)
                            },
                            {
                                title: 'Solde',
                                field: 'balance',
                                type: 'numeric',
                                render: rowData => `${parseFloat(rowData.balance).toFixed(2)} €`,
                                hidden: tableProps.isHidden(`members_balance`)
                            },
                            {
                                title: 'Promotion',
                                field: 'promotion_id',
                                hidden: tableProps.isHidden(`members_promotion`),
                                render: rowData => rowData.promotion && `${rowData.promotion.year}`,
                                editComponent: props => <SelectPromotionField {...props} />
                            },
                            {
                                title: 'Cotisation',
                                field: 'is_member',
                                type: 'boolean',
                                editComponent: props => (
                                    <Checkbox
                                        checked={!!+props.rowData.is_member}
                                        onChange={e => props.onChange(e.target.checked ? '1' : '0')}
                                        color="primary"
                                    />
                                ),
                                render: rowData => !!+rowData.is_member ? <Check /> : <Cancel />,
                                hidden: tableProps.isHidden(`members_is_member`)
                            },
                            {
                                title: 'Activation',
                                field: 'is_enabled',
                                type: 'boolean',
                                editComponent: props => (
                                    <Checkbox
                                        checked={!!+props.rowData.is_enabled}
                                        onChange={e => props.onChange(e.target.checked ? '1' : '0')}
                                        color="primary"
                                    />
                                ),
                                render: rowData => !!+rowData.is_enabled ? <Check /> : <Cancel />,
                                hidden: tableProps.isHidden(`members_is_enabled`)
                            },
                        ]}
                        data={CRUD.remote('/users')}
                        title="Membres"
                        editable={{
                            onRowAdd: newData => this.handleSubmit(newData),
                            onRowUpdate: (newData, oldData) => this.handleSubmit(newData),
                            onRowDelete: oldData => CRUD.delete({
                                url: `/users/${oldData.id}`,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            })
                        }}
                        actions={[
                            {
                                icon: Tune,
                                tooltip: 'Éditer les permissions',
                                onClick: (event, rowData) => {
                                    this.props.history.push(`/hub/dashboard/members/${rowData.id}`)
                                },
                                disabled: this.state.authorizer ? !this.state.authorizer.isAuthorized('/hub/dashboard/members/:id') : true
                            },
                            {
                                icon: CardAccountDetailsOutline,
                                isFreeAction: true,
                                tooltip: 'Résumé des permissions',
                                onClick: (event, rowData) => {
                                    this.setState({ isSummaryOpen: true });
                                },
                                disabled: this.state.authorizer ? !this.state.authorizer.isAuthorized('/hub/dashboard/members/:id') : true
                            }
                        ]}
                        icons={tableProps.tableIcons}
                        options={{
                            ...tableProps.tableOptions, ...{
                                exportCsv: (columns, data) => tableProps.tableExporter('/users', 'Members', columns, data),
                                showTitle: !fullScreen,
                                searchFieldAlignment: fullScreen ? "left" : "right",
                                pageSize: tableProps.getPageSize('members')
                            }
                        }}
                        localization={tableProps.tableLocalization}
                    />

                    <Summary 
                        isOpen={isSummaryOpen} 
                        handleOpen={this.handleIsSummaryOpen}
                    />

                </MuiThemeProvider>
            </Container>
        );
    }
}))));
