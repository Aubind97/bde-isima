import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import Delete from 'mdi-material-ui/Delete';
import Grid from '@material-ui/core/Grid';
import { useTheme, useMediaQuery, Typography } from '@material-ui/core';
import { CRUD } from 'utils';
import { useLoads } from 'react-loads';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
    margin: {
        margin: theme.spacing(2),
    },
}));

export default function Summary({ isOpen, handleOpen }) {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const [selected, setSelected] = React.useState(null);
    const [isLoading, setIsLoading] = React.useState(false);
    const [users, setUsers] = React.useState([]);

    // Query scopes and user info
    const getScopes= React.useCallback(() => CRUD.get({ url: '/oauth/scopes' }), []);
    const { response: scopes, isPending, isResolved, isRejected } = useLoads(getScopes);

    const handleSelected = scope => () => setSelected(scope);

    const handleUpdateUser = user => () => {
        CRUD.update({
            url: `/users/${user.id}`,
            newData: { scopes: user.scopes.filter(x => x.id !== selected.id) },
            onSucceed: () => setUsers(users.filter(x => x.id !== user.id)),
            props: { enqueueSnackbar, closeSnackbar }
        })
    };

    React.useEffect(() => {
        if(selected) {
            setUsers([]);
            setIsLoading(true);            
            CRUD.get({
                url: '/users/compact',
                params: { scope_id: selected.id },
            })
            .then(res => {
                setUsers(res.data);
                setIsLoading(false);  
            });
        }
    }, [selected]);

    return (
        <Dialog
            open={isOpen}
            fullScreen={fullScreen}
            onClose={handleOpen(false)}
            scroll="paper"
            aria-labelledby="summary-dialog-title"
            aria-describedby="summary-dialog-description"
        >
            <DialogTitle id="summary-dialog-title">
                Résumé des permissions
            </DialogTitle>

            <DialogContent dividers>
                <Grid container>
                    <Grid container item xs={6} justify="center">

                        <List dense component={Grid} container>
                            <Typography 
                                variant="subtitle1" 
                                color="textSecondary"
                                component={Grid}
                                item
                                xs={12}
                            >
                                Autorisations :
                                <Divider className={classes.margin} />
                            </Typography>

                            {(isPending || isRejected) && <CircularProgress className={classes.margin} size={25} color="inherit" />}

                            {isResolved && (
                                scopes.data.map((scope, idx) => (
                                    <ListItem
                                        key={idx}
                                        selected={selected && selected.id === scope.id}
                                        onClick={handleSelected(scope)}
                                        button
                                        component={Grid}
                                        item
                                        md={6}
                                        xs={12}
                                    >
                                        <ListItemText primary={scope.description} />
                                    </ListItem>
                                ))
                            )}
                        </List>

                    </Grid>

                    <Grid container item xs={6} justify="center">

                        <List dense>
                            <Typography
                                variant="subtitle1"
                                color="textSecondary"
                                component={Grid}
                                item
                                xs={12}
                            >
                                Octroyé à :
                                <Divider className={classes.margin} />
                            </Typography>

                            {isLoading && <CircularProgress className={classes.margin} size={25} color="inherit" />}

                            {users && users.map((user, idx) => (
                                <ListItem key={idx}>
                                    <ListItemText primary={`${user.card} - ${user.firstname} ${user.lastname} ${user.nickname ? `(${user.nickname})` : ''}`} />
                                    <ListItemSecondaryAction>
                                        <IconButton onClick={handleUpdateUser(user)} edge="end" aria-label="delete">
                                            <Delete />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))}
                        </List>

                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleOpen(false)} color="inherit">
                    Fermer
                </Button>
            </DialogActions>
        </Dialog>
    );
}
