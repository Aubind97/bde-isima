import React from 'react';
import { withSnackbar } from 'notistack';
import Container from '@material-ui/core/Container';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import MaterialTable from 'material-table';
import { MuiThemeProvider, withTheme } from '@material-ui/core';
import { tableProps, CRUD } from 'utils';

export default withTheme(withMobileDialog()(withSnackbar(class Promotions extends React.Component {

    render() {
        const { fullScreen, theme } = this.props;
        return (
            <Container maxWidth="xl">
                <MuiThemeProvider theme={tableProps.tableTheme(theme)}>
                    <MaterialTable
                        onChangeColumnHidden={tableProps.onChangeColumnHidden('promotions')}
                        onChangeRowsPerPage={tableProps.onChangeRowsPerPage('promotions')}
                        columns={[
                            {
                                title: 'Année de promotion',
                                field: 'year',
                                hidden: tableProps.isHidden(`promotions_year`)
                            },
                            {
                                title: 'ID Groupe Facebook',
                                field: 'fb_group_id',
                                hidden: tableProps.isHidden(`promotions_fb_group_id`)
                            },
                            {
                                title: 'Email',
                                field: 'list_email',
                                hidden: tableProps.isHidden(`promotions_list_email`)
                            }
                        ]}
                        data={CRUD.remote('/promotions', {
                            orderBy: 'year',
                            orderDirection: 'DESC',
                        })}
                        title="Promotions"
                        editable={{
                            onRowAdd: newData => CRUD.add({
                                url: '/promotions',
                                newData,
                                props: this.props
                            }),
                            onRowUpdate: (newData, oldData) => CRUD.update({
                                url: `/promotions/${newData.id}`,
                                newData,
                                props: this.props
                            }),
                            onRowDelete: oldData => CRUD.delete({
                                url: `/promotions/${oldData.id}`,
                                props: this.props
                            })
                        }}
                        icons={tableProps.tableIcons}
                        options={{
                            ...tableProps.tableOptions, ...{
                                exportButton: false,
                                showTitle: !fullScreen,
                                searchFieldAlignment: fullScreen ? "left" : "right",
                                pageSize: tableProps.getPageSize('promotions')
                            }
                        }}
                        localization={tableProps.tableLocalization}
                    />
                </MuiThemeProvider>
            </Container>
        );
    }
})));
