import React from 'react';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { Loads } from 'react-loads';
import { CRUD } from 'utils';
import Plus from 'mdi-material-ui/Plus';
import Skeleton from '@material-ui/lab/Skeleton';
import EditViewSub from '../SubManager/EditViewSub';
import AddSub from '../SubManager/AddSub';
import DeleteSub from '../SubManager/DeleteSub';

export default function SubscriptionViewer({ classes, values }) {

    const [anchorEl, setAnchorEl] = React.useState(null);
    const [onEdit, setOnEdit] = React.useState(null);
    const [selected, setSelected] = React.useState(null);

    const [isAddDialogOpen, setIsAddDialogOpen] = React.useState(false);
    const [isDeleteDialogOpen, setIsDeleteDialogOpen] = React.useState(false);

    const getSubscriptions = id => () => CRUD.get({ url: `/subscriptions/event/all/${id}` }); 

    return (
        <Grid container item direction="column" xs={12}>

            <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>Inscriptions pour l'événement</FormLabel>
            <Divider className={classes.divider} />

            <Grid container item xs={12} justify="flex-end">
                <Button
                    className={classes.margin}
                    startIcon={<Plus />}
                    aria-label="Ajouter un inscrit"
                    onClick={() => setIsAddDialogOpen(true)}
                    variant="contained"
                    color="primary"
                >
                    Ajouter
                </Button>
            </Grid>

            {(values && values.id) && (
                <>
                    <FormGroup style={{ margin: '4px 0px 4px 0px', padding: '0px 8px 0px 8px' }}>
                        <Grid container item spacing={2} justify="flex-start">

                            <Loads load={getSubscriptions(values.id)}>
                                {({ response, isResolved }) => (
                                    <>
                                        {!isResolved && [...Array(3).keys()].map(idx => (
                                            <Grid key={idx} container item xs={12} md={4}>
                                                <Box display="flex" flexDirection="column" flexGrow={1}>
                                                    <Skeleton height={300} width="100%" />
                                                </Box>
                                            </Grid>
                                        ))}

                                        {(isResolved && response.data.length === 0) && (
                                            <Box
                                                display="flex"
                                                flexGrow={1}
                                                justifyContent="center"
                                                alignItems="center"
                                                flexDirection="column"
                                            >
                                                <img
                                                    src="/images/illustrations/NoData.svg"
                                                    height="auto"
                                                    width="300"
                                                    alt="Aucune donnée"
                                                />
                                                <Typography
                                                    variant="subtitle2"
                                                    gutterBottom
                                                >
                                                    Aucune inscription pour le moment !
                                                </Typography>
                                            </Box>
                                        )}

                                        <Menu
                                            id="subscription-viewer-menu"
                                            anchorEl={anchorEl}
                                            open={Boolean(anchorEl)}
                                            onClose={() => setAnchorEl(null)}
                                        >
                                            <MenuItem onClick={onEdit} onMouseUp={() => setAnchorEl(null)}>
                                                Éditer
                                            </MenuItem>
                                            <MenuItem onClick={() => setIsDeleteDialogOpen(true)} onMouseUp={() => setAnchorEl(null)}>
                                                Supprimer
                                            </MenuItem>
                                        </Menu>

                                        {isResolved && response.data.map(subscription => (
                                            <EditViewSub
                                                key={subscription.id}
                                                subscription={subscription}
                                                subscriptions={response.data}
                                                setOnEdit={setOnEdit}
                                                setSelected={setSelected}
                                                setAnchorEl={setAnchorEl}
                                                classes={classes}
                                            />
                                        ))}

                                        <AddSub
                                            open={isAddDialogOpen}
                                            eventId={values.id}
                                            subscriptions={isResolved ? response.data : null}
                                            onClose={() => setIsAddDialogOpen(false)}
                                            classes={classes}
                                        />

                                        <DeleteSub
                                            open={isDeleteDialogOpen}
                                            selected={selected}
                                            subscriptions={isResolved ? response.data : null}
                                            onClose={() => setIsDeleteDialogOpen(false)}
                                            classes={classes}
                                        />
                                    </>
                                )}
                            </Loads>
                        </Grid>
                    </FormGroup>
                </>
            )}
        </Grid>
    )
};
