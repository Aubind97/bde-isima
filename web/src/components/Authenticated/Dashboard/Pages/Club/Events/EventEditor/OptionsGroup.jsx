import React from 'react';
import Grid from '@material-ui/core/Grid';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import DotsVertical from 'mdi-material-ui/DotsVertical';
import Close from 'mdi-material-ui/Close';
import PlusCircleOutline from 'mdi-material-ui/PlusCircleOutline';
import IconButton from '@material-ui/core/IconButton';
import Plus from 'mdi-material-ui/Plus';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Zoom from '@material-ui/core/Zoom';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import { Field, SelectField } from 'utils/forms';

export default props => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [selectedField, setSelectedField] = React.useState(null);

    const addOption = () => {
        props.fields.push(props.type, {
            name: '',
            type: 'combinable',
            price: 0,
            items: [],
        });
    };

    return (
        <Grid container item direction="column">

            <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>{props.title}</FormLabel>

            <Divider className={props.classes.divider} />

            <Grid container item xs={12} justify="flex-end">
                <Button
                    className={props.classes.margin}
                    startIcon={<Plus />}
                    aria-label="Ajouter un produit"
                    onClick={addOption}
                    variant="contained"
                    color="primary"
                >
                    Ajouter
                </Button>
            </Grid>

            <FormGroup style={{ margin: '4px 0px 8px 0px', padding: '0px 8px 0px 8px' }}>

                <Grid container item spacing={2}>

                    <Menu
                        id="options-group-menu"
                        anchorEl={anchorEl}
                        open={Boolean(anchorEl)}
                        onClose={() => setAnchorEl(null)}
                    >
                        <MenuItem onClick={() => props.fields.remove(selectedField)} onMouseUp={() => setAnchorEl(null)}>
                            Supprimer
                        </MenuItem>
                    </Menu>

                    {props.fields.map(`${props.type}.*`, (item, itemIdx) => (
                        <Zoom key={item} in={true}>
                            <Grid container item xs={12} md={6} direction="column">
                                <Box display="flex" flexDirection="column" className={props.classes.card} component={Card}>

                                    <CardHeader
                                        action={(
                                            <IconButton 
                                                aria-label="Options" 
                                                onClick={event => {
                                                    setAnchorEl(event.currentTarget);
                                                    setSelectedField(item);
                                                }}
                                            >
                                                <DotsVertical />
                                            </IconButton>
                                        )}
                                        title={`${props.label} #${itemIdx + 1}`}
                                    />

                                    <Field
                                        type="text"
                                        name={`${item}.name`}
                                        label="Nom du groupe"
                                        fullWidth
                                    />

                                    <SelectField
                                        type="text"
                                        name={`${item}.type`}
                                        label="Type d'options"
                                    >
                                        <MenuItem value={"combinable"}>Combinables</MenuItem>
                                        <MenuItem value={"exclusive"}>Exclusives</MenuItem>
                                    </SelectField>

                                    <Grid container spacing={1}>
                                        {props.fields.map(`${item}.items.*`, (item, itemIdx) => (
                                            <React.Fragment key={itemIdx}>
                                                <Grid item xs={5}>
                                                    <Field
                                                        type="text"
                                                        name={`${item}.name`}
                                                        label={`Nom de l'option #${itemIdx + 1}`}
                                                    />
                                                </Grid>
                                                <Grid item xs={5}>
                                                    <Field
                                                        type="number"
                                                        inputProps={{
                                                            step: "0.01"
                                                        }}
                                                        name={`${item}.price`}
                                                        label={`Prix de l'option #${itemIdx + 1}`}
                                                    />
                                                </Grid>
                                                <Grid container item xs={2} alignContent="center">
                                                    <IconButton
                                                        className={props.classes.closeBtn}
                                                        onClick={() => props.fields.remove(item)}
                                                        aria-label="Supprimer"
                                                    >
                                                        <Close />
                                                    </IconButton>
                                                </Grid>
                                            </React.Fragment>
                                        ))}

                                        <Grid item xs>
                                            <Button
                                                className={props.classes.margin}
                                                startIcon={<PlusCircleOutline/>}
                                                onClick={() => props.fields.push(`${item}.items`, {
                                                    name: '',
                                                    price: 0,
                                                })}
                                                variant="contained"
                                                color="primary"
                                                aria-label="Ajouter une option"
                                            >
                                                Ajouter une option
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </Grid>
                        </Zoom>
                    ))}

                </Grid>

                {props.variant === "button" && (
                    <Button
                        className={props.classes.margin}
                        startIcon={<PlusCircleOutline />}
                        onClick={() => props.fields.push(`${props.type}`, {
                            name: '',
                            type: 'exclusive',
                            items: [],
                        })}
                        variant="contained"
                        color="primary"
                        size="large"
                        aria-label={props.addLabel}
                    >
                        {props.addLabel}
                    </Button>
                )}

            </FormGroup>

        </Grid>
    )
};
