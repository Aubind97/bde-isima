import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Plus from 'mdi-material-ui/Plus';
import Button from '@material-ui/core/Button';
import SearchUser from 'components/Authenticated/Hub/Pages/Home/Components/SearchUser';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import { CRUD } from 'utils';

/**
 * This component is responsible for displaying a confirmation dialog prior to adding a subscription to an event
 */
export default function AddSub({ classes, open, subscriptions, eventId, onClose }) {

    /** States initialisation */
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const [searchInput, setSearchInput] = React.useState('');
    const [selected, setSelected] = React.useState(null);

    /**
     * HTTP request that adds a subscription to an event to the API
     */
    function addSubscription() {
        CRUD.add({
            url: '/subscriptions',
            newData: {
                user_id: selected.id,
                event_id: eventId,
                payment_method: 'bde',
                cart: []
            },
            onSucceed: result => {
                onAddConfirm(result.data.inserted_id);
                setSelected(null);
                setSearchInput('');
                onClose();
            },
            props: { enqueueSnackbar, closeSnackbar }
        });
    };

    /**
     * On click handler that locally add the event to the locale variables
     */
    function onAddConfirm(id) {
        subscriptions.push({
            id,
            user_id: selected.id,
            event_id: eventId,
            payment_method: 'bde',
            cart: []
        });
    };

    return (
        <Dialog
            open={open}
            fullScreen={fullScreen}
            PaperProps={{ className: classes.dialog }}
            onClose={onClose}
            aria-labelledby="alert-subscribe-title"
            aria-describedby="alert-subscribe-description"
        >
            <DialogTitle id="alert-subscribe-title">Ajout d'un nouvel inscrit</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-subscribe-description">
                    Veuillez rechercher un membre pour l'ajouter.
                </DialogContentText>

                <SearchUser
                    id="search_subscriber"
                    searchInput={searchInput}
                    label="Membre à inscrire"
                    component={TextField}
                    selected={selected}
                    onChange={searchInput => setSearchInput(searchInput)}
                    onSelection={selected => setSelected(selected)}
                    onClearRequested={() => {
                        setSelected(null);
                        setSearchInput('');
                    }}
                />

            </DialogContent>
            <DialogActions>
                <Button aria-label="Annuler" variant="outlined" onClick={onClose} color="inherit">
                    Annuler
                </Button>
                <Button
                    variant="contained"
                    startIcon={<Plus />}
                    onClick={addSubscription}
                    color="primary"
                    aria-label="Ajouter"
                    autoFocus
                >
                    Ajouter
                </Button>
            </DialogActions>
        </Dialog>
    );
};

AddSub.propTypes = {
    /** Boolean that orders whether or not the dialog should be open */
    open: PropTypes.bool,
    /** Locale variable containing the list of subscriptions of a specific event */
    subscriptions: PropTypes.array,
    /** The event id to which add the subscription */
    eventId: PropTypes.number,
    /** Callback to close the dialog */
    onClose: PropTypes.func,
    /** CSS classes for inline-style */
    classes: PropTypes.object
};
