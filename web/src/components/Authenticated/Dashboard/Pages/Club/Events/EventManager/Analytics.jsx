import React from 'react';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import FormLabel from '@material-ui/core/FormLabel';
import { Loads } from 'react-loads';
import { CRUD } from 'utils';
import { Typography, Box, Card } from '@material-ui/core';
import Splash from 'components/General/Splash';

export default props => {

    const getSubscriptions = id => () => CRUD.get({ url: `/subscriptions/event/all/${id}` });

    return (
        <Grid container item direction="column" xs={12}>

            <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>Résumé de l'évènement</FormLabel>
            <Divider className={props.classes.divider} />

            <Loads load={getSubscriptions(props.selected.id)}>
                {({ response, isRejected, isResolved, isPending }) => (
                    <div style={{ padding: 20 }}>
                        <Grid container className={props.classes.card}>

                            {(isRejected || isPending) && <Splash />}

                            {isResolved && (
                                <Grid item xs={12}>
                                    <Card className={props.classes.card}>
                                        <Grid container>
                                            <Grid item xs={12} md={3} component={Box} display="flex" flexDirection="column">

                                                <Typography variant="subtitle1" color="textSecondary">
                                                    Audience engagée
                                                    </Typography>
                                                <Typography variant="h6" color="inherit" gutterBottom>
                                                    {response.data.length} inscrit(s)
                                                </Typography>

                                                <Divider className={props.classes.divider} />

                                                <Typography variant="subtitle1" color="textSecondary">
                                                    Recettes actuelles
                                                </Typography>
                                                <Typography variant="h6" color="inherit" gutterBottom>
                                                    {response.data.reduce((prev, curr) =>
                                                        prev + curr.cart.reduce((prev, curr) => prev + parseFloat(curr.price), 0), 0).toFixed(2)} €
                                                    </Typography>

                                                <Divider className={props.classes.divider} />
                                            </Grid>
                                        </Grid>
                                    </Card>
                                </Grid>
                            )}
                        </Grid>
                    </div>
                )}
            </Loads>
        </Grid>
    )
};