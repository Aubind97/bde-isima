import React from 'react';
import { dripForm } from 'react-drip-form';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import ChevronLeft from 'mdi-material-ui/ChevronLeft';
import ChevronRight from 'mdi-material-ui/ChevronRight';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from 'mdi-material-ui/Close';
import CheckIcon from 'mdi-material-ui/Check';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import MobileStepper from '@material-ui/core/MobileStepper';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import MainFields from './MainFields';
import OptionsGroup from './OptionsGroup';
import StandaloneFields from './StandaloneFields';
import ProductsField from './ProductsField';
import { useSwipeable } from 'react-swipeable';

const getSteps = () => ["Informations générales", "Produits", "Options", "Options spécifiques"];

function TabPanel(props) {
    const { children, value, index, dir, ...other } = props;

    return (
        <Typography 
            style={{ width: '80%' }}
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`event-tabpanel-${index}`}
            aria-labelledby={`event-tab-${index}`}
            {...other}
        >
            {children}
        </Typography>
    );
}

function a11yProps(index) {
    return {
        id: `event-tab-${index}`,
        'aria-controls': `event-tabpanel-${index}`,
    };
}

export default withMobileDialog()(dripForm({
    validations: {
        name: {
            required: true
        },
        takes_place_at: {
            required: true
        },
        subscriptions_end_at: {
            required: true
        }
    },
    messages: {
        name: {
            required: 'Ce champ est requis.'
        },
        takes_place_at: {
            required: 'Ce champ est requis.'
        },
        subscriptions_end_at: {
            required: 'Ce champ est requis.'
        }
    },
})(({
    handlers,
    fields,
    submitting,
    values,
    open,
    classes,
    ...props
}) => {

    /** States initialisation */
    const [index, setIndex] = React.useState(0);

    const swipeableHandlers = useSwipeable({
        onSwipedLeft: () => setIndex(index > 2 ? index : index + 1),
        onSwipedRight: () => setIndex(index < 1 ? index : index - 1),
        onSwipedDown: () => props.onClose(),
    });

    return open && (
        <Dialog
            open={open}
            onClose={props.onClose}
            fullScreen
            {...swipeableHandlers}
        >
            <AppBar className={classes.appBar}>
                <Toolbar>

                    <Grid container>

                        <Grid container item xs={6} md={3} justify='flex-start'>
                            <IconButton 
                                type="button" 
                                color="inherit" 
                                onClick={() => {
                                    props.onClose(); 
                                    setIndex(0);
                                }} 
                                aria-label="Fermer l'éditeur"
                            >
                                <CloseIcon />
                            </IconButton>
                        </Grid>

                        <Grid container item className={classes.title} xs={12} md={6} justify="center" alignContent="center">
                            <Typography variant="h6" color="inherit" gutterBottom>
                                Éditeur d'évènements
                            </Typography>
                        </Grid>

                        <Grid container item xs={6} md={3} justify='flex-end'>
                            <IconButton className={classes.cancelBtn} type="submit" color="inherit" onClick={handlers.onSubmit} aria-label="Valider">
                                {submitting ? <CircularProgress size={25} color="secondary" /> : <CheckIcon /> }
                            </IconButton>
                        </Grid>

                    </Grid>

                </Toolbar>
            </AppBar>

            <form onSubmit={handlers.onSubmit}>
                <Grid container justify="center">

                    {props.fullScreen && (
                        <MobileStepper
                            className={classes.stepper}
                            variant="dots"
                            steps={getSteps().length}
                            position="static"
                            activeStep={index}
                            nextButton={
                                <Button aria-label="Suivant" size="small" onClick={() => setIndex(prevIndex => prevIndex + 1)} disabled={index === getSteps().length - 1}>
                                    Suivant
                                    {props.theme.direction === 'rtl' ? <ChevronLeft /> : <ChevronRight />}
                                </Button>
                            }
                            backButton={
                                <Button aria-label="Retour" size="small" onClick={() => setIndex(prevIndex => prevIndex - 1)} disabled={index === 0}>
                                    {props.theme.direction === 'rtl' ? <ChevronRight /> : <ChevronLeft />}
                                    Retour
                                </Button>
                            }
                        />
                    )}

                    {!props.fullScreen && (
                        <Stepper 
                            className={classes.stepper}
                            activeStep={index}
                            alternativeLabel 
                            nonLinear
                        >
                            {getSteps().map((step, idx) => (
                                <Step key={idx} {...a11yProps(idx)}>
                                    <StepButton onClick={() => setIndex(idx)} >
                                        {step}
                                    </StepButton>
                                </Step>
                            ))}
                        </Stepper>
                    )}

                    <TabPanel value={index} index={0} dir={props.theme.direction}>
                        <MainFields 
                            classes={classes} 
                            values={values} 
                        />
                    </TabPanel>

                    <TabPanel value={index} index={1} dir={props.theme.direction}>
                        <ProductsField
                            fields={fields}
                            classes={classes}
                        />
                    </TabPanel>

                    <TabPanel value={index} index={2} dir={props.theme.direction}>
                        <OptionsGroup
                            type="options"
                            title="Groupe(s) d'option(s)"
                            addLabel="Ajouter un groupe d'option(s)"
                            label="Groupe d'option(s)"
                            fields={fields}
                            classes={classes}
                        />
                    </TabPanel>

                    <TabPanel value={index} index={3} dir={props.theme.direction}>
                        <StandaloneFields
                            title="Groupe(s) d'option(s) spécifique(s)"
                            fields={fields}
                            classes={classes}
                        />
                    </TabPanel>
                    
                </Grid>
            </form>
        </Dialog>
    );
}
));
