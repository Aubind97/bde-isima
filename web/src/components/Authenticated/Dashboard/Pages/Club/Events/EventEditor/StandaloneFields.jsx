import React from 'react';
import Grid from '@material-ui/core/Grid';
import DotsVertical from 'mdi-material-ui/DotsVertical';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';
import Zoom from '@material-ui/core/Zoom';
import Menu from '@material-ui/core/Menu';
import Plus from 'mdi-material-ui/Plus';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Divider from '@material-ui/core/Divider';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import { Field } from 'utils/forms';
import OptionsGroup from './OptionsGroup';

export default props => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [selectedField, setSelectedField] = React.useState(null);

    const addStandalone = () => {
        props.fields.push('standalone', {
            name: '',
            price: 0,
            options: [],
        });
    };
    
    return (
        <Grid container item direction="column">

            <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>{props.title}</FormLabel>

            <Divider className={props.classes.divider} />

            <Grid container item xs={12} justify="flex-end">
                <Button
                    className={props.classes.margin}
                    startIcon={<Plus />}
                    aria-label="Ajouter un produit spécifique"
                    onClick={addStandalone}
                    variant="contained"
                    color="primary"
                >
                    Ajouter
                </Button>
            </Grid>

            <FormGroup style={{ margin: '4px 0px 4px 0px', padding: '0px 8px 0px 8px' }}>

                <Grid container item spacing={2}>

                    <Menu
                        id="standalone-fields-menu"
                        anchorEl={anchorEl}
                        open={Boolean(anchorEl)}
                        onClose={() => setAnchorEl(null)}
                    >
                        <MenuItem onClick={() => props.fields.remove(selectedField)} onMouseUp={() => setAnchorEl(null)}>
                            Supprimer
                        </MenuItem>
                    </Menu>

                    {props.fields.map('standalone.*', (item, itemIdx) => (
                        <Zoom key={item} in={true}>
                            <Grid key={item} container item xs={12} direction="column">
                                <Box display="flex" flexDirection="column" className={props.classes.card} component={Card}>

                                    <CardHeader
                                        action={(
                                            <IconButton 
                                                aria-label="Options" 
                                                onClick={event => {
                                                    setAnchorEl(event.currentTarget);
                                                    setSelectedField(item);
                                                }}
                                            >
                                                <DotsVertical />
                                            </IconButton>
                                        )}
                                        title={`Produit spécifique #${itemIdx + 1}`}
                                    />

                                    <Field
                                        type="text"
                                        name={`${item}.name`}
                                        label="Nom du produit"
                                        fullWidth
                                    />

                                    <Field
                                        type="number"
                                        inputProps={{
                                            step: "0.01"
                                        }}
                                        name={`${item}.price`}
                                        label="Prix du produit"
                                        fullWidth
                                    />

                                    <OptionsGroup
                                        type={`${item}.options`}
                                        variant="button"
                                        title="Option(s)"
                                        addLabel="Ajouter une option"
                                        label="Option"
                                        fields={props.fields}
                                        {...props}
                                    />

                                </Box>
                            </Grid>
                        </Zoom>
                    ))}

                </Grid>

            </FormGroup>

        </Grid>
    )
};
