import React from 'react';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';
import DotsVertical from 'mdi-material-ui/DotsVertical';
import Check from 'mdi-material-ui/Check';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Card from '@material-ui/core/Card';
import Select from '@material-ui/core/Select';
import CardHeader from '@material-ui/core/CardHeader';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Skeleton from '@material-ui/lab/Skeleton';
import Plus from 'mdi-material-ui/Plus';
import Close from 'mdi-material-ui/Close';
import { Loads } from 'react-loads';
import { CRUD } from 'utils';
import { withSnackbar } from 'notistack';

export default withSnackbar(class EditViewSub extends React.Component {
    state = {
        isEditing: false,
        selected: null,
        duplicata: null,
    };

    getUser = id => () => CRUD.get({
        url: `/users/compact/${id}`,
    });

    updateSubscription = () => {
        const { selected } = this.state;

        CRUD.update({
            url: `/subscriptions/${selected.id}`,
            newData: selected,
            onSucceed: () => this.setState({ selected: null, duplicata: null, isEditing: false }),
            props: this.props
        });
    };

    handleOnChange = (name, value) => () => this.setState({ [name]: value });

    handleOnSelectionChange = (name, idx) => event => {
        const { selected } = this.state;

        if (idx !== null) {
            selected.cart[idx][name] = event.target.value;
        }
        else {
            selected[name] = event.target.value;
        }

        this.setState({ selected });
    };

    handleReset = () => {
        const { duplicata } = this.state;
        const { subscription } = this.props;

        Object.getOwnPropertyNames(subscription).forEach(propertyName => {
            subscription[propertyName] = duplicata[propertyName];
        });

        this.setState({ selected: null, isEditing: false });
    };

    handleAddToCart = () => {
        const { selected } = this.state;
        selected.cart.push({
            name: '',
            options: '',
            price: 0,
            quantity: 0,
            comment: null
        });
        this.setState({ selected });
    };

    handleRemoveFromCart = idx => () => {
        const { selected } = this.state;
        selected.cart.splice(idx, 1);
        this.setState({ selected });
    };

    render() {
        const { classes, subscription, setOnEdit, setAnchorEl, setSelected } = this.props;
        const { isEditing, selected } = this.state;

        return (
            <>
                <Grid container item xs={12} md={4} direction="column">

                    <Box display="flex" flexDirection="column" className={classes.card} component={Card}>

                        <Loads load={this.getUser(subscription.user_id)}>
                            {({ response, isResolved }) => (
                                <CardHeader
                                    action={(
                                        isEditing ? (
                                            <>
                                                <IconButton
                                                    aria-label="Annuler"
                                                    onClick={this.handleReset}
                                                >
                                                    <Close />
                                                </IconButton>

                                                <IconButton
                                                    aria-label="Sauvegarder"
                                                    onClick={this.updateSubscription}
                                                >
                                                    <Check />
                                                </IconButton>
                                            </>
                                        ) : (
                                                <IconButton
                                                    aria-label="Options"
                                                    onClick={event => {
                                                        setAnchorEl(event.currentTarget);
                                                        setOnEdit(() => this.handleOnChange('isEditing', true));
                                                        setSelected(subscription);
                                                        this.setState({ 
                                                            selected: subscription, 
                                                            duplicata: JSON.parse(JSON.stringify(subscription)) 
                                                        });
                                                    }}
                                                >
                                                    <DotsVertical />
                                                </IconButton>
                                            )
                                    )}
                                    title={isResolved ? (
                                        `${response.data.firstname} ${response.data.lastname}`
                                    ) : (
                                            <Skeleton height={12} width="50%" />
                                        )}
                                    subheader={isResolved ? (
                                        isEditing ? (
                                            <FormControl className={classes.formControl}>
                                                <InputLabel shrink htmlFor={`payment-${selected.id}`}>
                                                    Paiement
                                                </InputLabel>
                                                <Select
                                                    value={selected.payment_method}
                                                    onChange={this.handleOnSelectionChange('payment_method', null)}
                                                    inputProps={{
                                                        id: `payment-${selected.id}`,
                                                    }}
                                                    fullWidth
                                                >
                                                    <MenuItem value="bde">BDE</MenuItem>
                                                    <MenuItem value="lydia">Lydia</MenuItem>
                                                    <MenuItem value="cash">Cash</MenuItem>
                                                </Select>
                                            </FormControl>
                                        ) : (
                                                `Paiement par ${subscription.payment_method.toUpperCase()}`
                                            )) : (
                                            <Skeleton height={12} width="50%" />
                                        )}
                                />
                            )}
                        </Loads>

                        <List>
                            {(isEditing ? selected : subscription).cart.map((item, idx) => (
                                isEditing ? (
                                    <React.Fragment key={idx}>
                                        <ListItem>
                                            <TextField
                                                type="number"
                                                label="Quantité"
                                                value={item.quantity}
                                                inputProps={{ step: "1" }}
                                                onChange={this.handleOnSelectionChange('quantity', idx)}
                                                className={classes.itemText}
                                            />
                                            <TextField
                                                type="text"
                                                label="Produit"
                                                value={item.name}
                                                onChange={this.handleOnSelectionChange('name', idx)}
                                                className={classes.itemText}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <TextField
                                                type="text"
                                                label="Options"
                                                value={item.options}
                                                onChange={this.handleOnSelectionChange('options', idx)}
                                                className={classes.itemText}
                                            />
                                            <TextField
                                                type="number"
                                                label="Prix total"
                                                value={item.price}
                                                inputProps={{ step: "0.01" }}
                                                onChange={this.handleOnSelectionChange('price', idx)}
                                                className={classes.itemText}
                                            />
                                        </ListItem>

                                        <ListItem>
                                            <TextField
                                                type="text"
                                                label="Commentaires"
                                                value={item.comment}
                                                onChange={this.handleOnSelectionChange('comment', idx)}
                                                className={classes.itemText}
                                            />
                                            <ListItemIcon>
                                                <Button
                                                    aria-label="Supprimer l'élément"
                                                    variant="outlined"
                                                    onClick={this.handleRemoveFromCart(idx)}
                                                    className={classes.itemText}
                                                >
                                                    Supprimer
                                                </Button>
                                            </ListItemIcon>
                                        </ListItem>

                                        <Divider className={classes.divider} />
                                    </React.Fragment>
                                ) : (
                                        <div key={idx} className={classes.element}>
                                            <ListItem dense disableGutters>
                                                <ListItemIcon>
                                                    <ListItemText
                                                        className={classes.itemText}
                                                        secondary={`x${item.quantity}`}
                                                    />
                                                </ListItemIcon>

                                                <ListItemText
                                                    className={classes.itemText}
                                                    primary={item.name}
                                                    secondary={item.options}
                                                />

                                                <ListItemSecondaryAction>
                                                    <ListItemText primary={`${item.price} €`} />
                                                </ListItemSecondaryAction>
                                            </ListItem>

                                            {item.comment && (
                                                <ListItemText secondary={`Commentaires : ${item.comment}`} />
                                            )}
                                        </div>
                                    )
                            ))}

                            {isEditing && (
                                <Button
                                    className={classes.margin}
                                    startIcon={<Plus />}
                                    onClick={this.handleAddToCart}
                                    variant="contained"
                                    color="primary"
                                    size="large"
                                    aria-label="Ajouter un élément"
                                >
                                    Ajouter un élément
                                </Button>
                            )}
                        </List>

                    </Box>
                </Grid>
            </>
        );
    }
});
