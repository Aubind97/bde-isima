import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import DeleteOutline from 'mdi-material-ui/DeleteOutline';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import { CRUD } from 'utils';

/**
 * This component is responsible for displaying a confirmation dialog prior to deleting a subscription from an event
 */
export default function DeleteSub({ open, subscriptions, selected, onClose }) {

    /** States initialisation */
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    /**
     * HTTP request that deletes a subscription from an event from the API
     */
    function deleteSubscription(){
        CRUD.delete({
            url: `/subscriptions/${selected.id}`,
            onSucceed: onClose,
            props: { enqueueSnackbar, closeSnackbar }
        });
    }

    /**
     * On click handler that locally delete the event from the locale variables
     */
    function onDeleteConfirm() {
        subscriptions.splice(subscriptions.findIndex(x => x === selected), 1);
        deleteSubscription();
    };

    return (
        <Dialog
            open={open}
            fullScreen={fullScreen}
            onClose={onClose}
            aria-labelledby="alert-unsubscribe-title"
            aria-describedby="alert-unsubscribe-description"
        >
            <DialogTitle id="alert-unsubscribe-title">Voulez-vous vraiment annuler cette inscription ?</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-unsubscribe-description">
                    Toute erreur nécessitera de réinscrire le membre concerné.
                    </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button aria-label="Annuler" variant="outlined" onClick={onClose} color="primary">
                    Annuler
                    </Button>
                <Button
                    variant="contained"
                    startIcon={<DeleteOutline />}
                    onClick={onDeleteConfirm}
                    color="primary"
                    aria-label="Confirmer"
                    autoFocus
                >
                    Supprimer
                </Button>
            </DialogActions>
        </Dialog>
    );
};

DeleteSub.propTypes = {
    /** Boolean that orders whether or not the dialog should be open */
    open: PropTypes.bool,
    /** Locale variable containing the list of subscriptions of a specific event */
    subscriptions: PropTypes.array,
    /** The subscription which is about to be deleted */
    selected: PropTypes.object,
    /** Callback to close the dialog */
    onClose: PropTypes.func,
    /** CSS classes for inline-style */
    classes: PropTypes.object
};
