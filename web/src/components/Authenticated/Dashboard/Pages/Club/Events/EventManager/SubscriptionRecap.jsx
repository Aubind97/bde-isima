import React from 'react';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Container from '@material-ui/core/Container';
import { Loads } from 'react-loads';
import { CRUD } from 'utils';
import Skeleton from '@material-ui/lab/Skeleton';

export default props => {

    const { classes } = props;

    const getSubscriptions = id => () => CRUD.get({ url: `/subscriptions/event/all/${id}` }); 

    const getRecap = data => {
        const recap = [];
        data.forEach(sub => {
            sub.cart.forEach(item => {
                const exist = recap.find(x => `${x.name} ${x.options}` === `${item.name} ${item.options}`);
                if (exist) {
                    exist.quantity = parseInt(exist.quantity) + parseInt(item.quantity);
                }
                else {
                    const { comment, ...rest } = item;
                    recap.push(rest);
                }
            });
        });
        return recap;
    };

    return (
        <Grid container item direction="column" xs={12}>

            <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>Récapitulatif de l'événement</FormLabel>
            <Divider className={props.classes.divider} />

            {(props.values && props.values.id) && (
                <>
                    <FormGroup style={{ margin: '4px 0px 4px 0px', padding: '0px 8px 0px 8px' }}>
                        <Grid container item spacing={2} justify="flex-start">

                            <Loads load={getSubscriptions(props.values.id)}>
                                {({ response, isResolved }) => (
                                    <>
                                        {!isResolved && [...Array(3).keys()].map(idx => (
                                            <Grid key={idx} container item xs={12} md={4}>
                                                <Box display="flex" flexDirection="column" flexGrow={1}>
                                                    <Skeleton height={12} width="100%" />
                                                    <Skeleton height={12} width="100%" />
                                                </Box>
                                            </Grid>
                                        ))}

                                        <Container maxWidth="sm">
                                            {isResolved && getRecap(response.data).map((item, idx) => (
                                                <React.Fragment key={idx}>
                                                    <ListItem dense disableGutters>
                                                        <ListItemIcon>
                                                            <Typography variant="overline">
                                                                x{item.quantity}
                                                            </Typography>
                                                        </ListItemIcon>

                                                        <ListItemText className={classes.itemText} primary={item.name} secondary={item.options} />
                                                    </ListItem>
                                                </React.Fragment>
                                            ))}
                                        </Container>

                                    </>
                                )}
                            </Loads>
                        </Grid>
                    </FormGroup>
                </>
            )}
        </Grid>
    )
};
