import React from 'react';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import Switch from '@material-ui/core/Switch';
import { Field, DTPickerField } from 'utils/forms';

export default props => {

    const [showMaxSub, setShowMaxSub] = React.useState(props.values.max_subscribers);

    return (
        <Grid container item direction="column" xs={12}>

            <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>Détails de l'événement</FormLabel>
            <Divider className={props.classes.divider} />
            <FormGroup style={{ margin: '4px 0px 4px 0px', padding: '0px 8px 0px 8px' }}>

                <Field
                    type="text"
                    id="name"
                    name="name"
                    label="Nom de l'évènement"
                    fullWidth
                />

                <Field
                    type="text"
                    id="description"
                    name="description"
                    label="Description"
                    endAdornment={(
                        <InputAdornment position="end">
                            <Typography color="textSecondary">{props.values.description && props.values.description.length}/255</Typography>
                        </InputAdornment>
                    )}
                    fullWidth
                    multiline
                    rows={5}
                />

                <DTPickerField
                    id="takes_place_at"
                    name="takes_place_at"
                    label="Date de l'évènement"
                    clearLabel=""
                />

                <DTPickerField
                    id="subscriptions_end_at"
                    name="subscriptions_end_at"
                    label="Date limite d'inscription"
                    clearLabel=""
                />

                <FormControlLabel
                    control={
                        <Switch
                            checked={showMaxSub}
                            onChange={() => setShowMaxSub(!showMaxSub)}
                            value="showMaxSub"
                            color="primary"
                        />
                    }
                    label="Ajouter une limite de participants ?"
                />

                {showMaxSub && (
                    <Field
                        type="number"
                        id="max_subscribers"
                        name="max_subscribers"
                        label="Nombre max de participants"
                    />
                )}

            </FormGroup>
        </Grid>
    )
};