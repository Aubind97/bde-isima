import React from 'react';
import axios from 'axios';
import 'array-flat-polyfill';
import { withSnackbar } from 'notistack';
import { CsvBuilder } from 'filefy';
import Chip from '@material-ui/core/Chip';
import MaterialTable from 'material-table';
import Check from 'mdi-material-ui/Check';
import CheckAll from 'mdi-material-ui/CheckAll';
import History from 'mdi-material-ui/History';
import TimerSand from 'mdi-material-ui/TimerSand';
import withMobileDialog from '@material-ui/core/withMobileDialog'
import EventForm from './EventEditor/EventForm';
import PlusCircleOutline from 'mdi-material-ui/PlusCircleOutline';
import PencilOutline from 'mdi-material-ui/PencilOutline';
import ContentCopy from 'mdi-material-ui/ContentCopy';
import ViewDashboard from 'mdi-material-ui/ViewDashboard';
import ContentSaveMoveOutline from 'mdi-material-ui/ContentSaveMoveOutline';
import { MuiThemeProvider } from '@material-ui/core';
import { withStyles } from '@material-ui/core';
import { tableProps, CRUD, DateUtils } from 'utils';
import EventManager from './EventManager';
import { format } from 'date-fns';

const styles = theme => ({
    margin: {
        margin: theme.spacing(2)
    },
    appBar: {
        position: 'relative'
    },
    title: {
        [theme.breakpoints.down('sm')]: {
            order: 3
        }
    },
    card: {
        padding: theme.spacing(2)
    },
    fullWidth: {
        width: '100%'
    },
    divider: {
        margin: theme.spacing(1, 0)
    },
    cancelBtn: {
        '&:disabled': {
            color: '#434548',
            borderColor: '#434548'
        }
    },
    stepper: {
        width: '100%',
        flexGrow: 1,
        background: 'none',
        marginBottom: theme.spacing(2)
    },
    itemText: {
        textAlign: 'right',
        margin: theme.spacing(0, 2),
    },
});

export default withStyles(styles, { withTheme: true })(withMobileDialog()(withSnackbar(class Events extends React.Component {
    state = {
        selected: {},
        submitting: false,
        isViewerOpen: false
    };

    tableRef = React.createRef();

    handleInitialize = form => this.form = form;

    handleSelectedChange = selected => this.setState({ selected, isViewerOpen: false }, () => this.form.setValues(this.state.selected, false));

    getEvents = (query, club_id) => CRUD.remote('/events', {
        orderBy: 'subscriptions_end_at',
        orderDirection: 'desc',
        club_id,
    })(query);

    addEvent = newData => CRUD.add({
        url: `/events`,
        newData,
        onSucceed: this.resetState,
        onRejection: () => this.setState({ submitting: false }),
        props: this.props
    });

    updateEvent = newData => CRUD.update({
        url: `/events/${newData.id}`,
        newData,
        onSucceed: this.resetState,
        onRejection: () => this.setState({ submitting: false }),
        props: this.props
    });

    handleSubmit = data => {
        this.setState({ submitting: true });

        data.club_id = this.state.id;
        data.takes_place_at = DateUtils.toDatabaseDate(data.takes_place_at);
        data.subscriptions_end_at = DateUtils.toDatabaseDate(data.subscriptions_end_at);
        data.options = data.options ? data.options : [];
        data.products = data.products ? data.products : [];
        data.standalone = data.standalone ? data.standalone : [];

        (data.id ? this.updateEvent : this.addEvent)(data);
    };

    resetState = () => {
        this.setState({ submitting: false, isViewerOpen: false, selected: {} });

        if (this.tableRef.current)
            this.tableRef.current.onQueryChange();
    };

    render() {
        const { classes, fullScreen, theme } = this.props;
        const { selected, submitting, isViewerOpen } = this.state;

        return (
            <>
                <MuiThemeProvider theme={tableProps.tableTheme(theme)}>
                    <MaterialTable
                        tableRef={this.tableRef}
                        onChangeColumnHidden={tableProps.onChangeColumnHidden('events')}
                        onChangeRowsPerPage={tableProps.onChangeRowsPerPage('events')}
                        columns={[
                            {
                                title: 'Nom',
                                field: 'name',
                                hidden: tableProps.isHidden(`events_name`)
                            },
                            {
                                title: 'Statut',
                                field: 'status',
                                render: rowData => {
                                    switch (rowData.status) {
                                        //StandBy events chip
                                        case 0: return (
                                            fullScreen ?
                                                <TimerSand style={{ color: '#e67e22' }} />
                                                :
                                                <Chip
                                                    style={{ color: '#e67e22', borderColor: '#e67e22' }}
                                                    label="En attente de validation par le BDE"
                                                    icon={<TimerSand style={{ color: '#e67e22' }} />}
                                                    variant="outlined"
                                                />
                                        );
                                        //Ongoing or past events
                                        case 1: return (
                                            new Date() < DateUtils.withOffsetDate(rowData.subscriptions_end_at) ? (
                                                //Subscriptions are opened
                                                fullScreen ?
                                                    <History style={{ color: '#e67e22' }} />
                                                    :
                                                    <Chip
                                                        style={{ color: '#e67e22', borderColor: '#e67e22' }}
                                                        label="Inscriptions ouvertes"
                                                        icon={<History style={{ color: '#e67e22' }} />}
                                                        variant="outlined"
                                                    />
                                            ) : (
                                                    //Event is past
                                                    fullScreen ?
                                                        <Check style={{ color: '#27ae60' }} />
                                                        :
                                                        <Chip style={{ color: '#27ae60', borderColor: '#27ae60' }}
                                                            label="Clotûré et encaissable"
                                                            icon={<Check style={{ color: '#27ae60' }} />}
                                                            variant="outlined"
                                                        />
                                                )
                                        );
                                        //Event is past and checked out 
                                        default: return (
                                            fullScreen ?
                                                <CheckAll style={{ color: '#27ae60' }} />
                                                :
                                                <Chip
                                                    style={{ color: '#27ae60', borderColor: '#27ae60' }}
                                                    label="Terminé"
                                                    icon={<CheckAll style={{ color: '#27ae60' }} />}
                                                    variant="outlined"
                                                />
                                        );
                                    }
                                },
                                hidden: tableProps.isHidden(`events_status`)
                            },
                            {
                                title: 'Date de l\'événement',
                                field: 'takes_place_at',
                                render: rowData => format(DateUtils.withOffsetDate(rowData.takes_place_at), 'dd/MM/yyyy à HH:mm'),
                                hidden: tableProps.isHidden(`events_takes_place_at`)
                            },
                            {
                                title: 'Date de fin des inscriptions',
                                field: 'subscriptions_end_at',
                                render: rowData => format(DateUtils.withOffsetDate(rowData.subscriptions_end_at), 'dd/MM/yyyy à HH:mm'),
                                hidden: tableProps.isHidden(`events_subscriptions_end_at`)
                            }
                        ]}
                        data={query => {
                            const { id } = this.state;
                            if(id) {
                                return this.getEvents(query, id);
                            }
                            return CRUD.get({ url: `/clubs?name=${this.props.name}` })
                                .then(result => {
                                    const club = result.data.pop();
                                    if (club) this.setState({ id: club.id });
                                    return this.getEvents(query, club.id);
                                });
                        }}
                        title="Événements"
                        editable={{
                            onRowDelete: oldData => CRUD.delete({
                                url: `/events/${oldData.id}`,
                                onSucceed: this.resetState,
                                onRejection: this.resetState,
                                props: this.props
                            })
                        }}
                        actions={[
                            {
                                icon: React.forwardRef((props, ref) => <ViewDashboard {...props} ref={ref} />),
                                tooltip: 'Gérer',
                                onClick: (event, rowData) => {
                                    this.handleSelectedChange(rowData);
                                    this.setState({ isViewerOpen: true });
                                }
                            },
                            {
                                icon: React.forwardRef((props, ref) => <ContentCopy {...props} ref={ref} />),
                                tooltip: 'Créer depuis',
                                onClick: (event, rowData) => {
                                    const copy = JSON.parse(JSON.stringify(rowData));
                                    copy.id = null;
                                    copy.status = '0';
                                    this.handleSelectedChange(copy);
                                }
                            },
                            {
                                icon: React.forwardRef((props, ref) => <PencilOutline {...props} ref={ref} />),
                                tooltip: 'Éditer',
                                onClick: (event, rowData) => {
                                    this.handleSelectedChange(rowData);
                                }
                            },
                            rowData => ({
                                icon: React.forwardRef((props, ref) => <ContentSaveMoveOutline {...props} ref={ref} />),
                                tooltip: 'Exporter les inscrits en .CSV',
                                onClick: (event, rowData) => {
                                    CRUD.get({ url: `/subscriptions/event/all/${rowData.id}` })
                                        .catch(() => Promise.resolve({ data: [] }))
                                        .then(({ data: subscriptions }) => {
                                            if (Array.isArray(subscriptions)) {
                                                axios.all(subscriptions.map(subscription => CRUD.get({ url: `/users/compact/${subscription.user_id}` })))
                                                    .then(users => {
                                                        new CsvBuilder(`${rowData.name}.csv`)
                                                            //Default delimiter
                                                            .setDelimeter(',')
                                                            //We add the distinct columns to the CSV for readability considerations
                                                            .setColumns(['Moyen de paiement', 'Carte', 'Nom', 'Prénom', ...rowData.products.map(x => x.name), 'Total'])
                                                            //Then we add on the rows the corresponding information 
                                                            .addRows(users.map((user, idx) => {
                                                                return [
                                                                    subscriptions[idx].payment_method.toUpperCase(),
                                                                    user.data.card,
                                                                    user.data.lastname,
                                                                    user.data.firstname,
                                                                    ...rowData.products.reduce((prev, curr) => {
                                                                        const items = subscriptions[idx].cart.filter(item => item.name === curr.name);

                                                                        if (items.length) {
                                                                            return [
                                                                                ...prev, 
                                                                                items.map(x => `x${x.quantity} - ${x.name} ${x.options ? `(${x.options})` : ''} ${x.comment ? `(${x.comment})` : ''}`.trim()).join('\n')
                                                                            ];
                                                                        }
                                                                        return prev.concat('');
                                                                    }, []),
                                                                    subscriptions[idx].cart.reduce((acc, val) => acc + parseFloat(val.price), 0).toFixed(2)
                                                                ];
                                                            }))
                                                            .exportFile();
                                                    });
                                            }
                                        });
                                },
                            }),
                            {
                                icon: React.forwardRef((props, ref) => <PlusCircleOutline {...props} ref={ref} />),
                                tooltip: 'Ajouter',
                                isFreeAction: true,
                                onClick: event => {
                                    this.setState({
                                        selected: {
                                            name: '',
                                            takes_place_at: new Date(),
                                            subscriptions_end_at: new Date(),
                                            products: [],
                                            options: [],
                                            standalone: []
                                        }
                                    });
                                }
                            }
                        ]}
                        icons={tableProps.tableIcons}
                        options={{
                            ...tableProps.tableOptions, ...{
                                exportButton: false,
                                showTitle: !fullScreen,
                                searchFieldAlignment: fullScreen ? "left" : "right",
                                pageSize: tableProps.getPageSize('events')
                            }
                        }}
                        localization={tableProps.tableLocalization}
                    />
                </MuiThemeProvider>

                <EventForm
                    values={selected}
                    open={!(Object.entries(selected).length === 0 && selected.constructor === Object)}
                    submitting={submitting}
                    onInitialize={this.handleInitialize}
                    onValidSubmit={this.handleSubmit}
                    onClose={() => this.handleSelectedChange({})}
                    classes={classes}
                    theme={theme}
                />

                <EventManager
                    selected={selected}
                    open={isViewerOpen}
                    onClose={() => this.handleSelectedChange({})}
                    classes={classes}
                />
            </>
        );
    };
})));
