import React from 'react';
import { useTheme, makeStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from 'mdi-material-ui/Close';
import Container from '@material-ui/core/Container';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import Analytics from './Analytics';
import SubscriptionViewer from './SubscriptionViewer';
import SubscriptionRecap from './SubscriptionRecap';
import User from 'components/Authenticated/Auth/Components/User';

const useStyles = makeStyles(theme => ({
    appBar: {
        position: 'relative',
        width: '100%',
    },
    tabContainer: {
        overflow: 'hidden',
        marginBottom: theme.spacing(3), 
    },
    title: {
        [theme.breakpoints.down('sm')]: {
            order: 3,
        },
    },
    stepper: {
        width: '100%',
        flexGrow: 1,
        background: 'none',
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    card: {
        padding: theme.spacing(2),
        height: '100%',
    },
    itemText: {
        margin: theme.spacing(0, 1),
    },
    element: {
        margin: theme.spacing(2, 0),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    margin: {
        margin: theme.spacing(2),
    },
    dialog: {
        minHeight: 500,
    },
}));

const getSteps = () => ["Statistiques", "Liste des inscrits", "Récapitulatif"];

function TabPanel(props) {
    const { children, value, index, dir, ...other } = props;

    return (
        <Container 
            style={{ minWidth: '100vw' }} 
            maxWidth="lg" 
            dir={dir}
            component="span"
            role="tabpanel"
            hidden={value !== index}
            id={`manager-tabpanel-${index}`}
            aria-labelledby={`manager-tab-${index}`}
            {...other}
        >
            {children}
        </Container>
    );
}

function a11yProps(index) {
    return {
        id: `manager-tab-${index}`,
        'aria-controls': `manager-tabpanel-${index}`,
    };
}

export default props => {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const [index, setIndex] = React.useState(0);

    return props.open && (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            fullScreen
        >
            <AppBar className={classes.appBar}>
                <Toolbar>

                    <Grid container>

                        <Grid container item xs={6} md={3} justify='flex-start'>
                            <IconButton
                                type="button"
                                color="inherit"
                                onClick={() => {
                                    props.onClose();
                                    setIndex(0);
                                }}
                                aria-label="Fermer le manager"
                            >
                                <CloseIcon />
                            </IconButton>
                        </Grid>

                        <Grid container item className={classes.title} xs={12} md={6} justify="center" alignContent="center">
                            <Typography variant="h6" color="inherit" gutterBottom>
                                Manager d'évènements
                            </Typography>
                        </Grid>

                    </Grid>

                </Toolbar>
            </AppBar>

            <Grid container justify="center">

                <AppBar
                    className={classes.appBar}
                    color="inherit"
                    style={{ marginBottom: 3 * 8 }}
                >
                    <Tabs
                        className={classes.stepper}
                        value={index}
                        onChange={(e, idx) => setIndex(idx)}
                        variant="fullWidth"
                        textColor="inherit"
                        indicatorColor={!!+User.getPreferences('dark_mode') ? "secondary" : "primary"}
                    >
                        {getSteps().map((step, idx) => (
                            <Tab 
                                key={idx} 
                                value={idx} 
                                label={step} 
                                {...a11yProps(idx)}
                            />
                        ))}
                    </Tabs>
                </AppBar>

                <TabPanel value={index} index={0} dir={theme.direction}>
                    <Analytics selected={props.selected} classes={classes} />
                </TabPanel>

                <TabPanel value={index} index={1} dir={theme.direction}>
                    <SubscriptionViewer values={props.selected} classes={classes} />
                </TabPanel>

                <TabPanel value={index} index={2} dir={theme.direction}>
                    <SubscriptionRecap values={props.selected} classes={classes} />
                </TabPanel>
            </Grid>
        </Dialog>
    );
};
