import React from 'react';
import { Route, Link, withRouter } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { withStyles } from '@material-ui/core';
import ShieldLockOutline from 'mdi-material-ui/ShieldLockOutline';
import Info from './Info/Info';
import Events from './Events/Events';
import News from './News/News';
import MenuAuthorizer from '../../Components/Menu/MenuAuthorizer.ts';
import User from 'components/Authenticated/Auth/Components/User';
import ClubsContext from 'components/General/ClubsContext';
import scopes_mapping from 'configs/scopes_mapping';

const styles = theme => ({
    appBar: {
        marginBottom: theme.spacing(1),
    },
    title: {
        margin: theme.spacing(2),
    },
    link: {
        color: 'inherit !important',
    },
    leftIcon: {
        marginRight: theme.spacing(1),
    },
    tabPanel: {
        padding: theme.spacing(1, 0),
    },
});

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <span
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`club-tabpanel-${index}`}
            aria-labelledby={`club-tab-${index}`}
            {...other}
        >
            {children}
        </span>
    );
}

function a11yProps(index) {
    return {
        id: `club-tab-${index}`,
        'aria-controls': `club-tabpanel-${index}`,
    };
}

export default withRouter(withStyles(styles)(class extends React.Component {
    static contextType = ClubsContext;
    
    state = {
        index: 0,
        authorizer: new MenuAuthorizer(User.getData('scopes'), scopes_mapping(this.context)),
    }; 

    componentDidMount() {
        this.updateIndex();     
    }

    componentDidUpdate(prevProps) {
        if (prevProps.location.pathname !== this.props.location.pathname)
            this.updateIndex();
    };

    updateIndex = () => {   
        const match = this.props.location.pathname.split('/').pop();
        this.setState({
            index: [
                'edit',
                'events',
                'news'
            ].findIndex(x => x === match)
        });
    };

    render() {
        const { classes } = this.props;
        const { index, authorizer } = this.state;
        const name = this.props.match.params.name;

        const authorizations = [
            authorizer.isAuthorized(`/hub/dashboard/${name}/edit`),
            authorizer.isAuthorized(`/hub/dashboard/${name}/events`),
            authorizer.isAuthorized(`/hub/dashboard/${name}/news`)
        ];

        const routes = [
            authorizations[0] && <Route
                key="edit"
                path={`/hub/dashboard/${name}/edit`}
                render={props => <TabPanel key="edit" value={index} index={0}><Info {...props} name={name} /></TabPanel>}
            />,
            authorizations[1] && <Route
                key="events"
                path={`/hub/dashboard/${name}/events`}
                render={props => <TabPanel key="events" value={index} index={1}><Events {...props} name={name} /></TabPanel>}
            />,
            authorizations[2] && <Route
                key="news"
                path={`/hub/dashboard/${name}/news`}
                render={props => <TabPanel key="news" value={index} index={2}><News {...props} name={name} /></TabPanel>}
            />
        ];

        return (
            <>
                <AppBar className={classes.appBar} position="static" color="inherit" elevation={0}>
                    <Typography
                        className={classes.title}
                        variant="h4"
                        gutterBottom
                    >
                        { name.toUpperCase() }
                    </Typography>
                    <Tabs
                        value={index}
                        indicatorColor={!!+User.getPreferences('dark_mode') ? "secondary" : "primary"}
                        textColor="primary"
                        variant="fullWidth"
                        centered
                    >
                        <Tab
                            disabled={!authorizations[0]}
                            label={(
                                <Typography variant="body2" color={authorizations[0] ? "textPrimary" : "textSecondary"} component="div">
                                    <Box display="flex" alignContent="center" alignItems="center">
                                        {!authorizations[0] && <ShieldLockOutline className={classes.leftIcon} />}
                                        <div>Éditer</div>
                                    </Box>
                                </Typography>
                            )}
                            component={Link}
                            className={classes.link}
                            to={`/hub/dashboard/${name}/edit`}
                            {...a11yProps(0)}
                        />
                        <Tab
                            disabled={!authorizations[1]}
                            label={(
                                <Typography variant="body2" color={authorizations[1] ? "textPrimary" : "textSecondary"} component="div">
                                    <Box display="flex" alignContent="center" alignItems="center">
                                        {!authorizations[1] && <ShieldLockOutline className={classes.leftIcon} />}
                                        <div>Events</div>
                                    </Box>
                                </Typography>
                            )}
                            component={Link}
                            className={classes.link}
                            to={`/hub/dashboard/${name}/events`}
                            {...a11yProps(1)}
                        />
                        <Tab
                            disabled={!authorizations[2]}
                            label={(
                                <Typography variant="body2" color={authorizations[2] ? "textPrimary" : "textSecondary"} component="div">
                                    <Box display="flex" alignContent="center" alignItems="center">
                                        {!authorizations[2] && <ShieldLockOutline className={classes.leftIcon} />}
                                        <div>News</div>
                                    </Box>
                                </Typography>
                            )}
                            component={Link}
                            className={classes.link}
                            to={`/hub/dashboard/${name}/news`}
                            {...a11yProps(2)}
                        />
                    </Tabs>
                </AppBar>

                {routes.filter(Boolean)}
            </>
        );
    }
}));
