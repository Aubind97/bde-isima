import React from 'react';
import { dripForm } from 'react-drip-form';
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import InputAdornment from '@material-ui/core/InputAdornment';
import Divider from '@material-ui/core/Divider';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import { Field, HiddenField } from 'utils/forms';
import Update from 'mdi-material-ui/Update';
import { Accept, Image } from 'utils';

const styles = theme => ({
    root: {
        padding: theme.spacing(2),
        overflow: 'hidden',
        textAlign: 'left'
    },
    avatar: {
        width: 100,
        height: 100,
        marginRight: 'auto',
        marginLeft: 'auto',
    },
    divider: {
        margin: theme.spacing(1, 0, 1, 0)
    },
});

export default withMobileDialog()(withStyles(styles)(dripForm({
    validations: {
        name: {
            required: true,
            max: 255
        },
        email: {
            required: true,
            email: true,
            max: 255
        },
        facebook: {
            url: true,
            max: 255
        },
        twitter: {
            url: true,
            max: 255
        },
        description: {
            required: true,
            min: 10,
            max: 3000
        },
    },
    messages: {
        name: {
            required: 'Ce champ est requis.',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        email: {
            required: 'Ce champ est requis.',
            email: 'Ce n\'est pas un email valide.',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        facebook: {
            url: 'Ce n\'est pas une URL valide',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        twitter: {
            url: 'Ce n\'est pas une URL valide',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        description: {
            required: 'Ce champ est requis.',
            min: 'Ce champ doit contenir au moins 10 caractères.',
            max: 'Ce champ ne peut pas dépasser 3000 caractères.'
        }
    },
})(({
    handlers,
    meta: { invalid, pristine },
    submitting,
    isResolved,
    values,
    logo,
    ...props
}) => (
    <form className={props.classes.root} onSubmit={handlers.onSubmit}>
        <Grid container spacing={5} justify="center" component={Paper}>

            <Grid container item direction="column" xs={12}>

                <HiddenField
                    id="id"
                    name="id"
                />

                <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>Informations publiques</FormLabel>
                <Divider className={props.classes.divider} />
                <FormGroup style={{ margin: '4px 0px 4px 0px', padding: '0px 8px 0px 8px' }}>

                    <Field
                        type="text"
                        id="name"
                        name="name"
                        label="Nom"
                        disabled
                    />

                    <Field
                        type="email"
                        id="email"
                        name="email"
                        label="Adresse email"
                        disabled={!isResolved}
                    />

                    <Field
                        type="text"
                        id="description"
                        name="description"
                        label="Description"
                        endAdornment={(
                            <InputAdornment position="end">
                                <Typography color="textSecondary">{values.description && values.description.length}/3000</Typography>
                            </InputAdornment>
                        )}
                        fullWidth
                        multiline
                        rows={5}
                        disabled={!isResolved}
                    />

                </FormGroup>

                <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>Réseaux sociaux</FormLabel>
                <Divider className={props.classes.divider} />
                <FormGroup style={{ margin: '4px 0px 4px 0px', padding: '0px 8px 0px 8px' }}>
                    <Field
                        type="text"
                        id="facebook"
                        name="facebook"
                        label="Facebook"
                        disabled={!isResolved}
                    />

                    <Field
                        type="text"
                        id="twitter"
                        name="twitter"
                        label="Twitter"
                        disabled={!isResolved}
                    />

                    <Field
                        type="text"
                        id="instagram"
                        name="instagram"
                        label="Instagram"
                        disabled={!isResolved}
                    />
                </FormGroup>

                <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>Avatar</FormLabel>
                <Divider className={props.classes.divider} />
                <FormGroup style={{ margin: '4px 0px 4px 0px', padding: '0px 8px 0px 8px' }}>

                    <Avatar
                        className={props.classes.avatar}
                        src={Image.display(logo)}
                        alt={`Logo ${values.name}`}
                    />

                    <Accept onDrop={props.onDrop} onDelete={props.onDelete} files={props.files} progress={props.progress} />

                </FormGroup>

            </Grid>

            <Grid container item md={6} justify="center">
                <Button
                    type="submit"
                    startIcon={<Update />}
                    onClick={handlers.onSubmit}
                    disabled={invalid || pristine || submitting || !isResolved}
                    variant="contained"
                    color="primary"
                    size="large"
                    aria-label="Mettre à jour"
                >
                    {submitting ? <CircularProgress size={25} color="secondary" /> : "Mettre à jour"}
                </Button>
            </Grid>

        </Grid>
    </form>
))));