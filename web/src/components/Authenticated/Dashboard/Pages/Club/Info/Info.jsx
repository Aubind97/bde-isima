import React from 'react';
import { useLoads } from 'react-loads';
import { useSnackbar } from 'notistack';
import CircularProgress from '@material-ui/core/CircularProgress';
import InfoForm from './InfoForm'
import { CRUD, CompressionHelper } from 'utils';

export default function Info ({ name }) {

    /** States initialisation */
    const [progress, setProgress] = React.useState('');
    const [files, setFiles] = React.useState([]);
    const [submitting, setSubmitting] = React.useState(false);
    const [logo, setLogo] = React.useState(null);
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const form = React.useRef();

    const handleInitialize = fm => form.current = fm;

    const handleProgress = progress => setProgress(progress);

    const getClub = React.useCallback(() => CRUD.get({ url: `/clubs?name=${name}` }).then(result => {
        const club = result.data.pop();
        if (club) setLogo(club.logo);
        return club;
    }), [name]);
    const { response: club, update, isResolved, isPending, isRejected } = useLoads(getClub, {
        update: getClub,
    }); 

    const handleSubmit = newData => {
        setSubmitting(true);
        
        CRUD.add({
            url: `/clubs/${newData.id}`,
            newData: { ...newData, ...{ logo: files } },
            withFormData: true,
            onUploadProgress: handleProgress,
            onSucceed: () => {
                if(files[0]) setLogo(files[0].preview);
                resetState();
            },
            onRejection: resetState,
            props: { enqueueSnackbar, closeSnackbar },
        })
    };

    const onDelete = file => {
        files.splice(files.indexOf(file), 1);
        setFiles(files);
        setProgress('');
    };


    const resetState = () => {
        setProgress('');
        setFiles([]);
        setSubmitting(false);
    };

    React.useEffect(() => {
        update();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [name]);

    return (
        <>
            {(isPending || isRejected) && <CircularProgress size={25} color="inherit" />}

            {isResolved && (
                <InfoForm
                    values={club}
                    submitting={submitting}
                    isResolved={isResolved}
                    onInitialize={handleInitialize}
                    onValidSubmit={handleSubmit}
                    onDrop={CompressionHelper.onDrop({
                        handleOnStart: () => setProgress('Compression en cours ...'),
                        handleOnChange: files => {
                            setProgress('Compression terminée !');
                            setFiles(files);
                            form.current.updateDirty('name', true); //Choosing random field
                        }
                    })}
                    onDelete={onDelete}
                    progress={progress}
                    files={files}
                    logo={logo}
                />
            )}
        </>
    );
};
