import React from 'react';
import { withSnackbar } from 'notistack';
import MaterialTable from 'material-table';
import Slide from '@material-ui/core/Slide';
import NewsForm from './NewsForm';
import PlusCircleOutline from 'mdi-material-ui/PlusCircleOutline';
import PencilOutline from 'mdi-material-ui/PencilOutline';
import { tableProps, CRUD, DateUtils } from 'utils';
import { withMobileDialog, MuiThemeProvider, withTheme } from '@material-ui/core';
import { format } from 'date-fns';

export default withTheme(withMobileDialog()(withSnackbar(class News extends React.Component {
    state = {
        selected: {},
        isEditorOpen: false,
        submitting: false,
        promotions: [],
        flags: {
            publish_to_facebook: false,
            publish_by_email: false,
        },
        flags_params: {
            facebook_data: null,
        },
    };

    handleInitialize = form => this.form = form;

    responseFacebook = response => {
        if(response.status !== 'unknown') {
            this.setState({ flags_params: { ...this.state.flags_params, ...{ facebook_data: response } } })
        }
    };

    addNews = (newData, onSucceed) => CRUD.add({
        url: `/news`,
        newData: Object.assign(newData, { promotions: this.state.promotions.map(x => x.id) }),
        onSucceed,
        onRejection: this.resetState,
        props: this.props
    });

    updateNews = (newData, onSucceed) => CRUD.update({
        url: `/news/${newData.id}`,
        newData: Object.assign(newData, { promotions: this.state.promotions.map(x => x.id) }),
        onSucceed,
        onRejection: this.resetState,
        props: this.props
    });

    onPromotionsSelected = promotions => {
        this.setState({ promotions });
        this.onFieldChange();
    };

    onFlagsChange = name => event => {
        const { flags } = this.state;
        flags[name] = event.target.checked;
        this.setState({ flags });
    };

    handleSubmit = newData => () => {
        const { id, flags, flags_params } = this.state;

        this.setState({ submitting: true });

        (newData.id ? this.updateNews : this.addNews)(
            {
                ...Object.assign(newData, { 
                    is_published: true, 
                    club_id: id,
                    flags,
                    flags_params,
                }),
                ...{ published_at: DateUtils.toDatabaseDate(newData.published_at) }
            },
            () => this.setState({ selected: {}, isEditorOpen: false, submitting: false })
        );
    };

    handleSaveDraft = newData => () => {
        this.setState({ submitting: true });

        (newData.id ? this.updateNews : this.addNews)(
            {
                ...Object.assign(newData, {
                    is_published: false,                 
                    club_id: this.state.id,
                }),
                ...{ published_at: null }
            },
            result => this.setState({
                selected: Object.assign(newData, { id: newData.id ? result.data.updated_id : result.data.inserted_id }),
                submitting: false,
            })
        );
    };

    resetState = () => this.setState({ submitting: false });

    onFieldChange = () => {
        if (this.state.isEditorOpen)
            this.form.updateDirty("title", true);
    };

    onEditorOpen = () => this.setState({
        isEditorOpen: true,
        selected: {
            published_at: new Date(),
        }
    });

    onBackdrop = () => this.setState({ selected: {}, isEditorOpen: false });

    render() {
        const { fullScreen, theme } = this.props;
        const { selected, isEditorOpen, submitting, flags_params } = this.state;

        return (
            <>
                <Slide direction="right" in={!isEditorOpen} mountOnEnter unmountOnExit>
                    <div>
                        <MuiThemeProvider theme={tableProps.tableTheme(theme)}>
                            <MaterialTable
                                onChangeColumnHidden={tableProps.onChangeColumnHidden('news')}
                                onChangeRowsPerPage={tableProps.onChangeRowsPerPage('news')}
                                columns={[
                                    {
                                        title: 'Titre',
                                        field: 'title',
                                        render: rowData => `${!rowData.is_published ? '(BROUILLON) ' : ''} ${rowData.title.substring(0, 80)}${rowData.title.length > 80 ? '...' : ''}`,
                                        hidden: tableProps.isHidden(`news_title`)
                                    },
                                    {
                                        title: 'En-tête',
                                        field: 'header',
                                        render: rowData => `${!rowData.is_published ? '(BROUILLON) ' : ''} ${rowData.header.substring(0, 80)}${rowData.header.length > 80 ? '...' : ''}`,
                                        hidden: tableProps.isHidden(`news_header`)
                                    },
                                    {
                                        title: 'Audience',
                                        field: 'promotions',
                                        hidden: tableProps.isHidden(`news_promotions`),
                                        render: rowData => rowData.promotions.map(x => x.year).join(',')
                                    },
                                    {
                                        title: 'Date',
                                        field: 'published_at',
                                        render: rowData => rowData.published_at ? `${format(DateUtils.withOffsetDate(rowData.published_at), 'dd/MM/yyyy à HH:mm')}` : 'Non publié',
                                        hidden: tableProps.isHidden(`news_published_at`)
                                    }
                                ]}
                                data={query => (
                                    CRUD.get({
                                        url: `/clubs?name=${this.props.name}`
                                    }).then(result => {
                                        const club = result.data.pop();
                                        if (club) {
                                            this.setState({ id: club.id });
                                        }
                                        return CRUD.remote('/news', { 
                                            club_id: club ? club.id : undefined
                                        })(query);
                                    })
                                )}
                                title="News"
                                editable={{
                                    onRowDelete: oldData => CRUD.delete({
                                        url: `/news/${oldData.id}`,
                                        onSucceed: this.resetState,
                                        onRejection: this.resetState,
                                        props: this.props
                                    })
                                }}
                                actions={[
                                    {
                                        icon: React.forwardRef((props, ref) => <PencilOutline {...props} ref={ref} />),
                                        tooltip: 'Éditer',
                                        onClick: (event, rowData) => {
                                            if (!rowData.published_at)
                                                this.setState({ isEditorOpen: true, selected: { ...rowData, ...{ published_at: new Date() }} });
                                            else
                                                this.setState({ isEditorOpen: true, selected: rowData });
                                        }
                                    },
                                    {
                                        icon: React.forwardRef((props, ref) => <PlusCircleOutline {...props} ref={ref} />),
                                        tooltip: 'Ajouter',
                                        isFreeAction: true,
                                        onClick: () => {
                                            this.onEditorOpen();
                                        }
                                    }
                                ]}
                                icons={tableProps.tableIcons}
                                options={{
                                    ...tableProps.tableOptions, ...{
                                        exportButton: false,
                                        showTitle: !fullScreen,
                                        searchFieldAlignment: fullScreen ? "left" : "right",
                                        pageSize: tableProps.getPageSize('news')
                                    }
                                }}
                                localization={tableProps.tableLocalization}
                            />
                        </MuiThemeProvider>
                    </div>
                </Slide>

                <Slide direction="left" in={isEditorOpen} mountOnEnter unmountOnExit>
                    <NewsForm
                        values={selected}
                        submitting={submitting}
                        flags_params={flags_params}
                        onInitialize={this.handleInitialize}
                        onValidSubmit={this.handleSubmit}
                        onSave={this.handleSubmit}
                        onSaveDraft={this.handleSaveDraft}
                        onPromotionsSelected={this.onPromotionsSelected}
                        onBackdrop={this.onBackdrop}
                        onFieldChange={this.onFieldChange}
                        onFlagsChange={this.onFlagsChange}
                        onFacebookCallback={this.responseFacebook}
                    />
                </Slide>
            </>
        );
    };
})));
