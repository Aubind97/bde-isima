import React, { useState } from 'react';
import { dripForm } from 'react-drip-form';
import Button from "@material-ui/core/Button";
import { withStyles } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Paper from '@material-ui/core/Paper';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Grid from '@material-ui/core/Grid';
import ArrowLeft from 'mdi-material-ui/ArrowLeft';
import Facebook from 'mdi-material-ui/Facebook';
import Update from 'mdi-material-ui/Update';
import Publish from 'mdi-material-ui/Publish';
import ContentSaveOutline from 'mdi-material-ui/ContentSaveOutline';
import ReactQuill from 'react-quill';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { Field, SwitchField, DTPickerField, HiddenField, SelectMultiplePromotionFields } from 'utils/forms';
import './editor.snow.scss';

const styles = theme => ({
    root: {
        padding: theme.spacing(2),
        textAlign: 'left',
    },
    margin: {
        margin: theme.spacing(2),
    },
    divider: {
        margin: theme.spacing(1, 0),
    },
    facebook: {
        margin: theme.spacing(1, 0),
        backgroundColor: '#4267B2',
        '&:hover': {
            backgroundColor: '#304c82',
        },
    },
});

export default withStyles(styles)(dripForm({
    validations: {
        title: {
            required: true,
            max: 255
        },
        header: {
            required: true,
            max: 255
        },
    },
    messages: {
        title: {
            required: 'Ce champ est requis.',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
        header: {
            required: 'Ce champ est requis.',
            max: 'Ce champ ne peut pas dépasser 255 caractères.'
        },
    },
})(({
    handlers,
    meta: { invalid, pristine },
    values,
    submitting,
    flags_params,
    onSaveDraft,
    onFieldChange,
    onFlagsChange,
    onFacebookCallback,
    onPromotionsSelected,
    ...props
}) => {
    const [text, setText] = useState(values.content ? values.content : '');

    return (
        <Paper>
            <form className={props.classes.root}>
                <Grid container justify="center" direction="column">

                    <div style={{ textAlign: 'left' }}>
                        <Button
                            className={props.classes.margin}
                            startIcon={<ArrowLeft />}
                            variant="outlined"
                            aria-label="Retour"
                            onClick={props.onBackdrop}
                            color="inherit"
                        >
                            Retour
                    </Button>
                    </div>

                    <HiddenField
                        id="id"
                        name="id"
                    />

                    <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>Contenu de la publication</FormLabel>
                    <Divider className={props.classes.divider} />
                    <FormGroup style={{ margin: '4px 0px 4px 0px', padding: '0px 8px 0px 8px' }}>
                        <Field
                            id="title"
                            name="title"
                            label="Titre"
                        />

                        <Field
                            id="header"
                            name="header"
                            label="En-tête"
                        />

                        <ReactQuill
                            value={text}
                            onChange={value => {
                                setText(value);
                                onFieldChange();
                            }}
                            placeholder="Contenu de l'article"
                            modules={{
                                toolbar: [
                                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                                    ['blockquote', 'code-block'],

                                    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                                    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                                    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
                                    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
                                    [{ 'direction': 'rtl' }],                         // text direction

                                    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                                    [{ 'font': ['montserrat'] }],
                                    [{ 'align': [] }],

                                    ['link'],                                         // link formatting

                                    ['clean']                                         // remove formatting button
                                ],
                                clipboard: {
                                    matchVisual: false,
                                },
                            }}
                        />

                    </FormGroup>

                    <HiddenField
                        id="content"
                        name="content"
                        value={text}
                    />

                    <FormLabel style={{ fontWeight: 800, marginTop: 8 }}>Options de publication</FormLabel>
                    <Divider className={props.classes.divider} />
                    <FormGroup style={{ margin: '4px 0px 4px 0px', padding: '0px 8px 0px 8px' }}>

                        <SelectMultiplePromotionFields
                            promotions={values.promotions}
                            onPromotionsSelected={onPromotionsSelected}
                        />

                        {!values.is_published && (
                            <>

                                {(flags_params && flags_params.facebook_data) ? (
                                    <SwitchField
                                        id="publish_to_facebook"
                                        name="publish_to_facebook"
                                        value="publish_to_facebook"
                                        label="Publier sur Facebook"
                                        onChange={onFlagsChange('publish_to_facebook')}
                                    />
                                ) : (
                                        <div>
                                            <FacebookLogin
                                                language="fr_FR"
                                                appId="237417597136510"
                                                fields="name,email,picture"
                                                scope="publish_to_groups"
                                                callback={onFacebookCallback}
                                                render={renderProps => (
                                                    <Button
                                                        className={props.classes.facebook}
                                                        startIcon={<Facebook />}
                                                        onClick={renderProps.onClick}
                                                        disabled={renderProps.isDisabled || renderProps.isProcessing}
                                                        aria-label="Autoriser l'accès Facebook"
                                                        variant="contained"
                                                        color="primary"
                                                    >
                                                        {renderProps.isProcessing ? <CircularProgress size={25} color="secondary" /> : "Autoriser l'accès Facebook"}
                                                    </Button>
                                                )}
                                            />
                                        </div>
                                    )}

                                <SwitchField
                                    id="publish_by_email"
                                    name="publish_by_email"
                                    value="publish_by_email"
                                    label="Envoyer par email"
                                    onChange={onFlagsChange('publish_by_email')}
                                />

                                <DTPickerField
                                    id="published_at"
                                    name="published_at"
                                    label="Date de publication"
                                    clearLabel=""
                                />
                            </>
                        )}

                    </FormGroup>

                    <Grid container item xs={12} justify="center">

                        <ButtonGroup variant="contained" color="primary" size="large" aria-label="Actions de publication">
                            <Button
                                startIcon={values.is_published ? <Update /> : <Publish />}
                                onClick={props.onSave(values)}
                                disabled={invalid || pristine || submitting}
                                aria-label="Publier"
                            >
                                {submitting ? <CircularProgress size={25} color="secondary" /> : values.is_published ? 'Mettre à jour' : 'Publier'}
                            </Button>

                            {!values.is_published && (
                                <Button
                                    startIcon={<ContentSaveOutline />}
                                    onClick={onSaveDraft(values)}
                                    disabled={invalid || pristine || submitting}
                                    aria-label="Brouillon"
                                >
                                    {submitting ? <CircularProgress size={25} color="secondary" /> : 'Brouillon'}
                                </Button>
                            )}
                        </ButtonGroup>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    );
}
));