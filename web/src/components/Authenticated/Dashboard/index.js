import React from 'react';
import { Redirect, Switch } from 'react-router-dom';
import { withStyles, MuiThemeProvider } from '@material-ui/core';
import Hidden from '@material-ui/core/Hidden';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Menu from './Components/Menu/Menu';
import SearchBar from './Components/Search/SearchBar';
import AuthorizedRoute from './AuthorizedRoute';
import Splash from 'components/General/Splash';
import ListeuxContext from './ListeuxContext';
import ListeuxTheme from 'utils/listeux/ListeuxTheme';

const Planning = React.lazy(() => import('./Pages/BDE/Planning'));
const Market = React.lazy(() => import('./Pages/BDE/Market'));
const Events = React.lazy(() => import('./Pages/BDE/Events'))
const Promotions = React.lazy(() => import('./Pages/BDE/Promotions'))
const Clubs = React.lazy(() => import('./Pages/BDE/Clubs'));
const Partners = React.lazy(() => import('./Pages/BDE/Partners'));
const Members = React.lazy(() => import('./Pages/BDE/Members/Members'));
const Permissions = React.lazy(() => import('./Pages/BDE/Permissions'));
const Club = React.lazy(() => import('./Pages/Club'));
const Analytics = React.lazy(() => import('./Pages/BDE/Analytics/Analytics'));

const styles = theme => ({
    root: {
        padding: theme.spacing(2),
        margin: theme.spacing(2, 2),
        textAlign: 'center'
    },
    menu: {
        overflowX: 'hidden',
        [theme.breakpoints.up('lg')]: {
            marginLeft: 100,
        }
    }
});

export default withStyles(styles, { withTheme: true })(class extends React.Component {

    static contextType = ListeuxContext;

    render() {
        const { classes, theme } = this.props;

        return (
            <MuiThemeProvider theme={this.context ? ListeuxTheme : theme}>
                <Box display="flex" flexGrow={1} flexDirection="column">
                    <Hidden mdDown>
                        <Menu />
                    </Hidden>
                    <Box
                        className={classes.menu}
                        display="flex"
                        alignItems="center"
                        flexDirection="column"
                    >
                        <SearchBar />
                        <Container
                            className={classes.root}
                            maxWidth="xl"
                        >
                            <React.Suspense fallback={<Splash />}>
                                <Switch>
                                    <AuthorizedRoute
                                        path="/hub/dashboard/planning"
                                        component={Planning}
                                    />
                                    <AuthorizedRoute
                                        exact
                                        path="/hub/dashboard/market"
                                        component={Market}
                                    />
                                    <AuthorizedRoute
                                        exact
                                        path="/hub/dashboard/events"
                                        component={Events}
                                    />
                                    <AuthorizedRoute
                                        exact
                                        path="/hub/dashboard/promotions"
                                        component={Promotions}
                                    />
                                    <AuthorizedRoute
                                        exact
                                        path="/hub/dashboard/clubs"
                                        component={Clubs}
                                    />
                                    <AuthorizedRoute
                                        exact
                                        path="/hub/dashboard/partners"
                                        component={Partners}
                                    />
                                    <AuthorizedRoute
                                        exact
                                        path="/hub/dashboard/members"
                                        component={Members}
                                    />
                                    <AuthorizedRoute
                                        exact
                                        path="/hub/dashboard/members/:id"
                                        component={Permissions}
                                    />
                                    <AuthorizedRoute
                                        path="/hub/dashboard/:name/:module"
                                        component={Club}
                                    />
                                    <AuthorizedRoute
                                        path="/hub/dashboard/analytics"
                                        component={Analytics}
                                    />
                                    <Redirect to="/hub" />
                                </Switch>
                            </React.Suspense>
                        </Container>
                    </Box>
                </Box>
            </MuiThemeProvider>
        ); 
    }
});
