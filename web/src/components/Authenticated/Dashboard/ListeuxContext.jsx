import React from 'react';

/**
 * This component is responsible for passing isListeux through the whole dashboard
 */
const ListeuxContext = React.createContext({});

export const ListeuxProvider = ListeuxContext.Provider;
export const ListeuxConsumer = ListeuxContext.Consumer;
export default ListeuxContext;
