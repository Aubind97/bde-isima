import React, { useEffect } from 'react';
import MessengerCustomerChat from 'react-messenger-customer-chat';

export default function Messenger({ children }) {

    //On unmount, cleans up the messenger-chat from the DOM
    useEffect(() => () => document.getElementById('fb-root').remove());

    return (
        <>
            {children}

            <MessengerCustomerChat
                pageId="913191802071318"
                appId="237417597136510"
                htmlRef="fb-msgr"
                language="fr_FR"
            />
        </>
    );
};
