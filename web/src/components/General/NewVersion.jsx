import React from 'react';
import { makeStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(() => ({
    appBar: {
        borderRadius: 5,
        zIndex: 9999999999,
    },
}));

/**
 * This component is responsible for prompting the user to update the service worker/website version
 */
export default function NewVersion(props) {

    /** States initialisation */
    const classes = useStyles();
    const { enqueueSnackbar } = useSnackbar();

    const reload = () => window.location.reload();

    const displaySnack = () => {
        enqueueSnackbar('Une nouvelle version est disponible', {
            persist: true,
            preventDuplicate: true,
            anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'left',
            },
            content: () => (
                <AppBar className={classes.appBar} position="static">
                    <Toolbar>
                        <Grid container>
                            <Grid container item xs={8} justify="center" alignItems="center">
                                <Typography variant="caption">
                                    Une nouvelle version est disponible
                                </Typography>
                            </Grid>
                            <Grid container item xs={4} justify="flex-end" alignItems="center">
                                <Button
                                    variant="outlined"
                                    aria-label="Installer"
                                    color="secondary"
                                    onClick={reload}
                                >
                                    Installer
                                </Button>
                            </Grid>
                        </Grid>
                    </Toolbar>
                </AppBar>
            ),
        });       
    };

    /**
    * When component did mount, mount notification but hide it until the service worker triggers it 
    */
    React.useEffect(() => {    

        /**
        * Subscribe to onVersionUpdate
        */
        window.addEventListener('onVersionUpdate', displaySnack);

        /**
        * Unsubscribe from onVersionUpdate
        */
        return function cleanup() {
            window.removeEventListener('onVersionUpdate', displaySnack);
        };
    });

    return props.children;
};
