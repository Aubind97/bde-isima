import React from 'react';
import { withRouter } from 'react-router';

export default withRouter(function ScrollToTop ({ children, location: { pathname } }) {
    React.useEffect(() => {
        window.scrollTo(0, 0);
    }, [pathname]);

    return children || null;
});
