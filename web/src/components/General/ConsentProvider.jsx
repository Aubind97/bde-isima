import React from 'react';
import { makeStyles } from '@material-ui/core';
import CookieConsent from "react-cookie-consent";
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
    container: {
        background: `${theme.palette.background.paper} !important`,
        color: `${theme.palette.text.primary} !important`,
        padding: theme.spacing(1),
        boxShadow: '0 4px 8px 0 rgba(0,0,0,1)'
    },
    button: {
        marginLeft: 'auto', 
        marginRight: 'auto'
    }
}));

/**
 * This component is responsible for asking user consent about the use of cookies
 * @see (https://www.cookielaw.org/)
 */
export default function ConsentProvider(props) {

    /** States initialisation */
    const classes = useStyles();

    /**
    * Functional component to override default CookieConsent button
    * @param {Object} props properties for the button provided by the Cookie Consent API
    */
    function AgreeButton(props) {
        return (
            <Button className={classes.button} variant="contained" onClick={props.onClick} color="primary">
                {props.children}
            </Button>
        );
    };

    return (
        <>
            {props.children}

            <CookieConsent
                buttonText="Je comprends"
                ButtonComponent={AgreeButton}
                containerClasses={classes.container}
                acceptOnScroll
            >
                Ce site utilise des cookies pour améliorer l'expérience utilisateur.
            </CookieConsent>
        </>
    );
};
