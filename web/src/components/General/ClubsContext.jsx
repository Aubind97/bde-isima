import React from 'react';

/**
 * This component is responsible for passing clubs data through the whole application
 */
const ClubsContext = React.createContext({});

export const ClubsProvider = ClubsContext.Provider;
export const ClubsConsumer = ClubsContext.Consumer;
export default ClubsContext;
