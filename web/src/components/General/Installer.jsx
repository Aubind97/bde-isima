import React from 'react';
import { makeStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Close from 'mdi-material-ui/Close';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(() => ({
    appBar: {
        borderRadius: 5,
        zIndex: 9999999999,
    }
}));

/** The event has to be stored out of React state context, otherwise it's always null. */
let deferred = null;

/**
 * This component is responsible for prompting the user to install the Progressive Web App (PWA)
 * @see (https://developers.google.com/web/fundamentals/app-install-banners/promoting-install-mobile)
 */
export default function Installer(props) {

    /** States initialisation */
    const classes = useStyles();
    const [key, setKey] = React.useState(null);
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    /**
    * When component did mount, listen for beforeinstallprompt event
    * @see (https://developers.google.com/web/fundamentals/app-install-banners/) to know when this event is fired by the browser
    */
    React.useEffect(() => {

        /**
        * Subscribe to beforeinstallprompt
        */
        window.addEventListener('beforeinstallprompt', handleShow);

        /**
        * Dismiss the prompt and won't ask the user again for 15 days
        * @param {string} key The notistack key to be dismissed
        */
        function refuseInstall(key) {
            closeSnackbar(key);
            localStorage.setItem('promptInstall', new Date().setDate(new Date().getDate() + 15));
        };

        /**
        * Determines if the banner should be prompted to the user when it receives beforeinstallprompt
        * @param {event} event the beforeinstallprompt event
        */
        function handleShow(event) {
            event.preventDefault();
            deferred = event;

            const promptInstall = localStorage.getItem('promptInstall');
            if (!promptInstall || new Date(promptInstall) < new Date())
                triggerSnackbar();
        };

        /**
        * Trigger the original beforeinstallprompt as the user clicks on install
        * @param {event} event the click event
        */
        function triggerDeferred(event) {
            if (deferred) {
                deferred.prompt();

                deferred.userChoice
                    .then(choiceResult => {
                        if (choiceResult.outcome === 'accepted') {
                            refuseInstall(key);
                            deferred = null;
                        }
                    });
            }
        };

        /**
        * Make the install prompt visible, contains the install banner component
        */
        function triggerSnackbar() {
            setKey(enqueueSnackbar('Installer notre app progressive. Légère et rapide !', {
                persist: true,
                preventDuplicate: true,
                anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'left',
                },
                content: key => (
                    <AppBar className={classes.appBar} position="static">
                        <Toolbar>
                            <Grid container>
                                <Grid container item xs={2} justify="flex-start" alignItems="center">
                                    <IconButton aria-label="Fermer" color="secondary" onClick={() => refuseInstall(key)}>
                                        <Close />
                                    </IconButton>
                                </Grid>
                                <Grid container item xs={6} justify="center" alignItems="center">
                                    <Typography variant="caption">
                                        Installer notre app progressive. Légère et rapide !
                                    </Typography>
                                </Grid>
                                <Grid container item xs={4} justify="flex-end" alignItems="center">
                                    <Button
                                        variant="outlined"
                                        aria-label="Installer"
                                        color="secondary"
                                        onClick={triggerDeferred}
                                    >
                                        Installer
                                    </Button>
                                </Grid>
                            </Grid>
                        </Toolbar>
                    </AppBar>
                ),
            }));
        };

        /**
        * Unsubscribe from beforeinstallprompt
        */
        return function cleanup() {
            window.removeEventListener('beforeinstallprompt', handleShow);
        };
    });

    return props.children;
};
