import React from 'react';
import Particles from 'react-particles-js';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    container: {
        position: 'fixed',
        width: '100vw',
        height: '100vh',
        zIndex: -1,
    },
}));

//Slightly modified to begin from Listeux campaign start date 
function isWinter(date) {
    const m = date.getMonth();
    return m === 10 ? date.getDate() >= 15 : m === 2 ? date.getDate() < 21 : m < 2;
}

export default ({ children }) => {

    /** States initialisation */
    const classes = useStyles();

    return (
        <>
            {isWinter(new Date()) && (
                <Particles
                    className={classes.container}
                    params={{
                        "particles": {
                            "number": {
                                "value": 500,
                                "density": {
                                    "enable": false,
                                    "value_area": 200
                                }
                            },
                            "color": {
                                "value": "#dadde3"
                            },
                            "opacity": {
                                "value": 0.5,
                                "random": true,
                                "anim": {
                                    "enable": false,
                                    "speed": 1,
                                    "opacity_min": 0.1,
                                    "sync": false
                                }
                            },
                            "size": {
                                "value": 7,
                                "random": true,
                                "anim": {
                                    "enable": false,
                                    "speed": 40,
                                    "size_min": 0.1,
                                    "sync": false
                                }
                            },
                            "line_linked": {
                                "enable": false,
                                "distance": 500,
                                "color": "#222222",
                                "opacity": 0.4,
                                "width": 2
                            },
                            "move": {
                                "enable": true,
                                "speed": 3,
                                "direction": "bottom",
                                "random": false,
                                "straight": false,
                                "out_mode": "out",
                                "bounce": false,
                                "attract": {
                                    "enable": false,
                                    "rotateX": 600,
                                    "rotateY": 1200
                                }
                            }
                        },
                    }}
                />
            )}

            {children}
        </>
    );
}