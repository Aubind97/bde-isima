import React from 'react';
import { makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
    avatar: {
        width: '25vh',
        height: 'auto',
        margin: theme.spacing(2),
        [theme.breakpoints.down('md')]: {
            width: '30vw',
        },
    }
}));

/**
 * This component is responsible for showing Splash Screen while lazy loading components
 */
export default function Splash() {

    /** States initialisation */
    const classes = useStyles();

    return (
        <Box
            display="flex"
            alignItems="center"
            justifyContent="center"
            flexDirection="column"
            flexGrow={1}
        >
            <Avatar
                className={classes.avatar}
                src="/images/favicons/android-chrome-512x512.png"
                alt="Logo BDE ISIMA"
            />
            <CircularProgress color="inherit" />
        </Box>
    );
};
