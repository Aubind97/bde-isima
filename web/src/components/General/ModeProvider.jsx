import React from 'react';
import User from 'components/Authenticated/Auth/Components/User';

/**
 * This component is responsible for handling switch between Light and Dark theme
 * @see (https://material.io/design/color/dark-theme.html)
 */
export default function ModeProvider(props) {

    /** States initialisation */
    const [type, setType] = React.useState(!!+User.getPreferences('dark_mode') ? 'dark' : 'light');

    /**
    * State updater
    * @param {event} event the dispatched event
    */
    function onTypeChange(event) {
        setType(event.isDark ? 'dark' : 'light');
    }

    /**
    * Will listen for user preferences changes emitted on settings changes (Settings.jsx)
    */
    React.useEffect(() => {

        /**
        * Subscribe to user preferences changes emitted on settings changes (Settings.jsx)
        */
        window.addEventListener('onThemeChange', onTypeChange);

        /**
        * Unsubscribe from user preferences changes emitted on settings changes (Settings.jsx)
        */
        return function cleanup() {
            window.removeEventListener('onThemeChange', onTypeChange);
        };
    }, []);

    return props.children({ type });
}