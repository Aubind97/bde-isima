import React from 'react';
import { ShakeSlow } from 'reshake';
import { useSnackbar } from 'notistack';
import BellRing from 'mdi-material-ui/BellRing';
import Fab from '@material-ui/core/Fab';
import User from 'components/Authenticated/Auth/Components/User';
import { CRUD } from 'utils';

function urlBase64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

/**
 * This component is responsible for asking push notification permission to the user
 * @see (https://developers.google.com/web/ilt/pwa/introduction-to-push-notifications#push_api)
 */
export default function PushNotificationsManager(props) {

    /** States initialisation */
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    
    React.useEffect(() => {

        /**
        * Update user data with the new push endpoint
        */
        function updateUser(sub) {

            const key = sub.getKey('p256dh')
            const token = sub.getKey('auth')
            const contentEncoding = (PushManager.supportedContentEncodings || ['aesgcm'])[0]

            const data = {
                endpoint: sub.endpoint,
                publicKey: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
                authToken: token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null,
                contentEncoding
            }

            CRUD.update({
                url: '/push/store',
                newData: data,
                quietSucceed: true,
                quietRejection: true,
            });
        }
        
        /**
        * Ask the user for push notifications permission
        */
        const subscribeUser = key => () => {
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.ready.then(function (reg) {

                    reg.pushManager.subscribe({
                        userVisibleOnly: true,
                        applicationServerKey: urlBase64ToUint8Array('BFL01Ne_U6JKdEc1OUpGVgBWNPhzPYeghDK0ruxyyXVv8ELZkC-uvSipf3Eq0bE7npxlWOxbLz3SMlIARtBVyh4')
                    })
                    .then(sub => {
                        updateUser(sub);
                        closeSnackbar(key);
                    })
                    .catch(e => {
                        if (Notification.permission === 'denied') {
                            console.warn('Permission for notifications was denied');
                        } else {
                            console.error('Unable to subscribe to push', e);
                        }
                        closeSnackbar(key);
                    });

                })
            }
        }

        /**
        * Prompt the user to push notification authorization
        */
        function subscribeToPush() {
            if (User.hasActiveSession() && 'serviceWorker' in navigator) {
                navigator.serviceWorker.ready.then(function (reg) {
                    reg.pushManager.getSubscription().then(function (sub) {
                        if (sub === null) {
                            enqueueSnackbar('', {
                                persist: true,
                                preventDuplicate: true,
                                anchorOrigin: {
                                    vertical: 'bottom',
                                    horizontal: 'left'
                                },
                                content: key => (
                                    <Fab aria-label="add" onClick={subscribeUser(key)} color="primary">
                                        <ShakeSlow
                                            int={30}
                                            fixed={true}
                                            fixedStop={false}
                                            freez={false}
                                        >
                                            <BellRing />
                                        </ShakeSlow>
                                    </Fab>
                                ),
                            });
                        }
                    });
                })
                .catch(function (err) {
                    console.log('Service Worker registration failed: ', err);
                });
            }
        }

        /**
        * Subscribe to push notifications
        */
        subscribeToPush();
    }, [props.location, enqueueSnackbar, closeSnackbar]);

    return props.children;
};
