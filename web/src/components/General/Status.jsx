import React from 'react';
import { useSnackbar } from 'notistack';

/**
 * This component is responsible for prompting logged-in status to the user (Online/Offline)
 * @see (https://developer.mozilla.org/en-US/docs/Web/API/NavigatorOnLine/Online_and_offline_events)
 */
export default function Offline(props) {

    /** States initialisation */
    const [key, setKey] = React.useState(null);
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    /**
    * Shows an offline snackbar
    */
    function showOffline() {
        setKey(enqueueSnackbar('Vous êtes déconnecté.', {
            transitionDuration: 300,
            persist: true,
            variant: 'error',
            anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'right'
            }
        }));
    }

    /**
    * Shows an online snackbar
    */
    function showOnline() {
        closeSnackbar(key);
        enqueueSnackbar('Vous êtes en ligne !', {
            transitionDuration: 300,
            autoHideDuration: 1000,
            variant: 'success',
            anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'right'
            }
        });
    }

    
    React.useEffect(() => {

        /**
        * Subscribe to online/offline browser events
        */
        window.addEventListener('offline', showOffline);
        window.addEventListener('online', showOnline);

        /**
        * Unsubscribe from online/offline browser events
        */
        return function cleanup() {
            window.removeEventListener('offline', showOffline);
            window.removeEventListener('online', showOnline);
        }
    });

    return props.children;
};
