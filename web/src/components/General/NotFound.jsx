import React from 'react';
import { makeStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import ArrowLeft from 'mdi-material-ui/ArrowLeft';
import Fab from '@material-ui/core/Fab';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
    extendedIcon: {
        marginRight: theme.spacing(1),
    }
}));

/**
 * This component is the 404 page (URL Not Found)
 */
export default function NotFound() {

    /** States initialisation */
    const classes = useStyles();

    /**
    * Pulse animations is pulled from _animations.scss
    */
    return (
        <Box display="flex" alignItems="center" justifyContent="center" flexDirection="column" flexGrow={1}>
            <span className="pulse" />

            <img
                src="/images/illustrations/NotFound.svg"
                height="auto"
                width="50%"
                alt="Page non trouvée"
            />

            <Typography variant="h3" paragraph>
                Page non trouvée
            </Typography>

            <Typography variant="h5" paragraph>
                T'es pas là pour être ici !
            </Typography>

            <Fab variant="extended" aria-label="Retour à l'accueil" component={Link} to='/' color="primary">
                <ArrowLeft className={classes.extendedIcon} color="secondary" />
                <Typography variant="subtitle2" color="secondary">
                    Revenir à l'accueil
                </Typography>
            </Fab>
        </Box>
    );
};
