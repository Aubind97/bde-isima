import React from 'react';
import format from 'date-fns/format';
import frLocale from 'date-fns/locale/fr';
import DateFnsUtils from '@date-io/date-fns';
import { MuiThemeProvider, createMuiTheme, withTheme } from "@material-ui/core";
import { DateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateUtils from './DateUtils';

class LocalizedUtils extends DateFnsUtils {

  getDatePickerHeaderText(date) {
    return format(date, 'dd/MM/yyyy HH:mm', { locale: this.locale });
  };
};

export default withTheme(class DTPicker extends React.Component {
  render() {
    const { label, value, handleChange, onClear, fullWidth, theme, ...rest } = this.props;

    const materialTheme = createMuiTheme({
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary
        }
      },
    });

    return (
      <MuiThemeProvider theme={materialTheme}>
        <MuiPickersUtilsProvider utils={LocalizedUtils} locale={frLocale}>
          <DateTimePicker
              clearable
              label={label}
              value={DateUtils.withOffsetDate(value)}
              format="dd/MM/yyyy HH:mm"
              clearLabel="vider"
              cancelLabel="annuler"
              invalidDateMessage="Format de date invalide"
              onChange={handleChange}
              onClear={onClear}
              fullWidth={fullWidth}
              autoOk
              ampm={false}
              {...rest}
          />
        </MuiPickersUtilsProvider>
      </MuiThemeProvider>
    );
  }
});
