import React from 'react';
import format from 'date-fns/format';
import frLocale from 'date-fns/locale/fr';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { MuiThemeProvider, createMuiTheme, withStyles } from "@material-ui/core";
import DateUtils from './DateUtils';

class LocalizedUtils extends DateFnsUtils {
  getDatePickerHeaderText(date) {
    return format(date, 'dd/MM/yyyy', { locale: this.locale });
  };
};

export default withStyles({}, { withTheme: true })(class Picker extends React.Component {
  render() {
    const { label, date, handleChange, onClear, fullWidth, theme, ...rest } = this.props;

    const materialTheme = createMuiTheme({ 
      ...theme,
      palette: {
        ...theme.palette,
        primary: {
          main: theme.palette.text.primary
        }
      },
    });

    return (
      <MuiThemeProvider theme={materialTheme}>
        <MuiPickersUtilsProvider utils={LocalizedUtils} locale={frLocale}>
          <div style={{ marginTop: 8, marginBottom: 4 }}>
            <DatePicker
              clearable
              label={label}
              value={DateUtils.withOffsetDate(date)}
              format="dd/MM/yyyy"
              clearLabel="vider"
              cancelLabel="annuler"
              onChange={handleChange}
              onClear={onClear}
              fullWidth={fullWidth}
              autoOk
              {...rest}
            />
          </div>
        </MuiPickersUtilsProvider>
      </MuiThemeProvider>
    );
  }
});
