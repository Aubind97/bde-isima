import moment from 'moment-timezone';

function withOffsetDate(date) {
    return moment.parseZone(moment.tz(date, moment.tz.guess())).toDate();
}
function toDatabaseDate(date) {
    return moment(date).utc().format("YYYY-MM-DD HH:mm:ss");
}

export default { withOffsetDate, toDatabaseDate };