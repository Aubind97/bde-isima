import React from 'react';
import axios from 'axios';
import { useLoads } from 'react-loads';
import AvatarStack from './AvatarStack';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import { useTheme, useMediaQuery } from '@material-ui/core';
import LazyLoad from 'react-lazyload';
import { CRUD } from 'utils';
import UserAvatar from './UserAvatar';
import Skeleton from '@material-ui/lab/Skeleton';

/**
* This component is responsible for displaying users avatars as a stack
*/
export default React.memo(function AvatarGroup({ event_id }) {

    /** States initialisation */
    const theme = useTheme();
    const [isOpen, setIsOpen] = React.useState(false);
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const handleDialogOpen = isOpen => () => {
        setIsOpen(isOpen);
    };

    const getUsers = React.useCallback(() => 
        CRUD.get({ url: `/subscriptions/event/all/${event_id}` })
            .then(res => axios.all(res.data.map(subscription => CRUD.get({ url: `/users/compact/${subscription.user_id}` }))))
    , [event_id]); 
    const { response: users, isResolved } = useLoads(getUsers);

    return (
        <>
            <AvatarStack maxAvatarNumber={fullScreen ? 0 : 2} onNumberLeftClick={handleDialogOpen(true)}>

                {isResolved && users.map((user, idx) => (
                    <UserAvatar
                        key={idx}
                        data={user.data}
                        AvatarProps={{
                            alt: "Avatar utilisateur"
                        }}
                    />
                ))}
            </AvatarStack>

            <Dialog
                open={isOpen}
                fullScreen={fullScreen}
                onClose={handleDialogOpen(false)}
                aria-labelledby="attendents-dialog"
            >
                <DialogTitle id="attendents-dialog">Liste des participants</DialogTitle>
                <DialogContent>
                    <List dense>
                        {isResolved && users.map((user, idx) => (
                            <ListItem key={idx}>
                                <ListItemIcon>
                                    <LazyLoad
                                        height={40}
                                        offset={40}
                                        scroll
                                        overflow
                                        once
                                        placeholder={<Skeleton variant="circle" width={40} height={40} />}
                                    >
                                        <UserAvatar
                                            key={idx}
                                            data={user.data}
                                            AvatarProps={{
                                                alt: "Avatar utilisateur"
                                            }}
                                        />
                                    </LazyLoad>
                                </ListItemIcon>
                                <ListItemText primary={`${user.data.card} - ${user.data.firstname} ${user.data.lastname} ${user.data.nickname ? `(${user.data.nickname})` : ''}`} />
                            </ListItem>
                        ))}
                    </List>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDialogOpen(false)} color="inherit">
                        Fermer
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}); 
