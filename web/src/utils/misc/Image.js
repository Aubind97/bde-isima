class Image {

    display = url => {     
        if(!url)
            return '//:0'; // Port 0 is invalid so no request will be made 
            
        if (url.startsWith('blob'))
            return url;

        if (url.startsWith('/'))
            return window.location.origin + url;

        return (window.location.origin + '/' + url).replace('3000', '8000')
    };
};

export default new Image();