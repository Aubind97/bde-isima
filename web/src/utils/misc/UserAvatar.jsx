import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Skeleton from '@material-ui/lab/Skeleton';
import User from 'components/Authenticated/Auth/Components/User';
import { Image } from 'utils';

const useStyles = makeStyles(() => ({
    avatar: {
        backgroundColor: '#2A2E43 !important',
    },
    typography: {
        color: '#fff',
    },
}));

/**
* This component is responsible for displaying users avatars (or their initials if they don't have one)
*/
export default function UserAvatar({ data, username, AvatarProps }) {

    const { className: avatarClassName, ...avatarProps } = AvatarProps ? AvatarProps : {};

    /** States initialisation */
    const classes = useStyles();
    const [userData, setUserData] = React.useState(data ? data : User.getAllData());

    /**
    * On user data change handler: Updates the state
    */
    function handleOnUserChange() {
        setUserData(User.getAllData());
    };

    /**
    * Returns the user's username
    */
    function getUsername() {
        return userData.nickname ? userData.nickname.slice(0, 2) : `${userData.firstname[0]}${userData.lastname[0]}`;
    };

    /**
    * When component did mount, subscribes for User data changes
    */
    React.useEffect(() => {
        if(data) setUserData(data);

        window.addEventListener('onUserChange', handleOnUserChange);
        
        /**
         * When component will unmount, cancels the subscription
         */
        return function cleanup() {
            window.removeEventListener('onUserChange', handleOnUserChange);
        }
    }, [data]);

    return (
        userData ? (
            userData.picture ? (
                <Avatar
                    className={avatarClassName}
                    src={Image.display(userData.picture)}
                    {...avatarProps}
                />
            ) : (
                <Avatar
                    className={classnames(classes.avatar, avatarClassName)}
                    {...avatarProps}
                >
                    <Typography className={classes.typography} variant="subtitle1">
                        {username ? username.slice(0, 2) : getUsername()} 
                    </Typography>
                </Avatar>
            )
        ) : (
            <Skeleton variant="circle" />
        )
    );
};

UserAvatar.propTypes = {
    /** Optional default user data */
    data: PropTypes.object,
    /** Optional default username */
    username: PropTypes.string,
    /** Props which will be forwarded to the avatar component */
    AvatarProps: PropTypes.object,
};

