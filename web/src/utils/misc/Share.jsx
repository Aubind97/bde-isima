import React from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ShareIcon from 'mdi-material-ui/ShareVariant';
import Check from 'mdi-material-ui/Check';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import Box from '@material-ui/core/Box';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
    mini: {
        color: '#CECFD0',
        width: 20,
        height: 20
    }
})

export default function Share(props) {
    const [open, setOpen] = React.useState(false);
    const classes = useStyles();
    const { link, ...other } = props;

    function handleTooltipClose() {
        setOpen(false);
    }

    function handleTooltipOpen() {
        if (link)
            setOpen(true);
    }

    function handleClick(event) {
        event.stopPropagation();
    }

    return (
        <ClickAwayListener onClickAway={handleTooltipClose}>
            <div onClick={handleClick}>
                <CopyToClipboard onCopy={handleTooltipOpen} text={link}>
                    <Tooltip
                        PopperProps={{
                            disablePortal: true
                        }}
                        TransitionComponent={Zoom}
                        onClose={handleTooltipClose}
                        open={open}
                        disableFocusListener
                        disableHoverListener
                        disableTouchListener
                        title={(
                            <Box display="flex" alignItems="center" >
                                <Check className={classes.mini} />
                                <Typography variant="caption">Lien copié !</Typography>
                            </Box>
                        )}
                    >
                        <IconButton aria-label="Partager" {...other}>
                            <ShareIcon />
                        </IconButton>
                    </Tooltip>
                </CopyToClipboard>
            </div>
        </ClickAwayListener>
    );
}

