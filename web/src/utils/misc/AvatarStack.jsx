import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    layout: {
        display: 'flex',
        flexShrink: 0,
        '&:not(:first-of-type)': {
            marginRight: -theme.spacing(3),
        },
        minHeight: 56,
        margin: theme.spacing(1),
    },
    avatar: {
        marginRight: -theme.spacing(1),
    },
    avatarPlaceholder: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2A2E43',
        color: '#fff',
        width: 40,
        height: 40,
        borderRadius: '50%',
        userSelect: 'none',
    },
    noAttendant: {
        width: 50,
    },
}));

export default function AvatarStack ({ children = [], maxAvatarNumber = 9, onNumberLeftClick = () => { } }) {

    /** States initialisation */
    const classes = useStyles();

    return (
        <div className={classes.layout}>
            {Array.isArray(children)
                ? children.slice(0, maxAvatarNumber)
                    .map((child, idx) => (
                        <div key={idx} className={classes.avatar}>
                            {child}
                        </div>
                    ))
                : (
                <div className={classes.avatar}>
                    {children}
                </div>
            )}

            {(children.length > maxAvatarNumber || children.length === 0) && (
                <div className={classes.avatar}>
                    <div className={classes.avatarPlaceholder} onClick={children.length > 0 ? onNumberLeftClick : undefined}>
                        {children.length === 0 ? 0 : `+${children.length - maxAvatarNumber}`}
                    </div>
                </div>
            )}
        </div>
    );
};

AvatarStack.propTypes = {
    children: PropTypes.node,
    maxAvatarNumber: PropTypes.number,
    onNumberLeftClick: PropTypes.func,
};