import React, { useRef } from 'react';

export default function useInterval(callback, delay) {
    const savedCallback = useRef();

    // Remember last callback function
    React.useEffect(() => {
        savedCallback.current = callback;
    });

    // Configure interval
    React.useEffect(() => {
        function tick() {
            savedCallback.current();
        }
        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
}