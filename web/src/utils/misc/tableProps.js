import React from 'react';
import PlusCircleOutline from 'mdi-material-ui/PlusCircleOutline';
import Check from 'mdi-material-ui/Check';
import Close from 'mdi-material-ui/Close';
import DeleteOutline from 'mdi-material-ui/DeleteOutline';
import ChevronRight from 'mdi-material-ui/ChevronRight';
import ChevronLeft from 'mdi-material-ui/ChevronLeft';
import PencilOutline from 'mdi-material-ui/PencilOutline';
import Magnify from 'mdi-material-ui/Magnify';
import ArrowUp from 'mdi-material-ui/ArrowUp';
import Minus from 'mdi-material-ui/Minus';
import FilterVariant from 'mdi-material-ui/FilterVariant';
import ContentSaveMoveOutline from 'mdi-material-ui/ContentSaveMoveOutline';
import ViewColumn from 'mdi-material-ui/ViewColumn';
import PageFirst from 'mdi-material-ui/PageFirst';
import PageLast from 'mdi-material-ui/PageLast';
import { CsvBuilder } from 'filefy';
import { CRUD } from 'utils';
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core';

export default {
    tableTheme: theme => responsiveFontSizes(createMuiTheme({
        ...theme,
        palette: {
            ...theme.palette,
            secondary: {
                main: "#222222",
            },
            text: {
                secondary: {
                    main: '#fff'
                }
            }
        },
    })),
    tableIcons : {
        Add: React.forwardRef((props, ref) => <PlusCircleOutline {...props} ref={ref} />),
        Check: React.forwardRef((props, ref) => <Check {...props} ref={ref} />),
        Clear: React.forwardRef((props, ref) => <Close {...props} ref={ref} />),
        Delete: React.forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
        DetailPanel: React.forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: React.forwardRef((props, ref) => <PencilOutline {...props} ref={ref} />),
        Export: React.forwardRef((props, ref) => <ContentSaveMoveOutline {...props} ref={ref} />),
        Filter: React.forwardRef((props, ref) => <FilterVariant {...props} ref={ref} />),
        FirstPage: React.forwardRef((props, ref) => <PageFirst {...props} ref={ref} />),
        LastPage: React.forwardRef((props, ref) => <PageLast {...props} ref={ref} />),
        NextPage: React.forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: React.forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: React.forwardRef((props, ref) => <Close {...props} ref={ref} />),
        Search: React.forwardRef((props, ref) => <Magnify {...props} ref={ref} />),
        SortArrow: React.forwardRef((props, ref) => <ArrowUp {...props} ref={ref} />),
        ThirdStateCheck: React.forwardRef((props, ref) => <Minus {...props} ref={ref} />),
        ViewColumn: React.forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    },
    tableOptions: {
        addRowPosition: 'first',
        actionsColumnIndex: -1,
        debounceInterval: 600,
        showEmptyDataSourceMessage: true,
        emptyRowsWhenPaging: false,
        headerStyle: {
            zIndex: 0
        },
        columnsButton: true,
        exportButton: true,
        pageSize: 10
    },
    tableExporter: (url, filename, columns, data) => {
        CRUD.get({ url }).then(result => {
            data = result.data;
            columns = columns.filter(columnDef => columnDef.field && columnDef.export !== false);
            data = data.map(rowData => columns.map(columnDef => rowData[columnDef.field]));

            new CsvBuilder(`${filename}.csv`)
                .setDelimeter(',')
                .setColumns(columns.map(columnDef => columnDef.field))
                .addRows(data)
                .exportFile();
        });
    },
    tableLocalization: {
        pagination: {
            labelDisplayedRows: '{from} à {to} sur {count}', // {from}-{to} of {count}
            labelRowsSelect: 'lignes',
            labelRowsPerPage: 'Lignes par page :', // Rows per page:
            firstAriaLabel: 'Première page', // First Page
            firstTooltip: 'Première page', // First Page
            previousAriaLabel: 'Page précédente', // Previous Page
            previousTooltip: 'Page précédente', // Previous Page
            nextAriaLabel: 'Suivant', // Next Page
            nextTooltip: 'Page suivante', // Next Page
            lastAriaLabel: 'Dernière page', // Last Page
            lastTooltip: 'Dernière page', // Last Page
        },
        toolbar: {
            searchTooltip: 'Rechercher', // Search
            exportTitle: 'Exporter', 
            exportAriaLabel: 'Exporter',
            exportName: 'Exporter en .CSV',
            addRemoveColumns: 'Montrer/Cacher des colonnes',
            showColumnsTitle: 'Montrer les colonnes',
            showColumnsAriaLabel: 'Montrer les colonnes',
            searchPlaceholder: 'Rechercher'
        },
        header: {
            actions: 'Actions', // Actions
        },
        body: {
            addTooltip: 'Ajouter',
            deleteTooltip: 'Supprimer',
            editTooltip: 'Éditer',
            emptyDataSourceMessage: 'Aucune donnée à afficher', // No records to display
            editRow: {
                deleteText: 'Voulez-vous vraiment supprimer cet élément ?',
                cancelTooltip: 'Annuler',
                saveTooltip: 'Sauvegarder',
            }
        },
    },
    onChangeColumnHidden: scope => (column, hidden) => {
        const cv = JSON.parse(localStorage.getItem('cv')) || {};
        Object.assign(cv, { [`${scope}_${column.field}`]: hidden });
        localStorage.setItem('cv', JSON.stringify(cv));
    },
    isHidden: (attribute, defaultValue = false) => {
        const cv = JSON.parse(localStorage.getItem('cv'));
        return cv && cv[attribute] ? Boolean(cv[attribute]) : defaultValue;
    },
    onChangeRowsPerPage: scope => pageSize => {
        const cv = JSON.parse(localStorage.getItem('cv')) || {};
        Object.assign(cv, { [`${scope}_pageSize`]: pageSize });
        localStorage.setItem('cv', JSON.stringify(cv));
    },
    getPageSize: scope => {
        const cv = JSON.parse(localStorage.getItem('cv'));
        return cv && cv[`${scope}_pageSize`] ? cv[`${scope}_pageSize`] : 10;
    },
};
