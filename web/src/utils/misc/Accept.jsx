import React from 'react';
import { makeStyles } from '@material-ui/core';
import { useDropzone } from 'react-dropzone';
import CloudUpload from 'mdi-material-ui/CloudUpload';
import Cancel from 'mdi-material-ui/Cancel';
import IconButton from '@material-ui/core/IconButton';
import Close from 'mdi-material-ui/Close';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(2),
  },
  thumbsContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: theme.spacing(2, 0)
  },
  thumb: {
    display: 'inline-flex',
    borderRadius: 2,
    border: '1px solid #eaeaea',
    width: 100,
    height: 100,
    marginRight: -16,
    padding: 4,
    boxSizing: 'border-box'
  },
  thumbInner: {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden'
  },
  img: {
    display: 'block',
    width: 'auto',
    height: '100%'
  },
  icon: {
    marginTop: -theme.spacing(2)
  }
}));

export default function Accept(props) {
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const label = props.label ? props.label : '(Seuls les formats jpeg | jpg | png | gif | svg sont acceptés)';
  const icon = props.icon ? props.icon : <CloudUpload />;
  const accept = props.accept ? props.accept : 'image/jpeg, image/png, image/gif, image/svg+xml';

  const { getRootProps, getInputProps } = useDropzone({
      accept,
      maxSize: 10000000,
      multiple: props.multiple ? true : false,
      onDrop: props.onDrop,
      onDropAccepted: props.onDropAccepted,
      onDropRejected: () => {
        enqueueSnackbar("Un (des) fichiers sont trop lourds (max 10M) ou format incorrect", {
          variant: 'error',
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right'
          },
          action: key => (
            <IconButton aria-label="Fermer" color="secondary" onClick={() => closeSnackbar(key)}>
              <Close />
            </IconButton>
          )
        });
      }
  });

  const thumbs = props.files.map(file => (
    <Box key={file.name} display="flex" alignItems="flex-start">
      <div className={classes.thumb}>
        <div className={classes.thumbInner}>
          <img
            src={file.preview}
            className={classes.img}
            alt="Prévisualisation des uploads"
          />
        </div>
      </div>
      <IconButton className={classes.icon} aria-label="Supprimer" onClick={props.onDelete}>
        <Cancel />
      </IconButton>
    </Box>
  ));

  return (
    <Box className={!props.noLabel ? classes.container : undefined} display="flex" justifyContent="center" alignItems="center" flexDirection="column">

      <div {...getRootProps({className: 'dropzone'})} style={{ textAlign: 'center' }}>
        
        {!props.noButton ?
          <Tooltip title={label}>
            <IconButton aria-label={label}>
              {icon}
            </IconButton>
          </Tooltip> : icon
        }

        <input {...getInputProps()} />

        {!props.noLabel && (
          <>
            <p>Vous pouvez Drag 'n' drop des fichiers, ou cliquer pour en sélectionner.</p>
            <em>{label}</em>
          </>
        )}
      </div>

      {!props.noPreview && (
        <aside className={classes.thumbsContainer}>
          {thumbs}
        </aside>
      )}

      <Typography variant="subtitle2" align="center" gutterBottom>
        {props.progress}
      </Typography>

    </Box>
  );
};
