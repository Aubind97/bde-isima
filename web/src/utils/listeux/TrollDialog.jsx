import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useTheme, makeStyles, Typography } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ListeuxContext from 'components/Authenticated/Dashboard/ListeuxContext';

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const useStyles = makeStyles(() => ({
    dialog: {
        'animation': 'colorchange 5s',
        '-webkit-animation': 'colorchange 5s',
    },
}));

const IMAGES_COUNT = 18;

export default React.memo(function TrollDialog({ isCashingOpen, children }) {

    /** States initialisation */
    const classes = useStyles();
    const theme = useTheme();
    const [num, setNum] = React.useState(getRandomInt(IMAGES_COUNT));
    const [chance, setChance] = React.useState(getRandomInt(5));
    const [isOpen, setIsOpen] = React.useState(false);
    const isListeux = React.useContext(ListeuxContext);
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const handleDialogOpen = isOpen => () => setIsOpen(isOpen);

    if (isListeux && chance === 1) {
        document.body.dir = 'rtl';
    }

    React.useEffect(() => {
        async function waitAndUpdate() {
            await sleep(5000);
            setIsOpen(false);
            setNum(getRandomInt(IMAGES_COUNT));
            setChance(getRandomInt(1));
        }

        if(isListeux) {
            if (isCashingOpen) {
                setIsOpen(true);
            }
            waitAndUpdate();
        }
    }, [isListeux, isCashingOpen]);

    return (isListeux && isOpen) ? (
        <Dialog
            PaperProps={{ className: classes.dialog }}
            disableBackdropClick
            disableEscapeKeyDown
            open={isOpen}
            fullScreen={fullScreen}
            onClose={handleDialogOpen(false)}
            aria-labelledby="troll-dialog"
        >
            <DialogTitle id="troll-dialog">
                <Typography variant="h6" color="textSecondary" align="center">
                    THIS IS GONNA BE LEGEND... WAIT FOR IT !
                </Typography>
            </DialogTitle>
            <DialogContent>
                <img
                    src={`/images/easter_eggs/listeux/memelisteux${num}.jpg`}
                    height="auto"
                    width="100%"
                    alt="Meme"
                />
            </DialogContent>
        </Dialog>
    ) : children;
});