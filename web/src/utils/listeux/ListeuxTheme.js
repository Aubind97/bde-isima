import { useTheme, createMuiTheme, responsiveFontSizes } from '@material-ui/core';

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

export default function ListeuxTheme() {

    /** States initialisation */
    const theme = useTheme();
    const palette = ['#FF0000', '#001BFF', '#E8FF00', '#00FF32', '#FF00F7', '#FF5900', '#00FFD8'];

    return responsiveFontSizes(createMuiTheme({
        ...theme,
        palette: {
            ...theme.palette,
            common: {
                black: palette[getRandomInt(palette.length)],
            },
            primary: {
                main: '#222222',
            },
            secondary: {
                main: palette[getRandomInt(palette.length)],
            },
            text: {
                secondary: {
                    main: '#fff'
                }
            },
            background: {
                paper: palette[getRandomInt(palette.length)],
            }
        }
    }));
};