import axios from 'axios';
import HTTPInterface from './HTTPInterface';

class CRUDInterface extends HTTPInterface {

    remote = (url, params = []) => query => {
        return new Promise((resolve, reject) => {
            this.get({
                url,
                query,
                params: this.appendOAuthClient(params, false)
            })
                .then(results => {
                    this.count({
                        url: `${url}/count`,
                        query,
                        params: this.appendOAuthClient(params, false)
                    })
                        .then(response => resolve({
                            data: results.data,
                            page: query.page,
                            totalCount: response.data.count
                        }));
                })
                .catch(err => reject(err))
        }
        );
    };

    count = ({ url, query, params }) => {
        return axios.get(this.createURL(url), {
            params: {
                search: query.search,
                orderBy: query.orderBy ? query.orderBy.field : undefined,
                orderDirection: query.orderDirection,
                ...this.appendOAuthClient(params, false)
            },
            headers: this.appendHeaders(),
        });
    };

    get = ({ url, query = {}, params = [] }) => {
        return axios.get(this.createURL(url), {
            params: {
                offset: query.page && query.pageSize ? query.page * query.pageSize : undefined,
                limit: query.pageSize,
                search: query.search,
                orderBy: query.orderBy ? query.orderBy.field : undefined,
                orderDirection: query.orderDirection,
                ...this.appendOAuthClient(params, false)
            }, 
            headers: this.appendHeaders(),
        });
    };

    add = ({ url, newData = {}, withFormData, onUploadProgress, ...rest }) => {
        return this.dispatchRequest({
            request: this.createRequest({
                method: 'post',
                url: this.createURL(url),
                newData,
                withFormData,
                onUploadProgress
            }),
            ...rest
        });
    };

    delete = ({ url, ...rest }) => {
        return this.dispatchRequest({
            request: this.createRequest({
                method: 'delete',
                url: this.createURL(url)
            }),
            ...rest
        });
    };

    //Alias, prototype is identical
    update = ({ url, newData = {}, withFormData, onUploadProgress, ...rest }) => this.add({ url, newData, withFormData, onUploadProgress, ...rest });
};

export default new CRUDInterface();
