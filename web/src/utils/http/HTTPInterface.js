import React from 'react';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import Close from 'mdi-material-ui/Close';
import Undo from 'mdi-material-ui/Undo';
import User from 'components/Authenticated/Auth/Components/User';

function getId(url) {
    switch(url) {
        case 'localhost': return 1;
        case 'bde.dev.isima.fr': return 2;
        default: return 3;
    }
}

function getSecret(url) {
    switch (url) {
        case 'localhost': return 'c5OwQUh80aTVUm9nfyp5AqrUNLfUGxSMaC2aU6NP';
        case 'bde.dev.isima.fr': return 'c5OwQUh80aTVUm9nfyp5AqrUNLfUGxSMaC2aU6NM';
        default: return 'c5OwQUh80aTVUm9nfyp5AqrUNLfUGxSMaC2aU6NQ';
    }
}

export default class HTTPInterface {

    apiURL = process.env.REACT_APP_STAGE === 'dev' ? '/api' : `${window.location.origin}/api`;

    id = getId(window.location.hostname);

    secret = getSecret(window.location.hostname);

    formAppend = (formData, name, value) => {
        if (value !== null) {
            formData.append(name, value);
        }
    };

    createFormData = data => {
        var formData = new FormData();

        Object.getOwnPropertyNames(data).forEach(propertyName => {

            //Check for empty arrays
            if (data[propertyName] !== null && data[propertyName] !== undefined) {

                if (Array.isArray(data[propertyName])) {

                    if (!data[propertyName].some(x => !(x instanceof Blob))) {
                        data[propertyName].forEach(element => {
                            //If array contains multiple files, append [] after propertyName ([] are an HTTP standard to send an array of uploaded files)
                            formData.append(data[propertyName].length > 1 ? `${propertyName}[]` : `${propertyName}`, element, element.name);
                        });
                    }
                    else {
                        this.formAppend(formData, propertyName, JSON.stringify(data[propertyName]));
                    }
                }
                //Others data types here
                else {
                    this.formAppend(formData, propertyName, data[propertyName]);
                }
            }
        });

        return formData;
    };

    createURL = url => `${this.apiURL}${url}`;

    appendHeaders = (type = 'application/json') => ({
        'Content-Type': type,
        'Authorization': `Bearer ${User.getToken()}`,
    });

    appendOAuthClient = (params, isFormData) => {
        if(isFormData) {
            params.append("client_id", this.id);
            params.append("client_secret", this.secret);
            return params;
        }
        return ({
            ...params, ...{
                client_id: this.id,
                client_secret: this.secret,
            }
        });
    }

    createRequest = ({ method, url, newData, withFormData, onUploadProgress }) => axios({
        method,
        url,
        data: method !== 'delete' ? this.appendOAuthClient(withFormData ? this.createFormData(newData) : newData, withFormData) : this.appendOAuthClient({}, withFormData),
        headers: this.appendHeaders(withFormData ? 'multipart/form-data' : 'application/json'),
        onUploadProgress: progressEvent => {
            if (typeof onUploadProgress === 'function') {
                onUploadProgress(progressEvent.loaded / progressEvent.total === 1 ? '' : `Upload en cours ... ${parseInt(progressEvent.loaded / progressEvent.total * 100)}%`)
            }
        }
    });

    dispatchRequest = ({ request, onSucceed, onRejection, ...rest }) => (
        this.dispatchError({
            request: this.dispatchSuccess({
                request,
                onSucceed,
                ...rest
            }),
            onRejection,
            ...rest
        })
    );

    dispatchSuccess = ({ request, onSucceed, quietSucceed, props }) => request.then(response => {
        if(!quietSucceed) {
            props.enqueueSnackbar(response.data.messages, {
                variant: 'success',
                anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'right'
                },
                action: this.action(props)
            });
        }

        if (typeof onSucceed === 'function') {
            onSucceed(response);
        }
        
        return Promise.resolve(response);
    });

    dispatchError = ({ request, onRejection, quietRejection, props }) => request.catch(err => {
        if (process.env.REACT_APP_STAGE === 'dev') {
            console.log(err.response);
        }
        
        if (!quietRejection && err.response.status < 500) {
            // Handle custom API error messages (simple array)
            if (err.response.data.messages) {
                Object.keys(err.response.data.messages).forEach(x => this.displayMsg(err.response.data.messages[x], props));
            }
            // Handle API OAuth error message (simple string)
            else if (err.response.data.message) {
                this.displayMsg(err.response.data.message, props);
            }
        }
        
        if (typeof onRejection === 'function') {
            onRejection(err);
        }

        return Promise.reject(err);
    });

    displayMsg = (msg, props) => {
        props.enqueueSnackbar(msg, {
            ...{
                variant: 'error',
                anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'right'
                },
                action: this.action(props, true)
            },
            ...(props.content ? { content: props.content } : {}),
        });
    };

    action = (props, error) => key => (
        <>
            {(props.handleCancel && !error) && (
                <IconButton aria-label="Annuler" color="secondary" onClick={props.handleCancel}>
                    <Undo />
                </IconButton>
            )}
            <IconButton aria-label="Fermer" color="secondary" onClick={() => props.closeSnackbar(key)}>
                <Close />
            </IconButton>
        </>
    );
}
