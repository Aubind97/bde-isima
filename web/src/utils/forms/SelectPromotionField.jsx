import React from 'react';
import { useLoads } from 'react-loads';
import { makeStyles } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import CircularProgress from '@material-ui/core/CircularProgress';
import { CRUD } from 'utils';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
    },
}));

/**
 * This component is responsible for selecting one promotion in a list
 */
export default React.memo(function SelectPromotionField({ value, onChange }) {

    /** States initialisation */
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const getPromotions = React.useCallback(() => CRUD.get({ 
        url: '/promotions',
        params: {
            orderBy: 'year',
            orderDirection: 'DESC',
        }
    }), []);
    const { response, isPending, isResolved, isRejected } = useLoads(getPromotions);

    const handleMenuItemClick = (event, option) => {
        onChange(option.id);
        setAnchorEl(null);
    };

    const handleClickListItem = event => setAnchorEl(event.currentTarget);
    
    const handleClose = () => setAnchorEl(null);

    return (
        <>
            {(isPending, isRejected) && <CircularProgress size={25} color="primary" />}

            {isResolved && (
                <div className={classes.root}>
                    <List component="nav" aria-label="Selection de la promotion">
                        <ListItem
                            button
                            aria-haspopup="true"
                            aria-controls="lock-menu"
                            aria-label="Promotion"
                            onClick={handleClickListItem}
                        >
                            <ListItemText
                                secondary="Changer promotion"
                            />
                        </ListItem>
                    </List>
                    <Menu
                        id="lock-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                        PaperProps={{
                            style: {
                                maxHeight: 54 * 5,
                                width: 300,
                            },
                        }}
                    >
                        {response.data.map((option, index) => (
                            <MenuItem
                                key={index}
                                selected={option.year === value}
                                onClick={event => handleMenuItemClick(event, option)}
                            >
                                {option.year}
                            </MenuItem>
                        ))}
                    </Menu>
                </div>
            )}
        </>
    );
});
