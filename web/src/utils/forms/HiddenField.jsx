import React from 'react';
import { dripFormField } from 'react-drip-form';

/**
 * This component is responsible for wrapping a hidden input in a dripFormField
 */
export default dripFormField()(({ input, meta, ...props }) => (
  <input
    type="hidden"
    {...input}
    {...props}
  />
));