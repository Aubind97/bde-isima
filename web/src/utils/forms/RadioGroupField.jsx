import React from 'react';
import { dripFormField } from 'react-drip-form';
import RadioGroup from "@material-ui/core/RadioGroup";

/**
 * This component is responsible for wrapping a radio group in a dripFormField
 */
export default dripFormField()(({ input, meta, ...props }) => (
    <RadioGroup
        {...input}
        {...props}
    >
        {props.children}
    </RadioGroup>
));