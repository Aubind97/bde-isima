import React from 'react';
import { useLoads } from 'react-loads';
import { makeStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import CircularProgress from '@material-ui/core/CircularProgress';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Checkbox from '@material-ui/core/Checkbox';
import AccountMultiple from 'mdi-material-ui/AccountMultiple';
import { CRUD } from 'utils';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
    },
    btn: {
        margin: theme.spacing(2, 0),
    }
}));

/**
 * This component is responsible for selecting multiple promotions in a list
 */
export default React.memo(function SelectPromotionField({ onPromotionsSelected, promotions }) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [checked, setChecked] = React.useState(promotions ? promotions : []);

    const getPromotions = React.useCallback(() => CRUD.get({
        url: '/promotions',
        params: {
            orderBy: 'year',
            orderDirection: 'DESC',
        },
    }), []);
    const { response, isPending, isResolved, isRejected } = useLoads(getPromotions);

    const handleOpenMenu = event => setAnchorEl(event.currentTarget);

    const handleClose = () => setAnchorEl(null);

    const handleMenuItemClick = (event, option) => setChecked(prevChecked => prevChecked.find(x => x.id === option.id) ? prevChecked.filter(x => x.id !== option.id) : prevChecked.concat(option));

    React.useEffect(() => {
        onPromotionsSelected(checked);
    }, [onPromotionsSelected, checked]);

    return (
        <>
            {(isPending, isRejected) && <CircularProgress size={25} color="inherit" />}

            {isResolved && (
                <div className={classes.root}>

                    <Button
                        className={classes.btn}
                        startIcon={<AccountMultiple />}
                        onClick={handleOpenMenu}
                        variant="outlined"
                        size="large"
                        aria-label="Choisir l'audience visée"
                        color="inherit"
                    >
                        Choisir l'audience visée
                    </Button>

                    <Menu
                        id="promotions-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                        PaperProps={{
                            style: {
                                maxHeight: 54 * 5,
                                width: 300,
                            },
                        }}
                    >
                        {response.data.map((option, index) => {
                            const labelId = `promotions-list-item-${option.id}-label`;
                            
                            return (
                                <MenuItem key={index} onClick={event => handleMenuItemClick(event, option)}>
                                    <ListItemIcon>
                                        <Checkbox
                                            checked={checked.findIndex(x => x.id === option.id) !== -1}
                                            tabIndex={-1}
                                            disableRipple
                                            inputProps={{ 'aria-labelledby': labelId }}
                                            color="default"
                                        />
                                    </ListItemIcon>
                                    <ListItemText id={labelId} primary={option.year} />
                                </MenuItem>
                            )
                        })}
                    </Menu>
                </div>
            )}
        </>
    );
});
