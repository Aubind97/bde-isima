import React from 'react';
import { dripFormField } from 'react-drip-form';
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import Select from "@material-ui/core/Select";

/**
 * This component is responsible for wrapping a select in a dripFormSelect
 */
export default dripFormField('select')(({ input, meta, children, ...props }) => (
        <FormControl
            style={{ margin: '4px 0px 4px 0px' }} 
            error={meta.error && meta.touched && meta.dirty}
        >
            <InputLabel htmlFor={input.name}>{meta.label}</InputLabel>
            <Select
                {...input}
                {...props}
            >
                {children}
            </Select>
            <FormHelperText component="div">
                {meta.error && meta.touched && meta.dirty &&
                    <span>{meta.error}</span>}
            </FormHelperText>
        </FormControl>
));