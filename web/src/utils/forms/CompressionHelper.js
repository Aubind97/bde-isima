import axios from 'axios';
import mime from 'mime-types';
import imageCompression from 'browser-image-compression';

/**
 * This component is responsible for providing an interface to Blob preview and image compression using 'browser-image-compression' npm package
 * @see (https://www.npmjs.com/package/browser-image-compression)
 */
class CompressionHelper {

    EXCLUDED_MIME_TYPES = [
        "image/svg+xml",
        "image/gif"
    ];

    onDrop = ({ handleOnStart, handleOnChange }) => acceptedFiles => {
        var options = {
            maxSizeMB: 2,
            maxWidthOrHeight: 1920
        };

        handleOnStart();

        axios.all(acceptedFiles.map(file => this.EXCLUDED_MIME_TYPES.includes(file.type) ? file : imageCompression(file, options)))
            .then(compressedFiles => {
                return handleOnChange(compressedFiles.map(compressedFile => {
                    return Object.assign(new Blob([compressedFile], {
                        type: compressedFile.type
                    }), {
                        preview: URL.createObjectURL(compressedFile),
                        filename: `${compressedFile.name.substr(0, compressedFile.name.lastIndexOf("."))}.${mime.extension(compressedFile.type)}`,
                        name: `${compressedFile.name.substr(0, compressedFile.name.lastIndexOf("."))}.${mime.extension(compressedFile.type)}`,
                        lastModifiedDate: new Date()
                    });
                }));
            })
            .catch(error => console.log(error.message));
    };
};

export default new CompressionHelper();