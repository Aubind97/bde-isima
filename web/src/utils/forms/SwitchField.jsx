import React from 'react';
import { dripFormField } from 'react-drip-form';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import Switch from '@material-ui/core/Switch';

/**
 * This component is responsible for wrapping a switch in a dripFormCheckbox 
 */
export default dripFormField('checkbox')(({ input, meta, ...props }) => (
        <FormControlLabel
            style={{ margin: '4px 0px 4px 0px' }}
            error={meta.error && meta.touched && meta.dirty}
            label={meta.label}
            control={
                <Switch
                    {...input}
                    {...props}
                    type="checkbox"
                    color="primary"
                />
            }
        >
            <FormHelperText component="div">
                {meta.error && meta.touched && meta.dirty &&
                    <span>{meta.error}</span>}
            </FormHelperText>
        </FormControlLabel>
));