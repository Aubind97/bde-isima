import React from 'react';
import { dripFormField } from 'react-drip-form';
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import { Picker } from '..';

/**
 * This component is responsible for wrapping a Date picker in a dripFormField
 */
export default dripFormField()(({ input, meta, ...props }) => (
        <FormControl 
            style={{ margin: '4px 0px 4px 0px' }} 
            error={meta.error && meta.touched && meta.dirty}
        >
            <Picker 
                {...input}
                {...props}
                label={meta.label}
                fullWidth
            />
            
            <FormHelperText component="div">
                {meta.error && meta.touched && meta.dirty &&
                    <span>{meta.error}</span>}
            </FormHelperText>
        </FormControl>
    )
);