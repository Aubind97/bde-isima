import React from 'react';
import { dripFormField } from 'react-drip-form';
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";

/**
 * This component is responsible for wrapping a switch in a dripFormField
 */
export default dripFormField()(({ input, meta, ...props }) => (
    <FormControl
        style={{ margin: '4px 0px 4px 0px' }}
        error={meta.error && meta.touched && meta.dirty}
    >
        <InputLabel htmlFor={input.name}>{meta.label}</InputLabel>
        <Input
            fullWidth
            {...input}
            {...props}
        />
        <FormHelperText component="div">
            {meta.error && meta.touched && meta.dirty &&
                <span>{meta.error}</span>}
        </FormHelperText>
    </FormControl>
));