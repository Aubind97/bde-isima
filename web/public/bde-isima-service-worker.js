/* eslint-disable no-restricted-globals */
/* global workbox clients importScripts */

importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.0.0/workbox-sw.js');

workbox.setConfig({ debug: false });

if (workbox) {
    workbox.precaching.precacheAndRoute(self.__precacheManifest || []);
}

self.addEventListener('push', event => {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return;
    }

    // https://developer.mozilla.org/en-US/docs/Web/API/PushMessageData
    if (event.data) {
        event.waitUntil(self.registration.showNotification("BDE ISIMA", event.data.json()));
    }
});

self.addEventListener('notificationclick', function (e) {
    var notification = e.notification;
    var n_id = notification.data.n_id;
    var action = e.action;

    if (action === 'close') {
        notification.close();
    } 
    else {
        clients.openWindow(`http://bde.isima.fr/hub?n_id=${n_id}`);
        notification.close();
    }
});

self.addEventListener('message', event => {
    if (event.data === 'skipWaiting') {
        self.skipWaiting();
    }
});

self.addEventListener('activate', () => {
    self.clients.claim();
});


// @see (Not implemented) : https://medium.com/@madridserginho/how-to-handle-webpush-api-pushsubscriptionchange-event-in-modern-browsers-6e47840d756f
// https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/pushsubscriptionchange_event

self.addEventListener("pushsubscriptionchange", event => {
    event.waitUntil(self.registration.pushManager.subscribe(event.oldSubscription.options)
        .then(newSubscription => {
            return fetch('api/push/update', {
                method: "post",
                headers: new Headers({
                    'Content-Type': 'application/json',
                }),
                body: JSON.stringify({
                    oldSubscription: event.oldSubscription,
                    newSubscription,
                }),
        });
        })
  );
}, false);

//Cache Google Font 
workbox.routing.registerRoute(
    new RegExp('https://fonts.googleapis.com/(.*)'),
    new workbox.strategies.CacheFirst({
        cacheName: 'googleapis',
        cacheExpiration: {
            maxEntries: 30
        },
        cacheableResponse: { statuses: [0, 200] }
    })
);

//Cache all the static js, css assets
workbox.routing.registerRoute(
    /\.(?:js|css)$/,
    new workbox.strategies.NetworkFirst({
        cacheName: 'static-resources',
    })
);

//Cache all the static image assets
workbox.routing.registerRoute(
    /\.(?:png|gif|jpg|jpeg|svg|ico)$/,
    new workbox.strategies.CacheFirst({
        cacheName: 'images',
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 60,                    // Limit cache size 
                maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days before expiration
            }),
        ],
    })
);

//Cache all website routes
workbox.routing.registerRoute(
    new RegExp('https://bde.isima.fr/api/*'),
    new workbox.strategies.NetworkFirst({
        networkTimeoutSeconds: 3,
        cacheName: 'bde-isima',
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 60,         // Limit cache size
                maxAgeSeconds: 5 * 60, // 5 minutes
            }),
        ],
    })
);

//Cache Navigation Route (for ReactRouter)
workbox.routing.registerNavigationRoute('/index.html', {
    blacklist: [
        new RegExp('\\w.txt'),
        new RegExp('/phpmyadmin'),
    ],
});
