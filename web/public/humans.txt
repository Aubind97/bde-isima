/* TEAM */

Back-end developer: Aubin Deterne
Email: aubind97@orange.fr                
Location: Clermont-Ferrand, France

Front-end, Back-end developer: Adrien Lenoir
Email: adrien.lenoir42440@gmail.com                          
Location: Clermont-Ferrand, France
					              
							
/* THANKS */
							
Name: Caroline Rousset
Name: Laurent Malka
                            
/* SITE */
                            
Last update: 2019/07/25                    
Standards: HTML5, CSS3                      
Components: React, Material-UI, Slim PHP Framework                    