<?php

require_once __DIR__.'/../vendor/autoload.php';

(new Laravel\Lumen\Bootstrap\LoadEnvironmentVariables(
    dirname(__DIR__)
))->bootstrap();

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
*/

$app = new Laravel\Lumen\Application(
    dirname(__DIR__)
);

$app->withFacades();

$app->withEloquent();

$app->make('queue');

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
*/

//Add globals middlewares
$app->middleware([]);

// Register middlewares
$app->routeMiddleware([
    'scopes' => \Laravel\Passport\Http\Middleware\CheckScopes::class,
    'scope' => \Laravel\Passport\Http\Middleware\CheckForAnyScope::class,
    'auth' => App\Http\Middleware\Authenticate::class,
    'throttle' => App\Http\Middleware\ThrottleRequests::class,
    'check_client_domain' => App\Http\Middleware\CheckClientDomain::class,
    'merge_scopes' => App\Http\Middleware\MergeScopes::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
*/

$app->register(App\Providers\AuthServiceProvider::class);
$app->register(Illuminate\Mail\MailServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(Laravel\Passport\PassportServiceProvider::class);
$app->register(Dusterio\LumenPassport\PassportServiceProvider::class);
$app->register(NotificationChannels\WebPush\WebPushServiceProvider::class);
$app->register(Illuminate\Notifications\NotificationServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load config mail service
|--------------------------------------------------------------------------
*/

$app->configure('mail');
$app->alias('mailer', Illuminate\Mail\Mailer::class);
$app->alias('mailer', Illuminate\Contracts\Mail\Mailer::class);
$app->alias('mailer', Illuminate\Contracts\Mail\MailQueue::class);

/*
|--------------------------------------------------------------------------
| Load additional config files
|--------------------------------------------------------------------------
*/

$app->configure('auth');

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
*/
if (!function_exists('loadRoutes')) { // for test support
    function loadRoutes($router, string $dir)
    {
        $routes = scandir(__DIR__ . "/../routes/$dir/");
        foreach ($routes as $route)
            if (strpos($route, '.php'))
                require __DIR__ . "/../routes/$dir/" . $route;
    }
}

// Load all public routes
$app->router->group(
    [
        'prefix' => 'api',
        'middleware' => ['throttle']
    ],
    function ($router) { loadRoutes($router, 'public'); }
);

// Load all authenticated routes
$app->router->group(
    [
        'prefix' => 'api',
        'middleware' => ['throttle', 'auth', 'check_client_domain'],
    ],
    function ($router) { loadRoutes($router, 'authenticated'); }
);

return $app;
