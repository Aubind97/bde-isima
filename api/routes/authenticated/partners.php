<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in partners routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'partners',
    ],
    function () use ($router) {
        // Add a partner
        $router->post('', ['uses' => 'PartnerController@addPartner']);
        // Update a partner
        $router->post('/{id:[0-9]+}', ['uses' => 'PartnerController@updatePartner']);
        // Delete a partner
        $router->delete('/{id:[0-9]+}', ['uses' => 'PartnerController@deletePartner']);
    }
);
