<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in analytics routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'analytics',
    ],
    function () use ($router) {
        // Get all stats
        $router->get('', ['uses' => 'AnalyticController@getAll']);

        // Get the articles stats
        $router->get('/articles', ['uses' => 'AnalyticController@getArticlesStats']);

        // Get the leaderboard
        $router->get('/leaderboard', ['uses' => 'AnalyticController@getLeaderboard']);
    }
);
