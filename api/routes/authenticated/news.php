<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in clubs routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'news',
    ],
    function () use ($router) {
        // Add a news
        $router->post('', ['uses' => 'NewsController@addNews']);
        // Update a news
        $router->post('/{id:[0-9]+}', ['uses' => 'NewsController@updateNews']);
        // Delete a news
        $router->delete('/{id:[0-9]+}', ['uses' => 'NewsController@deleteNews']);
    }
);
