<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in subscriptions routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'subscriptions',
    ],
    function () use ($router) {
        // Get the subscription of the user querying for a specific event
        $router->get('/event/{id:[0-9]+}', ['uses' => 'SubscriptionController@getSubscriptionOf']);
        // Get all subscriptions of an event
        $router->get('/event/all/{id:[0-9]+}', ['uses' => 'SubscriptionController@getSubscriptionsOf']);
        // Add a subscription
        $router->post('/', ['uses' => 'SubscriptionController@addSubscription']);
        // Update a subscription
        $router->post('/{id:[0-9]+}', ['uses' => 'SubscriptionController@updateSubscription']);
        // Delete a subscription
        $router->delete('/{id:[0-9]+}', ['uses' => 'SubscriptionController@deleteSubscription']);
    }
);
