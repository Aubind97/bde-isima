<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in planning routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'planning',
    ],
    function () use ($router) {
        // Get all weeks of availabilities (prev week, this week and next week)
        $router->get('/availability', ['uses' => 'PlanningController@getAllAvailability']);
        // Update a week of availability
        $router->post('/availability/{id:[0-9]+}', ['uses' => 'PlanningController@updateAvailability']);
        // Generate a planning for this week of availability
        $router->post('/{id:[0-9]+}', ['uses' => 'PlanningController@generatePlanning']);
        // Get all users which can-update-planning and their stats
        $router->get('/users', ['uses' => 'PlanningController@getUsers']);
    }
);
