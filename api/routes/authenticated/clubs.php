<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in clubs routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'clubs',
    ],
    function () use ($router) {
        // Add a club
        $router->post('', ['uses' => 'ClubController@addClub']);
        // Update a club
        $router->post('/{id:[0-9]+}', ['uses' => 'ClubController@updateClub']);
        // Delete a club
        $router->delete('/{id:[0-9]+}', ['uses' => 'ClubController@deleteClub']);
    }
);
