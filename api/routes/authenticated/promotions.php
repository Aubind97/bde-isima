<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in clubs routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'promotions',
    ],
    function () use ($router) {
        // Add a promotion
        $router->post('', ['uses' => 'PromotionController@addPromotion']);
        // Update a promotion
        $router->post('/{id:[0-9]+}', ['uses' => 'PromotionController@updatePromotion']);
        // Delete a promotion
        $router->delete('/{id:[0-9]+}', ['uses' => 'PromotionController@deletePromotion']);
    }
);
