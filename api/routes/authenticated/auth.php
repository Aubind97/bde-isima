<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Authentification Routes
|--------------------------------------------------------------------------
*/

$router->group(
    ['namespace' => 'App\Http\Controllers'],
    function () use ($router) {
        // Retrieve user info
        $router->post('retrieve', ['uses' => 'AuthController@retrieve']);
        // Retrieve user authorizations
        $router->get('can', ['uses' => 'AuthController@can']);
        // Logout a user
        $router->post('logout', ['uses' => 'AuthController@logout']);
    }
);

