<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in users routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'users',
    ],
    function () use ($router) {
        // Return all users
        $router->get('', ['uses' => 'UserController@getAll']);
        // Return the user specified by this id
        $router->get('/{id:[0-9]+}', ['uses' => 'UserController@getOne']);

        // Return all users with compact info
        $router->get('/compact', ['uses' => 'UserController@getAllCompact']);
        // Return a user with compact info
        $router->get('/compact/{id:[0-9]+}', ['uses' => 'UserController@getOneCompact']);

        // Return users count
        $router->get('/count', ['uses' => 'UserController@getCount']);

        // Add a user
        $router->post('', ['uses' => 'UserController@addUser']);
        // Update a user
        $router->post('/{id:[0-9]+}', ['uses' => 'UserController@updateUser']);
        // Delete a user
        $router->delete('/{id:[0-9]+}', ['uses' => 'UserController@deleteUser']);
        // Revoke user's token
        $router->post('/revoke/{id:[0-9]+}', ['uses' => 'UserController@revokeTokens']);
        
        // Delete user's picture
        $router->delete('/picture/{id:[0-9]+}', ['uses' => 'UserController@deleteUserPicture']);
    }
);
