<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in articles routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'articles',
    ],
    function () use ($router) {
        // Add an article
        $router->post('', ['uses' => 'ArticleController@addArticle']);
        // Update an article
        $router->post('/{id:[0-9]+}', ['uses' => 'ArticleController@updateArticle']);
        // Update a batch of articles
        $router->post('/batch', ['uses' => 'ArticleController@updateBatch']);
        // Delete an article
        $router->delete('/{id:[0-9]+}', ['uses' => 'ArticleController@deleteArticle']);
    }
);
