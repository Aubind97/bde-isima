<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in notifications routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'notifications',
    ],
    function () use ($router) {
        // Get user notifications
        $router->get('/{id:[0-9]+}', ['uses' => 'NotificationController@getNotificationsOf']);
        // Add a notification
        $router->post('', ['uses' => 'NotificationController@addNotification']);
        // Update a notification
        $router->post('/{id:[0-9]+}', ['uses' => 'NotificationController@updateNotification']);
    }
);
