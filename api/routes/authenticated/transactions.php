<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in transactions routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'transactions',
    ],
    function () use ($router) {
        // Get all transactions of a user
        $router->get('/{id:[0-9]+}', ['uses' => 'TransactionController@getTransactionsOf']);
        // Get transactions count of a user
        $router->get('/{id:[0-9]+}/count', ['uses' => 'TransactionController@getCountOf']);
        // Add a transaction
        $router->post('', ['uses' => 'TransactionController@addTransaction']);
        // Delete a transaction
        $router->delete('/{id:[0-9]+}', ['uses' => 'TransactionController@deleteTransaction']);
    }
);
