<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in push routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'push',
    ],
    function () use ($router) {
        // Store push endpoint of a user
        $router->post('/store', ['uses' => 'PushController@store']);
        // Delete push endpoint of a user
        $router->post('/delete', ['uses' => 'PushController@delete']);
    }
);
