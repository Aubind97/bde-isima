<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in events routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'events',
    ],
    function () use ($router) {
        // Add an event
        $router->post('/', ['uses' => 'EventController@addEvent']);
        // Update an event
        $router->post('/{id:[0-9]+}', ['uses' => 'EventController@updateEvent']);
        // Validate an event
        $router->post('/validate/{id:[0-9]+}',  ['uses' => 'EventController@validateEvent']);
        // Delete an event
        $router->delete('/{id:[0-9]+}', ['uses' => 'EventController@deleteEvent']);
    }
);
