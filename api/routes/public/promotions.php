<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in clubs routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'promotions',
    ],
    function () use ($router) {
        // Get all promotions
        $router->get('', ['uses' => 'PromotionController@getAll']);
        // Get promotions count
        $router->get('/count', ['uses' => 'PromotionController@getCount']);
        // Get one promotion
        $router->get('/{id:[0-9]+}', ['uses' => 'PromotionController@getOne']);
    }
);
