<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Public Clubs Routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'clubs',
    ],
    function () use ($router) {
        // Return all clubs
        $router->get('', ['uses' => 'ClubController@getAll']);
        // Return the clubs count
        $router->get('/count',  ['uses' => 'ClubController@getCount']);
        // Return the clubs with this id
        $router->get('/{id:[0-9]+}', ['uses' => 'ClubController@getOne']);
    }
);

