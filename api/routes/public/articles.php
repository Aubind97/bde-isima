<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Public Articles Routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'articles'
    ],
    function () use ($router) {
        // Return all articles
        $router->get('', ['uses' => 'ArticleController@getAll']);
        // Return the articles count
        $router->get('/count',  ['uses' => 'ArticleController@getCount']);
        // Return the article with this id
        $router->get('/{id:[0-9]+}', ['uses' => 'ArticleController@getOne']);
    }
);

