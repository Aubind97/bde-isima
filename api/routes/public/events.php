<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Public Events Routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'events'
    ],
    function () use ($router) {
        // Return all events
        $router->get('', ['uses' => 'EventController@getAll']);
        // Return the events count
        $router->get('/count',  ['uses' => 'EventController@getCount']);
        // Return the event with this id
        $router->get('/{id:[0-9]+}', ['uses' => 'EventController@getOne']);
    }
);

