<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Public Partners Routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'partners'
    ],
    function () use ($router) {
        // Return all partners
        $router->get('', ['uses' => 'PartnerController@getAll']);
        // Return the partners count
        $router->get('/count',  ['uses' => 'PartnerController@getCount']);
        // Return the partner with this id
        $router->get('/{id:[0-9]+}', ['uses' => 'PartnerController@getOne']);
    }
);

