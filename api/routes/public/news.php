<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Public News Routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'news'
    ],
    function () use ($router) {
        // Return all news
        $router->get('', ['uses' => 'NewsController@getAll']);
        // Return the news count
        $router->get('/count',  ['uses' => 'NewsController@getCount']);
        // Return the news with this id
        $router->get('/{id:[0-9]+}', ['uses' => 'NewsController@getOne']);
    }
);

