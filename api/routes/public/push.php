<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Logged in push routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'push',
    ],
    function () use ($router) {
        // Update push endpoint of a user
        $router->post('/update', ['uses' => 'PushController@update']);
    }
);
