<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Authentification Routes
|--------------------------------------------------------------------------
*/

$router->group(
    ['namespace' => 'App\Http\Controllers'],
    function () use ($router) {
        // Init the password reset process
        $router->post('reset_init', ['uses' => 'AuthController@resetInit']);
        // Reset a password
        $router->post('reset', ['uses' => 'AuthController@reset']);
    }
);

