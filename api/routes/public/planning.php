<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Public in planning routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'planning',
    ],
    function () use ($router) {
        // Get iCal planning for a user
        $router->get('/users/{id:[0-9]+}', ['uses' => 'PlanningController@getPlanning']);
    }
);
