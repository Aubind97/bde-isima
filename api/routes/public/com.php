<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Authentification Routes
|--------------------------------------------------------------------------
*/

$router->group(
    [
        'namespace' => 'App\Http\Controllers',
        'prefix' => 'com',
    ],
    function () use ($router) {
        // Handle contact form mail send
        $router->post('/contact', ['uses' => 'ComController@postContact']);
        // Handle feedback form mail send
        $router->post('/feedback', ['uses' => 'ComController@postFeedback']);
    }
);

