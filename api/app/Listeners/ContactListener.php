<?php

namespace App\Listeners;

use App\Events\UserEvent;
use App\Mails\ContactMailer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserEvent $event
     * @return void
     */
    public function handle(UserEvent $event)
    {
        Mail::to(env('MAIL_FOR_CONTACT'))->queue(new ContactMailer($event->getSubject(), $event->getEmail(), $event->getMessage()));
    }
}