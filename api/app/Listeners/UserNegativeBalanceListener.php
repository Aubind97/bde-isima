<?php

namespace App\Listeners;

use App\Events\CreateTransactionEvent;
use App\Events\UserNegativeBalanceEvent;
use App\Notification;
use App\Providers\Helpers\Message;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserNegativeBalanceListener implements ShouldQueue
{
    /**
     * The time (seconds) before the job should be processed.
     *
     * @var int
     */
    public $delay = 30;

    /**
     * @var Notification
     */
    private $notification;

    /**
     * Create the event listener.
     *
     * @param Notification $notification
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param UserNegativeBalanceEvent $event
     * @return void
     * @throws \Exception
     */
    public function handle(UserNegativeBalanceEvent $event)
    {
        $user = $event->getUser();

        if($user->balance < 0) {
            $this->notification->send($user->id, Message::get('notification.balance.negative')[0]);
        }
    }
}
