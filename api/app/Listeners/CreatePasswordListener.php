<?php

namespace App\Listeners;

use App\Events\UserEvent;
use App\Mails\CreatePasswordMailer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreatePasswordListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserEvent $event
     * @return void
     */
    public function handle(UserEvent $event)
    {
        $user = $event->getUser();

        Mail::to($user->email)->queue(new CreatePasswordMailer($user));
    }
}