<?php

namespace App\Listeners;

use App\Promotion;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

class PublishToFacebookListener implements ShouldQueue
{
    use Queueable;

    /**
     * @var int
     */
    private $news_id;

    /**
     * @var Promotion
     */
    private $promotion;

    /**
     * @var string|null
     */
    private $fb_access_token;

    /**
     * Create the event listener.
     * @param int $news_id
     * @param Promotion $promotion
     * @param string|null $fb_access_token
     */
    public function __construct(int $news_id, Promotion $promotion, ?string $fb_access_token)
    {
        $this->news_id = $news_id;
        $this->promotion = $promotion;
        $this->fb_access_token = $fb_access_token;
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle()
    {
        // Get event attributes
        $news_id = $this->news_id;
        $promotion = $this->promotion;
        $fb_access_token = $this->fb_access_token;

        $fb = new \Facebook\Facebook([
            'app_id' => env('FB_APP_ID'),
            'app_secret' => env('FB_APP_SECRET'),
            'default_graph_version' => 'v4.0',
        ]);

        try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            $response = $fb->post(
                "$promotion->fb_group_id/feed", [
                    'link' => "https://bde.isima.fr/news/$news_id",
                ],
                $fb_access_token
            );
        } 
        catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } 
        catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }
}