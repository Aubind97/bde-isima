<?php

namespace App\Listeners;

use App\Events\BalanceChangeEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class BalanceChangeListener implements ShouldQueue
{
    /**
     *  Create the event listener.
     */
    public function __construct()
    {}

    /**
     * Handle the event.
     *
     * @param BalanceChangeEvent $event
     * @return void
     */
    public function handle(BalanceChangeEvent $event)
    {
        $amount = $event->getAmount();
        $user = $event->getUser();
        
        //Can be null when emitter_id is null
        if(isset($user)) {
            $user->balance += $amount;
            $user->save();
        }
    }
}
