<?php

namespace App\Listeners;

use App\User;
use App\Notification;
use App\Providers\Helpers\Message;
use App\Events\CreateTransactionEvent;
use App\Events\SendNotificationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotificationListener implements ShouldQueue
{
    /**
     * @var Notification
     */
    private $model;

    /**
     * Create the event listener.
     *
     * @param Notification $notification
     */
    public function __construct(Notification $model)
    {
        $this->model = $model;
    }

    /**
     * Handle the event.
     *
     * @param SendNotificationEvent $event
     * @return void
     * @throws \Exception
     */
    public function handle(SendNotificationEvent $event)
    {
        $receiver_id = $event->getReceiverId();
        $message = $event->getMessage();
        
        //If the notification is meant to be sent to a specific user
        if(isset($receiver_id)) {
            $this->model->send($receiver_id, $message);
        }
        //Else send it to all the users
        else {
            User::all()->each(function ($item, $key) use($message) {
                $this->model->send($item->id, $message);
            });
        }
    }
}
