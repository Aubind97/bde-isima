<?php

namespace App\Listeners;

use Carbon\Carbon;
use App\Mails\NewsMailer;
use App\Events\PublishNewsEvent;
use App\Listeners\PublishToFacebookListener;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;

class PublishNewsListener implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {}

    /**
     * Handle the event.
     *
     * @param PublishNewsEvent $event
     * @return void
     */
    public function handle(PublishNewsEvent $event)
    {
        // Get event attributes
        $news = $event->getNews();
        $flags = $event->getFlags();
        $flags_params = $event->getFlagsParams();

        if(isset($flags)) {

            if(isset($flags['publish_by_email'])) {
                foreach($news->promotions as $promotion) {
                    // Temporary workaround here with manually set timezone
                    Mail::to($promotion->list_email)->later(Carbon::parse($news->published_at)->diffInSeconds(Carbon::now()), new NewsMailer($news));
                }
            }

            if(isset($flags['publish_to_facebook'])) {
                foreach($news->promotions as $promotion) {
                    // Temporary workaround here with manually set timezone
                    $job = (new PublishToFacebookListener($news->id, $promotion, $flags_params['facebook_data']['accessToken']))
                        ->delay(Carbon::parse($news->published_at)->diffInSeconds(Carbon::now()));
                        
                    dispatch($job);
                }
            }
        }
    }
}
