<?php

namespace App\Listeners;

use App\Events\CreateTransactionEvent;
use App\Events\UserNegativeBalanceEvent;
use App\Events\BalanceChangeEvent;
use App\Events\ArticleStatsEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateTransactionListener implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {}

    /**
     * Handle the event.
     *
     * @param CreateTransactionEvent $event
     * @return void
     */
    public function handle(CreateTransactionEvent $event)
    {
        // Update stocks
        $article = $event->getArticle();
        $emitter = $event->getEmitter();
        $receiver = $event->getReceiver();
        $amount = $event->getAmount();

        if ($article !== null) {
           $article->stock = $article->stock > 0 ? $article->stock - 1 : 0;
           $article->save();
           event(new ArticleStatsEvent($receiver, $article->id));
        }

        // Update emitter balance
        if ($emitter !== null) {

            event(new BalanceChangeEvent(
                -$amount,
                $emitter
            ));

            if ($emitter->balance - $amount < 0)
                event(new UserNegativeBalanceEvent($emitter));
        }

        event(new BalanceChangeEvent(
            $amount,
            $receiver
        ));

        if ($receiver->balance + $amount < 0)
            event(new UserNegativeBalanceEvent($receiver));
    }
}
