<?php

namespace App\Listeners;

use App\Events\UserEvent;
use App\Mails\FeedbackMailer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserEvent $event
     * @return void
     */
    public function handle(UserEvent $event)
    {
        Mail::to(env('MAIL_FOR_FEEDBACK'))->queue(new FeedbackMailer($event->getSubject(), $event->getUsername(), $event->getMessage()));
    }
}