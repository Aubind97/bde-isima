<?php

namespace App\Listeners;

use App\Article;
use App\Events\ArticleStatsEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArticleStatsListener implements ShouldQueue
{
    /**
     *  Create the event listener.
     */
    public function __construct()
    {}

    /**
     * Handle the event.
     *
     * @param ArticleStatsEvent $event
     * @return void
     */
    public function handle(ArticleStatsEvent $event)
    {
        $user = $event->getUser();
        $article_id = $event->getArticleId();
        $user_stats = $user->stats;

        if($user_stats && @$user_stats['articles']) {
            $columns = array_column($user_stats['articles'], 'id');
            $key = array_search($article_id, $columns);

            if($key === false) {
                $user_stats['articles'][] = [
                    'id' => $article_id,
                    'units' => 1,
                ];
            }
            else {
                $user_stats['articles'][$key]['units'] += 1;
            }
        }
        else {
            $user_stats['articles'] = [
                [
                    'id' => $article_id,
                    'units' => 1,
                ],
            ];
        }
        $user->update(['stats' => $user_stats]);
    }
}
