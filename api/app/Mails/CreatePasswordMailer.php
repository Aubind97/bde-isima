<?php

namespace App\Mails;

use App\User;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreatePasswordMailer extends Mailable implements ShouldQueue
{
    /**
     * @var User
     */
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Création de ton compte BDE')
            ->text('mails.create_password.plain')
            ->view('mails.create_password.html')
            ->with([
                'firstname' => $this->user->firstname,
                'link' => "https://bde.isima.fr/reset_password?token=" . $this->user->password_reset_token
            ]);
    }
}
