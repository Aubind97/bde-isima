<?php

namespace App\Mails;

use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMailer extends Mailable implements ShouldQueue
{
    /**
     * @var string
     */
    public $subject;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $content;

    public function __construct(string $subject, string $email, string $content)
    {
        $this->subject = $subject;
        $this->email = $email;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
            ->text('mails.contact.plain')
            ->view('mails.contact.html')
            ->with([
                'subject' => $this->subject,
                'email' => $this->email,
                'content' => $this->content,
            ]);
    }
}
