<?php

namespace App\Mails;

use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackMailer extends Mailable implements ShouldQueue
{
    /**
     * @var string
     */
    public $subject;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $content;

    public function __construct(string $subject, string $username, string $content)
    {
        $this->subject = $subject;
        $this->username = $username;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("$this->subject (retour de $this->username)")
            ->text('mails.feedback.plain')
            ->view('mails.feedback.html')
            ->with([
                'subject' => $this->subject,
                'username' => $this->username,
                'content' => $this->content,
            ]);
    }
}
