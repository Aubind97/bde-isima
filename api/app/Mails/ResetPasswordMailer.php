<?php

namespace App\Mails;

use App\User;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordMailer extends Mailable implements ShouldQueue
{
    /**
     * @var User
     */
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Réinitialisation de votre mot de passe')
            ->text('mails.reset_password.plain')
            ->view('mails.reset_password.html')
            ->with([
                'firstname' => $this->user->firstname,
                'link' => "https://bde.isima.fr/reset_password?token=" . $this->user->password_reset_token,
            ]);
    }
}
