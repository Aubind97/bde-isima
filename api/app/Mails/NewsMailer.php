<?php

namespace App\Mails;

use App\News;
use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewsMailer extends Mailable implements ShouldQueue
{
    /**
     * @var News
     */
    public $news;

    public function __construct(News $news)
    {
        $this->news = $news;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $news = $this->news->load('club');
        return $this
            ->from($news->club->email, strtoupper($news->club->name))
            ->subject($news->title)
            ->text('mails.news.plain')
            ->view('mails.news.html')
            ->with([
                'logo' => $news->club->logo,
                'name' => $news->club->name,
                'title' => $news->title,
                'header' => $news->header,
                'content' => $news->content,
                'published_at' => $news->published_at,
                'link' => "https://bde.isima.fr/hub/settings"
            ]);
    }
}
