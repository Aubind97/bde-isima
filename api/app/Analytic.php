<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class Analytic
 * @mixin Builder
 * @package App
 */
class Analytic extends ValidateModel
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'current_balance',
        'total_positive_users',
        'total_negative_users',
        'created_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'current_balance' => 'numeric',
        'total_positive_users'  => 'integer',
        'total_negative_users'  => 'integer',
    ];

    /**
     * @overload the current_balance getter to automatically convert into float value
     * @param $value
     */
    public function getCurrentBalanceAttribute() {return floatval($this->attributes['current_balance']);}

    /**
     * Returns the fillable attributes
     * @return array
     */
    public function getFillable()
    {
        return $this->fillable;
    }
}
