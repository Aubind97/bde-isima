<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class Article
 * @mixin Builder
 * @package App
 */
class Article extends ValidateModel
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'member_price',
        'stock',
        'photo',
        'is_enabled',
        'created_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'name'  => 'string|max:50',
        'price'  => 'numeric|min:0',
        'member_price' => 'numeric|min:0',
        'stock'  => 'integer|min:0',
        'is_enabled'  => 'boolean',
        'photo'  => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:2000|nullable',
    ];

    /**
     * Apply some filter and return entities
     * @param Request $request
     * @return User|Builder
     */
    public function filter(Request $request, ?Builder $entities = null)
    {
        $entities = parent::filter($request, $entities);

        if ($request->filled('search')) {
            $search = $request->input('search');

            $entities = $entities->orWhere('name', 'LIKE', '%'.$search.'%');
        }

        return $entities;
    }
}
