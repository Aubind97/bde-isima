<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

/**
 * Class ValidateModel
 * @mixin Builder
 * @package App
 */
class ValidateModel extends Model
{
    /**
     * Validation rules
     * @var array
     */
    protected $rules = [];

    /**
     * Error mssages
     * @var array
     */
    protected $errors;

    /**
     * Apply some filter and return builder
     * @param Request $request
     * @return Builder
     */
    public function filter(Request $request, ?Builder $entities = null)
    {
        $entities = $entities ?? $this;

        $entities = $entities->orderBy(
            $request->filled('orderBy') ? $request->input('orderBy') : 'created_at',
            $request->filled('orderDirection') ? $request->input('orderDirection') : 'desc'
        );

        if ($request->filled('limit')) {
            $entities = $entities->limit($request->input('limit'));

            if ($request->filled('offset'))
                $entities = $entities->offset($request->input('offset'));
        }

        $inputs = $request->only($this->fillable);

        foreach($inputs as $key => $input) {
            $entities = $entities->where(function ($query) use($key, $input) {
                $query->where($key, 'LIKE', '%'.$input.'%')
                        ->orWhere($key, $input);
            });
        }

        return $entities;
    }

    /**
     * Validate model data
     * @param array $data
     * @param array|null $rules
     * @return bool
     */
    public function validate(array $data, ?array $rules = null)
    {
        if ($rules) {
            if (isset($rules['required']))
                foreach ($rules['required'] as $key) {
                    if (isset($this->rules[$key]))
                        $this->rules[$key] = "required|" . $this->rules[$key];
                    else {
                        $this->rules[$key] = "required";
                    }
                }

            if (isset($rules['rules']))
                $this->addValidationRules($rules['rules']);
        }


        $validator = Validator::make($data, $this->rules);

        if ($validator->fails())
        {
            $this->errors = $validator->errors();
            return false;
        }

        return true;
    }

    /**
     * Add validation rules
     * @param array $rules
     * @return $this
     */
    public function addValidationRules(array $rules)
    {
        foreach ($rules as $key => $rule)
            $this->rules[$key] .= "|$rule";

        return $this;
    }

    /**
     * Return errors
     * @return array
     */
    public function errors()
    {
        return $this->errors;
    }
}
