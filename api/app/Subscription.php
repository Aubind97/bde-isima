<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class Subscription
 * @mixin Builder
 * @package App
 */
class Subscription extends ValidateModel
{
    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'cart' => 'json',
    ];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'event_id',
        'user_id',
        'payment_method',
        'cart',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'event_id'  => 'exists:events,id|integer',
        'user_id'  => 'exists:users,id|integer',
        'payment_method'  => 'in:bde,lydia,cash|string',
        'cart' => 'sometimes|array'
    ];
}
