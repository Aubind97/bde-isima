<?php

namespace App\Events;

use App\News;

class PublishNewsEvent extends Event
{
    /**
     * @var News
     */
    private $news;

    /**
     * @var array|null
     */
    private $flags;

    /**
     * @var array|null
     */
    private $flags_params;

    /**
     * PublishNewsEvent constructor.
     * @param News $news
     * @param array|null $flags
     * @param array|null $flags_params
     */
    public function __construct(News $news, ?array $flags, ?array $flags_params)
    {
        $this->news = $news;
        $this->flags = $flags;
        $this->flags_params = $flags_params;
    }

    /**
     * @return News
     */
    public function getNews(): News
    {
        return $this->news;
    }

    /**
     * @return array|null
     */
    public function getFlags(): ?array
    {
        return $this->flags;
    }

    /**
     * @return array|null
     */
    public function getFlagsParams(): ?array
    {
        return $this->flags_params;
    }
}
