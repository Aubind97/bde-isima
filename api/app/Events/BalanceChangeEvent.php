<?php

namespace App\Events;

use App\Article;
use App\User;

class BalanceChangeEvent extends Event
{
    /**
     * @var float
     */
    private $amount;
    /**
     * @var User|null
     */
    private $user;

    /**
     * BalanceChangeEvent constructor.
     * @param float $amount
     * @param User|null $user
     */
    public function __construct(float $amount, ?User $user)
    {
        $this->amount = $amount;
        $this->user = $user;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return floatval($this->amount);
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }
}
