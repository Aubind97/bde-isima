<?php

namespace App\Events;

use App\Article;
use App\User;

class CreateTransactionEvent extends Event
{
    /**
     * @var float
     */
    private $amount;
    
    /**
     * @var User|null
     */
    private $emitter;

    /**
     * @var User|null
     */
    private $receiver;

    /**
     * @var Article|null
     */
    private $article;

    /**
     * CreateTransactionEvent constructor.
     * @param float $amount
     * @param User|null $emitter
     * @param User|null $receiver
     * @param Article|null $article
     */
    public function __construct(float $amount, ?User $emitter, ?User $receiver, ?Article $article = null)
    {
        $this->amount = $amount;
        $this->emitter = $emitter;
        $this->receiver = $receiver;
        $this->article = $article;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return floatval($this->amount);
    }

    /**
     * @return User|null
     */
    public function getEmitter(): ?User
    {
        return $this->emitter;
    }

    /**
     * @return User|null
     */
    public function getReceiver(): ?User
    {
        return $this->receiver;
    }

    /**
     * @return Article|null
     */
    public function getArticle(): ?Article
    {
        return $this->article;
    }
}
