<?php

namespace App\Events;

class ContactEvent extends UserEvent
{
    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $message;

    /**
     * ContactEvent constructor.
     * @param float $amount
     * @param User|null $user
     */
    public function __construct(string $subject, string $email, string $message)
    {
        $this->subject = $subject;
        $this->email = $email;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
