<?php

namespace App\Events;

class SendNotificationEvent extends Event
{
    /**
     * @var int|null
     */
    private $receiver_id;

    /**
     * @var string
     */
    private $message;

    /**
     * SendNotificationEvent constructor.
     * @param int|null $receiver_id
     * @param string $message
     */
    public function __construct(?int $receiver_id, string $message)
    {
        $this->receiver_id = $receiver_id;
        $this->message = $message;
    }

    /**
     * @return int|null
     */
    public function getReceiverId(): ?int
    {
        return $this->receiver_id;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
