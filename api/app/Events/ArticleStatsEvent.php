<?php

namespace App\Events;

use App\User;

class ArticleStatsEvent extends UserEvent
{
    /**
     * @var int
     */
    private $article_id;

    /**
     * ArticleStatsEvent constructor.
     * @param int $article_id
     */
    public function __construct(User $user, int $article_id)
    {
        parent::__construct($user);
        $this->article_id = $article_id;
    }

    /**
     * @return int
     */
    public function getArticleId(): int
    {
        return $this->article_id;
    }
}
