<?php

namespace App\Events;

class FeedbackEvent extends UserEvent
{
    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $message;

    /**
     * FeedbackEvent constructor.
     * @param float $amount
     * @param User|null $user
     */
    public function __construct(string $subject, string $username, string $message)
    {
        $this->subject = $subject;
        $this->username = $username;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
