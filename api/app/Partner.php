<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class Partner
 * @mixin Builder
 * @package App
 */
class Partner extends ValidateModel
{
    /**
     * @var bool disable timestamps
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'logo',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'name'  => 'string|max:50',
        'description'  => 'string|max:3000',
        'logo'  => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:2000|nullable',
    ];


    /**
     * Apply some filter and return entities
     * @param Request $request
     * @return User|Builder
     */
    public function filter(Request $request, ?Builder $entities = null)
    {
        $entities = parent::filter($request, $entities);

        if ($request->filled('search')) {
            $search = $request->input('search');

            $entities = $entities
                ->orWhere('name', 'LIKE', '%'.$search.'%');
        }

        return $entities;
    }
}
