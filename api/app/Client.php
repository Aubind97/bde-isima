<?php

namespace App;

/**
 * Class Client
 * @package App
 */

class Client extends \Laravel\Passport\Client
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'secret',
        'domain_name',
        'redirect',
        'personal_access_client',
        'password_client',
        'revoked',
    ];
}
