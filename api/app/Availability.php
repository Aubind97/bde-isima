<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Availability
 * @mixin Builder
 * @package App
 */
class Availability extends Model
{
    /**
     * The name of the table.
     * @var string
     */
    protected $table = 'availability';

    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'availabilities' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'week_start',
        'availabilities',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
