<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class Promotion
 * @mixin Builder
 * @package App
 */
class Promotion extends ValidateModel
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'year',
        'fb_group_id',
        'list_email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'year'  => 'integer|digits:4',
        'fb_group_id'  => 'integer|nullable',
        'list_email'  => 'email|max:255',
    ];

    /**
     * The users that belong to the promotion.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * The news that belong to the promotion.
     */
    public function news()
    {
        return $this->belongsToMany('App\News');
    }

    /**
     * Apply some filter and return entities
     * @param Request $request
     * @return User|Builder
     */
    public function filter(Request $request, ?Builder $entities = null)
    {
        $entities = parent::filter($request, $entities);

        if ($request->filled('search')) {
            $search = $request->input('search');

            $entities = $entities
                ->orWhere('year', 'LIKE', '%'.$search.'%');
        }

        return $entities;
    }
}
