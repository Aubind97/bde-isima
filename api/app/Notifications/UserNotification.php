<?php 

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Notification as ModelNotif;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;

class UserNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var ModelNotif
     */
    private $notification;

    public function __construct(ModelNotif $notification) {
        $this->notification = $notification;
    }

    public function via($notifiable)
    {
        return [WebPushChannel::class];
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title('BDE ISIMA')
            ->icon('https://bde.isima.fr/images/favicons/android-chrome-192x192.png')
            ->body($this->notification->message)
            ->action('Voir', 'see_more')
            ->data(['n_id' => $this->notification->id])
            ->requireInteraction()
            ->vibrate([20, 10, 20]);
    }
}