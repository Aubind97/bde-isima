<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Notifications\UserNotification;

/**
 * Class Notification
 * @mixin Builder
 * @package App
 */
class Notification extends ValidateModel
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'message',
        'is_read',
        'receiver_id',
        'created_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'message'  => 'required|string|max:255',
        'is_read'  => 'boolean',
        'receiver_id'  => 'exists:users,id|integer',
    ];

    /**
     * Apply some filter and return entities
     * @param Request $request
     * @return Notification|Builder
     */
    public function filter(Request $request, ?Builder $entities = null)
    {
        $entities = parent::filter($request, $entities);

        if ($request->filled('search')) {
            $search = $request->input('search');

            $entities = $entities->orWhere('message', 'LIKE', '%'.$search.'%');
        }

        return $entities;
    }

    /**
     * Notify a user
     * @param int $user_id
     * @param string $message
     * @return Notification|\Illuminate\Database\Eloquent\Model
     */
    public function send(int $user_id, string $message)
    {
        $user = User::pushSubscriptionsCount($user_id);

        $notification = $this->create([
            'receiver_id' => $user_id,
            'message' => $message,
        ]);
        
        if($user->push_subscriptions_count > 0) {
            $user->notify(new UserNotification($notification));
        }
    }
}
