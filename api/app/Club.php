<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class Club
 * @mixin Builder
 * @package App
 */
class Club extends ValidateModel
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'logo',
        'name',
        'email',
        'description',
        'facebook',
        'twitter',
        'instagram',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'logo'  => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:2000|nullable',
        'name' => 'string|max:50',
        'email'  => 'email|max:100',
        'description'  => 'string|max:3000',
        'facebook'  => 'url|max:255|nullable',        
        'twitter'  => 'url|max:255|nullable',        
        'instagram'  => 'url|max:255|nullable',        
    ];

    /**
     * Apply some filter and return entities
     * @param Request $request
     * @return User|Builder
     */
    public function filter(Request $request, ?Builder $entities = null)
    {
        $entities = parent::filter($request, $entities);

        if ($request->filled('search')) {
            $search = $request->input('search');

            $entities = $entities
                ->orWhere('name', 'LIKE', '%'.$search.'%')
                ->orWhere('email', 'LIKE', '%'.$search.'%')
                ->orWhere('facebook', 'LIKE', '%'.$search.'%')
                ->orWhere('twitter', 'LIKE', '%'.$search.'%')
                ->orWhere('instagram', 'LIKE', '%'.$search.'%');
        }

        return $entities;
    }
}
