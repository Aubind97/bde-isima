<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Carbon\Carbon;

/**
 * Class News
 * @mixin Builder
 * @package App
 */
class News extends ValidateModel
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'club_id',
        'title',
        'header',
        'content',
        'is_published',
        'published_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'club_id' => 'exists:clubs,id',
        'title' => 'max:255',
        'header' => 'max:150',
        'content' => 'string',
        'is_published' => 'boolean',
        'published_at' => 'date|nullable',
    ];

    /**
     * Get the club that owns the user.
     */
    public function club()
    {
        return $this->belongsTo('App\Club');
    }

    /**
     * Get the promotions for the news post.
     */
    public function promotions()
    {
        return $this->belongsToMany('App\Promotion', 'promotions_news');
    }

    /**
     * Apply some filter and return entities
     * @param Request $request
     * @return User|Builder
     */
    public function filter(Request $request, ?Builder $entities = null)
    {
        $entities = parent::filter($request, $entities);

        if ($request->filled('search')) {
            $search = $request->input('search');

            $entities = $entities
                ->where('title', 'LIKE', '%'.$search.'%')
                ->orWhere('header', 'LIKE', '%'.$search.'%');
        }

        // All news for which published_at attribute is not in the future
        if ($request->filled('noFuture'))
            $entities->whereDate('published_at', '<=', Carbon::today()->toDateString());

        // Filter by promotion
        if($request->filled('promotion')) {
            $entities = $entities
                ->orWhereHas('promotions', function (Builder $query) use ($request) {
                    $query->where('year', $request->input('promotion'));
                })
                ->orWhereDoesntHave('promotions');
        }

        return $entities;
    }
}
