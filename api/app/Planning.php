<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Planning
 * @mixin Builder
 * @package App
 */
class Planning extends Model
{
    /**
     * The name of the table.
     * @var string
     */
    protected $table = 'planning';

    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'repartition' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'week_start',
        'repartition',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
