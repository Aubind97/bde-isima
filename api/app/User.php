<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Lumen\Auth\Authorizable;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class User
 * @mixin Builder
 * @package App
 */
class User extends ValidateModel implements AuthenticatableContract, AuthorizableContract
{
    use HasPushSubscriptions, HasApiTokens, Notifiable, Authenticatable, Authorizable;

    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'preferences' => 'json',
        'scopes' => 'array',
        'stats' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //User ID
        'id',

        //User info
        'firstname',
        'lastname',
        'nickname',
        'email',
        'picture',
        'promotion_id',

        //Auth info
        'card',
        'password',
        'salt',

        //Password recovery
        'password_reset_token',
        'password_reset_at',

        //Managed by admin info
        'balance',
        'is_member',
        'is_enabled',

        //User preferences
        'preferences',

        //User scopes
        'scopes',

        //User stats
        'stats',
    ];

    /**
     * @var array of storable data
     */
    public static $storable = [
        'id',
        'nickname',
        'firstname',
        'lastname',
        'picture',
        'card',
        'salt',
        'promotion',
        'balance',
        'is_member',
        'is_enabled',
        'promotion_id',
        'preferences',
        'scopes',
        'stats',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password_reset_token',
        'password_reset_at',
        'password',
        'salt',
        'created_at',
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        //User info
        'lastname'  => 'string|max:100',
        'firstname'  => 'string|max:100',
        'nickname'  => 'string|max:50',
        'email'  => 'email|max:100',
        'picture' => 'sometimes|mimes:jpeg,jpg,png,gif,svg|max:2000|nullable',
        'promotion_id' => 'exists:promotions,id|integer',

        //Auth info
        'card' => 'integer',
        'password' => 'required_with:confirm|same:confirm',

        //Password recovery
        'password_reset_token' => 'nullable',
        'password_reset_at' => 'date|nullable',

        //Managed by admin info
        'balance' => 'numeric',
        'is_member' => 'boolean',
        'is_enabled'  => 'boolean',

        //User preferences
        'preferences' => 'sometimes|array|nullable',
        'preferences.dark_mode' => 'boolean',
        'preferences.course' => 'nullable|in:F1,F2,F3,F4,F5|string',

        //User scopes
        'scopes' => 'array|nullable',

        //User stats
        'stats' => 'sometimes|array|nullable',
        'stats.articles' => 'sometimes|array|nullable',
        'stats.articles.*.id' => 'integer|exists:articles,id',
        'stats.articles.*.units' => 'integer',
    ];

    /**
     * Get the promotion that owns the user.
     */
    public function promotion()
    {
        return $this->belongsTo('App\Promotion');
    }

    /**
     * Get the subscriptions count of a user
     */
    public static function pushSubscriptionsCount(int $user_id)
    {
        return self::withCount(['pushSubscriptions'])->where('id', $user_id)->first();
    }

    /**
     * @overload the balance getter to automatically convert into float value
     * @param $value
     */
    public function getBalanceAttribute() {return floatval($this->attributes['balance']);}

    /**
     * @overload the balance getter to automatically convert into float value
     * @return string
     */
    public function getUsername() {
        return $this->attributes['firstname'] . " " . $this->attributes['lastname'];
    }

    /**
     * Select only a few data
     * @return User
     */
    public function compact()
    {
        return $this->select(
            'id',

            //User info
            'lastname',
            'firstname',
            'nickname',
            'promotion_id',
            'picture',

            //Auth info
            'card',

            //Managed by admin info
            'balance',
            'is_member',
            'is_enabled',

            //User preferences
            'preferences',

            //User scopes
            'scopes',

            //User stats
            'stats'
        );
    }

    /**
     * Apply some filter and return entities
     * @param Request $request
     * @return User|Builder
     */
    public function filter(Request $request, ?Builder $entities = null)
    {
        $entities = parent::filter($request, $entities);

        if ($request->filled('search')) {
            $search = $request->input('search');

            $entities = $entities->orWhere('firstname', 'LIKE', '%'.$search.'%')
                ->orWhere('lastname', 'LIKE', '%'.$search.'%')
                ->orWhere('nickname', 'LIKE', '%'.$search.'%')
                ->orWhere('card', 'LIKE', '%'.$search.'%')
                ->orWhereHas('promotion', function (Builder $query) use ($search) {
                    $query->where('year', 'LIKE', '%'.$search.'%');
                })
                ->orWhere('preferences->course', 'LIKE', '%'.$search.'%');
        }

        if ($request->filled('scope_id')) {
            $entities = $entities->whereRaw('json_contains(`scopes`, \'{"id": "' . $request->input('scope_id') . '"}\')');
        }

        return $entities;
    }

    /**
     * Override the field which is used for username in the authentication
     * @param string $username
     * @return User|null
     */
    public function findForPassport($username)
    {
        return $this->where('email', $username)->first() ?? $this->where('card', $username)->first();
    }

    /**
     * Add a password validation callback
     *
     * @param type $password
     * @return boolean Whether the password is valid
     */
    public function validateForPassportPasswordGrant($password)
    {
        return hash_pbkdf2('sha512', $password, $this->salt, 90000) === $this->password;
    }
}
