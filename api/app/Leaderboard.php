<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Leaderboard
 * @mixin Builder
 * @package App
 */
class Leaderboard extends Model
{
    /**
     * The name of the table.
     * @var string
     */
    protected $table = 'leaderboard';

    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'ladder' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'ladder',
        'updated_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
    ];
}
