<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Carbon\Carbon;

/**
 * Class Event
 * @mixin Builder
 * @package App
 */
class Event extends ValidateModel
{
    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'options' => 'json',
        'products' => 'json',
        'standalone' => 'json',
    ];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'description',
        'club_id',
        'takes_place_at',
        'subscriptions_end_at',
        'status',
        'max_subscribers',
        'options',
        'products',
        'standalone',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'name'  => 'sometimes|string|max:255',
        'description'  => 'string|max:255',
        'club_id'  => 'sometimes|exists:clubs,id|integer',
        'takes_place_at'  => 'sometimes|date',
        'subscriptions_end_at'  => 'sometimes|date',
        'status' => 'sometimes|integer|in:0,1,2',
        'max_subscribers' => 'sometimes|integer|nullable|min:0',
        'options' => 'array',
        'products' => 'array',
        'standalone' => 'array',
    ];

    /**
     * Apply some filter and return entities
     * @param Request $request
     * @return User|Builder
     */
    public function filter(Request $request, ?Builder $entities = null)
    {
        $entities = parent::filter($request, $entities);

        if($request->filled('statusIn'))
            $entities = $entities->whereIn('status', $request->input('statusIn'))->whereDate('subscriptions_end_at', '>=', Carbon::today()->toDateString());

        if ($request->filled('search')) {
            $search = $request->input('search');

            $entities = $entities->where('name', 'LIKE', '%'.$search.'%');
        }

        // Search by club id
        if($request->filled('club_id'))
            $entities = $entities->where('club_id', $request->input('club_id'));

        return $entities;
    }
}
