<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ResetPasswordEvent' => [
            'App\Listeners\ResetPasswordListener',
        ],
        'App\Events\CreatePasswordEvent' => [
            'App\Listeners\CreatePasswordListener',
        ],
        'App\Events\CreateTransactionEvent' => [
            'App\Listeners\CreateTransactionListener',
        ],
        'App\Events\BalanceChangeEvent' => [
            'App\Listeners\BalanceChangeListener',
        ],
        'App\Events\UserNegativeBalanceEvent' => [
            'App\Listeners\UserNegativeBalanceListener',
        ],
        'App\Events\SendNotificationEvent' => [
            'App\Listeners\SendNotificationListener',
        ],
        'App\Events\PublishNewsEvent' => [
            'App\Listeners\PublishNewsListener',
        ],
        'App\Events\ContactEvent' => [
            'App\Listeners\ContactListener',
        ],
        'App\Events\FeedbackEvent' => [
            'App\Listeners\FeedbackListener',
        ],
        'App\Events\ArticleStatsEvent' => [
            'App\Listeners\ArticleStatsListener',
        ],
    ];
}
