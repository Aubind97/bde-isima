<?php

namespace App\Providers;

use App\User;
use App\Club;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Dusterio\LumenPassport\LumenPassport;
use Laravel\Passport\Passport;
use Carbon\Carbon;
use App\Providers\Helpers\Message;
use Laravel\Passport\RouteRegistrar;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        Passport::useClientModel(Client::class);

        LumenPassport::routes($this->app, ['prefix' => 'api/oauth', 'middleware' => 'merge_scopes']);

        LumenPassport::tokensExpireIn(Carbon::now()->addDays(15));
        LumenPassport::allowMultipleTokens();

        if (Schema::hasTable('clubs')){
            $config = array_merge([
                '*'    => Message::get('scopes.god')[0],

                'listeux'    => Message::get('scopes.listeux')[0],

                'can-read-analytics'            => Message::get('scopes.can-read-analytics')[0],

                'can-create-notifications'      => Message::get('scopes.can-create-notifications')[0],

                'can-create-transactions'       => Message::get('scopes.can-create-transactions')[0],
                'can-delete-transactions'       => Message::get('scopes.can-delete-transactions')[0],

                'can-create-users'              => Message::get('scopes.can-create-users')[0],
                'can-read-users'                => Message::get('scopes.can-read-users')[0],
                'can-read-user'                 => Message::get('scopes.can-read-user')[0],
                'can-update-users'              => Message::get('scopes.can-update-users')[0],
                'can-delete-users'              => Message::get('scopes.can-delete-users')[0],

                'can-create-clubs'              => Message::get('scopes.can-create-clubs')[0],
                'can-update-clubs'              => Message::get('scopes.can-update-clubs')[0],
                'can-delete-clubs'              => Message::get('scopes.can-delete-clubs')[0],

                'can-update-events'             => Message::get('scopes.can-update-events')[0],
                'can-validate-events'           => Message::get('scopes.can-validate-events')[0],
                'can-delete-events'             => Message::get('scopes.can-delete-events')[0],

                'can-create-articles'           => Message::get('scopes.can-create-articles')[0],
                'can-update-articles'           => Message::get('scopes.can-update-articles')[0],
                'can-delete-articles'           => Message::get('scopes.can-delete-articles')[0],

                'can-create-partners'           => Message::get('scopes.can-create-partners')[0],
                'can-update-partners'           => Message::get('scopes.can-update-partners')[0],
                'can-delete-partners'           => Message::get('scopes.can-delete-partners')[0],

                'can-create-promotions'         => Message::get('scopes.can-create-promotions')[0],
                'can-update-promotions'         => Message::get('scopes.can-update-promotions')[0],
                'can-delete-promotions'         => Message::get('scopes.can-delete-promotions')[0],

            ], Club::all()->flatMap(function($club) {
                return [
                    "can-update-$club->name" => Message::get("scopes.can-update-$club->name")[0],

                    "can-create-$club->name-events" => Message::get("scopes.can-create-$club->name-events")[0],
                    "can-update-$club->name-events" => Message::get("scopes.can-update-$club->name-events")[0],
                    "can-delete-$club->name-events" => Message::get("scopes.can-delete-$club->name-events")[0],

                    "can-create-$club->name-news" => Message::get("scopes.can-create-$club->name-news")[0],
                    "can-update-$club->name-news" => Message::get("scopes.can-update-$club->name-news")[0],
                    "can-delete-$club->name-news" => Message::get("scopes.can-delete-$club->name-news")[0],
                ];
            })->all());

            Passport::tokensCan($config);
        }
    }
}
