<?php

namespace App\Providers\Helpers;

use Exception;

class Message {

    private static $last_local_asked;

    private static $messages;

    /**
     * Return a message thanks to a key
     * @param string $key : message name
     * @param string|null $locale
     * @return string|array
     * @throws Exception
     */
    public static function get(string $key, ?string $locale = null)
    {
        $locale = $locale ?: env('APP_LOCAL');

        if (self::$messages === null || self::$last_local_asked !== $locale) {
            try {
                self::$messages = include(__DIR__ . "/../../../resources/lang/$locale/app_messages.php");
                self::$last_local_asked = $locale;
            }
            catch (Exception $e) {
                throw new Exception("Local '$locale' does not exists");
            }
        }

        return [
            self::$messages[$key],
        ];
    }
}
