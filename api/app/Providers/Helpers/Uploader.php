<?php

namespace App\Providers\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class Uploader {

    /**
     * Path setter.
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * If the key file is present in params upload the file, otherwise do nothing
     * @param Request $request
     * @param array $params
     * @param string $key 
     * @param null $old_file 
     * @param string $path destination dir 
     * @return array
     */
    public function uploadFile(Request $request, array $params, string $key, $old_file = null) {
        if (isset($params[$key]) && $request->hasFile($key) && $request->file($key)->isValid()) {

            $this->delete($old_file);

            $filename = Str::uuid().'.'.$request->file($key)->guessExtension();
            $request->file($key)->move(public_path("uploads/$this->path"), $filename);

            $params[$key] = "uploads/$this->path/$filename";       
        }
        else {
            unset($params[$key]);
        }

        return $params;
    }

    /**
     * Delete an uploaded file if he exsist
     * @param string|null $old_file
     */
    public function delete(?string $old_file) : void
    {
        if ($old_file) {
            $old_file = public_path() . DIRECTORY_SEPARATOR . $old_file;
            if (file_exists($old_file)) {
                unlink($old_file);
            }
        }
    }
}
