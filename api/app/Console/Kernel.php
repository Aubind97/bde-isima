<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Laravelista\LumenVendorPublish\VendorPublishCommand::class,
        \App\Console\Commands\CollectAnalytics::class,
        \App\Console\Commands\FlushNotification::class,
        \App\Console\Commands\BuildLeaderboard::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('analytics:collect')
            ->daily()
            ->runInBackground();

        $schedule->command('notif:flush')
            ->weekly()
            ->runInBackground();

        $schedule->command('leaderboard:build')
            ->hourly()
            ->runInBackground();
    }
}
