<?php

namespace App\Console\Commands;

use App\User;
use App\Article;
use App\Leaderboard;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Console\Command;

class BuildLeaderboard extends Command implements ShouldQueue
{
    use Queueable;

    /**
     * @var Leaderboard
     */
    protected $model;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leaderboard:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build the leaderboard';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Leaderboard $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    /**
     * Search for article position in array
     *
     * @return int|bool
     */
    private function searchKey(Array $articles_stats, int $id) {
        return array_search($id, array_column($articles_stats, 'id'));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ladder = [];
        $entry = $this->model->first();
        $articles = Article::all();
        $users = User::whereJsonLength('stats->articles', '>', 0)->get();

        foreach($articles as $article) {

            $max = $users->reduce(function ($max, $user) use($article) {
                $key = $this->searchKey($user->stats['articles'], $article->id);

                if($key !== false) {
                    $current_units = $user->stats['articles'][$key]['units'];

                    if($current_units >= $max[1] && $current_units > 0) {
                        $max[0] = $user;
                        $max[1] = $current_units;
                    }
                }

                return $max;
            }, [null, 0]);

            if(isset($max[0]) && $max[1] != 0) {
                $ladder [] = [
                    'article_id' => $article->id,
                    'article_photo' => $article->photo,
                    'article_name' => $article->name,
                    'picture' => $max[0]->picture,
                    'username' => isset($max[0]->nickname) && !empty($max[0]->nickname) ? $max[0]->nickname : "{$max[0]->firstname} {$max[0]->lastname}",
                    'units' => $max[1],
                ];
            }
        }

        $entry->ladder = $ladder;
        $entry->update();
    }
}
