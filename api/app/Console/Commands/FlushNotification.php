<?php
 
namespace App\Console\Commands;
 
use App\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Console\Command;

class FlushNotification extends Command implements ShouldQueue
{
    use Queueable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:flush';
 
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush notifications older than a month';
 
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
 
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $outdated_notifications = Notification::where('created_at', '<', 'NOW() - INTERVAL 30 DAY');

        foreach($outdated_notifications as $outdated_notif) {
            $outdated_notif->delete();
        }
    }
}