<?php
 
namespace App\Console\Commands;
 
use App\Analytic;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Console\Command;

class CollectAnalytics extends Command implements ShouldQueue
{
    use Queueable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'analytics:collect';
 
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect various analytics data';
 
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
 
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $total_positive_users = User::where('balance', '>=', 0);
        $total_negative_users = User::where('balance', '<', 0);

        Analytic::create([
            'current_balance' => floatval($total_positive_users->sum('balance') + $total_negative_users->sum('balance')),
            'total_positive_users' => $total_positive_users->count(),
            'total_negative_users' =>  $total_negative_users->count(),
        ]);
    }
}