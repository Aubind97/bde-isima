<?php

namespace App\Http\Controllers;

use App\Club;
use App\User;
use App\Notification;
use App\Events\SendNotificationEvent;
use App\Providers\Helpers\Message;
use Illuminate\Http\Request;

class NotificationController extends Controller {

    /**
     * @var Notification
     */
    protected $model;

    /**
     * NotificationController constructor.
     * @param Notification $model
     */
    public function __construct(Notification $model)
    {
        $this->model = $model;
    }

    /**
     * Return all notifications of a selected user
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotificationsOf(Request $request, int $id)
    {
        $notifications = $this->model->filter($request)->where('receiver_id', $id)->latest()->get();

        return response()->json($notifications, 200);
    }

    /**
     * Add a new notification
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addNotification(Request $request)
    {
        $params = $request->only('message', 'receiver_id');
        $rules = ['required' => ['message']];

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-create-notifications"))
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        // Dispatch event to notify user(s)
        event(new SendNotificationEvent($request->input('receiver_id'), $params['message']));

        return $this->response('notification.add.success', 200);
    }

    /**
     * Update a notification as read
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateNotification(Request $request, int $id)
    {
        $notification = $this->model->find($id);

        if (!isset($notification))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if ($request->user()->id != $notification->receiver_id)
            return $this->response('unauthorized', 401);

        $notification->is_read = true;
        $notification->save();

        return response()->json([
            'messages' => Message::get('notification.update.success'),
            'updated_id' => $notification->id,
        ], 200);
    }
}
