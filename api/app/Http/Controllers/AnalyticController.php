<?php

namespace App\Http\Controllers;

use App\Analytic;
use App\Transaction;
use App\Leaderboard;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

class AnalyticController extends Controller {

    /**
     * @var Analytic
     */
    protected $model;

    /**
     * @var Transaction
     */
    protected $transaction;

    /**
     * @var Leaderboard
     */
    protected $leaderboard;

    /**
     * AnalyticController constructor.
     * @param Analytic $model
     * @param Transaction $transaction
     */
    public function __construct(Analytic $model, Transaction $transaction, Leaderboard $leaderboard)
    {
        $this->model = $model;
        $this->transaction = $transaction;
        $this->leaderboard = $leaderboard;
    }

    /**
     * Filter model by date period
     * @param Model $model
     */
    public function bound(Model $model, Request $request)
    {
        $to = Carbon::now()->toDateTimeString();
        $from = Carbon::now()->subDays(7);
        $period = $request->input('period', 'month');

        switch($period) {
            case 'month': $from = Carbon::now()->subDays(30); break;
            case 'year':  $from = Carbon::now()->subDays(365); break;
        }

        return $model->whereBetween('created_at', [$from->toDateTimeString(), $to]);
    }

    /**
     * @Override
     * Get all
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getAll(Request $request)
    {
        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-read-analytics'))
            return $this->response('unauthorized', 401);

        return response()->json($this->bound($this->model, $request)->get(), 200);
    }

    /**
     * Get articles stats
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getArticlesStats(Request $request)
    {
        $articles_stats = Cache::remember("articles_stats_{$request->input('period', 'month')}", 60, function () use ($request) {

            $articles_stats = [];
            $transactions = $this->bound($this->transaction, $request)
                            ->whereHas('article')
                            ->with('article')
                            ->get();

            foreach($transactions as $transaction) {
                $columns = array_column($articles_stats, 'id');
                $key = array_search($transaction->article->id, $columns);

                if($key === false) {
                    $articles_stats[] = [
                        'id' => $transaction->article->id,
                        'name' => $transaction->article->name,
                        'units' => 1,
                    ];
                }
                else {
                    $articles_stats[$key]['units'] += 1;
                }
            }

            return $articles_stats;
        });

        return response()->json($articles_stats, 200);
    }

    /**
     * Get leaderboard
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getLeaderboard(Request $request)
    {
        return response()->json($this->leaderboard->first(), 200);
    }
}
