<?php

namespace App\Http\Controllers;

use App\Events\CreatePasswordEvent;
use App\Providers\Helpers\Message;
use App\Providers\Helpers\Uploader;
use App\User;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Token;

class UserController extends Controller {
    /**
     * @var User
     */
    protected $model;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * UserController constructor.
     * @param User $model
     * @param Uploader $uploader
     */
    public function __construct(User $model, Uploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
        $this->uploader->setPath('users');
    }

    /**
     * @Override
     * Return all users
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(Request $request)
    {
        $users = $this->model->filter($request);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-read-users')) {
            return $this->response('unauthorized', 401);
        }

        return response()->json(
            $users->with('promotion')->get()->makeHidden(['preferences']),
            200);
    }

    /**
     * Return all users with compact data
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCompact(Request $request)
    {
        $users = $this->model->filter($request, $this->model->compact());

        return response()->json(
            $users->with('promotion')->get()->makeHidden(['preferences']),
            200);
    }

    /**
     * @Override
     * Return a specific user
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getOne(Request $request, int $id)
    {
        $user = $this->model->with('promotion')->find($id);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-read-user') && $request->user()->id !== $id) {
            return $this->response('unauthorized', 401);
        }

        // ABORT if not found
        if (!isset($user))
            return $this->response('not_found', 404);

        return response()->json($user, 200);
    }

    /**
     * Return a specific user with compact data
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getOneCompact(Request $request, int $id)
    {
        $user = $this->model->compact()->with('promotion')->find($id);

        if (!isset($user))
            return $this->response('not_found', 404);

        return response()->json($user, 200);
    }

    /**
     * Add a user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addUser(Request $request)
    {
        $params = $request->only('picture', 'lastname', 'firstname', 'nickname', 'email', 'card', 'balance', 'promotion_id', 'preferences', 'is_member', 'is_enabled');
        $rules = [
            'required' => ['lastname', 'firstname', 'email', 'promotion_id'],
            'rules' => [
                "email" => "unique:users,email",
                "card" => "unique:users,card",
                "promotion_id" => "exists:promotions,id",
            ],
        ];

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-create-users')) {
            return $this->response('unauthorized', 401);
        }

        // ABORT if there are bad data
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        // Upload the image if there is one
        $params = $this->uploader->uploadFile($request, $params, 'picture');

        // Generate a random pass and init a reset process
        $tmpPassword = bin2hex(random_bytes(32));
        $params['salt'] = bin2hex(random_bytes(64));
        $params['password'] = hash_pbkdf2('sha512', $tmpPassword, $params['salt'], 90000);
        $params['password_reset_token'] = bin2hex(random_bytes(64));
        $params['password_reset_at'] = date('Y-m-d H:i:s', strtotime("+1 month"));

        // Generate 4 random digits card number
        if (!isset($params['card'])) {
            $cards = $this->model->select('card')->get();
            do {
                $card = substr(rand(1, mt_getrandmax()), 0, 4);
            } while($cards->contains($card));

            $params['card'] = $card;
        }

        // Set default user preferences
        $params['preferences'] = [
            'course' => null,
            'dark_mode' => false,
        ];

        $user = $this->model->make($params);
        $user->save();

        event(new CreatePasswordEvent($user));

        return response()->json([
            'messages' => Message::get('user.add.success'),
            'inserted_id' => $user->id,
        ], 200);
    }

    public function updateUser(Request $request, int $id)
    {
        if($request->user()->tokenCan('can-update-users')) {
            $params = $request->only('id', 'picture', 'lastname', 'firstname', 'nickname', 'email', 'password', 'confirm', 'card', 'balance', 'promotion_id', 'is_member', 'is_enabled', 'preferences', 'scopes');
            $user = $this->model->find($id);
        }
        // If the user is the owner
        else if ($request->user()->id === $id) {
            $params = $request->only('id', 'picture', 'nickname', 'email', 'password', 'confirm', 'preferences');
            $user = $request->user();
        }
        else {
            return $this->response('unauthorized', 401);
        }

        $rules = [
            'rules' => [
                'email' => "unique:users,email,$user->id",
                'card' => "unique:users,card,$user->id",
            ],
        ];

        // ABORT if there are bad data
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        // Update password if needed
        if (isset($params['password']) && isset($params['confirm'])) {

            if($params['password'] != $params['confirm'])
                return $this->response('user.password_mismatch', 403);

            $params['salt'] = bin2hex(random_bytes(64));
            $params['password'] = hash_pbkdf2('sha512', $params['password'], $params['salt'], 90000);
            unset($params['confirm']);
        }

        // Upload the image if there is one
        $params = $this->uploader->uploadFile($request, $params, 'picture', $user->picture);

        $user->update($params);

        return response()->json([
            'messages' => Message::get('user.update.success'),
            'updated_id' => $user->id,
        ], 200);
    }

    /**
     * Delete user's picture
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteUserPicture(Request $request, int $id)
    {
        $user = $this->model->find($id);

        // ABORT if not found
        if (!isset($user))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-update-users') && $request->user()->id != $id) {
            return $this->response('unauthorized', 401);
        }

        $this->uploader->delete($user->picture);
        $user->picture = null;
        $user->save();

        return $this->response('user.delete.picture.success', 200);
    }

    /**
     * Delete a user
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteUser(Request $request, int $id)
    {
        $user = $this->model->find($id);

        // ABORT if not found
        if (!isset($user))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-delete-users')) {
            return $this->response('unauthorized', 401);
        }

        $this->uploader->delete($user->picture);
        $user->delete();

        return response()->json([
            'messages' => Message::get('user.delete.success'),
            'data' => $user,
        ],
        200);
    }

    /**
     * Revoke user's token 
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function revokeTokens(Request $request, int $id)
    {
        $user = $this->model->find($id);

        // ABORT if not found
        if (!isset($user))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-update-users')) {
            return $this->response('unauthorized', 401);
        }

        $tokens = Token::where('user_id', $user->id)->get();

        foreach($tokens as $token) {
            $token->delete();
        }

        return response()->json([
            'messages' => Message::get('user.revoked.success'),
        ],
        200);
    }
}
