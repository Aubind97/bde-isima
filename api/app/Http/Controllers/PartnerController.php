<?php

namespace App\Http\Controllers;

use App\Partner;
use App\Providers\Helpers\Message;
use App\Providers\Helpers\Uploader;
use Illuminate\Http\Request;

class PartnerController extends Controller {
    /**
     * @var Partner
     */
    protected $model;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * PartnerController constructor.
     * @param Partner $model
     * @param Uploader $uploader
     */
    public function __construct(Partner $model, Uploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
        $this->uploader->setPath('partners');
    }

    /**
     * Add a new partner
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addPartner(Request $request)
    {
        $params = $request->only('logo', 'name', 'description');
        $rules = ['required' => ['name']];

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-create-partners"))
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        // Upload the image if there is one
        $params = $this->uploader->uploadFile($request, $params, 'logo');

        $partner = $this->model->make($params);
        $partner->save();

        return response()->json([
            'messages' => Message::get('partner.add.success'),
            'inserted_id' => $partner->id,
        ], 200);
    }

    /**
     * Update a partner
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updatePartner(Request $request, int $id) {

        $params = $request->only('name', 'description', 'logo');

        $partner = $this->model->find($id);

        //ABORT if not found
        if(!isset($partner))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-update-partners"))
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params))
            return $this->validation_fail();

        // Upload the image if there is one
        $params = $this->uploader->uploadFile($request, $params, 'logo', $partner->logo);

        $partner->update($params);

        return response()->json([
            'messages' => Message::get('partner.update.success'),
            'updated_id' => $partner->id,
        ], 200);
    }

    /**
     * Delete a partner
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deletePartner(Request $request, int $id) {
        $partner = $this->model->find($id);

        // ABORT if not found
        if(!isset($partner))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-delete-partners"))
            return $this->response('unauthorized', 401);

        $this->uploader->delete($partner->logo);
        $partner->delete();

        return response()->json([
            'messages' => Message::get('partner.delete.success'),
            'data' => $partner,
        ],
        200);
    }
}
