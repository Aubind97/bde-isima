<?php

namespace App\Http\Controllers;

use App\Event;
use App\Club;
use App\User;
use App\Subscription;
use App\Transaction;
use App\Events\CreateTransactionEvent;
use App\Providers\Helpers\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EventController extends Controller {
    /**
     * @var Event
     */
    protected $model;

    /**
     * @var Club
     */
    private $club;

    /**
     * @var Subscription
     */
    private $subscription;

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * EventController constructor.
     * @param Event $model
     */
    public function __construct(Event $model, Club $club, Subscription $subscription, Transaction $transaction)
    {
        $this->model = $model;
        $this->club = $club;
        $this->subscription = $subscription;
        $this->transaction = $transaction;
    }

    /**
     * Add a new event
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addEvent(Request $request)
    {
        $params = $request->only('name', 'description', 'club_id', 'takes_place_at', 'subscriptions_end_at', 'status', 'options', 'products', 'standalone', 'max_subscribers');
        $rules = ['required' => ['name', 'club_id', 'takes_place_at', 'subscriptions_end_at']];

        // ABORT if there are bad data
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        $club = $this->club->find($params['club_id']);

        // ABORT if not found
        if (!isset($club))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-create-$club->name-events"))
            return $this->response('unauthorized', 401);

        $event = $this->model->create($params);
        $event->save();

        return response()->json([
            'messages' => Message::get('event.add.success'),
            'inserted_id' => $event->id
        ], 200);
    }

    /**
     * Update a new event
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateEvent(Request $request, int $id)
    {
        $params = $request->only('name', 'description', 'club_id', 'takes_place_at', 'subscriptions_end_at', 'status', 'options', 'products', 'standalone', 'max_subscribers');
        
        $event = $this->model->find($id);

        // ABORT if not found
        if (!isset($event))
            return $this->response('not_found', 404);

        // ABORT if there are bad data
        if (!$this->model->validate($params))
            return $this->validation_fail();

        $club = $this->club->find($event->club_id);

        // ABORT if unauthorized
        if (($request->filled('status') && $request->input('status') != $event->status) && !$request->user()->tokenCan("can-update-events") || !$request->user()->tokenCan("can-update-$club->name-events"))
            return $this->response('unauthorized', 401);

        $event->update($params);

        return response()->json([
            'messages' => Message::get('event.update.success'),
            'updated_id' => $event->id
        ], 200);
    }

    /**
     * Validate an event
     * @param int $id
     */
    public function validateEvent(Request $request, int $id)
    {
        $user = $request->user();
        $event = $this->model->find($id);

        // ABORT if not found
        if (!isset($event))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-update-events"))
            return $this->response('unauthorized', 401);

        // ABORT if already validated
        if($event->status != '1')
            return $this->response('event.validation.fail', 403);

        $subscriptions = $this->subscription->where('event_id', $id)->get();

        foreach($subscriptions as $subscription) {

            $amount = array_reduce($subscription->cart, function($acc, $item) {
                return $acc + floatval($item['price']);
            }, 0);

            if($subscription->payment_method === 'bde' && $amount > 0) {

                $receiver = User::find($subscription->user_id);

                $transaction = $this->transaction->make([
                    'amount' => -$amount,
                    'receiver_id' => $receiver->id,
                    'receiver_name' => $receiver->lastname . ' ' . $receiver->firstname,
                    'emitter_name' => "(Membre BDE) $user->firstname $user->lastname",
                    'description' => date('d/m', strtotime($event->takes_place_at)) . ' - '. $event->name
                ]);

                $transaction->save();

                event(new CreateTransactionEvent(-$amount, null, $receiver));
            }
        }

        $event->status = 2;
        $event->save();

        return $this->response('event.validation.success', 200);
    }

    /**
     * Delete an event
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteEvent(Request $request, int $id)
    {
        $event = $this->model->find($id);

        // ABORT if not found
        if(!isset($event))
            return $this->response('not_found', 404);
        
        $club = $this->club->find($event->club_id);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-delete-events"))
            if(!$request->user()->tokenCan("can-delete-$club->name-events"))
                return $this->response('unauthorized', 401);

        $event->delete();        

        return response()->json([
            'messages' => Message::get('event.delete.success'),
            'data' => $event,
        ],
        200);
    }
}
