<?php

namespace App\Http\Controllers;

use App\Events\ContactEvent;
use App\Events\FeedbackEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Providers\Helpers\Message;

class ComController extends Controller {

    /**
     * ComController constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * Send contact email
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function postContact(Request $request)
    {
        $params = $request->only('subject', 'email', 'message');
        $rules = [
            'subject' => 'required|string:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ];
        
        $validator = Validator::make($params, $rules);

        // ABORT if there is bad data
        if ($validator->fails())
            return response()->json([
                'messages' => $validator->errors(),
            ]);

        event(new ContactEvent($params['subject'], $params['email'], $params['message']));

        return response()->json([
            'messages' => Message::get('com.contact.success'),
        ], 200);
    }

    /**
     * Send feedback email
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function postFeedback(Request $request)
    {
        $params = $request->only('subject', 'message');
        $rules = [
            'subject' => 'required|string:255',
            'message' => 'required|string',
        ];
        
        $validator = Validator::make($params, $rules);

        // ABORT if there is bad data
        if ($validator->fails())
            return response()->json([
                'messages' => $validator->errors(),
            ]);

        event(new FeedbackEvent($params['subject'], $request->user()->getUsername() , $params['message']));

        return response()->json([
            'messages' => Message::get('com.feedback.success'),
        ], 200);
    }
}
