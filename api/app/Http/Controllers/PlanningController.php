<?php

namespace App\Http\Controllers;

use App\User;
use App\Planning;
use App\Availability;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class PlanningController extends Controller {

    /**
     * @var Planning
     */
    protected $model;

    /**
     * @var Availability
     */
    protected $availability;

    /**
    * @var User
    */
    protected $user;

    /**
     * PlanningController constructor.
     * @param Planning $model
     * @param Availability $availability
     * @param User $user
     */
    public function __construct(Planning $model, Availability $availability, User $user)
    {
        $this->model = $model;
        $this->availability = $availability;
        $this->user = $user;
    }

    /**
     * Get all availability
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getAllAvailability(Request $request)
    {
        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-update-planning'))
            return $this->response('unauthorized', 401);

        $prev_week = date("Y-m-d", strtotime('monday last week'));
        $prev_week_av = $this->getOrCreate($prev_week);

        $this_week = date("Y-m-d", strtotime('monday this week'));
        $this_week_av = $this->getOrCreate($this_week);

        $next_week = date("Y-m-d", strtotime('monday next week'));
        $next_week_av = $this->getOrCreate($next_week);

        return response()->json([
            $prev_week_av,
            $this_week_av,
            $next_week_av,
        ], 200);
    }

    private function getOrCreate(string $date){
        $availability = $this->availability->where('week_start', $date)->get();

        if(!isset($availability)) {
            $availability = $this->availability->make([
                'week_start' => $date, 
                'availabilities' => [],
            ]);
            $availability->save();
        }

        return $availability;
    }

    /**
    * Update an availability
    * @param Request $request
    * @param int $id
    * @return \Illuminate\Http\JsonResponse
    * @throws \Exception
    */
    public function updateAvailability(Request $request, int $id)
    {
        $params = $request->only('availabilities');

        $availability = $this->availability->find($id);

        // ABORT if not found
        if (!$availability)
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-update-planning'))
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->availability->validate($params))
            return $this->validation_fail();

        $availability->update($params);

        return response()->json([
            'messages' => Message::get('availability.update.success'),
            'updated_id' => $availability->id
        ], 200);
    }

    /**
    * Generate a planning for this week of availability
    * @param Request $request
    * @return \Illuminate\Http\JsonResponse
    */
    public function generatePlanning(Request $request, int $id)
    {
        $availability = $this->availability->find($id);

        // ABORT if not found
        if (!$availability)
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-add-planning'))
            return $this->response('unauthorized', 401);

        $params = [
            'week_start' => $availability->week_start,
            'repartition' => //TODO
        ];

        $planning = $this->model->make($params);
        $planning->save();

        return response()->json([
            'messages' => Message::get('planning.add.success'),
            'inserted_id' => $article->id
        ], 200);
    }

    /**
    * Return all users which can-update-planning and their stats
    * @param Request $request
    * @return \Illuminate\Http\JsonResponse
    */
    public function getUsers(Request $request)
    {
        $users = $this->user->whereRaw('json_contains(`scopes`, \'{"id": "can-update-planning"}\')');

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-update-planning')) {
            return $this->response('unauthorized', 401);
        }

        return response()->json(
            $users,
            200);
    }

    /**
    * Get iCal planning for a user
    * @param Request $request
    * @return \Illuminate\Http\JsonResponse
    */
    public function getPlanning(Request $request, int $id)
    {
        $user = $this->user->find($id);
        

        $ical = //TODO

        return response()->json(
            $ical,
            200);
    }
}
