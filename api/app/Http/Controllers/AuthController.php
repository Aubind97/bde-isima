<?php

namespace App\Http\Controllers;

use App\Events\ResetPasswordEvent;
use App\Providers\Helpers\Message;
use App\User;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * @var User
     */
    protected $model;

    /**
     * Create a new controller instance.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Give credentials from access token
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function retrieve(Request $request)
    {
        return response()->json($this->model->with('promotion')->find($request->user()->id)->only(User::$storable));
    }

    /**
     * Return whether or not the user is authorized to access a scope
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function can(Request $request)
    {
        $params = $request->only('scopes');
        $rules = ['scopes' => 'required|array'];

        $validator = Validator::make($params, $rules);

        // ABORT if there is bad data
        if ($validator->fails())
            return response()->json([
                'messages' => $validator->errors(),
            ], 403);

        $is_authorized = array_reduce($params['scopes'], function($acc, $scope) use($request) {
            return $acc || $request->user()->tokenCan($scope);
        }, false);

        // ABORT if unauthorized
        if(!$is_authorized)
            return $this->response('unauthorized', 401);

        return $this->response('authorized', 200);
    }

    /**
     * Disconnect the user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return $this->response('logout', 200);
    }

    /**
     * Prepare the process to change a user's password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function resetInit(Request $request)
    {
        $email = $request->input('email');
        $user = $this->model->where('email', $email)->first();

        if (isset($user)) {
            $reset_token = Uuid::uuid();

            $user->password_reset_token = $reset_token;
            $user->password_reset_at = date('Y-m-d H:i:s', strtotime("+15 minutes"));
            $user->update();

            event(new ResetPasswordEvent($user));
        }

        return $this->response('password_reset', 200);
    }

    /**
     * Reset a user's password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function reset(Request $request)
    {
        $reset_token = $request->input('token');
        $password = $request->input('password');
        $confirm = $request->input('confirm');

        $user = $this->model->where('password_reset_token', $reset_token)->first();

        if (isset($password) &&
            isset($confirm) &&
            isset($user) &&
            $user->password_reset_at > date('Y-m-d H:i:s') &&
            $user->password_reset_token === $reset_token
        ) {
            if($password != $confirm)
                return $this->response('user.password_mismatch', 403);

            $user->password = hash_pbkdf2('sha512', $password, $user->salt, 90000);
            $user->password_reset_token = null;
            $user->password_reset_at = null;

            $user->update();

            return $this->response('password_reset.success', 200);
        }

        return $this->response('password_reset.fail', 401);
    }

}
