<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Providers\Helpers\Message;
use App\Providers\Helpers\Uploader;

class ArticleController extends Controller {

    /**
     * @var Article
     */
    protected $model;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * ArticleController constructor.
     * @param Article $model
     * @param Uploader $uploader
     */
    public function __construct(Article $model, Uploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
        $this->uploader->setPath('articles');
    }

    /**
     * Add a new article
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addArticle(Request $request)
    {
        $params = $request->only('name', 'price', 'member_price', 'stock', 'photo', 'is_enabled', 'created_at');

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-create-articles'))
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params))
            return $this->validation_fail();

        // Upload the image if there is one
        $params = $this->uploader->uploadFile($request, $params, 'photo');

        $article = $this->model->make($params);
        $article->save();

        return response()->json([
            'messages' => Message::get('article.add.success'),
            'inserted_id' => $article->id
        ], 200);
    }

    /**
     * Update an article
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateArticle(Request $request, int $id)
    {
        $params = $request->only('photo', 'name', 'price', 'member_price', 'stock', 'is_enabled', 'created_at');

        $article = $this->model->find($id);

         // ABORT if not found
        if (!$article)
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-update-articles'))
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params))
            return $this->validation_fail();

        // Upload the image if there is one
        $params = $this->uploader->uploadFile($request, $params, 'photo', $article->photo);

        $article->update($params);

        return response()->json([
            'messages' => Message::get('article.update.success'),
            'updated_id' => $article->id
        ], 200);
    }

    /**
     * Update multiple articles at once
     * * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateBatch(Request $request)
    {
        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-update-articles'))
            return $this->response('unauthorized', 401);

        if($request->has('articles')) {

            $articles = $request->input('articles');

            for($i = 0, $size = count($articles); $i < $size; ++$i) {

                // ABORT if there are bad data
                if (!$this->model->validate($articles[$i]))
                    return $this->validation_fail();

                $article = $this->model->find($articles[$i]['id']);

                $ids [] = $article->update($articles[$i]);
            }
        }

        return response()->json([
            'messages' => Message::get('article.update.batch.success'),
            'updated_ids' => $ids,
        ], 200);
    }

    /**
     * Delete an article
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteArticle(Request $request, int $id)
    {
        $article = $this->model->find($id);

        // ABORT if not found
        if(!isset($article))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-delete-articles'))
            return $this->response('unauthorized', 401);

        $this->uploader->delete($article->photo);
        $article->delete();

        return response()->json([
            'messages' => Message::get('article.delete.success'),
            'deleted_item' => $article,
        ], 200);
    }

}
