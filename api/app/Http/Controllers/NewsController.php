<?php

namespace App\Http\Controllers;

use App\Events\PublishNewsEvent;
use App\News;
use App\Club;
use App\Providers\Helpers\Message;
use Illuminate\Http\Request;

class NewsController extends Controller {

    /**
     * @var News
     */
    protected $model;

    /**
     * @var Club
     */
    private $club;

    /**
     * NewsController constructor.
     * @param News $model
     */
    public function __construct(News $model, Club $club)
    {
        $this->model = $model;
        $this->club = $club;
    }

    /**
     * @Override
     * Return the list of all news
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(Request $request)
    {
        return response()->json(
            $this->model->filter($request)->with('promotions')->get()
        , 200);
    }

    /**
     * @Override
     * Return a news
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getOne(Request $request, int $id)
    {
        $entity = $this->model->with(['club', 'promotions'])->find($id);

        // ABORT if not found
        if (!isset($entity))
            return $this->response('not_found', 404);

        return response()->json($entity, 200);
    }

    /**
     * Add a news
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addNews(Request $request) {
        $params = $request->only('image', 'club_id', 'title', 'header', 'content', 'is_published', 'published_at');

        $promotions = $request->input('promotions');
        $flags = $request->input('flags');
        $flags_params = $request->input('flags_params');
        
        $rules = [
            'required' => ['title', 'club_id', 'content'],
            'promotions' => 'array',
            'promotions.*.id' => 'integer|exists:promotions,id',
            'flags' => 'array',
            'flags.*' => 'boolean',
            'flags_params' => 'array',
            'flags_params.facebook_data' => 'array',
            'flags_params.facebook_data.accessToken' => 'string|required_with:flags_params.facebook_data',
        ];

        // ABORT if there is data errors
        if (!$this->model->validate(array_merge($params, [ $promotions, $flags, $flags_params ]), $rules))
            return $this->validation_fail();

        $club = $this->club->find($params['club_id']);

        // ABORT if not found
        if (!isset($club))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!isset($club) || !$request->user()->tokenCan("can-create-$club->name-news"))
            return $this->response('unauthorized', 401);

        // Create the news
        $news = $this->model->make($params);
        $news->save();
        $news->promotions()->sync($promotions);

        // Dispatch publication events
        if ($request->input('is_published')) {
            event(new PublishNewsEvent($news, $flags, $flags_params));
        }

        return response()->json([
            'messages' => Message::get('news.add.success'),
            'inserted_id' => $news->id,
        ], 200);
    }

    /**
     * Update a news
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateNews(Request $request, int $id) {
        $params = $request->only( 'club_id', 'title', 'header', 'content', 'is_published', 'published_at');
        $promotions = $request->input('promotions');
        $flags = $request->input('flags');
        $flags_params = $request->input('flags_params');

        $rules = [
            'promotions' => 'array',
            'promotions.*.id' => 'integer|exists:promotions,id',
            'flags' => 'array',
            'flags.*' => 'boolean',
            'flags_params' => 'array',
            'flags_params.facebook_data' => 'array',
            'flags_params.facebook_data.accessToken' => 'string|required_with:flags_params.facebook_data',
        ];

        // ABORT if there is data errors
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        $news = $this->model->find($id);
        $club = $this->club->find($news->club_id);

        //ABORT if not found
        if(!isset($news))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-update-$club->name-news"))
            return $this->response('unauthorized', 401);

        // Dispatch publication events
        if (!$news->is_published && $request->input('is_published')) {
            event(new PublishNewsEvent($news, $flags, $flags_params));
        }

        $news->update($params);
        $news->promotions()->sync($promotions);

        return response()->json([
            'messages' => Message::get('news.update.success'),
            'updated_id' => $news->id,
        ], 200);
    }

    /**
     * Delete a news
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteNews(Request $request, int $id)
    {
        $news = $this->model->find($id);

        // ABORT if not found
        if(!isset($news))
            return $this->response('not_found', 404);

        $club = $this->club->find($news->club_id);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-delete-$club->name-news"))
            return $this->response('unauthorized', 401);

        $news->delete();

        return response()->json([
            'messages' => Message::get('news.delete.success'),
            'data' => $news,
        ],
        200);
    }
}
