<?php

namespace App\Http\Controllers;

use App\User;
use App\Article;
use App\Transaction;
use Illuminate\Http\Request;
use App\Events\BalanceChangeEvent;
use App\Providers\Helpers\Message;
use App\Events\CreateTransactionEvent;

class TransactionController extends Controller {

    /**
     * @var Transaction
     */
    protected $model;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Article
     */
    private $article;

    public function __construct(Transaction $model, User $user, Article $article)
    {
        $this->model = $model;
        $this->user = $user;
        $this->article = $article;
    }

    /**
     * Return the list of all transactions
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransactionsOf(Request $request, int $id)
    {
        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-create-transactions') && $request->user()->id != $id)
            return $this->response('unauthorized', 401);

        $transaction = $this->model->filter($request, $this->model->findOfUser($id));

        return response()->json(
            $transaction->get()
        , 200);
    }

    /**
     * Return the count of transactions made by a user
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountOf(Request $request, int $id)
    {
        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-create-transactions') && $request->user()->id != $id)
            return $this->response('unauthorized', 401);

        $transactions = $this->model->filter($request, $this->model->findOfUser($id));

        return response()->json(
            ['count' => $transactions->count()]
        , 200);
    }

    /**
     * Add a transaction
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addTransaction(Request $request)
    {
        $params = $request->only('amount', 'description', 'receiver_id', 'receiver_name', 'emitter_id', 'emitter_name', 'article_id');

        $amount = round(floatval($request->input('amount')), 2);

        //The following tests are only run if the user has no special permission
        if($request->filled('emitter_id') || !$request->user()->tokenCan('can-create-transactions')) {
            // ABORT if it's a self transaction
            if ($request->input('receiver_id') === $request->input('emitter_id'))
                return $this->response('transaction.self', 401);

            // ABORT if the emitter's balance is too low
            if ($request->user()->balance < $amount)
                return $this->response('transaction.sold_out', 401);

            // ABORT if the transaction is negative
            if ($amount <= 0)
                return $this->response('transaction.negative', 401);

            // ABORT if the emitter is not the user
            if ((int)$request->input('emitter_id') !== $request->user()->id)
                return $this->response('transaction.owner', 401);
        }

        // ABORT if there are bad data
        $rules = ['required' => ['amount', 'description', 'receiver_id', 'receiver_name', 'article_id']];
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        //Add additional logs for non peer-to-peer transactions
        if(!isset($params['emitter_id'])) {
            $user = $request->user();
            $role = $user->tokenCan('listeux') && !$user->tokenCan('*') ? 'LISTEUX' : 'BDE';
            $params['emitter_name'] = "{$user->firstname} {$user->lastname} ($role)";
        }

        //Query the users
        $receiver = $this->user->find($request->input('receiver_id'));
        $emitter = $this->user->find($request->input('emitter_id'));

        //Add the previous receiver balance
        $params['prev_receiver_balance'] = $receiver->balance;
        
        //Add the previous emitter balance
        if(isset($emitter)) {
            $params['prev_emitter_balance'] = $emitter->balance;
        }

        $transaction = $this->model->make($params);
        $transaction->save();

        event(new CreateTransactionEvent(
            $amount,
            $emitter,
            $receiver,
            $this->article->find($request->input('article_id'))
        ));

        return response()->json([
            'messages' => Message::get('transaction.add.success'),
            'inserted_id' => $transaction->id,
        ], 200);
    }

    /**
     * Delete a transaction
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteTransaction(Request $request, int $id) {
        $transaction = $this->model->find($id);

        // ABORT if not found
        if(!isset($transaction))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-delete-transactions') && $request->user()->id != $transaction->emitter_id)
            return $this->response('unauthorized', 401);

        if(isset($transaction->emitter_id)) {
            event(new BalanceChangeEvent(-$transaction->amount, $this->user->find($transaction->receiver_id)));
            event(new BalanceChangeEvent($transaction->amount, $this->user->find($transaction->emitter_id)));
        }
        else {
            event(new BalanceChangeEvent(-$transaction->amount, $this->user->find($transaction->receiver_id)));
        }

        $transaction->delete();

        return response()->json([
            'messages' => Message::get('transaction.delete.success'),
            'data' => $transaction
        ],
        200);
    }
}
