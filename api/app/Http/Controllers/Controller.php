<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Providers\Helpers\Message;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    /**
     * Return the list of all entities
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(Request $request)
    {
        return response()->json(
            $this->model->filter($request)->get()
        , 200);
    }

    /**
     * Return an entity
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getOne(Request $request, int $id)
    {
        $entity = $this->model->find($id);

        // ABORT if not found
        if (!isset($entity))
            return $this->response('not_found', 404);

        return response()->json($entity, 200);
    }

    /**
     * Return the count of entities
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCount(Request $request)
    {
        return response()->json(
            ['count' => $this->model->filter($request)->count()]
        , 200);
    }

    /**
     * Return a validation failed message as a response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function validation_fail() {
        return response()->json([
            'messages' => $this->model->errors(),
        ], 403);
    }

    /**
     * Return any message as a response with specific http code
     * @param string $key
     * @param int $http_code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function response(string $key, int $http_code) {
        return response()->json([
            'messages' => Message::get($key),
        ], $http_code);
    }
}
