<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\Providers\Helpers\Message;
use Illuminate\Http\Request;

class PromotionController extends Controller {

    /**
     * @var Promotion
     */
    protected $model;

    /**
     * PromotionController constructor.
     * @param Promotion $model
     */
    public function __construct(Promotion $model)
    {
        $this->model = $model;
    }

    /**
     * Add a new promotion
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addPromotion(Request $request)
    {
        $params = $request->only('year', 'fb_group_id', 'list_email');
        $rules = ['required' => ['year', 'list_email']];

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-create-promotions"))
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        $promotion = $this->model->make($params);
        $promotion->save();

        return response()->json([
            'messages' => Message::get('promotion.add.success'),
            'inserted_id' => $promotion->id,
        ], 200);
    }

    /**
     * Update a promotion
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updatePromotion(Request $request, int $id) {

        $params = $request->only('year', 'fb_group_id', 'list_email');

        $promotion = $this->model->find($id);

        //ABORT if not found
        if(!isset($promotion))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-update-promotions"))
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params))
            return $this->validation_fail();

        $promotion->update($params);

        return response()->json([
            'messages' => Message::get('promotion.update.success'),
            'updated_id' => $promotion->id,
        ], 200);
    }

    /**
     * Delete a promotion
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deletePromotion(Request $request, int $id) {
        $promotion = $this->model->find($id);

        // ABORT if not found
        if(!isset($promotion))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan("can-delete-promotions"))
            return $this->response('unauthorized', 401);

        $promotion->delete();

        return response()->json([
            'messages' => Message::get('promotion.delete.success'),
            'data' => $promotion,
        ],
        200);
    }
}
