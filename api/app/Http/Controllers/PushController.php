<?php

namespace App\Http\Controllers;

use Auth;
use Notification;
use App\User;
use Illuminate\Http\Request;
use App\Notifications\PushDemo;
use NotificationChannels\WebPush\PushSubscription;

class PushController extends Controller
{
    /**
     * Store user's subscription.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, ['endpoint' => 'required']);

        $request->user()->updatePushSubscription(
            $request->endpoint,
            $request->publicKey,
            $request->authToken,
            $request->contentEncoding
        );

        return $this->response('push.success', 204);
    }

    /**
     * Update user's subscription.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'oldSubscription' => 'required',
            'newSubscription' => 'required',
        ]);

        $subscription = PushSubscription::where('endpoint', $request->oldSubscription)->first();

        if(isset($subscription)) {
            $user = User::where('id', $subscription->subscriable_id)->first();

            if(isset($user)) {
                $user->updatePushSubscription(
                    $request->endpoint,
                    $request->publicKey,
                    $request->authToken,
                    $request->contentEncoding
                );
            }
        }

        return $this->response('push.update.success', 204);
    }
    
    /**
     * Delete the specified subscription.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $this->validate($request, ['endpoint' => 'required']);
        $request->user()->deletePushSubscription($request->endpoint);
        return response()->json('push.delete.success', 204);
    }
}