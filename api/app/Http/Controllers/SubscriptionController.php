<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\Event;
use App\Providers\Helpers\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SubscriptionController extends Controller {

    /**
     * @var Subscription
     */
    protected $model;

    /**
     * @var Event
     */
    private $event;

    /**
     * SubscriptionController constructor.
     * @param Subscription $model
     */
    public function __construct(Subscription $model, Event $event)
    {
        $this->model = $model;
        $this->event = $event;
    }

    /**
     * Get the subscription of the user querying for a specific event
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getSubscriptionOf(Request $request, int $id)
    {
        $subscription = $this->model->where('event_id', $id)->where('user_id', $request->user()->id)->first();

        return response()->json(
            $subscription
            , 200);
    }

    /**
     * Get all subscriptions of an event
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubscriptionsOf(Request $request, int $id)
    {
        $subscriptions = $this->model->filter($request)->where('event_id', $id);

        return response()->json(
            $subscriptions->get()
            , 200);
    }

    /**
     * Add a new subscription
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addSubscription(Request $request)
    {
        $params = $request->only('id', 'event_id', 'user_id', 'payment_method', 'cart');
        $rules = ['required' => ['event_id', 'user_id', 'payment_method']];

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-create-events') && $request->user()->id != $params['user_id'])
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();
        
        $event = $this->event->find($params['event_id']);

        // ABORT if duplicate subscription
            if($this->model->where('event_id', $params['event_id'])->where('user_id', $params['user_id'])->count() > 0)
                return $this->response('subscription.add.duplicate', 403);

        // The following tests are only run if the user has no special permission
        if(!$request->user()->tokenCan('can-create-events')) {
            // ABORT if event is not yet validated or already checked out
            if($event->status != 1)
                return $this->response('subscription.add.impossible', 403);

            // ABORT if subscription is done past the event subscriptions end date
            if(time() > strtotime($event->subscriptions_end_at))
                return $this->response('subscription.add.closed', 403);

            // ABORT if event is full
            if(isset($event->max_subscribers) && $this->model->where('event_id', $params['event_id'])->count() >= $event->max_subscribers)
                return $this->response('subscription.add.full', 403);
        }

        $subscription = $this->model->create($params);
        $subscription->save();

        return response()->json([
            'messages' => Message::get('subscription.add.success'),
            'inserted_id' => $subscription->id,
        ], 200);
    }

    /**
     * Update a new subscription
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateSubscription(Request $request, int $id)
    {
        $params = $request->only('id', 'payment_method', 'cart');

        $subscription = $this->model->find($id);

        // ABORT if not found
        if (!isset($subscription))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-update-events') && $request->user()->id != $subscription->user_id)
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params))
            return $this->validation_fail();

        $subscription->update($params);

        return response()->json([
            'messages' => Message::get('subscription.update.success'),
            'updated_id' => $subscription->id
        ], 200);
    }

    /**
     * Delete a subscription
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteSubscription(Request $request, int $id)
    {
        $subscription = $this->model->find($id);

        //ABORT if not found
        if(!isset($subscription))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-delete-events') && $request->user()->id != $subscription->user_id)
            return $this->response('unauthorized', 401);

        $subscription->delete();

        return response()->json([
            'messages' => Message::get('subscription.delete.success'),
            'data' => $subscription,
        ],
        200);
    }
}
