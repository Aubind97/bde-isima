<?php

namespace App\Http\Controllers;

use App\Club;
use App\Providers\Helpers\Message;
use App\Providers\Helpers\Uploader;
use Illuminate\Http\Request;

class ClubController extends Controller {

    /**
     * @var Club
     */
    protected $model;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * ClubController constructor.
     * @param Club $model
     * @param Uploader $uploader
     */
    public function __construct(Club $model, Uploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
        $this->uploader->setPath('clubs');
    }

    /**
     * Add a new club
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addClub(Request $request) {
        $params = $request->only('name', 'email', 'description', 'facebook', 'twitter', 'instagram', 'logo');
        $rules = [
            'required' => ["name", "email", "description"],
            'rules' => [
                "name" => "unique:clubs,name",
                "email" => "unique:clubs,email",
            ],
        ];

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-create-clubs'))
            return $this->response('unauthorized', 401);

        // ABORT if there are bad data
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        // Upload the image if there is one
        $params = $this->uploader->uploadFile($request, $params, 'logo');

        $club = $this->model->make($params);
        $club->save();

        return response()->json([
            'messages' => Message::get('club.add.success'),
            'inserted_id' => $club->id,
        ], 200);
    }

    /**
     * Update a club
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateClub(Request $request, int $id) {

        $params = $request->only('name', 'email', 'description', 'facebook', 'twitter', 'instagram', 'logo');

        $club = $this->model->find($id);

        // ABORT if not found
        if(!isset($club))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-update-clubs') || !$request->user()->tokenCan("can-update-$club->name"))
            return $this->response('unauthorized', 401);

        $rules = [
            'rules' => [
                'name' => "unique:clubs,name,$club->id",
            'email' => "unique:clubs,email,$club->id"
            ],
        ];

        // ABORT if there are bad data
        if (!$this->model->validate($params, $rules))
            return $this->validation_fail();

        // Upload the image if there is one
        $params = $this->uploader->uploadFile($request, $params, 'logo', $club->image);

        $club->update($params);

        return response()->json([
            'messages' => Message::get('club.update.success'),
            'updated_id' => $club->id,
        ], 200);
    }

    /**
     * Delete a club
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteClub(Request $request, int $id) {
        $club = $this->model->find($id);

        // ABORT if not found
        if(!isset($club))
            return $this->response('not_found', 404);

        // ABORT if unauthorized
        if (!$request->user()->tokenCan('can-delete-clubs'))
            return $this->response('unauthorized', 401);
        
        $this->uploader->delete($club->logo);
        $club->delete();

        return response()->json([
            'messages' => Message::get('club.delete.success'),
            'data' => $club,
        ],
        200);
    }
}
