<?php

namespace App\Http\Middleware;

use App\Client;
use App\Providers\Helpers\Message;
use Illuminate\Support\Facades\Validator;

use Closure;

class CheckClientDomain
{
    /**
     * @var Client
     */
    protected $model;

    /**
     * Create a new middleware instance.
     *
     * @param Client $model
     * @return void
     */
    public function __construct(Client $model)
    {
        $this->model = $model;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make($request->only('client_id', 'client_secret'), [
            "client_id" => "required|integer",
            "client_secret" => "required|string",
        ]);

        // ABORT if there are bad data
        if ($validator->fails())
            return response()->json([
                'messages' => $validator->errors()->all(),
            ], 403);

        $client = $this->model->find($request->input('client_id'));
        $host = $request->getHost();

        if($client->domain_name == $host || $host == 'localhost') {
            return $next($request);
        }

        return response()->json([
            'messages' => Message::get('bad_domain_name'),
            'error' => "Expected $client->domain_name, got $host"
        ], 403);
    }
}
