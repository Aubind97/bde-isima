<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class MergeScopes
{
    /**
     * @var User
     */
    protected $model;

    /**
     * Create a new controller instance.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $params = $request->only('username');

        $user = $this->model->findForPassport(@$params['username']);

        if($user && $user->scopes) {
            $request->merge([
                'scope' => array_map(function($scope) {
                    return $scope['id'];
                }, $user->scopes),
            ]);
        }
        else {
            $request->merge([
                'scope' => null,
            ]);
        }

        return $next($request);
    }
}
