<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class Transaction
 * @mixin Builder
 * @package App
 */
class Transaction extends ValidateModel
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'amount',
        'description',
        'emitter_id',
        'emitter_name',
        'receiver_id',
        'receiver_name',
        'article_id',
        'prev_receiver_balance',
        'prev_emitter_balance',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
    ];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [
        'amount' => 'required|numeric|max:999.99',
        'description'  => 'required|string|max:100',
        'receiver_id'  => 'required|exists:users,id|integer',
        'receiver_name'  => 'required|string|max:50',
        'emitter_id'  => 'sometimes|nullable|exists:users,id|integer',
        'emitter_name'  => 'sometimes|nullable|string|max:50',
        'article_id'  => 'sometimes|nullable|exists:articles,id|integer',
    ];

    /**
     * Get the article that owns the transaction.
     */
    public function article()
    {
        return $this->belongsTo('App\Article');
    }

    /**
     * Return all transaction with the asked user
     * @param int $id
     * @return Transaction|Builder
     */
    public function findOfUser(int $id)
    {
        $transactions = $this;

        $transactions = $this
            ->orWhere('emitter_id', $id)
            ->orWhere('receiver_id', $id);

        return $transactions;
    }
}
