<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

// TODO: Test authorizations in addition of logging

class ArticleTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Define the base url
     * @var string
     */
    private $base_url = '/api/articles';

    /**
     * Get all articles test
     *
     * @return void
     */
    public function testGetAllArticles()
    {
        factory(\App\Article::class, 3)->create();

        $this->get($this->base_url);

        $this->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'id',
                    'name', 
                    'price', 
                    'member_price', 
                    'stock', 
                    'photo',
                    'created_at',
                    'updated_at',
                ]
            ]);

        $content = json_decode($this->response->getContent());
        $this->assertEquals(3, sizeof($content));
    }

    /**
     * Get an article test
     *
     * @return void
     */
    public function testGetArticle()
    {
        $article = factory(\App\Article::class, 'standard')->create();
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals((array)$article->getAttributes());
    }

    /**
     * Get an articles tests that does not exists
     *
     * @return void
     * @throws Exception
     */
    public function testGetUnexistingArticle()
    {
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(404)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('article.not_found'),
            ]);
    }

    /**
     * Add an article
     *
     * @throws Exception
     */
    public function testAddArticle()
    {
        $user = factory(App\User::class, 'connected')->create();

        $article = factory(App\Article::class, 'standard')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$article->getAttributes());

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('article.add.success'),
                'inserted_id' => 1,
            ]);
    }

    /**
     * Add an article with errors
     *
     * @throws Exception
     */
    public function testAddArticleWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();

        $article = factory(App\Article::class, 'invalid')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$article->getAttributes());

        $article->validate((array)$article->getAttributes());

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'errors' => $article->errors(),
                'message' => \App\Providers\Helpers\Message::get('article.validation.fail'),
            ]);
    }

    /**
     * Add an article with no connection
     *
     * @throws Exception
     */
    public function testAddArticleUnlogged()
    {
        $article = factory(App\Article::class, 'standard')->make();
        $this->post($this->base_url, (array)$article->getAttributes());

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Update an article
     *
     * @throws Exception
     */
    public function testUpdateArticle()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\Article::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['name' => 'New name']);

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('article.update.success'),
                'updated_id' => 1,
            ]);
    }

    /**
     * Update an article with errors
     *
     * @throws Exception
     */
    public function testUpdateArticleWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\Article::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url, ['price' => 'invalid price']);

        $this->seeStatusCode(403)
            ->seeJsonStructure([
                'errors', 'message',
            ]);
    }

    /**
     * Update unexisting article
     *
     * @throws Exception
     */
    public function testUpdateUnexistingArticle()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['name' => 'new name']);

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('article.not_found'),
            ]);
    }

    /**
     * Add an article with no connection
     *
     * @throws Exception
     */
    public function testUpdateArticleUnlogged()
    {
        factory(App\Article::class, 'standard')->create();

        $this->post($this->base_url . '/1', ['name' => 'new name']);

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Delete an article
     */
    public function testDeleteArticle()
    {
        $user = factory(App\User::class, 'connected')->create();
        $article = factory(App\Article::class, 'standard')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('article.delete'),
                'deleted_item' => (array)$article->getAttributes(),
            ]);
    }

    /**
     * Delete unexisting article
     */
    public function testDeleteUnexistingArticle()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('article.not_found'),
            ]);
    }

    /**
     * Delete an article with no connection
     * @throws Exception
     */
    public function testDeleteArticleUnlogged()
    {
        factory(App\Article::class, 'standard')->create();

        $this->delete($this->base_url . '/1');

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }
}
