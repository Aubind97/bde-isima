<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

// TODO: Test authorizations in addition of logging

class PartnerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Define the base url
     * @var string
     */
    private $base_url = '/api/partners';

    /**
     * Get all partners test
     *
     * @return void
     */
    public function testGetAllPartners()
    {
        factory(\App\Partner::class, 3)->create();

        $this->get($this->base_url);

        $this->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'logo',
                    'description',
                ]
            ]);

        $content = json_decode($this->response->getContent());
        $this->assertEquals(3, sizeof($content));
    }

    /**
     * Get a partner test
     *
     * @return void
     */
    public function testGetPartner()
    {
        $partner = factory(\App\Partner::class, 'standard')->create();
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals((array)$partner->getAttributes());
    }

    /**
     * Get a partner tests that does not exists
     *
     * @return void
     * @throws Exception
     */
    public function testGetUnexistingPartner()
    {
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(404)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('partner.not_found'),
            ]);
    }

    /**
     * Add a partner
     *
     * @throws Exception
     */
    public function testAddPartner()
    {
        $user = factory(App\User::class, 'connected')->create();

        $partner = factory(App\Partner::class, 'standard')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$partner->getAttributes());

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('partner.add.success'),
                'inserted_id' => 1,
            ]);
    }

    /**
     * Add a partner with errors
     *
     * @throws Exception
     */
    public function testAddPartnerWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();

        $partner = factory(App\Partner::class, 'invalid')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$partner->getAttributes());

        $partner->validate((array)$partner->getAttributes());

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'errors' => $partner->errors(),
                'message' => \App\Providers\Helpers\Message::get('partner.validation.fail'),
            ]);
    }

    /**
     * Add a partner with no connection
     *
     * @throws Exception
     */
    public function testAddPartnerUnlogged()
    {
        $partner = factory(App\Partner::class, 'standard')->make();
        $this->post($this->base_url, (array)$partner->getAttributes());

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Update a partner
     *
     * @throws Exception
     */
    public function testUpdatePartner()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\Partner::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['name' => 'New name']);

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('partner.update.success'),
                'updated_id' => 1,
            ]);
    }

    /**
     * Update a partner with errors
     *
     * @throws Exception
     */
    public function testUpdatePartnerWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\Partner::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url, ['logo' => 10]);

        $this->seeStatusCode(403)
            ->seeJsonStructure([
                'errors', 'message',
            ]);
    }

    /**
     * Update unexisting partner
     *
     * @throws Exception
     */
    public function testUpdateUnexistingPartner()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['name' => 'New name']);

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('partner.not_found'),
            ]);
    }

    /**
     * Add a partner with no connection
     *
     * @throws Exception
     */
    public function testUpdatePartnerUnlogged()
    {
        factory(App\Partner::class, 'standard')->create();

        $this->post($this->base_url . '/1', ['name' => 'New name']);

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Delete a partner
     */
    public function testDeletePartner()
    {
        $user = factory(App\User::class, 'connected')->create();
        $partner = factory(App\Partner::class, 'standard')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('partner.delete'),
                'deleted_item' => (array)$partner->getAttributes(),
            ]);
    }

    /**
     * Delete unexisting partner
     */
    public function testDeleteUnexistingPartner()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('partner.not_found'),
            ]);
    }

    /**
     * Delete a partner with no connection
     * @throws Exception
     */
    public function testDeletePartnerUnlogged()
    {
        factory(App\Partner::class, 'standard')->create();

        $this->delete($this->base_url . '/1');

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }
}
