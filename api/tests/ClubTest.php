<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

// TODO: Test authorizations in addition of logging

class ClubTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Define the base url
     * @var string
     */
    private $base_url = '/api/clubs';

    /**
     * Get all clubs test
     *
     * @return void
     */
    public function testGetAllClubs()
    {
        factory(\App\Club::class, 3)->create();

        $this->get($this->base_url);

        $this->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'id',
                    'logo',
                    'name',
                    'email',
                    'description',
                    'facebook',
                    'twitter',
                    'instagram',
                    'created_at',
                    'updated_at',
                ]
            ]);

        $content = json_decode($this->response->getContent());
        $this->assertEquals(3, sizeof($content));
    }

    /**
     * Get a club test
     *
     * @return void
     */
    public function testGetClub()
    {
        $club = factory(\App\Club::class, 'standard')->create();
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals((array)$club->getAttributes());
    }

    /**
     * Get a club test that does not exists
     *
     * @return void
     * @throws Exception
     */
    public function testGetUnexistingClub()
    {
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(404)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('club.not_found'),
            ]);
    }

    /**
     * Add a club
     *
     * @throws Exception
     */
    public function testAddClub()
    {
        $user = factory(App\User::class, 'connected')->create();

        $club = factory(App\Club::class, 'standard')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$club->getAttributes());

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('club.add.success'),
                'inserted_id' => 1,
            ]);
    }

    /**
     * Add a club with errors
     *
     * @throws Exception
     */
    public function testAddClubWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();

        $club = factory(App\Club::class, 'invalid')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$club->getAttributes());

        $club->validate((array)$club->getAttributes());

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'errors' => $club->errors(),
                'message' => \App\Providers\Helpers\Message::get('club.validation.fail'),
            ]);
    }

    /**
     * Add a club with no connection
     *
     * @throws Exception
     */
    public function testAddClubUnlogged()
    {
        $club = factory(App\Club::class, 'standard')->make();
        $this->post($this->base_url, (array)$club->getAttributes());

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Update an club
     *
     * @throws Exception
     */
    public function testUpdateClub()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\Club::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['name' => 'New name']);

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('club.update.success'),
                'updated_id' => 1,
            ]);
    }

    /**
     * Update a club with errors
     *
     * @throws Exception
     */
    public function testUpdateClubWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\Club::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url, ['email' => 'Invalid email']);

        $this->seeStatusCode(403)
            ->seeJsonStructure([
                'errors', 'message',
            ]);
    }

    /**
     * Update unexisting club
     *
     * @throws Exception
     */
    public function testUpdateUnexistingClub()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['name' => 'New name']);

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('club.not_found'),
            ]);
    }

    /**
     * Add a club with no connection
     *
     * @throws Exception
     */
    public function testUpdateClubUnlogged()
    {
        factory(App\Club::class, 'standard')->create();

        $this->post($this->base_url . '/1', ['name' => 'New name']);

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Delete a club
     */
    public function testDeleteClub()
    {
        $user = factory(App\User::class, 'connected')->create();
        $club = factory(App\Club::class, 'standard')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('club.delete'),
                'deleted_item' => (array)$club->getAttributes(),
            ]);
    }

    /**
     * Delete unexisting club
     */
    public function testDeleteUnexistingClub()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('club.not_found'),
            ]);
    }

    /**
     * Delete a club with no connection
     * @throws Exception
     */
    public function testDeleteClubUnlogged()
    {
        factory(App\Club::class, 'standard')->create();

        $this->delete($this->base_url . '/1');

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }
}
