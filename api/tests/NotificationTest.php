<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

// TODO: Test authorizations in addition of logging

class NotificationTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Define the base url
     * @var string
     */
    private $base_url = '/api/notifications';

    /**
     * Get all notifications test
     *
     * @return void
     */
    public function testGetAllNotifications()
    {
        factory(\App\Notification::class, 3)->create();

        $this->get($this->base_url);

        $this->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'id',
                    'message',
                    'created_at',
                    'is_read',
                    'receiver_id',
                ]
            ]);

        $content = json_decode($this->response->getContent());
        $this->assertEquals(3, sizeof($content));
    }

    /**
     * Get a notification test
     *
     * @return void
     */
    public function testGetNotification()
    {
        $notification = factory(\App\Notification::class, 'standard')->create();
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals((array)$notification->getAttributes());
    }

    /**
     * Get a notification tests that does not exists
     *
     * @return void
     * @throws Exception
     */
    public function testGetUnexistingNotification()
    {
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(404)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('notification.not_found'),
            ]);
    }

    /**
     * Add a notification
     *
     * @throws Exception
     */
    public function testAddNotification()
    {
        $user = factory(App\User::class, 'connected')->create();

        $notification = factory(App\Notification::class, 'standard')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$notification->getAttributes());

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('notification.add.success'),
                'inserted_id' => 1,
            ]);
    }

    /**
     * Add a notification with errors
     *
     * @throws Exception
     */
    public function testAddNotificationWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();

        $notification = factory(App\Notification::class, 'invalid')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$notification->getAttributes());

        $notification->validate((array)$notification->getAttributes());

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'errors' => $notification->errors(),
                'message' => \App\Providers\Helpers\Message::get('notification.validation.fail'),
            ]);
    }

    /**
     * Add a notification with no connection
     *
     * @throws Exception
     */
    public function testAddNotificationUnlogged()
    {
        $notification = factory(App\Notification::class, 'standard')->make();
        $this->post($this->base_url, (array)$notification->getAttributes());

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Update a notification
     *
     * @throws Exception
     */
    public function testUpdateNotification()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\Notification::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['message' => 'New message']);

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('notification.update.success'),
                'updated_id' => 1,
            ]);
    }

    /**
     * Update a notification with errors
     *
     * @throws Exception
     */
    public function testUpdateNotificationWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\Notification::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url, ['is_read' => 10]);

        $this->seeStatusCode(403)
            ->seeJsonStructure([
                'errors', 'message',
            ]);
    }

    /**
     * Update unexisting notification
     *
     * @throws Exception
     */
    public function testUpdateUnexistingNotification()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['message' => 'New message']);

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('notification.not_found'),
            ]);
    }

    /**
     * Add a notification with no connection
     *
     * @throws Exception
     */
    public function testUpdateNotificationUnlogged()
    {
        factory(App\Notification::class, 'standard')->create();

        $this->post($this->base_url . '/1', ['message' => 'New message']);

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Delete a notification
     */
    public function testDeleteNotification()
    {
        $user = factory(App\User::class, 'connected')->create();
        $notification = factory(App\Notification::class, 'standard')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('notification.delete'),
                'deleted_item' => (array)$notification->getAttributes(),
            ]);
    }

    /**
     * Delete unexisting notification
     */
    public function testDeleteUnexistingNotification()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('notification.not_found'),
            ]);
    }

    /**
     * Delete a notification with no connection
     * @throws Exception
     */
    public function testDeleteNotificationUnlogged()
    {
        factory(App\Notification::class, 'standard')->create();

        $this->delete($this->base_url . '/1');

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }
}
