<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

// TODO: Test authorizations in addition of logging

class TransactionTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Define the base url
     * @var string
     */
    private $base_url = '/api/transactions';

    /**
     * Get all transactions test
     *
     * @return void
     */
    public function testGetAllTransactions()
    {
        factory(\App\Transaction::class, 3)->create();

        $this->get($this->base_url);

        $this->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'id',
                    'amount',
                    'description',
                    'emitter_id',
                    'emitter_name',
                    'receiver_id',
                    'receiver_name',
                ]
            ]);

        $content = json_decode($this->response->getContent());
        $this->assertEquals(3, sizeof($content));
    }

    /**
     * Get a transaction test
     *
     * @return void
     */
    public function testGetTransaction()
    {
        $transaction = factory(\App\Transaction::class, 'standard')->create();
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals((array)$transaction->getAttributes());
    }

    /**
     * Get a transaction tests that does not exists
     *
     * @return void
     * @throws Exception
     */
    public function testGetUnexistingTransaction()
    {
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(404)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('transaction.not_found'),
            ]);
    }

    /**
     * Add a transaction
     *
     * @throws Exception
     */
    public function testAddTransaction()
    {
        $transaction = factory(App\Transaction::class, 'connected')->create();

        $transaction = factory(App\Transaction::class, 'standard')->make();
        $this->actingAs($transaction)
            ->post($this->base_url, (array)$transaction->getAttributes());

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('transaction.add.success'),
                'inserted_id' => 1,
            ]);
    }

    /**
     * Add a transaction with errors
     *
     * @throws Exception
     */
    public function testAddTransactionWithErrors()
    {
        $transaction = factory(App\Transaction::class, 'connected')->create();

        $transaction = factory(App\Transaction::class, 'invalid')->make();
        $this->actingAs($transaction)
            ->post($this->base_url, (array)$transaction->getAttributes());

        $transaction->validate((array)$transaction->getAttributes());

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'errors' => $transaction->errors(),
                'message' => \App\Providers\Helpers\Message::get('transaction.validation.fail'),
            ]);
    }

    /**
     * Add a transaction with no connection
     *
     * @throws Exception
     */
    public function testAddTransactionUnlogged()
    {
        $transaction = factory(App\Transaction::class, 'standard')->make();
        $this->post($this->base_url, (array)$transaction->getAttributes());

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Update a transaction
     *
     * @throws Exception
     */
    public function testUpdateTransaction()
    {
        $transaction = factory(App\Transaction::class, 'connected')->create();
        factory(App\Transaction::class, 'standard')->create();

        $this->actingAs($transaction)
            ->post($this->base_url . '/1', ['amount' => 42]);

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('transaction.update.success'),
                'updated_id' => 1,
            ]);
    }

    /**
     * Update a transaction with errors
     *
     * @throws Exception
     */
    public function testUpdateTransactionWithErrors()
    {
        $transaction = factory(App\Transaction::class, 'connected')->create();
        factory(App\Transaction::class, 'standard')->create();

        $this->actingAs($transaction)
            ->post($this->base_url, ['amount' => 'Invalid amount']);

        $this->seeStatusCode(403)
            ->seeJsonStructure([
                'errors', 'message',
            ]);
    }

    /**
     * Update unexisting transaction
     *
     * @throws Exception
     */
    public function testUpdateUnexistingTransaction()
    {
        $transaction = factory(App\Transaction::class, 'connected')->create();

        $this->actingAs($transaction)
            ->post($this->base_url . '/1', ['amount' => 42]);

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('transaction.not_found'),
            ]);
    }

    /**
     * Add a transaction with no connection
     *
     * @throws Exception
     */
    public function testUpdateTransactionUnlogged()
    {
        factory(App\Transaction::class, 'standard')->create();

        $this->post($this->base_url . '/1', ['amount' => 10]);

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Delete a transaction
     */
    public function testDeleteTransaction()
    {
        $transaction = factory(App\Transaction::class, 'connected')->create();
        $transaction = factory(App\Transaction::class, 'standard')->create();

        $this->actingAs($transaction)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('transaction.delete'),
                'deleted_item' => (array)$transaction->getAttributes(),
            ]);
    }

    /**
     * Delete unexisting transaction
     */
    public function testDeleteUnexistingTransaction()
    {
        $transaction = factory(App\Transaction::class, 'connected')->create();

        $this->actingAs($transaction)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('transaction.not_found'),
            ]);
    }

    /**
     * Delete a transaction with no connection
     * @throws Exception
     */
    public function testDeleteTransactionUnlogged()
    {
        factory(App\Transaction::class, 'standard')->create();

        $this->delete($this->base_url . '/1');

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }
}
