<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

// TODO: Test authorizations in addition of logging

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Define the base url
     * @var string
     */
    private $base_url = '/api/users';

    /**
     * Get all users test
     *
     * @return void
     */
    public function testGetAllUsers()
    {
        factory(\App\User::class, 3)->create();

        $this->get($this->base_url);

        $this->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'id',
                    'firstname',
                    'lastname',
                    'email',
                    'card',
                    'balance',
                    'is_member',
                    'picture',
                    'nickname',
                    'promotion',
                    'preferences',
                    'authorizations',
                    'password',
                    'access_token',
                    'access_expires_at',
                    'password_reset_token',
                    'password_reset_at',
                ]
            ]);

        $content = json_decode($this->response->getContent());
        $this->assertEquals(3, sizeof($content));
    }

    /**
     * Get a user test
     *
     * @return void
     */
    public function testGetUser()
    {
        $user = factory(\App\User::class, 'standard')->create();
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals((array)$user->getAttributes());
    }

    /**
     * Get a user tests that does not exists
     *
     * @return void
     * @throws Exception
     */
    public function testGetUnexistingUser()
    {
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(404)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('user.not_found'),
            ]);
    }

    /**
     * Add a user
     *
     * @throws Exception
     */
    public function testAddUser()
    {
        $user = factory(App\User::class, 'connected')->create();

        $user = factory(App\User::class, 'standard')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$user->getAttributes());

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('user.add.success'),
                'inserted_id' => 1,
            ]);
    }

    /**
     * Add a user with errors
     *
     * @throws Exception
     */
    public function testAddUserWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();

        $user = factory(App\User::class, 'invalid')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$user->getAttributes());

        $user->validate((array)$user->getAttributes());

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'errors' => $user->errors(),
                'message' => \App\Providers\Helpers\Message::get('user.validation.fail'),
            ]);
    }

    /**
     * Add a user with no connection
     *
     * @throws Exception
     */
    public function testAddUserUnlogged()
    {
        $user = factory(App\User::class, 'standard')->make();
        $this->post($this->base_url, (array)$user->getAttributes());

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Update a user
     *
     * @throws Exception
     */
    public function testUpdateUser()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\User::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['firstname' => 'New firstname']);

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('user.update.success'),
                'updated_id' => 1,
            ]);
    }

    /**
     * Update a user with errors
     *
     * @throws Exception
     */
    public function testUpdateUserWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\User::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url, ['firstname' => 10]);

        $this->seeStatusCode(403)
            ->seeJsonStructure([
                'errors', 'message',
            ]);
    }

    /**
     * Update unexisting user
     *
     * @throws Exception
     */
    public function testUpdateUnexistingUser()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['firstname' => 'New firstname']);

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('user.not_found'),
            ]);
    }

    /**
     * Add a user with no connection
     *
     * @throws Exception
     */
    public function testUpdateUserUnlogged()
    {
        factory(App\User::class, 'standard')->create();

        $this->post($this->base_url . '/1', ['firstname' => 'New firstname']);

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Delete a user
     */
    public function testDeleteUser()
    {
        $user = factory(App\User::class, 'connected')->create();
        $user = factory(App\User::class, 'standard')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('user.delete'),
                'deleted_item' => (array)$user->getAttributes(),
            ]);
    }

    /**
     * Delete unexisting user
     */
    public function testDeleteUnexistingUser()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('user.not_found'),
            ]);
    }

    /**
     * Delete a user with no connection
     * @throws Exception
     */
    public function testDeleteUserUnlogged()
    {
        factory(App\User::class, 'standard')->create();

        $this->delete($this->base_url . '/1');

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }
}
