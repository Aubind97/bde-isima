<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

// TODO: Test authorizations in addition of logging

class NewsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Define the base url
     * @var string
     */
    private $base_url = '/api/news';

    /**
     * Get all news test
     *
     * @return void
     */
    public function testGetAllNews()
    {
        factory(\App\News::class, 3)->create();

        $this->get($this->base_url);

        $this->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'id',
                    'club_id',
                    'title',
                    'header',
                    'content',
                    'promotions',
                    'is_published',
                    'published_at',
                    'created_at',
                    'updated_at',
                ]
            ]);

        $content = json_decode($this->response->getContent());
        $this->assertEquals(3, sizeof($content));
    }

    /**
     * Get a news test
     *
     * @return void
     */
    public function testGetNews()
    {
        $news = factory(\App\News::class, 'standard')->create();
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals((array)$news->getAttributes());
    }

    /**
     * Get a news test that does not exists
     *
     * @return void
     * @throws Exception
     */
    public function testGetUnexistingNews()
    {
        $this->get($this->base_url . '/1');

        $this->seeStatusCode(404)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('news.not_found'),
            ]);
    }

    /**
     * Add a news
     *
     * @throws Exception
     */
    public function testAddNews()
    {
        $user = factory(App\User::class, 'connected')->create();

        $news = factory(App\News::class, 'standard')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$news->getAttributes());

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('news.add.success'),
                'inserted_id' => 1,
            ]);
    }

    /**
     * Add a news with errors
     *
     * @throws Exception
     */
    public function testAddNewsWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();

        $news = factory(App\News::class, 'invalid')->make();
        $this->actingAs($user)
            ->post($this->base_url, (array)$news->getAttributes());

        $news->validate((array)$news->getAttributes());

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'errors' => $news->errors(),
                'message' => \App\Providers\Helpers\Message::get('news.validation.fail'),
            ]);
    }

    /**
     * Add a news with no connection
     *
     * @throws Exception
     */
    public function testAddNewsUnlogged()
    {
        $news = factory(App\News::class, 'standard')->make();
        $this->post($this->base_url, (array)$news->getAttributes());

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Update an news
     *
     * @throws Exception
     */
    public function testUpdateNews()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\News::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['title' => 'New title']);

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('news.update.success'),
                'updated_id' => 1,
            ]);
    }

    /**
     * Update a news with errors
     *
     * @throws Exception
     */
    public function testUpdateNewsWithErrors()
    {
        $user = factory(App\User::class, 'connected')->create();
        factory(App\News::class, 'standard')->create();

        $this->actingAs($user)
            ->post($this->base_url, ['published_at' => 'Invalid date']);

        $this->seeStatusCode(403)
            ->seeJsonStructure([
                'errors', 'message',
            ]);
    }

    /**
     * Update unexisting news
     *
     * @throws Exception
     */
    public function testUpdateUnexistingNews()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->post($this->base_url . '/1', ['title' => 'New title']);

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('news.not_found'),
            ]);
    }

    /**
     * Add a news with no connection
     *
     * @throws Exception
     */
    public function testUpdateNewsUnlogged()
    {
        factory(App\News::class, 'standard')->create();

        $this->post($this->base_url . '/1', ['title' => 'New title']);

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }

    /**
     * Delete a news
     */
    public function testDeleteNews()
    {
        $user = factory(App\User::class, 'connected')->create();
        $news = factory(App\News::class, 'standard')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(200)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('news.delete'),
                'deleted_item' => (array)$news->getAttributes(),
            ]);
    }

    /**
     * Delete unexisting news
     */
    public function testDeleteUnexistingNews()
    {
        $user = factory(App\User::class, 'connected')->create();

        $this->actingAs($user)
            ->delete($this->base_url . '/1');

        $this->seeStatusCode(403)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('news.not_found'),
            ]);
    }

    /**
     * Delete a news with no connection
     * @throws Exception
     */
    public function testDeleteNewsUnlogged()
    {
        factory(App\News::class, 'standard')->create();

        $this->delete($this->base_url . '/1');

        $this->seeStatusCode(401)
            ->seeJsonEquals([
                'message' => \App\Providers\Helpers\Message::get('unauthorized'),
            ]);
    }
}
