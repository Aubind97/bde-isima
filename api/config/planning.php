<?php

function weekOfAvailability(string $date) {

    $times = [
        [
            from    => '09:50', 
            to      => '10:10',
        ],
        [
            from    => '12:00', 
            to      => '13:30',
        ],
        [
            from    => '15:20', 
            to      => '15:40',
        ],
        [
            from    => '17:30', 
            to      => '19:30',
        ]
        [
            from    => '19:30', 
            to      => '21:00',
        ],
        [
            from    => '21:00', 
            to      => '23:59',
        ]
    ];

    return [
        'threshold' => 6,
        'events' => [
            
        ],
    ];
}
