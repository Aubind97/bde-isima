<?php

return [

    'default' => env('DB_CONNECTION'),

    'migrations' => 'migrations',

    'connections' => [
        

        # Primary/Default database connection
        'mysql' => [
            'driver'    => env('DB_CONNECTION'),
            'host'      => env('DB_HOST'),
            'database'  => env('DB_DATABASE'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'prefix'    => '',
        ],

        # Secondary database connection
        'old_db' => [
            'driver'    => env('DB_CONNECTION'),
            'host'      => env('DB_HOST'),
            'database'  => env('DB_OLD_DATABASE'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'prefix'    => '',
        ],
    ],
];
