<?php

return [
    'locale' => env('APP_LOCAL'),
    'fallback_locale' => 'en',
    'key' => env('APP_KEY'),
    'cipher' => env('APP_CIPHER','AES-256-CBC'),
];
