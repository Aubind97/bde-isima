<?php

return [
    'authorized' => "Authorized",
    'unauthorized' => "Not authorized",
    'bad_credentials' => "Bad credentials",
    'logout' => "Logged out",

    'password_reset' => "Email sent to reset your password",
    'password_reset.success' => "Password changed",
    'password_reset.fail' => "An error occurred during your password reset",

    'article.add.success' => "Article added",
    'article.update.success' => "Article updated",
    'article.delete.success' => "Article deleted",
    'article.not_found' => "Article not found",

    'news.add.success' => "News added",
    'news.update.success' => "News updated",
    'news.delete.success' => "News deleted",
    'news.not_found' => "News not found",

    'club.add.success' => "Club added",
    'club.update.success' => "Club updated",
    'club.delete.success' => "Club deleted",
    'club.not_found' => "Club not found",

    'event.add.success' => "Event added",
    'event.update.success' => "Event updated",
    'event.delete.success' => "Event deleted",
    'event.not_found' => "Event not found",

    'partner.add.success' => "Partner added",
    'partner.update.success' => "Partner updated",
    'partner.delete.success' => "Partner deleted",
    'partner.not_found' => "Partner not found",

    'transaction.add.success' => "Transaction proceed",
    'transaction.owner' => "You can't process a transaction for an other person",
    'transaction.negative' => "Negative transactions not allowed",
    'transaction.self' => "You can't process a self-transaction",
    'transaction.delete' => "Transaction deleted",

    'notification.add.success' => "Notification sent",
    'notification.update.success' => "Notification updated",

    'user.add.success' => "User added",
    'user.update.success' => "User updated",
    'user.delete.success' => "User deleted",
    'user.password.mismatch' => "Passwords don't match",
    'user.not_found' => "User not found",
];
