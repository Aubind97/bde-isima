<?php

use App\Club;

$messages = array_merge([
    'authorized' => "Vous êtes autorisé",
    'unauthorized' => "Vous n'êtes pas autorisé",
    'not_found' => "L'objet demandé n'existe pas",
    'login' => "Connexion réussie",
    'logout' => "Déconnecté",
    'bad_domain_name' => "Nom de domaine différent du client",

    'push.success' => "Infos push enregistrées",
    'push.update.success' => "Infos push mises à jour",
    'push.delete.success' => "Infos push supprimées",
    'push.fail' => "Infos push incorrectes",

    'com.contact.success' => "L'email a été envoyé",
    'com.feedback.success' => "Le retour a été envoyé",

    'password_reset' => "Un mail de réinitilisation de votre mot de passe vous a été envoyé",
    'password_reset.success' => "Votre mot de passe a été mis à jour",
    'password_reset.fail' => "Réinitilisation expirée, veuillez recommencer",

    'article.add.success' => "Article ajouté",
    'article.update.success' => "Article mis à jour",
    'article.update.batch.success' => "Les articles ont été mis à jour",
    'article.delete.success' => "Article supprimé",

    'news.add.success' => "News ajoutée",
    'news.update.success' => "News mise à jour",
    'news.delete.success' => "News supprimée",

    'club.add.success' => "Club ajouté",
    'club.update.success' => "Club mis à jour",
    'club.delete.success' => "Club supprimé",

    'event.add.success' => "Événement ajouté",
    'event.update.success' => "Événement mis à jour",
    'event.delete.success' => "Événement supprimé",
    'event.validation.success' => "Événement validé",
    'event.validation.fail' => "Événement déjà validé",

    'subscription.add.success' => "Inscription ajoutée",
    'subscription.add.closed' => "L'évènement est clos",
    'subscription.add.duplicate' => "Déjà inscrit",
    'subscription.add.impossible' => "L'événement ne permet pas ou plus les inscriptions",
    'subscription.add.full' => "L'événement est complet",
    'subscription.update.success' => "Inscription mise à jour",
    'subscription.delete.success' => "Inscription supprimée",

    'partner.add.success' => "Partenaire ajouté",
    'partner.update.success' => "Partenaire mis à jour",
    'partner.delete.success' => "Partenaire supprimé",

    'transaction.add.success' => "Transaction effectuée",
    'transaction.negative' => "Transaction négative non-autorisée",
    'transaction.self' => "Vous ne pouvez pas faire de transactions vers vous-même",
    'transaction.sold_out' => "Solde insuffisant",
    'transaction.owner' => "Vous ne pouvez pas faire de transaction pour une autre personne",
    'transaction.delete.success' => "Transaction supprimée",

    'notification.add.success' => "Notification envoyée",
    'notification.update.success' => "Notification mise à jour",
    'notification.balance.negative' => "Attention, solde négatif !",

    'user.add.success' => "Utilisateur ajouté",
    'user.update.success' => "Utilisateur modifié",
    'user.delete.success' => "Utilisateur supprimé",
    'user.revoked.success' => "Tokens d'accès supprimés",
    'user.delete.picture.success' => "Photo de profil supprimée",
    'user.password.mismatch' => "Les mots de passe ne correspondent pas",

    'promotion.add.success' => "Promotion ajoutée",
    'promotion.update.success' => "Promotion mise à jour",
    'promotion.delete.success' => "Promotion supprimée",

    'availability.update.success' => "Dispos mises à jour",

    'scopes.god' => 'Dieu',

    'scopes.listeux' => 'Listeux',

    'scopes.can-read-analytics' => 'Lecture des statistiques',

    'scopes.can-create-notifications' => 'Créer des notifications',

    'scopes.can-create-transactions' => 'Créer des transactions',
    'scopes.can-delete-transactions' => 'Supprimer des transactions',

    'scopes.can-create-users' => 'Créer des membres',
    'scopes.can-read-users' => 'Consulter les membres',
    'scopes.can-read-user' => 'Consulter un membre',
    'scopes.can-update-users' => 'Mettre à jour des membres',
    'scopes.can-delete-users' => 'Supprimer des membres',
    
    'scopes.can-create-clubs' => 'Créer des clubs',
    'scopes.can-update-clubs' => 'Mettre à jour des clubs',
    'scopes.can-delete-clubs' => 'Supprimer des clubs',

    'scopes.can-update-events' => 'Mettre à jour des events',
    'scopes.can-validate-events' => 'Encaisser des events',
    'scopes.can-delete-events' => 'Supprimer des events',

    'scopes.can-create-articles' => 'Créer des articles',
    'scopes.can-update-articles' => 'Mettre à jour des articles',
    'scopes.can-delete-articles' => 'Supprimer des articles',

    'scopes.can-create-partners' => 'Créer des partenaires',
    'scopes.can-update-partners' => 'Mettre à jour des partenaires',
    'scopes.can-delete-partners' => 'Supprimer des partenaires',

    'scopes.can-create-promotions' => 'Créer des promotions',
    'scopes.can-update-promotions' => 'Mettre à jour des promotions',
    'scopes.can-delete-promotions' => 'Supprimer des promotions',

], Club::all()->flatMap(function($club) {
    $name = strtoupper($club->name);
    return [
        "scopes.can-update-$club->name" => "Mettre à jour $name",

        "scopes.can-create-$club->name-events" => "Créer des events pour $name",
        "scopes.can-update-$club->name-events" => "Mettre à jour des events pour $name",
        "scopes.can-delete-$club->name-events" => "Supprimer des events pour $name",

        "scopes.can-create-$club->name-news" => "Créer de news pour $name",
        "scopes.can-update-$club->name-news" => "Mettre à jour des news pour $name",
        "scopes.can-delete-$club->name-news" => "Supprimer des news pour $name",
    ];
})->all());

return $messages;
