Salut {{ $firstname }} !

Tu as récemment demandé de réinitisaliser ton mot de passe pour le site du BDE ISIMA.

Clique sur le lien pour changer ton mot de passe (Le lien est valable 15 minutes, ce délai dépassé tu devras recommencer la procédure) :
{{ $link }}

Ceci est un mail automatique, il est inutile de répondre. En cas de soucis, consulter un membre du BDE. 
