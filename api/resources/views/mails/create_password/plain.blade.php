Salut {{ $firstname }} !

Ton compte BDE ISIMA vient d'être créé. Tu peux dès à présent l'utiliser en cliquant sur le lien pour choisir ton mot de passe.

{{ $link }}

Le lien est valable 1 mois, ce délai dépassé tu devras recommencer la procédure manuellement à l'adresse : https://bde.isima.fr/reset_password

Ceci est un mail automatique, il est inutile de répondre. En cas de soucis, consulter un membre du BDE. 
