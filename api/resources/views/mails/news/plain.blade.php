{{ $name }},

{{ $title }},
{{ $header }}

{!! $content !!}

{{ $published_at }}

Pour ne plus recevoir aucun mail, modifiez vos paramètres {{ $link }} (mais on vous le recommande pas).