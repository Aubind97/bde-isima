<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('clubs')){
            Schema::create('clubs', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('logo', 255)->nullable();

                $table->string('name', 50)->unique();

                $table->string('email', 100)->unique();

                $table->text('description', 3000);

                $table->string('facebook', 255)->nullable();

                $table->string('twitter', 255)->nullable();

                $table->string('instagram', 255)->nullable();
                
                $table->timestampsTz();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
