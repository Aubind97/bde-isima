<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('subscriptions')){
            Schema::create('subscriptions', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->unsignedBigInteger('event_id')->nullable();

                $table->unsignedBigInteger('user_id')->nullable();

                $table->enum('payment_method', ['bde', 'lydia', 'cash'])->default('bde');
                
                $table->json('cart');

                $table->foreign('event_id')
                    ->references('id')
                    ->on('events')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

                $table->timestampsTz();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
