<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('articles')){
            Schema::create('articles', function (Blueprint $table) {
                $table->bigIncrements('id');
                
                $table->string('name', 50);

                $table->decimal('price', 5, 2)->default(0.00);

                $table->decimal('member_price', 5, 2)->default(0.00);

                $table->unsignedInteger('stock')->default(0);

                $table->string('photo', 255)->nullable();

                //Is enabled for deactivated articles
                $table->boolean('is_enabled')->default(true);
                
                $table->timestampsTz();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
