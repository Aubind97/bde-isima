<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('transactions')){
            // Create the transactions table
            Schema::create('transactions', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->decimal('amount', 5, 2);
                $table->string('description', 100);

                $table->unsignedBigInteger('emitter_id')->nullable();
                $table->string('emitter_name', 50)->nullable();

                $table->unsignedBigInteger('receiver_id')->nullable();
                $table->string('receiver_name', 50);

                $table->unsignedBigInteger('article_id')->nullable();

                $table->decimal('prev_receiver_balance', 5, 2);
                $table->decimal('prev_emitter_balance', 5, 2)->nullable();

                $table->foreign('emitter_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

                $table->foreign('receiver_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

                $table->foreign('article_id')
                    ->references('id')
                    ->on('articles')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

                $table->timestampsTz();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
