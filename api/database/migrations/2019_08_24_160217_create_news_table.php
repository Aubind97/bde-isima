<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('news')){
            Schema::create('news', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->unsignedBigInteger('club_id')->nullable();

                $table->string('title', 255);

                $table->string('header', 100);

                $table->text('content');

                $table->boolean('is_published')->default(false);

                $table->timestampTz('published_at')->nullable();

                $table->foreign('club_id')
                    ->references('id')
                    ->on('clubs')
                    ->onUpdate('cascade')
                    ->onDelete('set null');
                
                $table->timestampsTz();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
