<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')){
            Schema::create('users', function (Blueprint $table) {
                $table->bigIncrements('id');

                //User info
                $table->string('lastname', 100);
                $table->string('firstname', 100);
                $table->string('nickname', 50)->nullable();
                $table->string('email', 100)->unique();
                $table->string('picture')->nullable();

                //Auth info
                $table->integer('card')->nullable()->unique();
                $table->string('password');
                $table->string('salt');
                
                //Password recovery
                $table->string('password_reset_token')->nullable()->unique();
                $table->timestampTz('password_reset_at')->nullable();

                //Managed by admin info
                $table->decimal('balance', 5, 2)->default(0);
                $table->boolean('is_member')->default(false);

                //User preferences
                $table->json('preferences')->nullable();

                //User scopes
                $table->json('scopes')->nullable();

                //User stats
                $table->json('stats')->nullable();

                //Foreign key on Promotions table
                $table->unsignedBigInteger('promotion_id')->nullable();
                
                $table->foreign('promotion_id')
                    ->references('id')
                    ->on('promotions')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

                //Is enabled for deactivated users
                $table->boolean('is_enabled')->default(true);

                $table->timestampsTz();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
