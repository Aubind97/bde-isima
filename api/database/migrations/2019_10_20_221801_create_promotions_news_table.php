<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions_news', function (Blueprint $table) {
            
            $table->unsignedBigInteger('promotion_id')->nullable();
            $table->unsignedBigInteger('news_id')->nullable();

            $table->foreign('promotion_id')
                ->references('id')
                ->on('promotions')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('news_id')
                ->references('id')
                ->on('news')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions_news');
    }
}
