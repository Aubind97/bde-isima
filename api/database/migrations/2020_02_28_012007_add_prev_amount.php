<?php

use App\User;
use App\Transaction;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrevAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $transactions_to_delete = Transaction::where('receiver_id', null)->get();
        foreach($transactions_to_delete as $transaction) {
            $transaction->delete();
        }

        $transactions = Transaction::orderBy('created_at', 'desc')->get();
        $users = User::all();
        $ids = [];

        foreach($transactions as $transaction) {
            //Avoid collision
            if(!in_array($transaction->id, $ids)) {
                //Retrieve receiver from local variables, not db to update his balance over the iterations 
                $receiver = $users[$users->search(function($user) use($transaction) {
                    return $user->id === $transaction->receiver_id;
                })];
                $amount = abs($transaction->amount);

                //P2P transaction
                if(isset($transaction->emitter_id)) {
                    //Retrieve emitter from local variables, not db to update his balance over the iterations
                    $emitter = $users[$users->search(function($user) use($transaction) {
                        return $user->id === $transaction->emitter_id;
                    })];

                    //Give the amount back to the emitter
                    $transaction->prev_emitter_balance = $emitter->balance + $amount;
                    $emitter->balance = $transaction->prev_emitter_balance;

                    //And take it back from the receiver
                    $transaction->prev_receiver_balance = $receiver->balance - $amount;
                    $receiver->balance = $transaction->prev_receiver_balance;
                }
                //Admin transaction
                else {
                    //Give back transaction amount as is by inverting sign (with -)
                    $transaction->prev_receiver_balance = $receiver->balance - $transaction->amount;
                    $receiver->balance = $transaction->prev_receiver_balance;
                }

                $ids[] = $transaction->id;
                $transaction->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
