<?php

use App\User;
use App\Article;
use App\Events\ArticleStatsEvent;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecoverOldStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $transactions = DB::connection('old_db')->table('transactions')->whereNotNull('id_article')->get();

        foreach($transactions as $transaction) {
            $old_article = DB::connection('old_db')->table('articles')->where('id', $transaction->id_article)->first();
            $new_article = Article::where('name', $old_article->nom)->first();

            if(isset($new_article)) {
                $old_user = DB::connection('old_db')->table('membres')->where('id', $transaction->id_personne)->first();
                $new_user = User::where('card', $old_user->numero)->first();
                
                if(isset($new_user)) {
                    event(new ArticleStatsEvent($new_user, $new_article->id));
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
