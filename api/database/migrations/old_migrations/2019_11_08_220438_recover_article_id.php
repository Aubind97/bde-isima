<?php

use App\Transaction;
use App\Article;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecoverArticleId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $transactions = Transaction::all();

        foreach($transactions as $transaction) {
            $article = Article::where('name', $transaction->description)->first();
            $transaction->article_id = $article ? $article->id : null;
            $transaction->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
