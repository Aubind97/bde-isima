<?php

use App\User;
use App\Transaction;
use App\Events\ArticleStatsEvent;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecoverStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = User::all();

        foreach($users as $user) {
            $transactions = Transaction::where('receiver_id', $user->id)
                ->whereHas('article')
                ->with('article')
                ->get();

            foreach($transactions as $transaction) {
                event(new ArticleStatsEvent($user, $transaction->article->id));
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
