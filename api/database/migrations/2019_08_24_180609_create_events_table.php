<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('events')){
            Schema::create('events', function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('name', 255)->nullable();

                $table->string('description', 255)->nullable();

                $table->unsignedBigInteger('club_id')->nullable();

                $table->dateTimeTz('takes_place_at')->useCurrent();
                
                $table->dateTimeTz('subscriptions_end_at')->useCurrent();
                
                $table->unsignedInteger('status')->default(0); // 0 need validation, 1 subscription open, 2 ended
                
                $table->unsignedInteger('max_subscribers')->nullable();
                
                $table->json('options');
                $table->json('products');
                $table->json('standalone');
                
                $table->foreign('club_id')
                    ->references('id')
                    ->on('clubs')
                    ->onUpdate('cascade')
                    ->onDelete('set null');

                $table->timestampsTz();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
