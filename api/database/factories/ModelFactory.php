<?php

use \Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
*/

$salt = bin2hex(random_bytes(64));
$password = hash_pbkdf2('sha512', '1234', $salt, 90000);

// Client Factory
$factory->defineAs(App\Client::class, 'localhost', function (Faker\Generator $faker) {
    return [
        'id' => 1,
        'name' => 'bde-isima',
        'secret' => 'c5OwQUh80aTVUm9nfyp5AqrUNLfUGxSMaC2aU6NP',
        'domain_name' => 'http://localhost:8000',
        'redirect' => 'http://localhost:8000',
        'personal_access_client' => false,
        'password_client' => true,
        'revoked' => false,
    ];
});

$factory->defineAs(App\Client::class, 'bde-dev', function (Faker\Generator $faker) {
    return [
        'id' => 2,
        'name' => 'bde-dev-isima',
        'secret' => 'c5OwQUh80aTVUm9nfyp5AqrUNLfUGxSMaC2aU6NM',
        'domain_name' => 'bde.dev.isima.fr',
        'redirect' => 'https://bde.dev.isima.fr',
        'personal_access_client' => false,
        'password_client' => true,
        'revoked' => false,
    ];
});

$factory->defineAs(App\Client::class, 'bde', function (Faker\Generator $faker) {
    return [
        'id' => 3,
        'name' => 'bde-isima',
        'secret' => 'c5OwQUh80aTVUm9nfyp5AqrUNLfUGxSMaC2aU6NQ',
        'domain_name' => 'bde.isima.fr',
        'redirect' => 'https://bde.isima.fr',
        'personal_access_client' => false,
        'password_client' => true,
        'revoked' => false,
    ];
});

// User Factories
$factory->define(App\User::class, function (Faker\Generator $faker) use ($password, $salt) {
    $promotion = (new App\Promotion())->inRandomOrder()->first();
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'nickname' => $faker->name,
        'email' => $faker->safeEmail,
        'picture' => null,
        'password' => $password,
        'salt' => $salt,
        'is_member' => 0,
        'promotion_id' => $promotion->id,
        'card' => $faker->randomNumber(3),
        'balance' => $faker->numberBetween(-10, 50),
        'preferences' => [
            'course' => null,
        ],
        'scopes' => [
            [
                'id' => 'can-update-events',
                'description' => 'Mettre à jour des events',
            ]
        ],
    ];
});

$factory->defineAs(App\User::class, 'connected', function (Faker\Generator $faker) use ($password, $salt) {
    $promotion = (new App\Promotion())->inRandomOrder()->first();
    return [
        'firstname' => 'John',
        'lastname' => 'Doe',
        'nickname' => 'Joe',
        'email' => 'joe@doe.fr',
        'picture' => null,
        'password' => $password,
        'salt' => $salt,
        'is_member' => 1,
        'promotion_id' => $promotion->id,
        'card' => 9942,
        'balance' => 10,
        'preferences' => [
            'course' => null,
        ],
        'scopes' => [
            [
                'id' => 'can-update-events',
                'description' => 'Mettre à jour des events',
            ]
        ],
    ];
});

// Article Factories
$factory->define(App\Article::class, function (Faker\Generator $faker) {
    $price = $faker->randomFloat(2, 0.1, 5);
    return [
        'name' => $faker->word,
        'photo' => '/images/logos/logo.svg',
        'price' => $price,
        'member_price' => $price + 0.5,
        'stock' => $faker->numberBetween(0, 100)
    ];
});

$factory->defineAs(App\Article::class, 'standard', function (Faker\Generator $faker) {
    return [
        'name' => 'Standard article',
        'photo' => '/images/logos/logo.svg',
        'price' => 10,
        'member_price' => 10.5,
        'stock' => 100
    ];
});

$factory->defineAs(App\Article::class, 'invalid', function (Faker\Generator $faker) {
    return [
        'name' => 'Invalid article',
        'photo' => 2,
        'price' => '10 euros',
        'member_price' => '10.5 money',
        'stock' => '100 units'
    ];
});

// Club Factory
$factory->define(App\Club::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->userName,
        'email' => $faker->safeEmail,
        'description' => $faker->text,
        'facebook' => null,
        'twitter' => null,
        'instagram' => null,
        'logo' => null,
    ];
});

$factory->defineAs(App\Club::class, 'standard', function (Faker\Generator $faker) {
    return [
        'name' => 'Standard club',
        'email' => 'joe@doe.fr',
        'description' => 'Standard description',
        'facebook' => null,
        'twitter' => null,
        'instagram' => null,
        'logo' => null,
    ];
});

$factory->defineAs(App\Club::class, 'invalid', function (Faker\Generator $faker) {
    return [
        'name' => 'Invalid club',
        'email' => 2,
        'description' => 10,
        'facebook' => null,
        'twitter' => null,
        'instagram' => null,
        'logo' => null,
    ];
});

// Partner Factory
$factory->define(App\Partner::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->userName,
        'description' => $faker->text,
    ];
});

$factory->defineAs(App\Partner::class, 'standard', function (Faker\Generator $faker) {
    return [
        'name' => 'Standard partner',
        'description' => 'Standard description',
    ];
});

$factory->defineAs(App\Partner::class, 'invalid', function (Faker\Generator $faker) {
    return [
        'name' => 'Invalid partner',
        'description' => 10,
    ];
});

// Transactions Factory
$factory->define(App\Transaction::class, function (Faker\Generator $faker) {

    $users = (new App\User())->inRandomOrder()->get();
    $emitter = $users[0];
    $receiver = $users[1];

    return [
        'amount' => $faker->randomFloat(2, 0, 20),
        'emitter_id' => $emitter->id,
        'emitter_name' => $emitter->firstname,
        'receiver_id' => $receiver->id,
        'receiver_name' => $receiver->firstname,
        'description' => $faker->text(100),
        'prev_receiver_balance' => $faker->randomFloat(2, 0, 20),
        'prev_emitter_balance' => $faker->randomFloat(2, 0, 20),
    ];
});

$factory->defineAs(App\Transaction::class, 'standard', function (Faker\Generator $faker) {
    return [
        'amount' => 42,
        'emitter_id' => 1,
        'emitter_name' => 'Joe Doe',
        'receiver_id' => 2,
        'receiver_name' => 'Jeffrey Lane',
        'description' => 'Standard description',
        'prev_receiver_balance' => 0,
        'prev_emitter_balance' => 0,
    ];
});

$factory->defineAs(App\Transaction::class, 'invalid', function (Faker\Generator $faker) {
    return [
        'amount' => 'Invalid amount',
        'emitter_id' => 'Invalid id',
        'emitter_name' => 42,
        'receiver_id' => 'Invalid id',
        'receiver_name' => 42,
        'description' => 42,
        'prev_receiver_balance' => 'Invalid id',
        'prev_emitter_balance' => 'Invalid id',
    ];
});


// Notifications Factory
$factory->define(App\Notification::class, function (Faker\Generator $faker) {

    $receiver_id = (new App\User())->inRandomOrder()->first()->id;

    return [
        'message' => $faker->text(200),
        'is_read' => false,
        'receiver_id' => $receiver_id,
    ];
});

$factory->defineAs(App\Notification::class, 'standard', function (Faker\Generator $faker) {
    return [
        'message' => 'Standard message',
        'is_read' => true,
        'receiver_id' => 1,
    ];
});

$factory->defineAs(App\Notification::class, 'invalid', function (Faker\Generator $faker) {
    return [
        'message' => 'Invalid message',
        'is_read' => 10,
        'receiver_id' => 'Invalid receiver_id',
    ];
});


// News Factory
$factory->define(App\News::class, function (Faker\Generator $faker) {
    $club = (new App\Club())->inRandomOrder()->first();

    return [
        'club_id' => 1,
        'title' => $faker->sentence,
        'header' => $faker->sentence,
        'content' => $faker->text,
        'is_published' => $faker->boolean,
        'published_at' => $faker->date,
    ];
});

$factory->defineAs(App\News::class, 'standard', function (Faker\Generator $faker) {
    return [
        'club_id' => 1,
        'title' => 'Standard title',
        'header' => 'Standard header',
        'content' => 'Standard content',
        'is_published' => false,
        'published_at' => $faker->date,
    ];
});

$factory->defineAs(App\News::class, 'invalid', function (Faker\Generator $faker) {
    return [
        'club_id' => 'Invalid club_id',
        'title' => 42,
        'header' => 42,
        'content' => 42,
        'is_published' => 'Invalid is_published',
    ];
});


// Events Factory
$factory->define(App\Event::class, function (Faker\Generator $faker) {
    $club = (new App\Club())->inRandomOrder()->first();

    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
        'club_id' => $club->id,
        'takes_place_at' => date('Y-m-d H:i:s', strtotime("+4 days")),
        'subscriptions_end_at' => date('Y-m-d H:i:s', strtotime("+3 days")),
        'max_subscribers' => $faker->numberBetween(10, 100),
        'products' => [["name" => "Test", "price" => 0], ["name" => "Test 2", "price" => 1]],
        'options' => [
            [
                "name" => "Suppléments",
                "type" => "combinable",
                "items" => [["name" => "Mozzarella", "price" => 3], ["name" => "Peperroni", "price" => 2.5]]
            ],
            [
                "name" => "Type de pâte",
                "type" => "exclusive",
                "items" => [["name" => "Medium", "price" => 0], ["name" => "Medium Cheezy Crust", "price" => 2]]
            ]
        ],
        'standalone' => [
            [
                "name" => "Peperroni Lovers",
                "price" => 5,
                "options" => [
                    [
                        "name" => "Suppléments",
                        "type" => "combinable",
                        "items" => [["name" => "Peperroni", "price" => 2.5], ["name" => "Viande hachée", "price" => 2.3]]
                    ],
                    [
                        "name" => "Type de pâte",
                        "type" => "exclusive",
                        "items" => [["name" => "Medium", "price" => 0], ["name" => "Medium Cheezy Crust", "price" => 2]]
                    ]
                ]
            ]
        ]
    ];
});

$factory->defineAs(App\Event::class, 'standard', function (Faker\Generator $faker) {
    return [
        'name' => "Standard event",
        'description' => "Standard event",
        'club_id' => 1,
        'takes_place_at' => date('Y-m-d H:i:s', strtotime("+4 days")),
        'subscriptions_end_at' => date('Y-m-d H:i:s', strtotime("+3 days")),
        'max_subscribers' => 10,
        'products' => [["name" => "Test", "price" => 0], ["name" => "Test 2", "price" => 1]],
        'options' => [
            [
                "name" => "Suppléments",
                "type" => "combinable",
                "items" => [["name" => "Mozzarella", "price" => 3], ["name" => "Peperroni", "price" => 2.5]]
            ],
            [
                "name" => "Type de pâte",
                "type" => "exclusive",
                "items" => [["name" => "Medium", "price" => 0], ["name" => "Medium Cheezy Crust", "price" => 2]]
            ]
        ],
        'standalone' => [
            [
                "name" => "Peperroni Lovers",
                "price" => 5,
                "options" => [
                    [
                        "name" => "Suppléments",
                        "type" => "combinable",
                        "items" => [["name" => "Peperroni", "price" => 2.5], ["name" => "Viande hachée", "price" => 2.3]]
                    ],
                    [
                        "name" => "Type de pâte",
                        "type" => "exclusive",
                        "items" => [["name" => "Medium", "price" => 0], ["name" => "Medium Cheezy Crust", "price" => 2]]
                    ]
                ]
            ]
        ]
    ];
});

$factory->defineAs(App\Event::class, 'invalid', function (Faker\Generator $faker) {
    return [
        'name' => 42,
        'club_id' => 'Invalid club_id',
        'max_subscribers' => 'Invalid max_subscribers',
        'products' => 42,
        'options' => 42,
        'standalone' => 42
    ];
});
