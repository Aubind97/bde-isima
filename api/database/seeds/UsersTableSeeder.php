<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salt = bin2hex(random_bytes(64));
        $password = hash_pbkdf2('sha512', '1234', $salt, 90000);

        // Create a known user
        $user = DB::select("select * from users where email = 'joe@doe.fr'");
        $promotion = (new App\Promotion())->inRandomOrder()->first();
        if (sizeof($user) === 0)
            (new User([
                'firstname' => 'John',
                'lastname' => 'Doe',
                'nickname' => 'Joe',
                'email' => 'joe@doe.fr',
                'picture' => null,
                'password' => $password,
                'salt' => $salt,
                'is_member' => 1,
                'promotion_id' => $promotion->id,
                'card' => 941,
                'balance' => 10,
                'preferences' => null,
                'scopes' => [
                    [
                        'id' => '*',
                        'description' => 'Dieu',
                    ]
                ],
            ]))->save();

        // Create other user
        factory(User::class, 10)->create();
    }
}
