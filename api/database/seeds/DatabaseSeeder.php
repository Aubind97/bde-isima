<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate all atvlea
        /*DB::statement("SET foreign_key_checks=0");
        $databaseName = DB::getDatabaseName();
        $tables = DB::select("SELECT * FROM information_schema.tables WHERE table_schema = '$databaseName'");
        foreach ($tables as $table) {
            $name = $table->TABLE_NAME;
            // skip the migration table
            if ($name == 'migrations') continue;
            DB::table($name)->truncate();
        }
        DB::statement("SET foreign_key_checks=1");*/

        // Fill all tables
        $this->call([
            OAuthClientsTableSeeder::class,
            /*PromotionsTableSeeder::class,
            UsersTableSeeder::class,
            ArticlesTableSeeder::class,
            ClubsTableSeeder::class,
            TransactionsTableSeeder::class,
            NotificationsTableSeeder::class,
            PartnersTableSeeder::class,
            NewsTableSeeder::class,
            EventsTableSeeder::class,*/
        ]);
    }
}
