<?php

use Illuminate\Database\Seeder;

class OAuthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Client::class, 'localhost')->create();
        factory(\App\Client::class, 'bde-dev')->create();
        factory(\App\Client::class, 'bde')->create();
    }
}
