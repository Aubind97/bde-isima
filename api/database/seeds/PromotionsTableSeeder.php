<?php

use Illuminate\Database\Seeder;

class PromotionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<18;++$i) {
            (new \App\Promotion([
                'year' => 2005+$i,
                'fb_group_id' => null,
                'list_email' => 'bde@isima.fr',
            ]))->save();
        }
    }
}
