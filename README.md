# 🐭 BDE ISIMA
The aim of this project is to develop a collaborative environment allowing every ZZ to improve the experience of the [website](https://bde.isima.fr/) with pull requests, by giving feedback on issues or just by giving ideas 💡.  

The project is divided into two parts, a backend **Restful API** written in PHP to manage data and a front-end single page application written in React (and a **PWA**) to improve performance and connectivity issues.

## 🤝 Help us to improve the project
First if you want to give us your help on the project, you'll need to install it on your machine.

> **Requirements**
> For the Back  
> - PHP v7.3 or greater
> - MySQL 5.7 or greater
>
> For the Front
> - NodeJS v12 or greater
> - npm (or yarn)

> In dev, you can use the docker-compose to run all the needed services
> - NodeJS 12 exposed on localhost:3000
> - Apache2 with PHP 7.3 exposed on localhost:8000
> - MailDev exposed on localhost:1080
> - PhpMyAdmin exposed on localhost:8080
> - MySQL exposed on localhost:3306
> - /api, /web and every servers are mounted on volumes. Docker user is mapped on your current user so permissions should be good (non-root).

To recreate the environment you have to go through these steps:

- `make dev` to launch all services
- Create the database called `bde_isima` (You can do it through phpMyAdmin on port 8080)
- Launch the back container in interactive mode `docker exec -it bde-isima-back bash`
- Run migrations `make migrate` to create all tables
- Run seeding `make seed` to fill all tables with dummy data

You are now ready to dev 👍. Hot-reloading is enabled with remote host so you can edit the code in your favorite editor and see the changes live in your browser. 

## ⚙️ Technologies
It's a little list of used technologies to give you some links.

### API
- API [Slim](https://www.slimframework.com/)
- Migrations/Seeding [Phinx](https://phinx.org/)
- Seeding [Faker](https://github.com/fzaninotto/Faker)
- QueryBuilder [Pixie](https://github.com/usmanhalalit/pixie)
- SwiftMailer [SwiftMailer](https://swiftmailer.symfony.com/)
- MJML [MJML](https://mjml.io/)
- Validation [php-validator](https://github.com/Aubind97/php-validator)

> To send mail we decided to use **MJML** to have well format HTML mails with easy markdown language. But for compatibility reasons, we also need a `.txt` mail version. So even if the `.mjml` file is only used to generate the `.html` one, you will be pleased to keep the template to generate the file quickly without installing nothing. You can use the online IDE available on the MJML website.

### FrontEnd
- App [React](https://reactjs.org/)
- Front-end framework [MaterialUI](https://material-ui.com/)  
- Router Front-end [React-Router](https://reacttraining.com/react-router/web/guides/philosophy)
- Style [SASS](https://sass-lang.com/)

## 📖 Workflow
Like in all collaborative projects, we defined some rules to create a workflow that allows everyone to work together. More over it allows a better reliability in the code and make the scalability easier.
We have setup Continuous Integration/Continuous Delivery and Deployment (CI/CD) so it makes it easier to deploy builds on the live server when any commit is done on the master branch.  

### 💾 Versionning
To share the code and keep an history of all modifications, we use a version control system. To avoid conflits, with the use of **[Gitflow](https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html)**

> Keep in mind that it's really important to follow all these rules, otherwise the program life will be a mess 💩

### 🎨 Coding Style
To respect some coding style rules, we configured some tools.

#### Editorconfig
You will be pleased to install an **editorconfig** plugin in your IDE. This plugin will read the `.editorconfig` file add set your preference to respect some style configuration like tabulation spaces or end of line style.

#### PHP CS
For the php, there is **[PHP CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)** configured to follow the [PSR-2](https://www.php-fig.org/psr/psr-2/) conventions.  

You have access to commands for style checking or to correct possible mistakes (when it's possible).
- `make cs` to have a report of all errors
- `make cbf` to correct errors

> The make command are just shortcuts you can have access to those commands with `./api/vendor/bin/phpcs` or `./api/vendor/bin/phpcbf` (assuming you launch the command from the root folder)

#### JS Prettier
For the js *(it also does the scss)*, there is **[Prettier](https://prettier.io/)** that can correct mistakes.  

You can fix the errors with `make prettier`
> You can also run it manually with `node_modules/.bin/prettier --config web/.prettierrc --write "web/src/**/*.{js,jsx,scss,css}"` (assuming you launch the command from the root folder)

#### Tips
##### Readable commit messages
When you commit some changes it's good to use the **[Gitmoji](https://gitmoji.carloscuesta.me/)** rules.
##### Fix Format errors
You can also run both correct methods with `make format`
##### Be sure to have a code format
To be sure that your code is pretty when you commit and prevent commits to happen if it does not respect the rules, you can configure a **git hook**. To do so, go in the `.git/hooks/` file (in the root folder) and edit the `pre-commit`.

> You can also add a `commit-msg` hook to be sure that your gitmoji is well written

## ❓ Notes
> 📄 - The name of all sensible data (database password, etc ...) used in the API are written in `api/.env` file.
>
> 📫 - During the development you can use [MailDev](https://danfarrelly.nyc/MailDev/) to catch all sent mails.
