.PHONY: dev stop format prettier cs cbf help
.DEFAULT_GOAL = help

# DEV variables
FRONT	= ./web
BACK	= ./api
CONTAINERS = `docker ps -a -q`

help: ## List all available commands
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-10s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##
## LAUNCH RULES
##

dev: ## Launch docker containers in development
	docker-compose up

stop: ## Stop all running docker containers
	docker stop $(CONTAINERS)

restart: ## Restart the dev environnment
	docker stop $(CONTAINERS) && docker-compose up

##
## CODE LINTING RULES
##

format: cbf prettier ## Lint the code

prettier: ## Lint all project with Prettier (front)
	$(FRONT)/node_modules/.bin/prettier --config $(FRONT)/.prettierrc --write "$(FRONT)/src/**/*.{js,jsx,scss,css}"

cs: ## Scan code linting (back)
	cd $(BACK) && ./vendor/bin/phpcs

cbf: ## Fix code linting (back)
	cd $(BACK) && ./vendor/bin/phpcbf
